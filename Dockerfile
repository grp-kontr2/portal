FROM kontr2/base-pipenv

LABEL maintainer="Barbora Kompisova"

RUN apt-get install -y python3-dev postgresql-client

ADD . /app
WORKDIR /app
EXPOSE 8000

ONBUILD COPY Pipfile Pipfile
ONBUILD COPY Pipfile.lock Pipfile.lock

RUN pipenv install --system



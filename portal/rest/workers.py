from flask_jwt_extended import jwt_required
from flask_restplus import Namespace

from portal import logger
from portal.rest import rest_helpers
from portal.rest.custom_resource import CustomResource
from portal.rest.schemas import SCHEMAS
from portal.tools.decorators import access_log

workers_namespace = Namespace('workers')
log = logger.get_logger(__name__)


@workers_namespace.route('')
class WorkerList(CustomResource):
    @jwt_required
    # @workers_namespace.response(200, 'List of workers', model=workers_schema)
    @workers_namespace.response(403, 'Not allowed to list workers')
    def get(self):
        log.debug(f"[REST] Get workers by {self.client.log_name}")
        self.permissions().require.sysadmin()
        workers_list = self.facades.workers.find_all()
        return SCHEMAS.dump('workers', workers_list)

    @jwt_required
    @access_log
    # @workers_namespace.response(201, 'Created worker', model=worker_schema)
    @workers_namespace.response(403, 'Not allowed to add worker')
    def post(self):
        self.permissions().require.sysadmin()
        data = rest_helpers.parse_request_data(action='create', resource='worker')
        log.info(f"[REST] Create new worker by {self.client.log_name}: {data}")
        new_worker = self.facades.workers.create(**data)
        return SCHEMAS.dump('worker', new_worker), 201


@workers_namespace.route('/<string:wid>')
@workers_namespace.param('wid', 'Worker id')
@workers_namespace.response(404, 'Worker not found')
class WorkerResource(CustomResource):

    @jwt_required
    # @workers_namespace.response(200, 'Worker', model=worker_schema)
    @workers_namespace.response(403, 'Not allowed to access worker')
    def get(self, wid: str):
        log.debug(f"[REST] Get worker by {self.client.log_name}: {wid}")
        self.permissions().require.sysadmin_or_self(wid)
        worker = self.find.worker(wid)
        return SCHEMAS.dump('worker', worker)

    @jwt_required
    @access_log
    @workers_namespace.response(204, 'Worker deleted')
    @workers_namespace.response(403, 'Not allowed to delete worker')
    def delete(self, wid: str):
        self.permissions().require.sysadmin()
        worker = self.find.worker(wid)
        log.info(f"[REST] Delete worker {worker.log_name} by {self.client.log_name}")
        self.facades.workers.delete(worker)
        return '', 204

    @jwt_required
    @access_log
    @workers_namespace.response(204, 'Worker updated')
    @workers_namespace.response(403, 'Not allowed to update worker')
    def put(self, wid: str):
        self.permissions().require.sysadmin_or_self(wid)
        data = rest_helpers.parse_request_data(action='update', resource='worker', partial=True)
        worker = self.find.worker(wid)
        log.info(f"[REST] Update worker {worker.log_name} by {self.client.log_name}: {data}")
        self.facades.workers.update(worker, **data)
        return '', 204


@workers_namespace.route('/<string:wid>/status')
@workers_namespace.param('wid', 'Worker id')
@workers_namespace.response(404, 'Worker not found')
class WorkerStatusResource(CustomResource):

    @jwt_required
    # @workers_namespace.response(200, 'Worker', model=worker_schema)
    @workers_namespace.response(403, 'Not allowed to access worker')
    def get(self, wid: str):
        log.debug(f"[REST] Get worker stats by {self.client.log_name}: {wid}")
        self.permissions().require.sysadmin()
        worker = self.find.worker(wid)
        return self.facades.workers.get_worker_status(worker)


@workers_namespace.route('/<string:wid>/images')
@workers_namespace.param('wid', 'Worker id')
@workers_namespace.response(404, 'Worker not found')
class WorkerImagesResource(CustomResource):
    @jwt_required
    # @workers_namespace.response(200, 'Worker', model=worker_schema)
    @workers_namespace.response(403, 'Not allowed to access worker')
    def get(self, wid: str):
        log.debug(f"[REST] Get worker images by {self.client.log_name}: {wid}")
        self.permissions().require.sysadmin()
        worker = self.find.worker(wid)
        return self.facades.workers.list_images_available(worker)

from flask import request
from flask_jwt_extended import jwt_required
from flask_restplus import Namespace

from portal import logger, tools
from portal.database.enums import ClientType
from portal.rest import rest_helpers
from portal.rest.custom_resource import CustomResource
from portal.rest.schemas import SCHEMAS
from portal.service import errors
from portal.tools import s2b
from portal.tools.decorators import access_log

projects_namespace = Namespace('')  # pylint: disable=invalid-name

log = logger.get_logger(__name__)


@projects_namespace.route('/courses/<string:cid>/projects')
@projects_namespace.param('cid', 'Course id')
@projects_namespace.response(404, 'Course not found')
class ProjectsList(CustomResource):
    @jwt_required
    @projects_namespace.response(404, 'Course not found')
    # @projects_namespace.response(200, 'Projects list', model=projects_schema)
    def get(self, cid: str):
        course = self.find.course(cid)
        log.debug(f"[REST] Get projects in {course.log_name} by {self.client.log_name}")
        with_archived = self.request.args.get('archived')
        projects = self.facades.projects.find_all(course, with_archived=s2b(with_archived))
        return SCHEMAS.dump('projects', projects)

    @jwt_required
    @access_log
    @projects_namespace.response(404, 'Course not found')
    # @projects_namespace.response(201, 'Project created', model=project_schema)
    def post(self, cid: str):
        course = self.find.course(cid)
        # authorization
        self.permissions(course=course).require.update_course()

        data = rest_helpers.parse_request_data(action='create', resource='project')
        log.info(f"[REST] Create new project by {self.client.log_name} "
                 f"in {course.log_name}: {data}")
        new_project = self.facades.projects.create(course=course, **data)
        return SCHEMAS.dump('project', new_project), 201


@projects_namespace.route('/courses/<string:cid>/projects/<string:pid>')
@projects_namespace.param('cid', 'Course id')
@projects_namespace.param('pid', 'Project id')
@projects_namespace.response(404, 'Course not found')
@projects_namespace.response(404, 'Project not found')
class ProjectResource(CustomResource):
    @jwt_required
    # @projects_namespace.response(200, 'Project found', model=project_schema)
    @projects_namespace.response(403, 'Not allowed to get project')
    def get(self, cid: str, pid: str):
        course = self.find.course(cid)
        # authorization
        self.permissions(course=course).require.view_course()
        log.debug(f"[REST] Get project in {course.log_name} by {self.client.log_name}: {pid}")
        project = self.find.project(course, pid)
        return SCHEMAS.dump('project', project)

    @jwt_required
    @access_log
    @projects_namespace.response(204, 'Project deleted')
    @projects_namespace.response(403, 'Not allowed to delete project')
    def delete(self, cid: str, pid: str):
        course = self.find.course(cid)
        # authorization
        self.permissions(course=course).require.update_course()
        project = self.find.project(course, pid)
        log.info(f"[REST] Delete project by {self.client.log_name}: {project.log_name}")
        self.facades.projects.delete(project)
        return '', 204

    @jwt_required
    @access_log
    @projects_namespace.response(204, 'Project updated')
    @projects_namespace.response(403, 'Not allowed to update project')
    def put(self, cid: str, pid: str):
        course = self.find.course(cid)
        # authorization
        self.permissions(course=course).require.write_projects()

        data = rest_helpers.parse_request_data(action='update', resource='project', partial=True)
        project = self.find.project(course, pid)
        log.info(f"[REST] Update project {project.log_name} by {self.client.log_name}: {data}")
        self.facades.projects.update(project, **data)
        return '', 204


@projects_namespace.route('/courses/<string:cid>/projects/<string:pid>/config')
@projects_namespace.param('cid', 'Course id')
@projects_namespace.param('pid', 'Project id')
@projects_namespace.response(404, 'Course not found')
@projects_namespace.response(404, 'Project not found')
class ProjectConfigResource(CustomResource):
    @jwt_required
    # @projects_namespace.response(200, 'Config of the project', model=config_schema)
    # @projects_namespace.response(200, 'Config of the project', model=config_schema_reduced)
    def get(self, cid: str, pid: str):
        course = self.find.course(cid)
        project = self.find.project(course, pid)
        # authorization
        log.debug(f"[REST] Get project config in {project.log_name}"
                  f" by {self.client.log_name}")
        schema = self.get_config_schema_based_on_permissions(course)
        return schema.dump(project.config)

    def get_config_schema_based_on_permissions(self, course):
        perm_service = self.permissions(course=course)
        if perm_service.check.permissions(['view_course_full']):
            return SCHEMAS.config()
        elif perm_service.check.permissions(['view_course_limited']):
            return SCHEMAS.config_reduced()
        else:
            raise errors.ForbiddenError(perm_service.client)

    @jwt_required
    @access_log
    @projects_namespace.response(204, 'Project config updated')
    def put(self, cid: str, pid: str):
        course = self.find.course(cid)
        # authorization
        self.permissions(course=course).require.write_projects()

        data = rest_helpers.parse_request_data(action='update', resource='config')
        project = self.find.project(course, pid)
        log.info(f"[REST] Update project config in "
                 f"{project.log_name} by {self.client.log_name}: {data}")
        if 'project' in data.keys():
            del data['project']
        self.facades.projects.update_project_config(project=project, **data)
        return '', 204


@projects_namespace.route('/courses/<string:cid>/projects/<string:pid>/process_config')
@projects_namespace.param('cid', 'Course id')
@projects_namespace.param('pid', 'Project id')
@projects_namespace.response(404, 'Course not found')
@projects_namespace.response(404, 'Project not found')
class ProjectProcessConfig(CustomResource):
    @jwt_required
    # @projects_namespace.response(200, 'Config of the project', model=config_schema)
    # @projects_namespace.response(200, 'Config of the project', model=config_schema_reduced)
    def get(self, cid: str, pid: str):
        course = self.find.course(cid)
        project = self.find.project(course, pid)
        # authorization
        log.debug(f"[REST] Get project submission processing config in {project.log_name}"
                  f" by {self.client.log_name}")
        config = self.services.storage.project_config(project)
        return tools.create_response(config, type='yaml')


@projects_namespace.route('/courses/<string:cid>/projects/<string:pid>/test-files-refresh')
@projects_namespace.param('cid', 'Course id')
@projects_namespace.param('pid', 'Project id')
@projects_namespace.response(404, 'Course not found')
@projects_namespace.response(404, 'Project not found')
class ProjectTestFilesRefresh(CustomResource):
    @jwt_required
    @access_log
    @projects_namespace.response(204, 'Project test_files updated')
    def post(self, cid: str, pid: str):
        course = self.find.course(cid)
        project = self.find.project(course, pid)
        # authorization
        self.permissions(course=course).require.write_projects()
        log.info(f"[REST] Update project test-files for "
                 f"{project.log_name} by {self.client.log_name}")
        self.facades.projects.update_project_test_files(project)
        return '', 204


@projects_namespace.route('/courses/<string:cid>/projects/<string:pid>/is_muni/notepad')
@projects_namespace.param('cid', 'Course id')
@projects_namespace.param('pid', 'Project id')
@projects_namespace.response(404, 'Course not found')
@projects_namespace.response(404, 'Project not found')
class ProjectWriteISMUNINotes(CustomResource):
    @jwt_required
    @access_log
    @projects_namespace.response(200, 'Project is muni notepad read')
    def get(self, cid: str, pid: str):
        course = self.find.course(cid)
        project = self.find.project(course, pid)
        user_id = self.get_query_param('user_id', required=True)
        user = self.find.user(user_id)
        # authorization
        self.permissions(course=course).require.read_submission()
        log.debug(f"[REST] Get is muni notes for "
                  f"{project.log_name} by {self.client.log_name}")
        result = self.facades.submissions.is_read_notepad(project=project, user=user)
        return result

    @jwt_required
    @access_log
    @projects_namespace.response(200, 'Project test_files updated')
    def post(self, cid: str, pid: str):
        course = self.find.course(cid)
        project = self.find.project(course, pid)
        user_id = self.get_query_param('user_id', required=True)
        user = self.find.user(user_id)
        # authorization
        self.permissions(course=course).require.evaluate_submissions()
        log.debug(f"[REST] Get is muni notes for "
                  f"{project.log_name} by {self.client.log_name}")
        self.facades.submissions.is_read_notepad(project=project, user=user)
        return '', 204


@projects_namespace.route(
    '/courses/<string:cid>/projects/<string:pid>/submissions')
@projects_namespace.param('cid', 'Course id')
@projects_namespace.param('pid', 'Project id')
@projects_namespace.response(404, 'Course not found')
@projects_namespace.response(404, 'Project not found')
class ProjectSubmissions(CustomResource):
    @jwt_required
    # @projects_namespace.response(200, 'Project submissions list', model=submissions_schema)
    def get(self, cid: str, pid: str):
        course = self.find.course(cid)
        # authorization
        perm = ['read_submissions_all']
        self.permissions(course=course).require.permissions(perm)

        user_id = request.args.get('user')
        project = self.find.project(course, pid)
        log.debug(f"[REST] Get project submissions in {project.log_name}"
                  f" by {self.client.log_name} for user {user_id}")
        submissions = self.facades.projects.find_project_submissions(project, user_id)
        return SCHEMAS.dump('submissions', submissions)

    @jwt_required
    @access_log
    # @projects_namespace.response(201, 'Project submission create', model=submission_schema)
    def post(self, cid: str, pid: str):
        course = self.find.course(cid)
        user = request.args.get('user')
        user = self.find.user(user) if user is not None else self.client
        # authorization
        self.permissions(course=course).require.create_submission(user)

        # check if a new submission can be created by this user in this project
        project = self.find.project(course, pid)
        self._check_user_can_create_submission(project, user)

        data = rest_helpers.parse_request_data(
            schema=SCHEMAS.submission_create, action='create', resource='submission'
        )
        log.info(f"[REST] Create submission in {project.log_name} "
                 f"by {self.client.log_name} for user {user.log_name}: {data}")

        # data for Kontr processing
        new_submission = self.facades.submissions.create(user=user, project=project,
                                                         submission_params=data)
        return SCHEMAS.dump('submission', new_submission), 201

    def _check_user_can_create_submission(self, project, user):
        if self.client.type == ClientType.USER:
            client_instance = self.find.user(self.client.id)
            if not client_instance.is_admin:
                self.facades.projects.check_submission_create(project, client=user)


@projects_namespace.route(
    '/courses/<string:cid>/projects/<string:pid>/submissions/check')
@projects_namespace.param('cid', 'Course id')
@projects_namespace.param('pid', 'Project id')
@projects_namespace.response(404, 'Course not found')
@projects_namespace.response(404, 'Project not found')
class ProjectSubmissionsCheck(CustomResource):
    @jwt_required
    @access_log
    # @projects_namespace.response(201, 'Project submission create', model=submission_schema)
    def get(self, cid: str, pid: str):
        course = self.find.course(cid)
        user = request.args.get('user')
        user = self.find.user(user) if user is not None else self.client
        # authorization
        self.permissions(course=course).require.create_submission(user)

        # check if a new submission can be created by this user in this project
        project = self.find.project(course, pid)
        self._check_user_can_create_submission(project, user)
        log.info(f"[REST] Check Create submission in {project.log_name} "
                 f"by {self.client.log_name} for user {user.log_name}")
        return {'message': 'User is able to create submission'}, 200

    def _check_user_can_create_submission(self, project, user):
        if self.client.type == ClientType.USER:
            client_instance = self.find.user(self.client.id)
            if not client_instance.is_admin:
                self.facades.projects.check_submission_create(project, client=user)


@projects_namespace.route('/courses/<string:cid>/projects/<string:pid>/files')
@projects_namespace.param('cid', 'Course id')
@projects_namespace.param('pid', 'Project id')
@projects_namespace.response(404, 'Course not found')
@projects_namespace.response(404, 'Project not found')
class ProjectTestFiles(CustomResource):
    @jwt_required
    def get(self, cid: str, pid: str):
        course = self.find.course(cid)
        self.permissions(course=course).require.view_course_full()
        project = self.find.project(course, pid)
        log.debug(f"[REST] Get project test files in {project.log_name}"
                  f" by {self.client.log_name}")
        result = self.facades.projects.get_test_files(project)
        return result

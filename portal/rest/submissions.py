import flask
from flask_jwt_extended import jwt_required
from flask_restplus import Namespace

from portal import logger
from portal.logger import FILES
from portal.rest import rest_helpers
from portal.rest.custom_resource import CustomResource
from portal.rest.schemas import SCHEMAS
from portal.tools import request_helpers
from portal.tools.decorators import access_log

submissions_namespace = Namespace('submissions')

log = logger.get_logger(__name__)


def log_files_access(what, submission, client):
    FILES.info(f"{what} for {submission.log_name} accessed by: {client.log_name}"
               f" IP: {request_helpers.get_ip()}, UA: {flask.request.user_agent}")


@submissions_namespace.route('')
class SubmissionsResource(CustomResource):
    @jwt_required
    def get(self):
        submissions = self.facades.submissions.find_all()
        log.debug(f"[REST] Get submissions by {self.client.log_name}")
        return SCHEMAS.dump('submissions', submissions)


@submissions_namespace.route('/<string:sid>')
@submissions_namespace.param('sid', 'Submission id')
@submissions_namespace.response(404, 'Submission not found')
class SubmissionResource(CustomResource):
    @jwt_required
    # @submissions_namespace.response(200, 'Submission found', model=submission_schema)
    def get(self, sid: str):
        submission = self.find.submission(sid)
        course = submission.project.course
        # Authorization
        log.debug(f"[REST] Get submission by {self.client.log_name}: {submission.log_name}")
        self.permissions(course=course).require.read_submission(submission)
        return SCHEMAS.dump('submission', submission)

    @jwt_required
    @access_log
    @submissions_namespace.response(204, 'Submission deleted')
    def delete(self, sid: str):
        self.permissions.require.sysadmin()
        submission = self.find.submission(sid)
        log.info(f"[REST] Delete submission {submission.log_name} by {self.client.log_name}")
        self.facades.submissions.delete(submission)
        return '', 204


@submissions_namespace.route('/<string:sid>/state')
@submissions_namespace.param('sid', 'Submission id')
@submissions_namespace.response(404, 'Submissions not found')
class SubmissionState(CustomResource):
    @jwt_required
    # @submissions_namespace.response(200, 'Submission state', model=submission_state_schema)
    def get(self, sid: str):
        submission = self.find.submission(sid)
        course = submission.project.course
        log.debug(f"[REST] Get submission state by {self.client.log_name}: {submission.log_name}")
        self.permissions(course=course).require.read_submission(submission)
        return SCHEMAS.dump('submission', submission)

    @jwt_required
    @access_log
    @submissions_namespace.response(204, 'Submission state updated')
    def put(self, sid: str):
        submission = self.find.submission(sid)
        self.permissions.require.sysadmin_or_self(submission.user.id)
        data = rest_helpers.parse_request_data(action='update_state', resource='submission_state')
        log.info(f"[REST] Update submission state for {submission.log_name}  by "
                 f"{self.client.log_name}: {data}")
        self.facades.submissions.update_submission_state(submission, **data)
        return '', 204


@submissions_namespace.route('/<string:sid>/stats')
@submissions_namespace.param('sid', 'Submission id')
@submissions_namespace.response(404, 'Submissions not found')
class SubmissionStatistics(CustomResource):
    @jwt_required
    # @submissions_namespace.response(200, 'Submission state', model=submission_state_schema)
    def get(self, sid: str):
        submission = self.find.submission(sid)
        course = submission.project.course
        log.debug(f"[REST] Get submission statistics "
                  f"by {self.client.log_name}: {submission.log_name}")
        self.permissions(course=course).require.read_submission(submission)
        return self.facades.submissions.get_stats(submission)


@submissions_namespace.route('/<string:sid>/files')
@submissions_namespace.param('sid', 'Submission id')
@submissions_namespace.response(404, 'Submissions not found')
class SubmissionFiles(CustomResource):
    @jwt_required
    def get(self, sid: str):
        raise NotImplementedError()


@submissions_namespace.route('/<string:sid>/files/sources/tree')
@submissions_namespace.doc({'sid': 'Submission id'})
@submissions_namespace.response(404, 'Submissions not found')
class SubmissionSourcesTree(CustomResource):
    @jwt_required
    def get(self, sid: str):
        submission = self.find.submission(sid)
        course = submission.project.course
        log.debug(f"[REST] Get submission sources tree"
                  f" by {self.client.log_name}: {submission.log_name}")
        self.permissions(course=course).require.read_submission(submission)
        log_files_access(f"Submission source tree", submission=submission, client=self.client)
        return self.facades.submissions.send_sources_tree(submission)


@submissions_namespace.route('/<string:sid>/files/sources')
@submissions_namespace.param('sid', 'Submission id')
@submissions_namespace.response(404, 'Submissions not found')
class SubmissionSourceFiles(CustomResource):
    @jwt_required
    def get(self, sid: str):
        submission = self.find.submission(sid)
        course = submission.project.course
        log.debug(f"[REST] Get submission source files "
                  f"by {self.client.log_name}: {submission.log_name}")

        self.permissions(course=course).require.read_submission(submission)
        log_files_access(f"Submission source files", submission=submission, client=self.client)
        return self.facades.submissions.get_source_files(submission)


@submissions_namespace.route('/<string:sid>/files/test_files/tree')
@submissions_namespace.doc({'sid': 'Submission id'})
@submissions_namespace.response(404, 'Submissions not found')
class SubmissionTestFilesTree(CustomResource):
    @jwt_required
    def get(self, sid: str):
        submission = self.find.submission(sid)
        course = submission.project.course
        log.debug(f"[REST] Get submission test files tree by "
                  f"{self.client.log_name}: {submission.log_name}")
        self.permissions(course=course).require.read_submission_group(submission)

        log_files_access(f"Test files tree", submission=submission, client=self.client)
        return self.facades.submissions.test_files_tree(submission)


@submissions_namespace.route('/<string:sid>/files/test_files')
@submissions_namespace.doc({'sid': 'Submission id'})
@submissions_namespace.response(404, 'Submissions not found')
class SubmissionTestFiles(CustomResource):
    @jwt_required
    def get(self, sid: str):
        submission = self.find.submission(sid)
        course = submission.project.course
        log.debug(f"[REST] Get submission test files"
                  f" by {self.client.log_name}: {submission.log_name}")
        self.permissions(course=course).require.read_submission_group(submission)
        log_files_access(f"Test files sources", submission=submission, client=self.client)
        return self.facades.submissions.get_test_files(submission)


@submissions_namespace.route('/<string:sid>/files/results/tree')
@submissions_namespace.param('sid', 'Submission id')
@submissions_namespace.response(404, 'Submissions not found')
class SubmissionResultFilesTree(CustomResource):
    @jwt_required
    def get(self, sid: str):
        submission = self.find.submission(sid)
        log.debug(f"[REST] Get submission result tree"
                  f" by {self.client.log_name}: {submission.log_name}")
        self.permissions(submission=submission).require.read_submission_group(submission)
        log_files_access(f"Result files tree", submission=submission, client=self.client)
        return self.facades.submissions.result_files_tree(submission)


@submissions_namespace.route('/<string:sid>/files/results')
@submissions_namespace.param('sid', 'Submission id')
@submissions_namespace.response(404, 'Submissions not found')
class SubmissionResultFiles(CustomResource):
    @jwt_required
    def get(self, sid: str):
        submission = self.find.submission(sid)
        log.debug(f"[REST] Get submission result files"
                  f" by {self.client.log_name}: {submission.log_name}")
        self.permissions(submission=submission).require.read_submission_group(submission)
        log_files_access(f"Result files", submission=submission, client=self.client)
        return self.facades.submissions.get_result_files(submission)

    @jwt_required
    @access_log
    def post(self, sid: str):
        submission = self.find.submission(sid)
        # authorization
        self.permissions(submission=submission).require.permissions(['evaluate_submissions'])
        # todo: authorize worker
        log.info(f"[REST] Upload submission results for {submission.log_name} by "
                 f"{self.client.log_name}")
        task = self.facades.submissions.upload_results_to_storage(submission)
        return {'new_task': task.id}


@submissions_namespace.route('/<string:sid>/resubmit')
@submissions_namespace.param('sid', 'Submission id')
@submissions_namespace.response(404, 'Submissions not found')
class SubmissionResubmit(CustomResource):
    @jwt_required
    @access_log
    # @submissions_namespace.response(201, 'New submission', model=submission_schema)
    def post(self, sid: str):
        source_submission = self.find.submission(sid)
        # authorization
        perm_service = self.permissions(submission=source_submission)
        perm_service.require.permissions(permissions=['resubmit_submissions'])

        data = rest_helpers.parse_request_data(action='resubmit', resource='submission')
        # create new submission by copying files from the source submission in
        # storage
        service = self.facades.submissions
        log.info(f"[REST] Resubmit submission {source_submission.log_name} by "
                 f"{self.client.log_name}: {data}")
        new_submission = service.copy_submission(source_submission, note=data['note'])
        return SCHEMAS.dump('submission', new_submission), 201


@submissions_namespace.route('/<string:sid>/resend')
@submissions_namespace.param('sid', 'Submission id')
@submissions_namespace.response(404, 'Submissions not found')
class SubmissionResend(CustomResource):
    @jwt_required
    @access_log
    # @submissions_namespace.response(201, 'New submission', model=submission_schema)
    def post(self, sid: str):
        submission = self.find.submission(sid)
        # authorization
        self.permissions.require.sysadmin()
        log.info(f"[REST] Resend submission {submission.log_name} by "
                 f"{self.client.log_name}")
        self.facades.submissions.resend_submission(submission)
        return SCHEMAS.dump('submission', submission), 200


@submissions_namespace.route('/<string:sid>/worker_params')
@submissions_namespace.param('sid', 'Submission id')
@submissions_namespace.response(404, 'Submissions not found')
class SubmissionWorkerParams(CustomResource):
    @jwt_required
    @access_log
    # @submissions_namespace.response(201, 'New submission', model=submission_schema)
    def get(self, sid: str):
        submission = self.find.submission(sid)
        # authorization
        perm_service = self.permissions(submission=submission)
        perm_service.require.read_submission(submission)
        log.debug(f"[REST] Worker params for submission {submission.log_name}"
                  f": {submission.worker_parameters}")
        return submission.worker_parameters, 200


@submissions_namespace.route('/<string:sid>/cancel')
@submissions_namespace.param('sid', 'Submission id')
@submissions_namespace.response(404, 'Submissions not found')
@submissions_namespace.response(204, 'Submissions canceled')
class SubmissionCancel(CustomResource):
    @jwt_required
    @access_log
    # @submissions_namespace.response(201, 'New submission', model=submission_schema)
    def post(self, sid: str):
        submission = self.find.submission(sid)
        # authorization
        perm_service = self.permissions(submission=submission)
        perm_service.require.sysadmin_or_self(submission.user.id)
        # create new submission by copying files from the source submission in
        # storage
        log.info(f"[REST] Cancel submission {submission.log_name} by {self.client.log_name}")
        self.facades.submissions.cancel_submission(submission)
        return '', 204


@submissions_namespace.route('/<string:sid>/review')
@submissions_namespace.param('sid', 'Submission id')
@submissions_namespace.response(404, 'Submissions not found')
class SubmissionReview(CustomResource):
    @jwt_required
    # @submissions_namespace.response(200, 'Submissions review', model=review_schema)
    def get(self, sid: str):
        submission = self.find.submission(sid)
        log.debug(f"[REST] Get submission review"
                  f" by {self.client.log_name}: {submission.log_name}")
        self.permissions(submission=submission).require.read_submission(submission)
        return SCHEMAS.dump('review', submission.review)

    @jwt_required
    @access_log
    # @submissions_namespace.response(201, 'Submissions review created', model=review_schema)
    def post(self, sid: str):
        submission = self.find.submission(sid)
        perm_service = self.permissions(submission=submission)
        perm_service.require.write_review_for_submission(submission)
        data = rest_helpers.parse_request_data(action='create', resource='review')
        log.info(f"[REST] Create submission review {submission.log_name} by "
                 f"{self.client.log_name}: {data}")
        review = self.facades.reviews.create(submission=submission, **data)
        return SCHEMAS.dump('review', review), 201


@submissions_namespace.route('/<string:sid>/review/is_muni/notepad')
@submissions_namespace.param('sid', 'Submission id')
@submissions_namespace.response(404, 'Submissions not found')
class SubmissionISNotes(CustomResource):
    @jwt_required
    # @submissions_namespace.response(200, 'Submissions review', model=review_schema)
    def get(self, sid: str):
        submission = self.find.submission(sid)
        self.permissions(submission=submission).require.read_submission()
        log.debug(f"[REST] Get IS MUNI Notepad for project {submission.project.name}"
                  f" by {self.client.log_name}: {submission.log_name}")
        return self.facades.submissions.is_read_notepad(submission=submission)

    @jwt_required
    @access_log
    # @submissions_namespace.response(201, 'Submissions review created', model=review_schema)
    def post(self, sid: str):
        submission = self.find.submission(sid)
        self.permissions(submission=submission).require.evaluate_submissions()
        data = rest_helpers.require_data(action='update', resource='is-muni-notepad')
        log.info(f"[REST] Update notepads content {submission.log_name} by "
                 f"{self.client.log_name}: {data}")
        self.facades.submissions.is_write_notepad(submission=submission, data=data)
        return {'message': 'Success'}, 200


@submissions_namespace.route('/<string:sid>/review/<string:rid>')
@submissions_namespace.param('sid', 'Submission id')
@submissions_namespace.param('rid', 'Review item id')
@submissions_namespace.response(404, 'Submissions not found')
@submissions_namespace.response(404, 'Review item not found')
class SubmissionReviewItem(CustomResource):
    @jwt_required
    # @submissions_namespace.response(200, 'Submissions review', model=review_schema)
    def get(self, sid: str, rid: str):
        submission = self.find.submission(sid)
        course = submission.project.course
        log.debug(f"[REST] Get submission review"
                  f" by {self.client.log_name}: {submission.log_name}")
        self.permissions(course=course).require.read_submission(submission)
        item = self.find.review_item(rid)
        return SCHEMAS.dump('reviewitem', item)

    @jwt_required
    @access_log
    # @submissions_namespace.response(201, 'Submissions review created', model=review_schema)
    def put(self, sid: str, rid: str):
        submission = self.find.submission(sid)
        course = submission.project.course
        perm_service = self.permissions(course=course)
        perm_service.require.write_review_for_submission(submission)
        data = rest_helpers.parse_request_data(action='update', resource='review_item')
        log.info(f"[REST] Update submission review item for {submission.log_name} by "
                 f"{self.client.log_name}: {data}")
        item = self.find.review_item(rid)
        review = self.facades.reviews.update_item(item=item, **data)
        return SCHEMAS.dump('review', review), 204

    @jwt_required
    @access_log
    # @submissions_namespace.response(201, 'Submissions review created', model=review_schema)
    def delete(self, sid: str, rid: str):
        submission = self.find.submission(sid)
        course = submission.project.course
        perm_service = self.permissions(course=course)
        perm_service.require.write_review_for_submission(submission)
        log.info(f"[REST] Delete submission review item for {submission.log_name} by "
                 f"{self.client.log_name}")
        item = self.find.review_item(rid)
        review = self.facades.reviews.delete_item(item=item)
        return SCHEMAS.dump('review', review), 204


@submissions_namespace.route('/<string:sid>/review/<string:rid>/versions')
@submissions_namespace.param('sid', 'Submission id')
@submissions_namespace.param('rid', 'Review item id')
@submissions_namespace.response(404, 'Submissions not found')
@submissions_namespace.response(404, 'Review item not found')
class SubmissionReviewItemVersions(CustomResource):
    @jwt_required
    def get(self, sid: str, rid: str):
        submission = self.find.submission(sid)
        course = submission.project.course
        log.debug(f"[REST] Get submission review versions"
                  f" by {self.client.log_name}: {submission.log_name}")
        self.permissions(course=course).require.read_submission(submission)
        item = self.find.review_item(rid)
        versions = item.versions.all()
        return SCHEMAS.dump('review_items', versions)

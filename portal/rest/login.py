from flask_jwt_extended import create_access_token, create_refresh_token, get_jwt_identity, \
    jwt_refresh_token_required, jwt_required
from flask_restplus import Namespace, fields

from portal import logger
from portal.rest.custom_resource import CustomResource
from portal.service import errors

log = logger.get_logger(__name__)

auth_namespace = Namespace('auth')

login_schema = auth_namespace.model('LoginSchema', {
    'id': fields.String,
    'access_token': fields.String,
    'refresh_token': fields.String
})

refresh_schema = auth_namespace.model('RefreshSchema', {
    'access_token': fields.String
})

logout_schema = auth_namespace.model('LogoutSchema', {
    'access_token': fields.String,
    'refresh_token': fields.String
})


@auth_namespace.route('/login')
class Login(CustomResource):
    @auth_namespace.marshal_with(login_schema)
    @auth_namespace.response(401, 'Client is not authorized')
    @auth_namespace.response(400, 'Missing login type.')
    @auth_namespace.response(400, 'Missing component name.')
    @auth_namespace.response(400, 'Missing component secret.')
    @auth_namespace.response(400, 'Invalid login type.')
    def post(self):
        data = self.request.get_json()
        client = self.auth.login_client(**data)
        return dict(id=client.id,
                    access_token=create_access_token(identity=client.id),
                    refresh_token=create_refresh_token(identity=client.id))


@auth_namespace.route('/refresh')
class Refresh(CustomResource):
    @jwt_refresh_token_required
    @auth_namespace.marshal_with(refresh_schema)
    @auth_namespace.response(401, 'Client is not authorized')
    def post(self):
        client_id = authorized_client()
        return dict(access_token=create_access_token(identity=client_id))


@auth_namespace.route('/logout')
class Logout(CustomResource):
    @jwt_required
    @auth_namespace.marshal_with(logout_schema)
    @auth_namespace.response(401, 'Client is not authorized')
    def post(self):
        client_id = authorized_client()
        return dict(id=client_id, access_token=None, refresh_token=None)


def authorized_client() -> str:
    client = get_jwt_identity()
    if not client:
        raise errors.UnauthorizedError()
    return client

from pathlib import Path

from flask_restplus import Namespace, fields

from portal import logger
from portal.rest.custom_resource import CustomResource
from portal.tools import paths

management_namespace = Namespace('management')
log = logger.get_logger(__name__)

status_schema = management_namespace.model('StatusSchema', {
    'status': fields.String()
})

versions_schema = management_namespace.model('VersionsSchema', {
    'status': fields.String()
})


@management_namespace.route('/status')
class StatusReport(CustomResource):
    @management_namespace.marshal_with(status_schema)
    def get(self):
        return dict(status="works")


def extract_version(path):
    import git
    repo = git.Repo(path)
    return {
        'commit_id': repo.head.commit.commit_id,
        'commit_message': repo.head.commit.message,
    }


@management_namespace.route('/versions')
class VersionsReport(CustomResource):
    @management_namespace.marshal_with(versions_schema)
    def get(self):
        versions = dict(portal=extract_version(paths.ROOT_DIR))
        fe_path = self.flask_app.config.get('FRONTEND_LOCAL_REPO')
        if fe_path and Path(fe_path).exists():
            versions['frontend'] = extract_version(fe_path)
        return versions

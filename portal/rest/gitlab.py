from typing import List

from flask import jsonify, request
from flask_jwt_extended import jwt_required
from flask_restplus import Namespace
from gitlab.v4 import objects

from portal import logger, oauth
from portal.rest import rest_api
from portal.rest.custom_resource import CustomResource
from portal.service import errors

log = logger.get_logger(__name__)

oauth_namespace = Namespace('oauth')


def register_gitlab_app(app):
    base_url = app.config['GITLAB_URL']
    client_id = app.config['GITLAB_CLIENT_ID']
    client_secret = app.config['GITLAB_CLIENT_SECRET']
    api_base_url = base_url + '/api/v4/'

    params = dict(
        client_id=client_id,
        client_secret=client_secret,
        base_url=api_base_url,
        request_token_url=None,
        access_token_url=base_url + "/oauth/token",
        authorize_url=base_url + "/oauth/authorize",
        access_token_method='POST',
    )

    oauth.register('gitlab', **params)
    return oauth


@oauth_namespace.route('/login')
class OAuthLogin(CustomResource):
    def get(self):
        self.facades.gitlab.check_oauth_enabled()

        callback = rest_api.url_for(OAuthLoginAuthorized, _external=True)
        log.debug(f"Callback set: {callback}")
        return oauth.gitlab.authorize_redirect(callback)


@oauth_namespace.route('/login/authorized')
class OAuthLoginAuthorized(CustomResource):
    def get(self):
        self.facades.gitlab.check_oauth_enabled()
        resp = oauth.gitlab.authorize_access_token()
        log.debug(f"Access token get: {resp}")
        if resp is None:
            return 'Access denied: reason=%s error=%s' % (
                request.args['error'],
                request.args['error_description']
            )

        token = resp['access_token']
        log.debug(f"[GITLAB] Received token: {token}")
        login = self.facades.gitlab.user_login(token=token, token_type='oauth')
        log.debug(f"[GITLAB] Gitlab Login response: {login}")
        return login


gitlab_namespace = Namespace('gitlab')


@gitlab_namespace.route('/projects')
class GitlabProjects(CustomResource):
    @jwt_required
    # @courses_namespace.response(200, 'Course found', model=course_schema)
    def get(self):
        self.facades.gitlab.check_gl_enabled()
        args = {**self.request.args}
        args['membership'] = args.get('membership') or True
        args['all'] = args.get('all') or True
        projects: List[objects.Project] = self.facades.gitlab.list_projects(**args)
        return jsonify(_extract(projects))


def _get_project_name() -> str:
    pname = request.args.get('project')
    if not pname:
        raise errors.PortalAPIError(code=400, message="Gitlab project name is missing.")
    return pname


def _extract(obj):
    if isinstance(obj, list):
        return [_extract(item) for item in obj]
    return obj.attributes


@gitlab_namespace.route('/project')
class GitlabProjects(CustomResource):
    @jwt_required
    # @courses_namespace.response(200, 'Course found', model=course_schema)
    def get(self):
        self.facades.gitlab.check_gl_enabled()
        pname = _get_project_name()
        project: objects.Project = self.facades.gitlab.get_project(project_name=pname)
        return _extract(project)


@gitlab_namespace.route('/project/members')
class GitlabProjectMembers(CustomResource):
    @jwt_required
    # @courses_namespace.response(200, 'Course found', model=course_schema)
    def get(self):
        self.facades.gitlab.check_gl_enabled()
        pname = _get_project_name()
        members = self.facades.gitlab.list_members(project_name=pname)
        return jsonify(_extract(members))

    @jwt_required
    def post(self):
        self.facades.gitlab.check_gl_enabled()
        pname = _get_project_name()
        result = self.facades.gitlab.add_member_to_project(project_name=pname)
        return jsonify(_extract(result))


@gitlab_namespace.route('/service_token')
class GitlabServiceToken(CustomResource):
    @jwt_required
    def get(self):
        self.facades.gitlab.check_gl_enabled()
        self.permissions.require.sysadmin()
        kontr_token = self.facades.gitlab.kontr_gl_service.token
        kontr_token_type = self.facades.gitlab.kontr_gl_service.token_type
        return self.facades.gitlab.user_login(token=kontr_token,
                                              token_type=kontr_token_type,
                                              redir=False)

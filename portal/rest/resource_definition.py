import logging

import flask
import yaml
from flask import request
from flask_jwt_extended import jwt_required
from flask_restplus import Namespace

from portal.rest.custom_resource import CustomResource
from portal.rest.schemas import SCHEMAS
from portal.tools.decorators import access_log

definition_namespace = Namespace('definition')
log = logging.getLogger(__name__)


def _get_response(dump, serial_type):
    if serial_type == 'json':
        return dump
    safe_dump = yaml.safe_dump(dump)
    return flask.Response(safe_dump, content_type='text/yaml')


@definition_namespace.route('/courses')
class CoursesDefinition(CustomResource):
    @jwt_required
    # @courses_namespace.response(200, 'Created course', model=course_schema)
    # @courses_namespace.response(403, 'Not allowed to create course', model=course_schema)
    @access_log
    def post(self):
        self.permissions.require.sysadmin()
        data = yaml.safe_load(request.data)
        log.info(f"[REST] Sync project from definition "
                 f"by {self.client.log_name}: {data} ")
        course = self.services.resource_definition.sync_course(data, save=True)
        return SCHEMAS.dump('course', course), 201

    @jwt_required
    def get(self):
        self.permissions().require.sysadmin()
        log.debug(f"[REST] Get courses definition by {self.client.log_name}")
        serial_type = request.args.get('format', 'yaml')
        courses_dump = [
            self.services.resource_definition.dump_course(course)
            for course in self.facades.courses.find_all()
        ]
        return _get_response(dict(courses=courses_dump), serial_type)


@definition_namespace.route('/courses/<string:cid>')
class CourseDefinition(CustomResource):
    @jwt_required
    def get(self, cid):
        course = self.find.course(cid)
        self.permissions(course=course).require.view_course_full()
        log.debug(f"[REST] Get course definition by {self.client.log_name} for {course.log_name}")
        serial_type = request.args.get('format', 'yaml')
        dump = self.services.resource_definition.dump_course(course)
        return _get_response(dump, serial_type)


@definition_namespace.route('/courses/<string:cid>/projects/<string:pid>')
class CoursesRolesOneDefinition(CustomResource):
    @jwt_required
    def get(self, cid, pid):
        course = self.find.course(cid)
        self.permissions(course=course).require.view_course_full()
        project = self.find.project(course, pid)
        log.debug(f"[REST] Get project definition by {self.client.log_name} "
                  f"for {project.log_name}")
        serial_type = request.args.get('format', 'yaml')
        dump = self.services.resource_definition.dump_project(project)
        return _get_response(dump, serial_type)


@definition_namespace.route('/courses/<string:cid>/projects')
@definition_namespace.param('cid', 'Course id')
class CourseProjectDefinition(CustomResource):
    @jwt_required
    # @courses_namespace.response(200, 'Created course', model=course_schema)
    # @courses_namespace.response(403, 'Not allowed to create course', model=course_schema)
    @access_log
    def post(self, cid: str):
        course = self.find.course(cid)
        self.permissions(course=course).require.write_projects()
        data = yaml.safe_load(request.data)
        log.info(f"[REST] Sync project from definition "
                 f"by {self.client.log_name}: {data} ")
        project = self.services.resource_definition \
            .sync_project(course, data, save=True)
        return SCHEMAS.dump('project', project), 201

    @jwt_required
    def get(self, cid):
        course = self.find.course(cid)
        self.permissions(course=course).require.view_course_full()
        log.debug(f"[REST] Get projects definition by {self.client.log_name}")
        serial_type = request.args.get('format', 'yaml')
        collection = [
            self.services.resource_definition.dump_project(item)
            for item in course.projects
        ]
        return _get_response(dict(projects=collection), serial_type)


@definition_namespace.route('/courses/<string:cid>/roles/<string:rid>')
class CoursesRolesOneDefinition(CustomResource):
    @jwt_required
    def get(self, cid, rid):
        course = self.find.course(cid)
        self.permissions(course=course).require.view_course_full()
        role = self.find.role(course, rid)
        log.debug(f"[REST] Get role definition by {self.client.log_name} for {role.log_name}")
        serial_type = request.args.get('format', 'yaml')
        dump = self.services.resource_definition.dump_role(role)
        return _get_response(dump, serial_type)


@definition_namespace.route('/courses/<string:cid>/roles')
@definition_namespace.param('cid', 'Course id')
class CourseRoleDefinition(CustomResource):
    @jwt_required
    # @courses_namespace.response(200, 'Created course', model=course_schema)
    # @courses_namespace.response(403, 'Not allowed to create course', model=course_schema)
    @access_log
    def post(self, cid: str):
        course = self.find.course(cid)
        self.permissions(course=course).require.write_roles()
        data = yaml.safe_load(request.data)
        log.info(f"[REST] Sync role from definition "
                 f"by {self.client.log_name}: {data} ")
        role = self.services.resource_definition \
            .sync_role(course, data, save=True)
        return SCHEMAS.dump('role', role), 201

    @jwt_required
    def get(self, cid):
        course = self.find.course(cid)
        self.permissions(course=course).require.view_course_full()
        log.debug(f"[REST] Get roles definition by {self.client.log_name}")
        serial_type = request.args.get('format', 'yaml')
        collection = [
            self.services.resource_definition.dump_role(item) for item in course.roles
        ]
        return _get_response(dict(roles=collection), serial_type)


@definition_namespace.route('/courses/<string:cid>/groups/<string:rid>')
class CoursesGroupOneDefinition(CustomResource):
    @jwt_required
    def get(self, cid, rid):
        course = self.find.course(cid)
        self.permissions(course=course).require.view_course_full()
        group = self.find.group(course, rid)
        log.debug(f"[REST] Get group definition by {self.client.log_name} for {group.log_name}")
        serial_type = request.args.get('format', 'yaml')
        dump = self.services.resource_definition.dump_group(group)
        return _get_response(dump, serial_type)


@definition_namespace.route('/courses/<string:cid>/groups')
@definition_namespace.param('cid', 'Course id')
class CourseGroupDefinition(CustomResource):
    @jwt_required
    # @courses_namespace.response(200, 'Created course', model=course_schema)
    # @courses_namespace.response(403, 'Not allowed to create course', model=course_schema)
    @access_log
    def post(self, cid: str):
        course = self.find.course(cid)
        self.permissions(course=course).require.write_groups()
        data = yaml.safe_load(request.data)
        log.info(f"[REST] Sync group from definition "
                 f"by {self.client.log_name}: {data} ")
        group = self.services.resource_definition \
            .sync_group(course, data, save=True)
        return SCHEMAS.dump('group', group), 201

    @jwt_required
    def get(self, cid):
        course = self.find.course(cid)
        self.permissions(course=course).require.view_course_full()
        log.debug(f"[REST] Get groups definition by {self.client.log_name}")
        serial_type = request.args.get('format', 'yaml')
        collection = [
            self.services.resource_definition.dump_group(item) for item in course.groups
        ]
        return _get_response(dict(groups=collection), serial_type)

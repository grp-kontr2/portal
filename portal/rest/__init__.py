"""
Rest layer module
"""
import flask
from flask import Flask
from flask_restplus import Api


API_PREFIX = "/api/v1.0"


def create_api(blueprint: flask.Blueprint):
    return Api(blueprint,
               title='Kontr Portal API',
               version='1.0',
               description='Kontr Portal API',
               doc=f"/docs/"
               )


api_blueprint = flask.Blueprint('api', __name__, url_prefix=API_PREFIX)
rest_api = create_api(api_blueprint)


def register_namespaces(app: Flask):  # pylint: disable=cyclic-import
    """Registers blue prints for the application
    Args:
        app(Flask): Flask application
    Returns(Flask): Flask application

    """
    from portal.rest.login import auth_namespace
    from portal.rest.courses import courses_namespace
    from portal.rest.roles import roles_namespace
    from portal.rest.groups import groups_namespace
    from portal.rest.submissions import submissions_namespace
    from portal.rest.projects import projects_namespace
    from portal.rest.workers import workers_namespace
    from portal.rest.users import users_namespace
    from portal.rest.management import management_namespace
    from portal.rest.client import client_namespace, clients_namespace
    from portal.rest.resource_definition import definition_namespace

    app.register_blueprint(api_blueprint)
    rest_api.add_namespace(auth_namespace)
    rest_api.add_namespace(courses_namespace)
    rest_api.add_namespace(roles_namespace)
    rest_api.add_namespace(groups_namespace)
    rest_api.add_namespace(submissions_namespace)
    rest_api.add_namespace(projects_namespace)
    rest_api.add_namespace(workers_namespace)
    rest_api.add_namespace(users_namespace)
    rest_api.add_namespace(management_namespace)
    rest_api.add_namespace(client_namespace)
    rest_api.add_namespace(clients_namespace)
    rest_api.add_namespace(definition_namespace)

    if app.config.get('GITLAB_URL'):
        from portal.rest.gitlab import oauth_namespace, gitlab_namespace
        rest_api.add_namespace(oauth_namespace)
        rest_api.add_namespace(gitlab_namespace)

    # rest_api.init_app(api_blueprint)
    # rest_api.init_app(app)

    from .error_handlers import load_errors
    load_errors(app)
    return app

"""
Schemas used to serialize/deserialize and validate of the models in the DB
"""
import functools
from typing import List

from marshmallow import Schema, ValidationError, fields, validates_schema
from marshmallow_enum import EnumField

from portal.database.models import Client, Course, Group, Project, \
    ReviewItem, Role, Secret, Submission, User, Worker, Review
from portal.database import ProjectState, SubmissionState
from portal.database.enums import ClientType, CourseState, WorkerState


def _in(prefix, params) -> List[str]:
    return [f'{prefix}.{p}' for p in params]


class NestedCollection:
    USERS_NESTED = _in('user', ['username', 'codename', 'name', 'uco', 'id'])
    COURSE_NESTED = _in('course', Course.listable_params())
    NESTED_REPR = dict(
        Role=Role.listable_params() + COURSE_NESTED,
        Group=Group.listable_params() + COURSE_NESTED,
        Project=Project.listable_params() + COURSE_NESTED,
        Course=Course.listable_params(),
        User=User.listable_params(),
        Worker=Worker.listable_params(),
        Client=Client.listable_params(),
        Submission=[*Submission.listable_params(), *_in('project', Project.listable_params()),
                    *COURSE_NESTED, *USERS_NESTED],
        ReviewItem=ReviewItem.listable_params() + USERS_NESTED + _in('review', ['id']),
        Review=Review.base_params(),
        Secret=Secret.listable_params()
    )

    def __init__(self, mod_name: str):
        self.mod_name = mod_name
        self.collection = {}
        self.__build_nested()

    def class_path(self, schema_name: str):
        return f'{self.mod_name}.{schema_name}Schema'

    def __call__(self, *args, **kwargs):
        return self.nested(*args, **kwargs)

    def nested(self, schema_name, *args, only=None, **kwargs):
        params = []
        if only:
            params.extend(only)
        if args:
            params.extend(args)
        other = {**kwargs}
        if params:
            other['only'] = params
        return fields.Nested(self.class_path(schema_name), **other)

    def _set_nested(self, schema_name: str, params, many=False):
        lower = schema_name.lower()
        if many:
            lower = lower + 's'
        self.collection[lower] = self.nested(schema_name, *params, many=many)

    def _set_nested_and_list(self, schema_name, params):
        self._set_nested(schema_name=schema_name, params=params)
        # List version
        self._set_nested(schema_name=schema_name, params=params, many=True)

    def __build_nested(self):
        for (name, params) in self.entities.items():
            self._set_nested_and_list(name, params=params)

    @property
    def entities(self):
        return self.__class__.NESTED_REPR

    def get(self, name):
        return self.collection[name]

    def __getitem__(self, item):
        return self.get(item)


NESTED = NestedCollection(__name__)


# pylint: disable=too-few-public-methods
class BaseSchema(object):
    id = fields.Str(dump_only=True, required=True)
    created_at = fields.LocalDateTime(dump_only=True, allow_none=True)
    updated_at = fields.LocalDateTime(dump_only=True, allow_none=True)


class NamedSchema(object):
    name = fields.Str(required=True)
    codename = fields.Str(required=True)
    description = fields.Str(required=False, allow_none=True)
    namespace = fields.Str(required=False, allow_none=True)


class UserSchema(BaseSchema, Schema):
    """User schema
    """
    uco = fields.Int(required=True)
    email = fields.Email(required=True)
    username = fields.Str(required=True)
    gitlab_username = fields.Str()
    name = fields.Str()
    codename = fields.Str()
    type = fields.Str()
    is_admin = fields.Bool(default=False, missing=False)
    managed = fields.Bool(default=False, missing=False)
    submissions = NESTED['submissions']
    review_items = NESTED['reviewitems']
    roles = NESTED['roles']
    groups = NESTED['groups']


class PasswordChangeSchema(Schema):
    """Password Change Schema
    """
    old_password = fields.Str()
    new_password = fields.Str(required=True)


class CourseSchema(BaseSchema, NamedSchema, Schema):
    """Course Schema
    """
    state = EnumField(CourseState, by_value=True)
    faculty_id = fields.Int(required=False, allow_none=True)
    roles = NESTED['roles']
    groups = NESTED['groups']
    projects = NESTED['projects']


class SubmissionFileSourceSchema(Schema):
    """Submissions File Source Schema
    """
    type = fields.Str(required=True)
    url = fields.Str(allow_none=True)  # git@gitlab.fi.muni.cz:foo/project.git
    branch = fields.Str(allow_none=True)
    checkout = fields.Str()

    @validates_schema
    def validate_present_fields(self, data):
        if data.get('type') is None:
            raise ValidationError(
                f"Submission source type is required. Supported values: 'git', 'zip'.")
        if data['type'] not in ('git', 'zip'):
            raise ValidationError(
                f"Submission source unsupported: {data['type']}. Supported sources: git, zip.")
        if data['type'] == 'git':
            if any(key not in data.keys()
                   for key in ['url', 'branch', 'checkout']):
                raise ValidationError(
                    f"File source 'git' require precisely 'url', "
                    f"'branch' and 'checkout' attributes."
                )
        if data['type'] == 'zip':
            if any(key in data.keys()
                   for key in ['url', 'branch', 'checkout']):
                raise ValidationError(
                    f"File source 'zip' does not require any other attributes.")


class SubmissionFileParamsSchema(Schema):
    """Submission File Params Schema
    """
    source = NESTED('SubmissionFileSource', required=True)
    from_dir = fields.Str(allow_none=True)


class SubmissionCreateSchema(Schema):
    """Submission Create Schema
    """
    file_params = NESTED('SubmissionFileParams', required=True)
    project_params = fields.Dict(required=False, allow_none=True)


class ProjectSchema(BaseSchema, NamedSchema, Schema):
    """Project Schema
    """
    assignment_url = fields.Str(required=False, allow_none=True)
    submit_instructions = fields.Str(required=False, allow_none=True)
    submit_configurable = fields.Bool(required=False, allow_none=True)
    config = NESTED('ProjectConfig',
                    exclude=('id', '_submissions_allowed_from',
                             '_submissions_allowed_to', '_archive_from'))
    state = EnumField(ProjectState)
    course = NESTED['course']
    submissions = NESTED['submissions']
    groups = NESTED['groups']


class ProjectConfigSchema(BaseSchema, Schema):
    """Project config Schema
    """
    project = NESTED['project']
    submissions_cancellation_period = fields.Number(allow_none=True)
    test_files_source = fields.Str(allow_none=True)
    test_files_subdir = fields.Str(allow_none=True)
    file_whitelist = fields.Str(allow_none=True)
    config_subdir = fields.Str(allow_none=True)
    config_file = fields.Str(allow_none=True)
    config_path = fields.Str(allow_none=True)
    submission_parameters = fields.Str(allow_none=True)
    submissions_allowed_from = fields.LocalDateTime(allow_none=True)
    submissions_allowed_to = fields.LocalDateTime(allow_none=True)
    archive_from = fields.LocalDateTime(allow_none=True)


class RoleSchema(BaseSchema, NamedSchema, Schema):
    """Role Schema
    """
    clients = NESTED['clients']
    course = NESTED['course']
    view_course_limited = fields.Bool()
    view_course_full = fields.Bool()
    update_course = fields.Bool()
    handle_notes_access_token = fields.Bool()

    write_roles = fields.Bool()
    write_groups = fields.Bool()
    write_projects = fields.Bool()
    archive_projects = fields.Bool()

    create_submissions = fields.Bool()
    create_submissions_other = fields.Bool()
    resubmit_submissions = fields.Bool()
    evaluate_submissions = fields.Bool()

    read_submissions_all = fields.Bool()
    read_submissions_groups = fields.Bool()
    read_submissions_own = fields.Bool()
    read_all_submission_files = fields.Bool()

    read_reviews_all = fields.Bool()
    read_reviews_groups = fields.Bool()
    read_reviews_own = fields.Bool()

    write_reviews_all = fields.Bool()
    write_reviews_group = fields.Bool()
    write_reviews_own = fields.Bool()


class ClientSchema(BaseSchema, Schema):
    type = EnumField(ClientType, by_value=True)


class RolePermissionsSchema(BaseSchema, Schema):
    """Role Permissions Schema
    """
    view_course_limited = fields.Bool()
    view_course_full = fields.Bool()
    update_course = fields.Bool()
    handle_notes_access_token = fields.Bool()

    write_roles = fields.Bool()
    write_groups = fields.Bool()
    write_projects = fields.Bool()
    archive_projects = fields.Bool()

    create_submissions = fields.Bool()
    create_submissions_other = fields.Bool()
    resubmit_submissions = fields.Bool()
    evaluate_submissions = fields.Bool()

    read_submissions_all = fields.Bool()
    read_submissions_groups = fields.Bool()
    read_submissions_own = fields.Bool()
    read_all_submission_files = fields.Bool()

    read_reviews_all = fields.Bool()
    read_reviews_groups = fields.Bool()
    read_reviews_own = fields.Bool()

    write_reviews_all = fields.Bool()
    write_reviews_group = fields.Bool()
    write_reviews_own = fields.Bool()


class GroupSchema(BaseSchema, NamedSchema, Schema):
    """Group Schema
    """
    course = NESTED['course']
    users = NESTED['users']
    projects = NESTED['projects']


class SubmissionSchema(BaseSchema, Schema):
    """Submission Schema
    """
    note = fields.Str()
    state = EnumField(SubmissionState)
    scheduled_for = fields.LocalDateTime()
    parameters = fields.Dict()
    project = NESTED['project']
    course = NESTED['course']
    user = NESTED['user']
    points = fields.Number()
    result = fields.Str(allow_none=True)
    source_hash = fields.Str(allow_none=True)


class ReviewSchema(BaseSchema, Schema):
    """Review Schema
    """
    submission = NESTED['submission']
    review_items = NESTED['reviewitems']


class ReviewItemSchema(BaseSchema, Schema):
    """Review Item Schema
    """
    content = fields.Str()
    file = fields.Str(allow_none=True)
    line = fields.Int(allow_none=True)
    line_start = fields.Int(allow_none=True)
    line_end = fields.Int(allow_none=True)
    review = NESTED('Review', only=('id', 'submission.id'))
    user = NESTED['user']


class WorkerSchema(BaseSchema, Schema):
    """Component Schema
    """
    name = fields.Str()
    codename = fields.Str(allow_none=True)
    type = fields.Str()
    url = fields.Str(allow_none=True)
    tags = fields.Str(allow_none=True)
    portal_secret = fields.Str(allow_none=True)
    state = EnumField(WorkerState, by_value=True)


class SecretSchema(BaseSchema, Schema):
    """Secret schema
    """
    name = fields.Str()
    expires_at = fields.LocalDateTime(allow_none=True)
    expired = fields.Boolean
    client = NESTED['client']


class CourseImportConfigSchema(Schema):
    """Course Import Config Schema
    """
    roles = fields.Str()
    groups = fields.Str()
    projects = fields.Str()


class CourseImportSchema(Schema):
    """Course Import Schema
    """
    source_course = fields.Str(required=True)
    config = NESTED('CourseImportConfig', required=True)


class ClientListUpdateSchema(Schema):
    """User List Update Schema
    """
    add = fields.List(fields.Str())
    remove = fields.List(fields.Str())


class GroupImportSchema(Schema):
    """Group Import Schema
    """
    source_course = fields.Str()
    source_group = fields.Str()
    with_users = fields.Str()


class RoleImportSchema(Schema):
    source_course = fields.Str()
    source_role = fields.Str()
    with_users = fields.Str()


class ProjectImportSchema(Schema):
    source_course = fields.Str()
    source_project = fields.Str()


class SubmissionResultTemplateSchema(Schema):
    """Submission Result Template Schema
    """
    template = fields.Str(required=True)
    role = fields.Str()
    user = fields.Str()


class SubmissionResultSchema(Schema):
    """Submission Result Schema
    """
    data = fields.Dict(required=True)
    templates = fields.List(
        fields.Nested(SubmissionResultTemplateSchema, required=True)
    )


# pylint: enable=too-few-public-methods


def fn_name(func):
    @functools.wraps(func)
    def __wrap(*args, **kwargs):
        return func(*args, select_params=func.__name__, **kwargs)

    return __wrap


class Schemas:
    ALWAYS_ALLOWED = ['updated_at', 'created_at', 'id']
    CODENAME_W_DESC = [*ALWAYS_ALLOWED, 'name', 'codename', 'description']
    ENT_W_COURSE = (*CODENAME_W_DESC, 'course')
    PARAMS = {
        'users': User.base_params(),
        'clients': Client.base_params(),
        'submissions': [*Submission.base_params(), 'course', 'project', 'user'],
        'submission_state': Submission.base_params(),
        'roles': [*Role.base_params(), 'course'],
        'groups': [*Group.base_params(), 'course'],
        'projects': [*Project.base_params(), 'course'],
        'course_reduced': Course.base_params(),
        'courses': Course.base_params(),
        'config_reduced': (*ALWAYS_ALLOWED, 'project', 'submissions_allowed_from',
                           'submissions_allowed_to', 'file_whitelist'),
        'secrets': (*Secret.base_params(), *_in('client', ['id', 'type', 'name', 'codename'])),
        'secret': (*Secret.base_params(), *_in('client', ['id', 'type', 'name', 'codename']))
    }

    def __get_schema(self, schema_klass, select_params=None, only=None, strict=True, **kwargs):
        if only is None:
            only = self._select_params(select_params)
        params = {'strict': strict, **kwargs}
        if only is not None:
            params['only'] = only
        return schema_klass(**params)

    def _select_params(self, select_params):
        return self.__class__.PARAMS.get(select_params)

    @fn_name
    def user(self, **kwargs):
        return self.__get_schema(UserSchema, **kwargs)

    @fn_name
    def users(self, **kwargs):
        return self.__get_schema(UserSchema, many=True, **kwargs)

    @fn_name
    def client(self, **kwargs):
        return self.__get_schema(ClientSchema, **kwargs)

    @fn_name
    def clients(self, **kwargs):
        return self.__get_schema(ClientSchema, many=True, **kwargs)

    @fn_name
    def password_change(self, **kwargs):
        return self.__get_schema(PasswordChangeSchema, **kwargs)

    @fn_name
    def submission(self, **kwargs):
        return self.__get_schema(SubmissionSchema, **kwargs)

    @fn_name
    def submissions(self, **kwargs):
        return self.__get_schema(SubmissionSchema, many=True, **kwargs)

    @fn_name
    def submission_create(self, **kwargs):
        return self.__get_schema(SubmissionCreateSchema, strict=False, **kwargs)

    @fn_name
    def submission_state(self, **kwargs):
        return self.__get_schema(SubmissionSchema, **kwargs)

    @fn_name
    def reviews(self, **kwargs):
        return self.__get_schema(ReviewSchema, many=True, **kwargs)

    @fn_name
    def review(self, **kwargs):
        return self.__get_schema(ReviewSchema, **kwargs)

    @fn_name
    def review_items(self, **kwargs):
        return self.__get_schema(ReviewItemSchema, many=True, **kwargs)

    @fn_name
    def review_item(self, **kwargs):
        return self.__get_schema(ReviewItemSchema, **kwargs)

    @fn_name
    def courses(self, **kwargs):
        return self.__get_schema(CourseSchema, many=True, **kwargs)

    @fn_name
    def course(self, **kwargs):
        return self.__get_schema(CourseSchema, **kwargs)

    @fn_name
    def course_reduced(self, **kwargs):
        return self.__get_schema(CourseSchema, **kwargs)

    @fn_name
    def roles(self, **kwargs):
        return self.__get_schema(RoleSchema, many=True, **kwargs)

    @fn_name
    def role(self, **kwargs):
        return self.__get_schema(RoleSchema, **kwargs)

    @fn_name
    def group(self, **kwargs):
        return self.__get_schema(GroupSchema, **kwargs)

    @fn_name
    def groups(self, **kwargs):
        return self.__get_schema(GroupSchema, many=True, **kwargs)

    @fn_name
    def project(self, **kwargs):
        return self.__get_schema(ProjectSchema, **kwargs)

    @fn_name
    def projects(self, **kwargs):
        return self.__get_schema(ProjectSchema, many=True, **kwargs)

    @fn_name
    def permissions(self, **kwargs):
        return self.__get_schema(RolePermissionsSchema, **kwargs)

    @fn_name
    def config(self, **kwargs):
        return self.__get_schema(ProjectConfigSchema, **kwargs)

    @fn_name
    def config_reduced(self, **kwargs) -> ProjectConfigSchema:
        return self.__get_schema(ProjectConfigSchema, **kwargs)

    @fn_name
    def client_list_update(self, **kwargs) -> ClientListUpdateSchema:
        return self.__get_schema(ClientListUpdateSchema, **kwargs)

    @fn_name
    def group_import(self, **kwargs) -> GroupImportSchema:
        return self.__get_schema(GroupImportSchema, **kwargs)

    @fn_name
    def role_import(self, **kwargs) -> RoleImportSchema:
        return self.__get_schema(RoleImportSchema, **kwargs)

    @fn_name
    def project_import(self, **kwargs) -> ProjectImportSchema:
        return self.__get_schema(ProjectImportSchema, **kwargs)

    @fn_name
    def course_import(self, **kwargs) -> CourseImportSchema:
        return self.__get_schema(CourseImportSchema, **kwargs)

    @fn_name
    def worker(self, **kwargs) -> ProjectImportSchema:
        return self.__get_schema(WorkerSchema, **kwargs)

    @fn_name
    def workers(self, **kwargs) -> WorkerSchema:
        return self.__get_schema(WorkerSchema, many=True, **kwargs)

    @fn_name
    def submission_result(self, **kwargs) -> SubmissionResultSchema:
        return self.__get_schema(SubmissionResultSchema, **kwargs)

    @fn_name
    def secret(self, **kwargs) -> SecretSchema:
        return self.__get_schema(SecretSchema, **kwargs)

    @fn_name
    def secrets(self, **kwargs) -> SecretSchema:
        return self.__get_schema(SecretSchema, many=True, **kwargs)

    def dump(self, schema_name: str, obj: object) -> dict:
        schema: Schema = getattr(self, schema_name)()
        return schema.dump(obj)[0]


SCHEMAS = Schemas()

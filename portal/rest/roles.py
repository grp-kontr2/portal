from flask import request
from flask_jwt_extended import jwt_required
from flask_restplus import Namespace

from portal import logger
from portal.database.enums import ClientType
from portal.rest import rest_helpers
from portal.rest.custom_resource import CustomResource
from portal.rest.schemas import SCHEMAS
from portal.tools.decorators import access_log

roles_namespace = Namespace('')

log = logger.get_logger(__name__)


@roles_namespace.route('/courses/<string:cid>/roles')
@roles_namespace.param('cid', 'Course id')
class RoleList(CustomResource):
    @jwt_required
    # @roles_namespace.response(200, 'Roles list', model=roles_schema)
    def get(self, cid):
        course = self.find.course(cid)
        roles = self.facades.roles.find_all(course)
        log.debug(f"[REST] Get roles in {course.log_name} by {self.client.log_name}")
        return SCHEMAS.dump('roles', roles)

    @jwt_required
    @access_log
    # @roles_namespace.response(201, 'Role created', model=role_schema)
    def post(self, cid):
        course = self.find.course(cid)
        # authorization
        self.permissions(course=course).require.update_course()

        data = rest_helpers.parse_request_data(action='create', resource='role')
        log.info(f"[REST] Create new role for {course.log_name} "
                 f"by {self.client.log_name}: {data}")
        new_role = self.facades.roles.create(course=course, **data)
        return SCHEMAS.dump('role', new_role), 201


@roles_namespace.route('/courses/<string:cid>/roles/<string:rid>')
@roles_namespace.param('cid', 'Course id')
@roles_namespace.param('rid', 'Role id')
@roles_namespace.response(404, 'Course not found')
@roles_namespace.response(404, 'Role not found')
class RoleResource(CustomResource):
    @jwt_required
    # @roles_namespace.response(200, 'Role in the course', model=role_schema)
    def get(self, cid: str, rid: str):
        course = self.find.course(cid)
        # authorization
        self.permissions(course=course).require.view_course()
        log.debug(f"[REST] Get role in {course.log_name} by {self.client.log_name}: {rid}")
        role = self.find.role(course, rid)
        return SCHEMAS.dump('role', role)

    @jwt_required
    @access_log
    @roles_namespace.response(204, 'Deleted role')
    def delete(self, cid, rid):
        course = self.find.course(cid)
        # authorization
        self.permissions(course=course).require.update_course()

        role = self.find.role(course, rid)
        log.info(f"[REST] Delete role {role.log_name} by {self.client.log_name}")
        self.facades.roles.delete(role)
        return '', 204

    @jwt_required
    @access_log
    @roles_namespace.response(204, 'Role updated')
    def put(self, cid: str, rid: str):
        course = self.find.course(cid)
        # authorization
        self.permissions(course=course).require.write_roles()

        data = rest_helpers.parse_request_data(action='update', resource='role', partial=True)

        role = self.find.role(course, rid)
        log.info(f"[REST] Update role {role.log_name} by {self.client.log_name}: {data}")
        self.facades.roles.update(role, **data)
        return '', 204


@roles_namespace.route('/courses/<string:cid>/roles/<string:rid>/permissions')
@roles_namespace.param('cid', 'Course id')
@roles_namespace.param('rid', 'Role id')
@roles_namespace.response(404, 'Course not found')
@roles_namespace.response(404, 'Role not found')
class RolePermissions(CustomResource):
    @jwt_required
    # @roles_namespace.response(200, 'Get permissions for role', model=permissions_schema)
    def get(self, cid: str, rid: str):
        course = self.find.course(cid)
        role = self.find.role(course, rid)
        log.debug(f"[REST] Get role permissions in {role.log_name} by {self.client.log_name}")
        # authorization
        self.permissions(course=course).require.belongs_to_role(role)

        return SCHEMAS.dump('permissions', role)

    @jwt_required
    @access_log
    # @roles_namespace.response(204, 'Update permissions for course', model=permissions_schema)
    def put(self, cid: str, rid: str):
        course = self.find.course(cid)
        # authorization
        self.permissions(course=course).require.write_roles()

        data = rest_helpers.parse_request_data(action='update', resource='permissions',
                                               partial=True)
        role = self.find.role(course, rid)
        log.info(f"[REST] Update role permissions for"
                 f" {role.log_name} by {self.client.log_name}: {data}")
        self.facades.roles.update_permissions(role, **data)
        return SCHEMAS.dump('permissions', role), 200


@roles_namespace.route('/courses/<string:cid>/roles/<string:rid>/clients')
@roles_namespace.param('cid', 'Course id')
@roles_namespace.param('rid', 'Role id')
@roles_namespace.response(404, 'Course not found')
@roles_namespace.response(404, 'Role not found')
class RoleUsersList(CustomResource):
    @jwt_required
    # @roles_namespace.response(200, 'Gets users in the role', model=users_schema)
    def get(self, cid: str, rid: str):
        course = self.find.course(cid)
        role = self.find.role(course, rid)
        type = request.args.get('type')
        # authorization
        log.debug(f"[REST] Get role clients in {role.log_name} by {self.client.log_name}")
        self.permissions(course=course).require.belongs_to_role(role)
        schema_type = 'clients'
        clients = role.clients
        if type and type in ['user', 'worker']:
            schema_type = f'{type}s'
            clients = [client for client in role.clients
                       if client.type == ClientType[type.upper()]]
        return SCHEMAS.dump(schema_type, clients)

    @jwt_required
    @access_log
    @roles_namespace.response(204, 'Updates users membership in role')
    def put(self, cid: str, rid: str):
        course = self.find.course(cid)
        # authorization
        self.permissions(course=course).require.write_roles()

        data = rest_helpers.parse_request_data(
            schema=SCHEMAS.client_list_update, action='update', resource='role_membership'
        )

        # everything from users_add is added, THEN everything from users_remove
        # is subtracted
        role = self.find.role(course, rid)
        log.info(f"[REST] Update role membership for"
                 f" {role.log_name} by {self.client.log_name}: {data}")
        self.facades.roles.update_clients_membership(role, **data)
        return '', 204


@roles_namespace.route(
    '/courses/<string:cid>/roles/<string:rid>/clients/<string:clid>')
@roles_namespace.param('cid', 'Course id')
@roles_namespace.param('rid', 'Role id')
@roles_namespace.param('clid', 'Client id')
@roles_namespace.response(404, 'Course not found')
@roles_namespace.response(404, 'Role not found')
@roles_namespace.response(404, 'Client not found')
class RoleClient(CustomResource):
    @jwt_required
    @access_log
    @roles_namespace.response(204, 'Adds permissions to role')
    def put(self, cid: str, rid: str, clid: str):
        course = self.find.course(cid)
        # authorization
        self.permissions(course=course).require.write_roles()
        role = self.find.role(course, rid)
        client_type = request.args.get('type')
        client = self.find.client(clid, client_type=client_type)
        log.info(f"[REST] Update role membership for"
                 f" {role.log_name} by {self.client.log_name} - add client: {client.log_name}")
        self.facades.roles.add_client(role, client)
        return '', 204

    @jwt_required
    @access_log
    @roles_namespace.response(204, 'Removes permissions from role')
    def delete(self, cid: str, rid: str, clid: str):
        course = self.find.course(cid)
        self.permissions(course=course).require.write_roles()
        role = self.find.role(course, rid)
        client_type = request.args.get('type')
        client = self.find.client(clid, client_type=client_type)
        log.info(f"[REST] Update role membership for"
                 f" {role.log_name} by {self.client.log_name} - add remove: {client.log_name}")
        self.facades.roles.remove_client(role, client)
        return '', 204

import flask
from flask_restplus import Resource

from portal.database.models import Client
from portal.facade.collection import FacadesCollection
from portal.service import errors
from portal.service.auth import AuthService
from portal.service.find import FindService
from portal.service.permissions import PermissionsService
from portal.service.services_collection import ServicesCollection


class CustomResource(Resource):
    @property
    def services(self) -> ServicesCollection:
        if not hasattr(self, '__services'):
            setattr(self, '__services', ServicesCollection.get())
        return getattr(self, '__services')

    @property
    def facades(self) -> FacadesCollection:
        if not hasattr(self, '__facades'):
            setattr(self, '__facades', FacadesCollection.get())
        return getattr(self, '__facades')

    @property
    def find(self) -> FindService:
        if not hasattr(self, '__find_service'):
            setattr(self, '__find_service', FindService())
        return getattr(self, '__find_service')

    @property
    def permissions(self) -> PermissionsService:
        if not hasattr(self, '__perm_service'):
            setattr(self, '__perm_service', PermissionsService())
        return getattr(self, '__perm_service')

    @property
    def auth(self) -> AuthService:
        if not hasattr(self, '__auth_service'):
            setattr(self, '__auth_service', AuthService())
        return getattr(self, '__auth_service')

    @property
    def client(self) -> Client:
        return self.auth.client

    @property
    def client_name(self):
        if self.client:
            return self.client.log_name
        return 'Unknown client'

    @property
    def request(self) -> flask.Request:
        return flask.request

    @property
    def flask_app(self) -> flask.Flask:
        return flask.current_app

    def get_query_param(self, name: str, required=False, as_list=False):
        method = self.request.args.getlist if as_list else self.request.args.get
        value = method(name)
        if not value and required:
            raise errors.PortalAPIError(message=f"Required query param is missing: {name}")
        return value

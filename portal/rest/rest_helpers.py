"""
Helpers to work and process the services requests and responses
"""
import logging
from typing import Dict, List, Optional, Union

import flask
from werkzeug.datastructures import ImmutableMultiDict

from portal.database import Course, Group, Project, Role, User
from portal.rest.schemas import SCHEMAS
from portal.service.errors import DataMissingError

log = logging.getLogger(__name__)


def parse_request_data(schema=None, action: str = None, resource: str = None,
                       partial=False) -> dict:
    """Parses the request data using schema
    Args:
        schema(Schema): Resource schema
        action(str): Action that is used
        resource(str): Name of the resource
        partial: Whether to ignore missing fields.
        If its value is an iterable, only missing fields listed in that iterable will be ignored.

    Returns(dict): Parsed dictionary
    """
    if schema is None:
        schema = getattr(SCHEMAS, resource)
    if callable(schema):
        schema = schema()
    json_data = require_data(action, resource=resource)
    return schema.load(json_data, partial=partial)[0]


def require_data(action: str, resource: str) -> Union[Dict, str, List]:
    """Parses data from json and if they are missing, throws an exception
    Args:
        action(str): Name of the action
        resource(str): Name of the resource

    Returns(dict): Parsed json data

    """
    json_data = flask.request.get_json()
    if not json_data:
        log.warning(f"[DATA] Data are missing in the request: {flask.request}")
        raise DataMissingError(action=action, resource=resource)
    return json_data


class FlaskRequestArgsHelper:
    def __init__(self, parser: 'FlaskRequestHelper'):
        self._parser = parser
        from portal.service.services_collection import ServicesCollection
        self._rest = ServicesCollection()

    @property
    def _args(self):
        return self._parser.request_args

    def course(self) -> Optional[Course]:
        course = self._args.get('course')
        if course is None:
            return None
        return self._rest.find.course(course)

    def user(self) -> Optional[User]:
        user = self._args.get('user')
        if user is None:
            return None
        return self._rest.find.user(user)

    def project(self) -> Optional[Project]:
        project = self._args.get('project')
        if project is None:
            return None
        course = self.course()
        if course is None:
            return None
        return self._rest.find.project(course, project)

    def projects(self) -> List[str]:
        projects = self._args.getlist('project')
        course = self.course()
        if course is None:
            return []
        return [self._rest.find.project(course, projectId).id for projectId in projects]

    def groups(self) -> List[Group]:
        groups = self._args.getlist('group')
        course = self.course()
        if course is None:
            return []
        return [self._rest.find.group(course, groupId) for groupId in groups]

    def roles(self) -> List[Role]:
        roles = self._args.getlist('role')
        course = self.course()
        if course is None:
            return []
        return [self._rest.find.role(course, roleId) for roleId in roles]

    def state(self):
        self._args.get('state')


class FlaskRequestHelper:
    @property
    def args(self) -> FlaskRequestArgsHelper:
        return FlaskRequestArgsHelper(parser=self)

    @property
    def request(self) -> flask.Request:
        return flask.request

    @property
    def request_args(self) -> ImmutableMultiDict:
        return self.request.args

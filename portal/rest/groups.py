from flask import request
from flask_jwt_extended import jwt_required
from flask_restplus import Namespace

from portal import logger
from portal.rest import rest_helpers
from portal.rest.custom_resource import CustomResource
from portal.rest.schemas import SCHEMAS
from portal.tools.decorators import access_log

groups_namespace = Namespace('')

log = logger.get_logger(__name__)


@groups_namespace.route('/courses/<string:cid>/groups')
@groups_namespace.param('cid', 'Course id')
@groups_namespace.response(404, 'Course not found')
class GroupsList(CustomResource):
    @jwt_required
    # @groups_namespace.response(200, 'List of all groups', model=groups_schema)
    def get(self, cid: str):
        course = self.find.course(cid)
        log.debug(f"[REST] Get groups in {course.log_name} by {self.client.log_name}")
        groups = self.facades.groups.find_all(course)
        return SCHEMAS.dump('groups', groups)

    @jwt_required
    @access_log
    # @groups_namespace.response(201, 'Group created', model=group_schema)
    @groups_namespace.response(403, 'Not allowed to create group')
    def post(self, cid: str):
        course = self.find.course(cid)
        # authorization
        self.permissions(course=course).require.update_course()

        data = rest_helpers.parse_request_data(action='create', resource='group')
        log.info(f"[REST] Create new group by {self.client.log_name} in {course.log_name}: {data}")
        new_group = self.facades.groups.create(course=course, **data)
        return SCHEMAS.dump('group', new_group), 201


@groups_namespace.route('/courses/<string:cid>/groups/<string:gid>')
@groups_namespace.param('cid', 'Course id')
@groups_namespace.param('gid', 'Group id')
@groups_namespace.response(404, 'Course not found')
@groups_namespace.response(404, 'Group not found')
class GroupResource(CustomResource):
    @jwt_required
    # @groups_namespace.response(201, 'Group', model=group_schema)
    @groups_namespace.response(403, 'Not allowed to get group')
    def get(self, cid: str, gid: str):
        course = self.find.course(cid)
        # authorization
        log.debug(f"[REST] Get group in {course.log_name} by {self.client.log_name}: {gid}")
        self.permissions(course=course).require.view_course()
        group = self.find.group(course, gid)
        return SCHEMAS.dump('group', group)

    @jwt_required
    @access_log
    @groups_namespace.response(204, 'Group deleted')
    @groups_namespace.response(403, 'Not allowed to delete group')
    def delete(self, cid: str, gid: str):
        course = self.find.course(cid)
        # authorization
        self.permissions(course=course).require.update_course()
        group = self.find.group(course, gid)
        log.info(f"[REST] Delete group {group.log_name} by {self.client.log_name}")
        self.facades.groups.delete(group)
        return '', 204

    @jwt_required
    @access_log
    @groups_namespace.response(204, 'Group updated')
    @groups_namespace.response(403, 'Not allowed to update group')
    def put(self, cid: str, gid: str):
        course = self.find.course(cid)
        # authorization
        self.permissions(course=course).require.write_groups()
        data = rest_helpers.parse_request_data(action='update', resource='group', partial=True)
        group = self.find.group(course, gid)
        log.info(f"[REST] Update group {group.log_name} by {self.client.log_name}: {data}")
        self.facades.groups.update(group, **data)
        return '', 204


@groups_namespace.route('/courses/<string:cid>/groups/<string:gid>/users')
@groups_namespace.param('cid', 'Course id')
@groups_namespace.param('gid', 'Group id')
class GroupUsersList(CustomResource):
    @jwt_required
    # @groups_namespace.response(200, 'List of users in the group', model=users_schema)
    @groups_namespace.response(403, 'Not allowed to get users in the group')
    def get(self, cid: str, gid: str):
        course = self.find.course(cid)
        role_id = request.args.get('role')

        group = self.find.group(course, gid)
        # authorization
        log.debug(f"[REST] Get group users in {group.log_name} by {self.client.log_name}")
        self.permissions(course=course).require.belongs_to_group(group)

        users = self.facades.groups.find_users_by_role(group, role_id=role_id)
        return SCHEMAS.dump('users', users)

    @jwt_required
    @access_log
    @groups_namespace.response(204, 'User\'s list updated')
    @groups_namespace.response(403, 'Not allowed to add users to the group')
    def put(self, cid: str, gid: str):
        course = self.find.course(cid)
        # authorization
        self.permissions(course=course).require.write_groups()

        data = rest_helpers.parse_request_data(
            schema=SCHEMAS.client_list_update, action='update', resource='group_membership'
        )

        group = self.find.group(course, gid)
        log.info(f"[REST] Update group membership {group.log_name} by "
                 f"{self.client.log_name}: {data}")
        # everything from users_add is added, THEN everything from users_remove
        # is removed
        self.facades.groups.update_membership(group, **data)
        return '', 204


@groups_namespace.route(
    '/courses/<string:cid>/groups/<string:gid>/users/<string:uid>')
@groups_namespace.param('cid', 'Course id')
@groups_namespace.param('gid', 'Group id')
@groups_namespace.param('uid', 'User id')
@groups_namespace.response(404, 'Course not found')
@groups_namespace.response(404, 'Group not found')
@groups_namespace.response(404, 'User not found')
class GroupAddOrDeleteSingleUser(CustomResource):
    @jwt_required
    @access_log
    @groups_namespace.response(204, 'User\'s list updated')
    @groups_namespace.response(403, 'Not allowed to add user to the group')
    def put(self, cid: str, gid: str, uid: str):
        course = self.find.course(cid)

        # authorization
        self.permissions(course=course).require.write_groups()

        group = self.find.group(course, gid)
        user = self.find.user(uid)
        log.info(f"[REST] Update group membership {group.log_name} by "
                 f"{self.client.log_name}, add user {user.log_name}")
        self.facades.groups.add_user(group, user)
        return '', 204

    @jwt_required
    @access_log
    @groups_namespace.response(204, 'User\'s list updated')
    @groups_namespace.response(403, 'Not allowed to delete users to the group')
    def delete(self, cid: str, gid: str, uid: str):
        course = self.find.course(cid)

        self.permissions(course=course).require.write_groups()

        group = self.find.group(course, gid)
        user = self.find.user(uid)
        log.info(f"[REST] Update group membership {group.log_name} by "
                 f"{self.client.log_name}, remove user {user.log_name}")
        self.facades.groups.remove_user(group, user)
        return '', 204


@groups_namespace.route('/courses/<string:cid>/groups/<string:gid>/projects')
@groups_namespace.param('cid', 'Course id')
@groups_namespace.param('gid', 'Group id')
@groups_namespace.response(404, 'Course not found')
@groups_namespace.response(404, 'Group not found')
class GroupProjectsList(CustomResource):
    @jwt_required
    def get(self, cid: str, gid: str):
        course = self.find.course(cid)
        group = self.find.group(course, gid)
        # authorization
        self.permissions(course=course).require.belongs_to_group(group)
        log.debug(f"[REST] Get group projects in {group.log_name} by {self.client.log_name}")
        projects = self.facades.groups.find_projects(group)
        return SCHEMAS.dump('projects', projects)


@groups_namespace.route(
    '/courses/<string:cid>/groups/<string:gid>/projects/<string:pid>')
@groups_namespace.param('cid', 'Course id')
@groups_namespace.param('gid', 'Group id')
@groups_namespace.param('pid', 'Project id')
@groups_namespace.response(404, 'Course not found')
@groups_namespace.response(404, 'Group not found')
@groups_namespace.response(404, 'Project not found')
class GroupAddOrDeleteProject(CustomResource):
    @jwt_required
    @access_log
    @groups_namespace.response(204, 'Projects\'s list updated')
    @groups_namespace.response(403, 'Not allowed to add project to the group')
    def put(self, cid: str, gid: str, pid: str):
        course = self.find.course(cid)

        # authorization
        self.permissions(course=course).require.write_groups()

        group = self.find.group(course, gid)
        project = self.find.project(course, pid)
        log.info(f"[REST] Update group membership {group.log_name} by "
                 f"{self.client.log_name}, add project {project.log_name}")
        self.facades.groups.add_project(group, project)
        return '', 204

    @jwt_required
    @access_log
    @groups_namespace.response(204, 'Projects\'s list updated')
    @groups_namespace.response(
        403, 'Not allowed to delete project from the group')
    def delete(self, cid: str, gid: str, pid: str):
        course = self.find.course(cid)

        # authorization
        self.permissions(course=course).require.write_groups()

        group = self.find.group(course, gid)
        project = self.find.project(course, pid)
        log.info(f"[REST] Update group membership {group.log_name} by "
                 f"{self.client.log_name}, remove project {project.log_name}")
        self.facades.groups.remove_project(group, project)
        return '', 204


@groups_namespace.route('/courses/<string:cid>/groups/import')
@groups_namespace.param('cid', 'Course id')
@groups_namespace.response(404, 'Course not found')
class GroupImport(CustomResource):
    @jwt_required
    @access_log
    @groups_namespace.response(201, 'Group import')
    @groups_namespace.response(404, 'Group not found')
    @groups_namespace.response(403, 'Not allowed to import to the group')
    def put(self, cid: str):
        target_course = self.find.course(cid)
        # authorization
        self.permissions(course=target_course).require.write_groups()
        data = rest_helpers.parse_request_data(
            schema=SCHEMAS.group_import, action='import', resource='group'
        )
        log.info(f"[REST] Groups import by {self.client.log_name} to "
                 f"{target_course.log_name}: {data}")
        new_group = self.facades.groups.import_groups(target_course, **data)
        return SCHEMAS.dump('group', new_group), 201

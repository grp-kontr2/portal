from flask import jsonify


def api_abort(code=400, message=None, **kwargs):
    response = jsonify({
        'status': code,
        'message': message,
        **kwargs
    })
    response.status_code = code
    return response


def is_api_abort(message=None, code=400, course=None, **kwargs):
    if not message:
        message = "IS Muni Api is not enabled for course"
    if course:
        message += f": {course.log_name}"
    else:
        message += "."
    return api_abort(code=code, message=message, **kwargs)


def missing_param(param, **kwargs):
    message = f"Required query param is missing: {param}"
    return api_abort(message=message, param_name=param, **kwargs)
from flask_jwt_extended import jwt_required
from flask_restplus import Namespace

from portal import logger
from portal.database.enums import ClientType
from portal.rest import rest_helpers
from portal.rest.custom_resource import CustomResource
from portal.rest.schemas import SCHEMAS
from portal.tools.decorators import access_log

client_namespace = Namespace('client')
clients_namespace = Namespace('clients')

log = logger.get_logger(__name__)


@client_namespace.route('')
class ClientController(CustomResource):
    @jwt_required
    # @users_namespace.response(200, 'List of users', model=users_schema)
    def get(self):
        client = self.permissions.client_owner
        schema = SCHEMAS.user() if client.type == ClientType.USER else SCHEMAS.worker()
        log.debug(f"[REST] Get current client {client.log_name} by {self.client.log_name}")
        return schema.dump(client)[0], 200


@clients_namespace.route('/<string:cid>')
@clients_namespace.param('cid', "Client's id")
@clients_namespace.response(404, 'Client not found')
class ClientController(CustomResource):
    @jwt_required
    # @clients_namespace.response(200, 'Client detail', model=clients_schema)
    def get(self, cid: str):
        self.permissions.require.sysadmin_or_self(cid)
        client = self.find.client(cid)
        log.debug(f"[REST] Get client {client.log_name} by {self.client.log_name}")
        schema = SCHEMAS.user() if client.type == ClientType.USER else SCHEMAS.worker()
        return schema.dump(client)[0], 200


@clients_namespace.route('/<string:cid>/secrets')
@clients_namespace.param('wid', "Client's id")
@clients_namespace.response(404, 'Client not found')
class ClientSecretsController(CustomResource):
    @jwt_required
    @clients_namespace.response(200, 'List client secrets')
    def get(self, cid: str):
        self.permissions.require.sysadmin_or_self(cid)
        client = self.find.client(cid)
        log.debug(f"[REST] Get client secrets {client.log_name} by {self.client.log_name}")
        return SCHEMAS.dump('secrets', client.secrets)

    @jwt_required
    @access_log
    # @workers_namespace.response(201, 'Created worker secret', model=secret_schema)
    def post(self, cid: str):
        self.permissions.require.sysadmin_or_self(cid)
        data = rest_helpers.parse_request_data(action='create', resource='secret')
        client = self.find.client(cid)
        log.info(f"[REST] Create new secret by {self.client.log_name} "
                 f"for {client.log_name}: {data} ")
        new_secret, value = self.facades.secrets.create(client=client, **data)
        return {'id': new_secret.id, 'value': value}, 201


@clients_namespace.route('/<string:cid>/secrets/<string:sid>')
@clients_namespace.param('wid', "Client's id")
@clients_namespace.param('sid', "Secret id")
@clients_namespace.response(404, 'Client not found')
class ClientSecretController(CustomResource):
    @jwt_required
    @clients_namespace.response(200, 'Client secret detail')
    def get(self, cid: str, sid: str):
        self.permissions.require.sysadmin_or_self(cid)
        client = self.find.client(cid)
        log.debug(f"[REST] Get secret for {client.log_name} by {self.client.log_name}: {sid}")
        secret = self.find.secret(client, sid)
        return SCHEMAS.dump('secret', secret)

    @jwt_required
    @access_log
    @clients_namespace.response(204, 'Client secret deleted')
    def delete(self, cid: str, sid: str):
        self.permissions.require.sysadmin_or_self(cid)
        client = self.find.client(cid)
        secret = self.find.secret(client, sid)
        log.info(f"[REST] Delete a secret by {self.client.log_name}: {secret.log_name} ")

        self.facades.secrets.delete(secret)
        return '', 204

    @jwt_required
    @access_log
    @clients_namespace.response(204, 'Client secret updated')
    @clients_namespace.response(403, 'Not allowed to update client secret')
    def put(self, cid: str, sid: str):
        self.permissions.require.sysadmin_or_self(cid)

        data = rest_helpers.parse_request_data(action='update', resource='secret', partial=True)

        client = self.find.client(cid)
        secret = self.find.secret(client, sid)
        log.info(f"[REST] Update a secret {secret.log_name} by {self.client.log_name}: {data} ")
        self.facades.secrets.update(secret, **data)
        return '', 204

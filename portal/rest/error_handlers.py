import flask
from flask import Flask
from flask_jwt_extended.exceptions import NoAuthorizationError
from marshmallow.exceptions import ValidationError
from sqlalchemy.exc import SQLAlchemyError

import portal.storage
from portal import logger
from portal.rest import rest_api
from portal.service.errors import IncorrectCredentialsError, PortalAPIError, UnauthorizedError

log = logger.get_logger(__name__)


def load_errors(app: Flask):
    log.trace("[LOAD] Custom error handlers loaded")
    for (ex, func) in rest_api.error_handlers.items():
        app.register_error_handler(ex, func)


def send_response(body):
    response: flask.Response = flask.jsonify(body)
    # response.headers['Access-Control-Allow-Origin'] = '*'
    return response


@rest_api.errorhandler
def default_error_handler():
    return send_response({'message': 'Default error handler has been triggered'}), 401


@rest_api.errorhandler(UnauthorizedError)
def handle_unauthorized_error(ex: UnauthorizedError):
    log.warning(f"[AUTH] {ex.message} ")
    return send_response({'message': ex.message, 'error': f'{ex}'}), 401


@rest_api.errorhandler(NoAuthorizationError)
def handle_missing_auth_header(ex: NoAuthorizationError):
    log.warning(f"[AUTH] Auth headers are missing: {ex} ")
    return send_response({'message': 'Auth headers is missing', 'error': f'{ex}'}), 401


@rest_api.errorhandler(IncorrectCredentialsError)
def handle_missing_auth_header(ex: IncorrectCredentialsError):
    log.warning(f"[AUTH] Credentials are incorrect: {ex} ")
    return send_response({'message': 'Credentials are incorrect', 'error': f'{ex}'}), 401


@rest_api.errorhandler(ValidationError)
def handle_validation_error(ex: ValidationError):
    log.warning(f"[VALID] Validation failed: {ex}")
    return send_response({'message': f"Validation failed on: {ex.messages}"}), 400


@rest_api.errorhandler(SQLAlchemyError)
def handle_db_error(ex: SQLAlchemyError):
    log.error(f"[DB] Error: {ex}", ex)
    return send_response({'message': f'Database error: {ex}'}), 400


@rest_api.errorhandler(NotImplementedError)
def handle_not_implemented_error(ex: NotImplementedError):
    log.warning(f"[WARN] Not implemented yet: {ex}")
    return send_response({'message': f'Not implemented yet!'}), 404


@rest_api.errorhandler(portal.storage.errors.NotFoundError)
def handle_storage_not_found_error(ex: portal.storage.errors.NotFoundError):
    log.warning(f"[STORAGE] Storage not found warning: {ex}")
    return send_response({'message': str(ex)}), 404


@rest_api.errorhandler(portal.storage.errors.KontrStorageError)
def handle_storage_error(ex: portal.storage.errors.KontrStorageError):
    log.warning(f"[STORAGE] Storage error: {ex}")
    return send_response({'message': str(ex)}), 400


@rest_api.errorhandler(PortalAPIError)
def handle_portal_api_error(ex: PortalAPIError):
    log.error(f"[API] Api error[{ex.code}]: {ex} ")
    return send_response({'message': ex.message}), ex.code


@rest_api.errorhandler(Exception)
def handle_default_exception(ex: Exception):
    log.critical(f"[ERROR] Fatal error: {ex}")
    if flask.current_app.config.get('ENV') != 'production':
        raise ex
    return send_response({'message': str(ex)}), 500

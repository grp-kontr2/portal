import flask
import yaml
from flask import request
from flask_jwt_extended import jwt_required
from flask_restplus import Namespace

from portal import logger
from portal.database.enums import ClientType
from portal.facade import features
from portal.rest import api_aborts, rest_helpers
from portal.rest.custom_resource import CustomResource
from portal.rest.schemas import SCHEMAS
from portal.service.errors import ForbiddenError, PortalAPIError
from portal.service.filters import filter_course_dump
from portal.tools.decorators import access_log

courses_namespace = Namespace('courses')
log = logger.get_logger(__name__)


@courses_namespace.route('')
class CourseList(CustomResource):
    @jwt_required
    # @courses_namespace.response(200, 'Courses list', model=courses_schema)
    @courses_namespace.response(403, 'Not allowed to see courses')
    def get(self):
        # authorization
        self.permissions.require.sysadmin()
        args = request.args
        log.debug(f"[REST] Get Courses by {self.client.log_name}: {args}")
        courses_list = self.facades.courses.find_all(**args)
        return SCHEMAS.dump('courses', courses_list)

    @jwt_required
    # @courses_namespace.response(200, 'Created course', model=course_schema)
    # @courses_namespace.response(403, 'Not allowed to create course', model=course_schema)
    @access_log
    def post(self):
        self.permissions.require.sysadmin()
        data = rest_helpers.parse_request_data(resource='course', action='create')
        log.info(f"[REST] Create new course by {self.client.log_name}: {data} ")
        new_course = self.facades.courses.create(**data)
        return SCHEMAS.dump('course', new_course), 201


@courses_namespace.route('/<string:cid>')
@courses_namespace.param('cid', 'Course id')
@courses_namespace.response(404, 'Course not found')
class CourseResource(CustomResource):

    @jwt_required
    # @courses_namespace.response(200, 'Course found', model=course_schema)
    @courses_namespace.response(403, 'Not allowed to see course')
    def get(self, cid: str):
        course = self.find.course(cid)
        # authorization
        log.debug(f"[REST] Get course {course.log_name} by {self.client.log_name}")
        perm_service = self.permissions(course=course)
        if perm_service.check.permissions(['view_course_full']):
            return SCHEMAS.dump('course', course)

        elif perm_service.check.permissions(['view_course_limited']):
            # TODO: Refactor
            dump = SCHEMAS.dump('course', course)
            filtered_course = filter_course_dump(course, dump, self.client)
            return filtered_course

        raise ForbiddenError(client=self.client)

    @jwt_required
    @access_log
    @courses_namespace.response(204, 'Course deleted')
    @courses_namespace.response(403, 'Not allowed to delete course')
    def delete(self, cid: str):
        self.permissions.require.sysadmin()
        course = self.find.course(cid)
        log.info(f"[REST] Delete course {course.log_name} by {self.client.log_name}")
        self.facades.courses.delete(course)
        return '', 204

    @jwt_required
    @access_log
    @courses_namespace.response(204, 'Course updated')
    @courses_namespace.response(403, 'Not allowed to update course')
    def put(self, cid: str):
        course = self.find.course(cid)
        # authorization
        self.permissions(course=course).require.update_course()
        data = rest_helpers.parse_request_data(action='update', resource='course', partial=True)
        log.info(f"[REST] Update course by {self.client.log_name}: {data} ")
        self.facades.courses.update(course, **data)
        return '', 204


@courses_namespace.route('/<string:cid>/definition')
@courses_namespace.param('cid', 'Course id')
@courses_namespace.response(404, 'Course not found')
class CourseResourceSchemaController(CustomResource):
    @jwt_required
    @courses_namespace.response(
        403, 'Not allowed to access the course definition')
    @courses_namespace.response(200, 'Course notes token')
    def get(self, cid):
        course = self.find.course(cid)
        # authorization
        log.debug(f"[REST] Get course definition for {course.log_name} by {self.client.log_name}")
        self.permissions(course=course).require.view_course_full()
        serial_type = request.args.get('format', 'yaml')
        dump_course = self.services.resource_definition.dump_course(course)
        return self._get_response(dump_course, serial_type)

    def _get_response(self, dump, serial_type):
        if serial_type == 'json':
            return dump
        safe_dump = yaml.safe_dump(dump)
        return flask.Response(safe_dump, content_type='text/yaml')


@courses_namespace.route('/<string:cid>/notes_access_token')
@courses_namespace.param('cid', 'Course id')
@courses_namespace.response(404, 'Course not found')
class CourseNotesToken(CustomResource):
    @jwt_required
    @courses_namespace.response(
        403, 'Not allowed to manipulate with course\'s notes access token')
    @courses_namespace.response(200, 'Course notes token')
    def get(self, cid):
        course = self.find.course(cid)
        # authorization
        log.debug(f"[REST] Get course notes token in {course.log_name} by {self.client.log_name}")
        self.permissions(course=course).require.course_access_token()
        return course.notes_access_token

    @jwt_required
    @access_log
    @courses_namespace.response(204, 'Course\'s notes access token updated')
    @courses_namespace.response(
        403, 'Not allowed to update course\'s notes access token')
    def put(self, cid):
        course = self.find.course(cid)
        # authorization
        self.permissions(course=course).require.course_access_token()

        json_data = rest_helpers.require_data(action='update_notes_token', resource='course')
        log.info(f"[REST] Update course access token by {self.client.log_name}: {json_data} ")
        extracted_token = json_data['token']
        self.facades.courses.update_notes_token(course, extracted_token)
        return '', 204


@courses_namespace.route('/<string:cid>/import')
@courses_namespace.param('cid', 'Course id')
@courses_namespace.response(404, 'Course not found')
class CourseImport(CustomResource):
    @jwt_required
    @access_log
    # @courses_namespace.response(200, 'Course has been imported', model=course_schema)
    @courses_namespace.response(403, 'Not allowed to import course')
    @courses_namespace.response(400, 'Cannot import course to itself.')
    def put(self, cid: str):
        target_course = self.find.course(cid)
        # authorization
        self.permissions(course=target_course).require.update_course()

        data = rest_helpers.parse_request_data(
            schema=SCHEMAS.course_import, action='import', resource='course'
        )
        source_course = self.find.course(data['source_course'])
        if source_course == target_course:
            raise PortalAPIError(
                400, f'[IMPORT] Cannot import course to itself. (id: {cid})')

        config = data['config']
        log.info(f"[REST] Import course by {self.client.log_name} "
                 f"from {source_course}: {config} ")

        copied_course = self.facades.courses.copy_course(source_course, target_course, **config)
        return SCHEMAS.dump('course', copied_course)


def extract_client_type():
    client_type = request.args.get('type')
    if client_type is not None:
        if client_type == 'worker':
            client_type = ClientType.WORKER
        elif client_type == 'user':
            client_type = ClientType.USER
        else:
            client_type = None
    return client_type


@courses_namespace.route('/<string:cid>/clients')
@courses_namespace.param('cid', 'Course id')
@courses_namespace.response(404, 'Course not found')
class CourseClients(CustomResource):
    @jwt_required
    # @courses_namespace.response(200, 'Users in the course', model=users_schema)
    @courses_namespace.response(403, 'Not allowed to see clients in the course')
    def get(self, cid):
        course = self.find.course(cid)
        self.permissions(course=course).require.view_course_full()
        group_ids = request.args.getlist('group')
        role_ids = request.args.getlist('role')
        client_type = extract_client_type()
        log.debug(f"[REST] Get course clients in {course.log_name} "
                  f"by {self.client.log_name}: roles={role_ids}, groups={group_ids}, "
                  f"type={client_type}")
        clients = self.facades.courses.get_clients_filtered(course, group_ids, role_ids,
                                                            client_type=client_type)
        return SCHEMAS.dump('clients', clients)


@courses_namespace.route('/<string:cid>/users')
@courses_namespace.param('cid', 'Course id')
@courses_namespace.response(404, 'Course not found')
class CourseUsers(CustomResource):
    @jwt_required
    # @courses_namespace.response(200, 'Users in the course', model=users_schema)
    @courses_namespace.response(403, 'Not allowed to see users in the course')
    def get(self, cid):
        course = self.find.course(cid)
        self.permissions(course=course).require.view_course_full()
        group_ids = request.args.getlist('group')
        role_ids = request.args.getlist('role')
        log.debug(f"[REST] Get course users in {course.log_name} "
                  f"by {self.client.log_name}: roles={role_ids}, groups={group_ids}")
        users = self.facades.courses.get_clients_filtered(course, group_ids, role_ids,
                                                          client_type=ClientType.USER)

        return SCHEMAS.dump('users', users)


@courses_namespace.route('/<string:cid>/workers')
@courses_namespace.param('cid', 'Course id')
@courses_namespace.response(404, 'Course not found')
class CourseWorkers(CustomResource):
    @jwt_required
    # @courses_namespace.response(200, 'Users in the course', model=users_schema)
    @courses_namespace.response(403, 'Not allowed to see workers in the course')
    def get(self, cid):
        course = self.find.course(cid)
        self.permissions(course=course).require.view_course_full()
        group_ids = request.args.getlist('group')
        role_ids = request.args.getlist('role')
        log.debug(f"[REST] Get course workers in {course.log_name} "
                  f"by {self.client.log_name}: roles={role_ids}, groups={group_ids}")
        workers = self.facades.courses.get_clients_filtered(course, group_ids, role_ids,
                                                            client_type=ClientType.WORKER)
        return SCHEMAS.dump('workers', workers)


@courses_namespace.route('/<string:cid>/is_muni/import/<string:users_type>')
@courses_namespace.param('cid', 'Course id')
@courses_namespace.param('users_type', 'Users type')
@courses_namespace.response(404, 'Course not found')
class CourseISImportUsers(CustomResource):
    @jwt_required
    def post(self, cid, users_type):
        course = self.find.course(cid)
        self.permissions(course=course).require.update_course()
        features.is_api_enabled(course=course, action='import users')
        role_name = request.args.get('role_name')
        if not role_name:
            return api_aborts.missing_param('role_name')
        log.debug(f"[REST] Import users to course {course.log_name} "
                  f"by {self.client.log_name}: "
                  f"role_name={role_name}, users_type={users_type}")

        return self.facades.courses.import_users(course=course,
                                                 users_type=users_type,
                                                 role_name=role_name)


@courses_namespace.route('/<string:cid>/is_muni/sync-seminaries')
@courses_namespace.param('cid', 'Course id')
@courses_namespace.response(404, 'Course not found')
class CourseISSyncSeminaries(CustomResource):
    @jwt_required
    def post(self, cid):
        course = self.find.course(cid)
        self.permissions(course=course).require.update_course()
        features.is_api_enabled(course=course, action='sync seminaries')
        log.debug(f"[REST] Sync seminaries for course {course.log_name} "
                  f"by {self.client.log_name}.")

        return self.facades.courses.sync_seminaries(course=course)

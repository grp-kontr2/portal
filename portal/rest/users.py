"""
Users REST API module
"""

from flask import request
from flask_jwt_extended import jwt_required
from flask_restplus import Namespace

from portal import logger
from portal.database.models import User
from portal.rest import rest_helpers
from portal.rest.custom_resource import CustomResource
from portal.rest.schemas import SCHEMAS
from portal.service import errors
from portal.service.services_collection import ServicesCollection
from portal.tools.decorators import access_log

users_namespace = Namespace('users')

log = logger.get_logger(__name__)


@users_namespace.route('')
class UserList(CustomResource):
    @jwt_required
    # @users_namespace.response(200, 'List of users', model=users_schema)
    def get(self):
        self.permissions().require.sysadmin()
        course_id = request.args.get('course')
        group_id = request.args.get('group')
        if group_id and not course_id:
            raise errors.PortalAPIError(400,
                                        "Invalid filter combination at /get/users: "
                                        "missing course id.")

        log.debug(f"[REST] Get users by {self.client.log_name}")
        filtered_users = self.facades.users.find_users_filtered(
            course_id=course_id, group_id=group_id)
        return SCHEMAS.dump('users', filtered_users), 200

    @jwt_required
    @access_log
    # @users_namespace.response(201, 'Created user', model=user_schema)
    def post(self):
        self.permissions().require.sysadmin()
        data = rest_helpers.parse_request_data(action='create', resource='user')
        log.info(f"[CREATE] New user by {self.client_name}: {data}")
        new_user = self.facades.users.create(**data)
        return SCHEMAS.dump('user', new_user), 201


@users_namespace.route('/<string:uid>')
@users_namespace.param('uid', "User's id")
@users_namespace.response(404, 'User not found')
class UserResource(CustomResource):
    @jwt_required
    # @users_namespace.response(200, "User reduced info", model=user_schema_reduced)
    # @users_namespace.response(200, "User info", model=users_schema)
    def get(self, uid: str):
        user = self.find.user(uid)
        # authorization
        log.debug(f"[REST] Get user {user.log_name} by {self.client.log_name}")
        schema = SCHEMAS.user()
        return schema.dump(user)[0], 200

    @jwt_required
    @access_log
    @users_namespace.response(204, 'User updated')
    def put(self, uid: str):
        user = self.find.user(uid)
        self.permissions().require.sysadmin_or_self(uid)
        data = rest_helpers.parse_request_data(action='update', resource='user', partial=True)
        log.info(f"[UPDATE] user {user.log_name} by {self.client_name}: {data}")
        self.facades.users.update(user, **data)
        return '', 204

    @jwt_required
    @access_log
    @users_namespace.response(204, 'User deleted')
    def delete(self, uid: str):
        self.permissions().require.sysadmin()
        user = self.find.user(uid)
        log.info(f"[DELETE] user {user.log_name} by {self.client_name}")
        self.facades.users.delete(user)
        return '', 204


@users_namespace.route('/<string:uid>/password')
@users_namespace.param('uid', "User's id")
@users_namespace.response(404, 'User not found')
class UserPassword(CustomResource):
    @jwt_required
    @access_log
    @users_namespace.response(204, 'User password updated')
    def put(self, uid: str):
        user = self.find.user(uid)
        self.permissions().require.sysadmin_or_self(uid)
        data = rest_helpers.parse_request_data(action='update', resource='password_change')
        log.info(f"[UPDATE] User's password {user.log_name} by {self.client_name}: {data}")
        self.facades.users.update_password(user, **data)
        return '', 204


@users_namespace.route('/<string:uid>/submissions')
@users_namespace.param('uid', "User's id")
@users_namespace.response(404, 'User not found')
class UserSubmissionList(CustomResource):
    @jwt_required
    # @users_namespace.response(200, 'Submissions list', model=submissions_schema)
    def get(self, uid: str):
        client = self.auth.client
        user = self.find.user(uid)

        course_id = request.args.get('course')
        project_ids = request.args.getlist('project')
        log.debug(f"[REST] Get submissions list for {user.log_name} by {self.client.log_name}"
                  f"in course={course_id} projects={project_ids}")

        if project_ids and not course_id:
            raise errors.PortalAPIError(400,
                                        f"Invalid filter combination at "
                                        f"/get/users/{uid}/submissions: missing course id.")

        submissions = get_submissions_based_on_permissions(
            client, user, course_id, project_ids)
        return SCHEMAS.dump('submissions', submissions), 200


def find_groups_intersection(user1: User, user2: User):
    other_groups = user2.groups
    res = [group for group in user1.groups if group in other_groups]
    return res


@users_namespace.route('/<string:uid>/roles')
@users_namespace.param('uid', "User's id")
@users_namespace.response(404, 'User not found')
class UserRoleList(CustomResource):
    @jwt_required
    # @users_namespace.response(200, 'User roles list', model=roles_schema)
    def get(self, uid: str):
        user = self.find.user(uid)
        self.permissions().require.sysadmin_or_self(uid)
        course_id = request.args.get('course')
        log.debug(f"[REST] Get user roles for {user.log_name} by {self.client.log_name}")
        roles = self.facades.users.find_roles_filtered(user, course_id=course_id)
        return SCHEMAS.dump('roles', roles)


@users_namespace.route('/<string:uid>/courses')
@users_namespace.param('uid', "User's id")
@users_namespace.response(404, 'User not found')
class UserCourseList(CustomResource):
    @jwt_required
    # @users_namespace.response(200, 'Courses roles list', model=courses_schema)
    def get(self, uid: str):
        user = self.find.user(uid)
        log.debug(f"[REST] Get user roles for {user.log_name} by {self.client.log_name}")
        self.permissions().require.sysadmin_or_self(uid)
        return SCHEMAS.dump('courses', user.courses)


@users_namespace.route('/<string:uid>/projects')
@users_namespace.param('uid', "User's id")
@users_namespace.response(404, 'User not found')
class UserProjectsList(CustomResource):
    @jwt_required
    # @users_namespace.response(200, 'Courses roles list', model=courses_schema)
    def get(self, uid: str):
        user = self.find.user(uid)
        # authorization
        log.debug(f"[REST] Get user projects for {user.log_name} by {self.client.log_name}")
        self.permissions().require.sysadmin_or_self(uid)
        projects_dump = self.facades.users.dump_projects_with_wait(user)
        return projects_dump


@users_namespace.route('/<string:uid>/groups')
@users_namespace.param('uid', "User's id")
@users_namespace.response(404, 'User not found')
class UserGroupList(CustomResource):
    @jwt_required
    # @users_namespace.response(200, 'User groups list', model=groups_schema)
    def get(self, uid: str):
        user = self.find.user(uid)
        # authorization
        log.debug(f"[REST] Get user groups for {user.log_name} by {self.client.log_name}")
        self.permissions().require.sysadmin_or_self(uid)
        course_id = request.args.get('course')
        groups = self.facades.users.find_groups_filtered(user, course_id=course_id)
        return SCHEMAS.dump('groups', groups)


@users_namespace.route('/<string:uid>/reviews')
@users_namespace.param('uid', "User's id")
@users_namespace.response(404, 'User not found')
class UserReviewList(CustomResource):
    @jwt_required
    # @users_namespace.response(200, 'User reviews list', model=reviews_schema)
    def get(self, uid: str):
        user = self.find.user(uid)
        # authorization
        log.debug(f"[REST] Get user reviews for {user.log_name} by {self.client.log_name}")
        self.permissions().require.sysadmin_or_self(uid)
        reviews = self.facades.users.find_reviews(user)
        return SCHEMAS.dump('reviews', reviews)


@users_namespace.route('/<string:uid>/permissions')
@users_namespace.param('uid', "User's id")
@users_namespace.response(404, 'User not found')
class UserEffectivePermissions(CustomResource):
    @jwt_required
    @users_namespace.response(200, 'Effective permissions')
    def get(self, uid: str):
        user = self.find.user(uid)
        course_id = request.args.get('course')

        # authorization
        log.debug(f"[REST] Get user effective permissions"
                  f" for {user.log_name} by {self.client.log_name} in course={course_id}")
        self.permissions().require.sysadmin_or_self(uid)
        perm = self.permissions(client=user).get_effective_permissions(course_id)
        return perm


@users_namespace.route('/<string:uid>/is_notes')
@users_namespace.param('uid', "User's id")
@users_namespace.response(404, 'User not found')
class UserNotes(CustomResource):
    @jwt_required
    @users_namespace.response(200, 'Effective permissions')
    def get(self, uid: str):
        user = self.find.user(uid)
        course_id = request.args.get('course')
        project_id = request.args.get('project')
        course = self.find.course(course_id)
        project = self.find.project(course, project_id)

        # authorization
        log.debug(f"[REST] Get user notes for project {project.log_name}"
                  f" for {user.log_name} by {self.client.log_name} in course={course_id}")
        self.permissions().require.sysadmin_or_self(uid)
        perm = self.permissions(client=user).get_effective_permissions(course_id)
        return perm


#
# Helper functions
#
def get_submissions_based_on_permissions(client, user, course_id, project_ids):
    rest_service = ServicesCollection.get()
    perm_service = rest_service.permissions
    if course_id:
        return get_submissions_based_on_permissions_for_course(
            client, course_id, project_ids, user)
    if perm_service.check.sysadmin():
        return user.submissions
    if rest_service.find.client_owner(client) == user:
        return user.submissions
    else:
        raise errors.ForbiddenError(client)


def get_submissions_based_on_permissions_for_course(client, course_id, project_ids, user):
    rest_service = ServicesCollection.get()
    course = rest_service.find.course(course_id)
    client_owner = rest_service.find.client_owner(client)
    perm_service = rest_service.permissions(course=course)
    checks = [
        perm_service.check.sysadmin(),
        perm_service.check.permissions(['read_submissions_all']),
        (perm_service.check.permissions(['read_submissions_groups'])
         and find_groups_intersection(client_owner, user)),
        (perm_service.check.permissions(['read_submissions_own'])
         and client_owner == user)
    ]
    perm_service.require.any_check(client, *checks)

    return rest_service.users(user).find_submissions_filtered(
        course_id=course_id, project_ids=project_ids)

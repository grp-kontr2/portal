from slugify import slugify


def sanitize_code_name(string: str, max_len: int = 25) -> str:
    return slugify(string)[:max_len]
"""
Custom decorators
"""

from functools import wraps

import flask
from flask_restplus import abort
from marshmallow import ValidationError
from sqlalchemy.exc import SQLAlchemyError

from portal import logger
from portal.logger import ACCESS
from portal.service.errors import PortalAPIError
from portal.tools import request_helpers

log = logger.get_logger(__name__)


def error_handler(func):
    """Wraps the method and catches an PortalAPIException and calls abort
    Args:
        func: Wrapped method

    Returns: Wrapper function

    """

    @wraps(func)
    def _func(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except PortalAPIError as ex:
            log.error(f"[API] Api error [{ex.code}]: {ex} ")
            abort(ex.code, message=ex.message)
        except ValidationError as ex:
            log.warning(f"[VALID] Validation failed: {ex}")
            abort(400, message=f"Validation failed on: {ex.messages}")
        except SQLAlchemyError as ex:
            log.error(f"[DB] Error: {ex}", ex)
            abort(400, message=f"Database error: {ex}")

    return _func


def access_log(func):
    @wraps(func)
    def _func(*args, **kwargs):
        request = flask.request
        from portal.service.services_collection import ServicesCollection
        client = ServicesCollection().auth.client

        ACCESS.info(f"[ACCESS] {client.log_name} {request_helpers.get_ip()}: "
                    f"{request.method} {request.path} {request.data} | UA: {request.user_agent}")
        return func(*args, **kwargs)

    return _func

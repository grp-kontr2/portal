from pathlib import Path
from typing import Dict, List

import json
from typing import Dict

import flask
import yaml

from portal.tools import yaml_extend

RESP_SERIAL_TYPES = dict(
    yaml={'type': 'application/yaml', 'func': yaml.safe_dump},
    json={'type': 'application/json', 'func': json.dumps},
)


def create_response(obj: Dict, type: str = 'json', code=200) -> flask.Response:
    if type not in RESP_SERIAL_TYPES.keys():
        type = 'json'
    sel = RESP_SERIAL_TYPES.get(type)
    dump = sel['func'](obj)
    return flask.Response(dump, content_type=sel['type'], status=code)


def load_yaml_file(fp) -> Dict:
    with Path(fp).open("r") as stream:
        return yaml.load(stream, Loader=yaml_extend.YAMLIncludeLoader)


def _extract_course_params(schema: Dict):
    codename = schema.get('codename') or schema.get('name')
    if not codename:
        from portal.service import errors
        raise errors.SchemaParseError("Course codename is not set in schema")
    name = schema.get('name') or codename
    notes_access_token = schema.get('notes_access_token')
    description = schema.get('description')
    faculty_id = schema.get('faculty_id')
    params = dict(name=name, codename=name, notes_access_token=notes_access_token,
                  description=description, faculty_id=faculty_id)
    return params


def make_role(role: Dict):
    pass


def make_roles(roles: List):
    return [make_role(role) for role in roles]


def make_projects(projects):
    pass


def make_groups(projects):
    pass


def make_course(schema: Dict) -> Dict:
    original = schema.get('course') or schema
    course_schema = _extract_course_params(original)
    roles = original.get('roles')
    projects = original.get('projects')
    groups = original.get('groups')
    course_schema['roles'] = make_roles(roles) if roles else None
    course_schema['projects'] = make_projects(projects) if projects else None
    course_schema['projects'] = make_groups(projects) if groups else None
    return course_schema

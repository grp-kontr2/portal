"""
Time module
"""

import json
from datetime import datetime

import pytz

UTC = pytz.UTC

# portal_timezone = datetime.timezone(offset=datetime.timedelta(hours=1), name='UTC+1')

portal_timezone = pytz.timezone('Europe/Prague')

DEFAULT_FORMAT = "%d-%m-%Y %H:%M:%S"
SIMPLE_FORMAT = "%Y-%m-%d_%H-%M-%S"


def current_time() -> datetime:
    return datetime.now(tz=portal_timezone)


def normalize_time(time) -> datetime:
    if hasattr(time, 'tzinfo') and time.tzinfo is not None:
        return time
    return portal_timezone.localize(time)


def strip_seconds(time: datetime) -> datetime:
    if not time:
        return time
    replaced = time.replace(second=0, microsecond=0)
    return normalize_time(replaced)


def format(time: datetime) -> str:
    return time.strftime(DEFAULT_FORMAT)


def parse(str_time) -> datetime:
    return datetime.strptime(str_time, DEFAULT_FORMAT)


class DateTimeEncoder(json.JSONEncoder):
    def default(self, o):  # pylint: disable=E0202
        if isinstance(o, datetime):
            return o.isoformat()

        return json.JSONEncoder.default(self, o)


def simple_fmt(time: datetime) -> str:
    return time.strftime(SIMPLE_FORMAT)


def simple_fmt2time(str_time: str) -> datetime:
    return datetime.strptime(str_time, SIMPLE_FORMAT)

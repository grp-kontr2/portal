"""
Tools module to help with general tasks
"""
import os
from distutils.util import strtobool

import giturlparse
from giturlparse.parser import Parsed
from .serializers import create_response, load_yaml_file


def s2b(bool_string: str) -> bool:
    if bool_string is None:
        return False
    if isinstance(bool_string, bool):
        return bool_string
    if isinstance(bool_string, str):
        return strtobool(bool_string)
    return False


def s2b_env(env: str, default) -> bool:
    return s2b(os.getenv(env, default))


def dict_dig(dictionary: dict, path: list):
    for item in path:
        if item not in dictionary:
            return None
        dictionary = dictionary[item]
        if not isinstance(dictionary, dict):
            return dictionary
    return dictionary


def extract_git_info(url: str) -> Parsed:
    return giturlparse.parse(url=url)

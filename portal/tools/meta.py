import inspect
from typing import List


def bound_update_class_var(cls, name: str) -> List[str]:
    cache_name = f'_CACHE_{name}'
    if not hasattr(cls, cache_name):
        parents = [klass for klass in inspect.getmro(cls) if hasattr(klass, name)]
        collection = []
        for klass in parents:
            collection += getattr(klass, name)
        setattr(cls, cache_name, collection)
    return getattr(cls, cache_name)

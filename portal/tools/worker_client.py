import urllib.parse
from typing import Union

import requests

from portal import logger

log = logger.get_logger(__name__)


def _parse_result(result: requests.Response) -> Union[dict, bytes]:
    try:
        return result.json()
    except ValueError:
        return result.content


class WorkerClient:
    def __init__(self, url: str = None, secret: str = None, worker=None, name: str = None):
        self._worker = worker
        self._url = url or worker.url
        self._secret = secret or worker.portal_secret
        self._session = None
        self._name = name

    @property
    def worker_name(self):
        if self.worker:
            return self.worker.log_name
        if self._name:
            return self._name
        if self._url:
            return self._url

    @property
    def _auth_header(self):
        return {'Authorization': f'Bearer {self._secret}'}

    @property
    def worker_url(self):
        return self._url

    @property
    def session(self) -> requests.Session:
        if not self._session:
            self._session = requests.session()
            self._session.headers.update(self._auth_header)
        return self._session

    @property
    def worker(self):
        return self._worker

    @property
    def credentials(self):
        cred = dict(
            url=self.worker.url,
            access_token=self.worker.portal_secret,
        )
        return cred

    def make_request(self, path, method='get', **kwargs):
        url = self._get_url(path)
        log.debug(f"[W-REQ] ({method}) {url}")
        result = self.session.request(url=url, method=method, **kwargs)
        if not result.ok:
            log.warning(f"[W-API] Worker returned the error "
                        f"({result.status_code}): {result.content}")
        else:

            log.debug(f"[W-RES] ({result.status_code}) {result.content}")
        return _parse_result(result)

    def execute_submission(self, submission):
        log.info(f"[W-API] Executing in {self.worker_name}: {submission.log_name}")
        result = self.make_request(f'/submissions/{submission.id}', method='post',
                                   json=submission.worker_parameters)
        return result

    def get_submission(self, submission):
        log.info(f"[W-API] Get submission in {self.worker_name}: {submission.log_name}")
        result = self.make_request(f'/submissions/{submission.id}', method='get')
        log.debug(f"[W-API] Submission: {result}")
        return result

    def list_submissions(self):
        log.info(f"[W-API] Get submissions for {self.worker_name}")
        result = self.make_request(f'/submissions', method='get')
        log.debug(f"[W-API] Submission: {result}")
        return result

    def status(self):
        log.info(f"[W-API] Get status for {self.worker_name}")
        result = self.make_request(f'/management/status', method='get')
        log.debug(f"[W-API] Status: {result}")
        return result

    def list_images(self, ):
        log.info(f"[W-API] Get images for {self.worker_name}")
        result = self.make_request(f'/submissions', method='get')
        log.debug(f"[W-API] Images: {result}")
        return result

    def remove_image(self, project):
        commit_hash = project.config.test_files_commit_hash
        log.info(f"[W-API] Removing image from test files in {self.worker_name} for "
                 f"{project.log_name}: {commit_hash}")
        result = self.make_request(f'/images/{commit_hash}/clean', method='get')
        log.debug(f"[W-API] State: {result}")
        return result

    def register(self):
        log.info(f"[W-API] Register worker {self.worker_name}")
        result = self.make_request(f'/management/registration', method='post')
        log.debug(f"[W-API] State: {result}")
        return result

    def _get_url(self, path: str) -> str:
        api_path = f"/api/v1.0{path}"
        urljoin = urllib.parse.urljoin(self.worker_url, api_path)
        return urljoin

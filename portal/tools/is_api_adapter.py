import logging
from typing import List, Optional, TYPE_CHECKING

import is_api
import is_api.entities
from flask import Flask


if TYPE_CHECKING:
    from portal.database import Course, Project, User

log = logging.getLogger(__name__)


class IsApiWrapper:
    def __init__(self, domain: str, faculty_id: str, course: 'Course' = None,
                 project: 'Project' = None):
        self._course = course or project.course
        self._project = project
        fac_id = course.faculty_id
        try:
            if fac_id is None and faculty_id is not None:
                fac_id = int(faculty_id)
        except Exception as ex:
            from portal.service import errors
            raise errors.PortalServiceError(f"Unable to extract faculty_id: {ex}")
        self._api = is_api.IsApiClient(domain=domain,
                                       faculty_id=fac_id,
                                       token=course.notes_access_token,
                                       course_code=course.codename)
        self._course_info = None

    @property
    def project(self) -> 'Project':
        return self._project

    @property
    def course(self) -> 'Course':
        return self._course

    @property
    def course_info(self) -> is_api.entities.CourseInfo:
        if self._course_info is None:
            self._course_info = self.is_client.course_info()
        return self._course_info

    @property
    def course_students(self) -> is_api.entities.CourseStudents:
        return self.is_client.course_list_students(registered=False)

    @property
    def is_client(self) -> is_api.IsApiClient:
        return self._api

    def read_notepad(self, users: List['User'] = None, shortcut: str = None) \
            -> Optional[is_api.entities.NotepadContent]:
        users = users or []
        ucos = [user.uco for user in users]
        if not ucos:
            return None
        log.info(f"[IS_API_ADAPT] Read notepad for {shortcut} to user {users}")
        content = self.is_client.notepad_content(shortcut, *ucos)
        return content

    def write_notepad(self, user: 'User', content=None, shortcut: str = None):
        log.info(f"[IS_API_ADAPT] Write notepad for {shortcut} to user {user.log_name}: {content}")
        return self.is_client.notepad_update(shortcut=shortcut, uco=user.uco,
                                      override=True, content=content)

    @property
    def course_teachers(self) -> List[is_api.entities.Teacher]:
        return self.course_info.teachers

    @property
    def course_seminars_list(self) -> List[is_api.entities.Seminar]:
        return self.course_info.seminars

    def create_note(self, shortcut: str, name: str, **kwargs):
        """Create new notepad in IS MUNI
        Args:
            shortcut(str): shortcut for the notepad
            name(str): Name of the notepad
            **kwargs:

        Returns:

        """
        log.info(f"[IS_API] Create new notepad for course {self.course.log_name} "
                 f": {name}({shortcut}) {kwargs} ")
        return self.is_client.notepad_new(name=name, shortcut=shortcut, **kwargs)


class IsApiFactory:
    def __init__(self, app: Flask = None):
        self._app = app

    def init_app(self, app: Flask):
        self._app = app

    @property
    def app(self) -> Flask:
        return self._app

    @property
    def is_domain(self) -> str:
        return self._app.config.get('IS_API_DOMAIN')

    @property
    def faculty_id(self) -> str:
        return self._app.config.get('IS_API_FACULTY_ID')

    def get_instance(self, course: 'Course') -> IsApiWrapper:
        params = self.__extract_params(course)
        return IsApiWrapper(**params)

    def __extract_params(self, course: 'Course') -> dict:
        from portal.service import errors
        if self.app is None:
            raise EnvironmentError("Flask app is not initialized.")
        if not self.is_api_enabled():
            raise EnvironmentError("IS API is not enabled.")
        if not course.notes_access_token:
            raise errors.AccessTokenForCourseMissingError(course)
        params = dict(domain=self.is_domain, faculty_id=self.faculty_id, course=course)
        return params

    def is_api_enabled(self) -> bool:
        return self.is_domain is not None

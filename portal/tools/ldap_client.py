"""
LDAP integration module
"""
import ldap3
from flask import Flask

from portal import logger

log = logger.get_logger(__name__)


class LDAPWrapper(object):
    def __init__(self, app: Flask = None):
        self.flask_app = None
        self._ldap_client = None
        self._ldap_server = None
        self._selector_base = ',dc=fi,dc=muni,dc=cz'
        if app:
            self.init_app(app)

    def init_app(self, app: Flask):
        self.flask_app = app
        self._ldap_client = None
        return app

    @property
    def ldap_url(self) -> str:
        """Returns the LDAP server URL
        Returns(str): URL to the LDAP server
        """
        return self.flask_app.config.get('LDAP_URL', None)

    @property
    def is_enabled(self) -> bool:
        """Whether the LDAP extension is enabled

        LDAP is enabled when LDAP url is set
        Returns(bool): true if is enabled, otherwise false
        """
        return self.ldap_url is not None

    @property
    def selector_base(self) -> str:
        """Base selector, for FI muni it is: 'dc=fi,dc=muni,dc=cz'

        Returns(str): Selector base string
        """
        return self._selector_base

    @property
    def ldap(self) -> ldap3.Server:
        """LDAP object instance
        Returns(LDAPObject): LDAP Object instance
        """
        if not self._ldap_server and self.is_enabled:
            self._ldap_server = ldap3.Server(self.ldap_url, get_info=ldap3.ALL)
            log.info(f"[LDAP] Creating LDAP server instance {self.ldap_url}: {self._ldap_server}")
        return self._ldap_server

    def ldap_connection(self):
        connection = None
        if self.ldap is not None:
            connection = ldap3.Connection(self.ldap, auto_bind=True)
            log.debug(f"[LDAP] Creating LDAP connection instance: {connection}")
        return connection

    def search(self, selector: str):
        selector = selector + self.selector_base
        log.debug(f"[LDAP] Search {self.ldap_url}: ({selector})")
        conn = self.ldap_connection()
        conn.search(selector, '(objectclass=*)', attributes=ldap3.ALL_ATTRIBUTES)
        entries = conn.entries
        if entries:
            result = entries[0]
            log.debug(f"[LDAP] Search ({selector}): {result}")
            return result
        else:
            log.warning(f"[LDAP] Not found any entry for selector: {selector}")
            return None

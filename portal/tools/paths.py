"""
Paths modules
"""

from os.path import dirname, abspath
from pathlib import Path

ROOT_DIR = Path(dirname(dirname(dirname(abspath(__file__)))))
RESOURCES_DIR = ROOT_DIR / 'resources'
LOG_DIR = ROOT_DIR / 'log'

# Management resources
MGMT_RESOUCES = ROOT_DIR / 'management' / 'data' / 'resources'

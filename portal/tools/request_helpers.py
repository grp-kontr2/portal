import flask


def get_ip(req: flask.Request = None):
    req = req or flask.request
    if req.remote_addr:
        return req.remote_addr
    real_ip = req.headers.get('X-Real-IP')
    if real_ip:
        return real_ip
    forwarded = req.headers.get('X-Forwarded-For')
    if forwarded:
        return forwarded
    return '<NO_IP>'

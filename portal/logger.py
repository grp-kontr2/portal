"""
Logging configuration module
"""

import logging
import os
from logging.config import dictConfig

from dotenv import load_dotenv

load_dotenv()


class Logging:
    def __init__(self, global_level: str = None, file_level=None, console_level=None):
        self._global_level = global_level or os.getenv('LOG_LEVEL_GLOBAL')
        self._file_level = file_level or os.getenv('LOG_LEVEL_FILE', self._global_level)
        self._console_level = console_level or os.getenv('LOG_LEVEL_CONSOLE', self._global_level)
        self.log = logging.getLogger(__name__)

    @property
    def global_log_level(self):
        return self._global_level

    @property
    def file_log_level(self):
        return self._file_level

    @property
    def console_log_level(self):
        return self._console_level

    @property
    def handlers(self):
        return {
            'console': self.get_handler_console(),
            'flask_console': self.get_handler_console(level='WARNING'),
            'portal_file': self.get_logger_file('portal'),
            'storage_file': self.get_logger_file('storage'),
            'flask_file': self.get_logger_file('flask'),
            'access_file': self.get_logger_file('access'),
            'auth_file': self.get_logger_file('auth'),
            'login_file': self.get_logger_file('login'),
            'submit_file': self.get_logger_file('submit'),
            'files_access_file': self.get_logger_file('files_access'),
            'gitlab_file': self.get_logger_file('gitlab'),
            'is_notes_files': self.get_logger_file('is_notes'),
        }

    @property
    def formatters(self):
        return {
            'verbose': {'format': '%(asctime)s - %(name)s - %(levelname)s - %(message)s'},

            'simple': {'format': '%(levelname)s %(message)s'},
            'colored_console': {
                '()': 'coloredlogs.ColoredFormatter',
                'format': "%(asctime)s - %(name)s - %(levelname)s - %(message)s",
                'datefmt': '%H:%M:%S'
            },
        }

    @property
    def loggers(self) -> dict:
        return {
            'portal': {
                'handlers': ['console', 'portal_file'],
                'level': self.global_log_level, 'propagate': True
            },
            'portal.storage': {
                'handlers': ['console', 'storage_file'],
                'level': self.global_log_level, 'propagate': True
            },
            'portal.auth_log': {
                'handlers': ['console', 'auth_file'],
                'level': self.global_log_level, 'propagate': True
            },
            'portal.files_access': {
                'handlers': ['console', 'files_access_file'],
                'level': self.global_log_level, 'propagate': True
            },
            'portal.login_log': {
                'handlers': ['console', 'login_file'],
                'level': self.global_log_level, 'propagate': True
            },
            'portal.submit_log': {
                'handlers': ['console', 'submit_file'],
                'level': self.global_log_level, 'propagate': True
            },
            'portal.access_log': {
                'handlers': ['console', 'access_file'],
                'level': self.global_log_level, 'propagate': True
            },
            'tests': {
                'handlers': ['console'],
                'level': self.global_log_level, 'propagate': True
            },
            'management': {
                'handlers': ['console'], 'level': self.global_log_level, 'propagate': True
            },
            'app': {
                'handlers': ['console'], 'level': self.global_log_level, 'propagate': True
            },
            'flask': {
                'handlers': ['flask_file'],
                'level': self.global_log_level,
                'propagate': True
            },
            'werkzeug': {
                'handlers': ['flask_file'],
                'level': 'ERROR', 'disabled': True,
            },
            'git': {
                'handlers': ['console', 'storage_file'],
                'level': self.global_log_level,
                'propagate': True
            },
            'gitlab': {
                'handlers': ['console', 'portal_file', 'gitlab_file'],
                'level': self.global_log_level,
                'propagate': True
            },
            'pytest': {
                'handlers': ['console'], 'level': 'ERROR', 'propagate': True
            },
        }

    @property
    def config(self):
        loggers = self.loggers
        handlers = self.handlers

        return {
            'version': 1,
            'disable_existing_loggers': False,
            'formatters': self.formatters,
            'handlers': handlers,
            'loggers': loggers,
        }

    def get_logger_file(self, name, level: str = None):
        level = level or self.file_log_level
        from portal.tools import paths
        filename = str(paths.LOG_DIR / f'{name}.log')

        return {
            'level': level,
            'class': 'logging.handlers.RotatingFileHandler',
            'formatter': 'verbose',
            'filename': filename,
            'maxBytes': 5000000,  # 5MB
            'backupCount': 5
        }

    def get_handler_console(self, level: str = None):
        level = level or self.console_log_level
        return {
            'level': level,
            'class': 'logging.StreamHandler',
            'formatter': 'colored_console'
        }

    def load_config(self):
        """Loads config based on the config type
        Args:
        """
        add_custom_log_level()
        dictConfig(self.config)
        self.reenable_loggers()

    def reenable_loggers(self):
        loggers = [logging.getLogger(name) for name in logging.root.manager.loggerDict]
        for logger in loggers:
            logger.disabled = False
        pass


TRACE_LOG_LVL = 9


def _trace(self, message, *args, **kws):
    if self.isEnabledFor(TRACE_LOG_LVL):
        # Yes, logger takes its '*args' as 'args'.
        self._log(TRACE_LOG_LVL, message, args, **kws)


def add_custom_log_level():
    logging.addLevelName(TRACE_LOG_LVL, 'TRACE')
    logging.Logger.trace = _trace


def get_logger(*args, **kwargs):
    logger = logging.getLogger(*args, **kwargs)
    return logger


ACCESS = logging.getLogger('portal.access_log')
FILES = logging.getLogger('portal.files_access')
AUTH = logging.getLogger('portal.auth_log')
LOGIN = logging.getLogger('portal.login_log')
SUBMIT = logging.getLogger('portal.submit_log')

"""
Main Portal module
"""
import logging
import os
from typing import Union

import yaml
from authlib.flask.client import OAuth
from celery import Celery
from flask import Flask
from flask_cors import CORS
from flask_jwt_extended import JWTManager
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy

from portal import logger, rest
from portal.config import CONFIGURATIONS
from portal.storage import Storage
from portal.tools.is_api_adapter import IsApiFactory
from portal.tools.ldap_client import LDAPWrapper
from portal.tools.paths import ROOT_DIR
from portal.tools.yaml_extend import YAMLIncludeLoader, construct_include

db = SQLAlchemy()
jwt = JWTManager()
oauth = OAuth()
cors = CORS(supports_credentials=True)
storage_wrapper = Storage()
migrate = Migrate(db=db)
ldap_wrapper = LDAPWrapper()
is_api_factory = IsApiFactory()
log = logging.getLogger(__name__)


def configure_app(app: Flask, env: str = None,
                  ignore_local: bool = False) -> Flask:
    """Configures the Flask app based on production stage.

    Args:
        app(Flask): Flask application
        env(str): Environment for the flask application
        ignore_local(bool): Whether to ignore portal.local.cfg
    """
    config_type = env or os.environ.get('PORTAL_CONFIG_TYPE', 'dev')
    config_object = _get_config_object(config_type)
    app.config.from_object(config_object)
    _load_portal_local(app, env, ignore_local)
    app.config['PORTAL_ENV'] = config_type

    return app


def _log_config(app):
    log.trace("[INIT] Loaded config: ")
    for (key, val) in app.config.items():
        log.trace(f"[CONFIG] {key}={val}")


def _load_portal_local(app, env, ignore_local):
    ignore_local = ignore_local or env == 'test'
    if not ignore_local:
        app.config.from_pyfile(ROOT_DIR / 'portal.local.cfg', silent=True)
        app.config.from_envvar('PORTAL_CONFIG', silent=True)


def _get_config_object(config_type):
    log.debug(f"[INIT] Portal Config Type: {config_type}")
    config_object = CONFIGURATIONS[config_type]
    if config_object is None:
        raise RuntimeError(f"There is no config object for: {config_type}")
    return config_object


def configure_storage(app: Flask) -> Flask:
    """Configures storage

    Args:
        app(Flask): Flask application

    Returns(Flask): Flask application
    """
    storage_config = dict(
        test_files_dir=app.config.get('PORTAL_STORAGE_TEST_FILES_DIR'),
        submissions_dir=app.config.get('PORTAL_STORAGE_SUBMISSIONS_DIR'),
        workspace_dir=app.config.get('PORTAL_STORAGE_WORKSPACE_DIR'),
        results_dir=app.config.get('PORTAL_STORAGE_RESULTS_DIR'),
        custom_ssh_key=app.config.get('GITPYTHON_CUSTOM_SSH_KEY')
    )
    storage_wrapper.init_storage(**storage_config)
    return app


def configure_extensions(app: Flask):
    """Configures all extensions

    Args:
        app(Flask): Flask application

    Returns(Flask): Flask application
    """
    db.init_app(app)
    migrate.init_app(app, db)
    jwt.init_app(app)
    oauth.init_app(app)
    cors.init_app(app)
    ldap_wrapper.init_app(app)
    is_api_factory.init_app(app)
    return app


def configure_async(app: Flask):
    from portal import async_celery
    async_celery.init_app(app)


def create_app(environment: str = None):
    """Creates and initializes the components used in the application

    Returns(Flask): Flask application instance
    """
    app = Flask(__name__)

    yaml.add_constructor(u'!include', construct_include, YAMLIncludeLoader)

    # app configuration
    configure_app(app, env=environment)
    _log_config(app)
    configure_storage(app)
    configure_extensions(app)
    configure_async(app)

    if app.config.get('GITLAB_URL') and app.config.get('GITLAB_CLIENT_ID'):
        import portal.rest.gitlab
        rest.gitlab.register_gitlab_app(app)

    rest.register_namespaces(app)
    return app


def get_celery(app: Flask) -> Union[Celery, None]:
    if not app.config.get('BROKER_URL'):
        return None
    from portal.async_celery import celery_app
    return celery_app

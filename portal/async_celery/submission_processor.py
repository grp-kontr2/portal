import json
import secrets
from pathlib import Path
from typing import Optional, TYPE_CHECKING

from portal import logger
from portal.database import Project, Submission, SubmissionState, Worker
from portal.database.enums import WorkerState
from portal.service import errors
from portal.storage import UploadedEntity, entities

if TYPE_CHECKING:
    from portal.service.submissions import SubmissionsService

log = logger.get_logger(__name__)


class SubmissionProcessor:
    def __init__(self, submission: Submission):
        self._submission = submission
        from portal.service.services_collection import ServicesCollection
        self._services = ServicesCollection()

    @property
    def submission_service(self) -> 'SubmissionsService':
        return self._services.submissions(self.submission)

    @property
    def submission(self) -> Submission:
        return self._submission

    @property
    def params(self) -> dict:
        return self.submission.parameters

    @property
    def project(self) -> Project:
        return self.submission.project

    @property
    def celery(self):
        from portal.async_celery import celery_app
        return celery_app

    @property
    def storage(self):
        from portal import storage_wrapper
        return storage_wrapper

    def reset_task_id(self, state=None, message: str = None):
        if state is not None:
            self.submission_service.set_state(state, message=message)

        self.submission.async_task_id = None
        self._save_submission()

    def submission_enqueue_ended(self):
        log.info(f"[ASYNC] Submission enqueue ended {self.submission.log_name}: {self.submission}")
        self.reset_task_id(state=SubmissionState.QUEUED, message="Submission queue ended.")

    def get_delay_for_submission(self):
        log.info(f"[ASYNC] Submission delay {self.submission.log_name}: {self.submission}")
        time_wait = self.project.config.submissions_cancellation_period
        return time_wait

    def dispatch_submission_processing(self):
        delay = self.get_delay_for_submission()
        args = (self.submission.id,)
        from .tasks import start_processing_submission
        self.submission.scheduled_for = delay
        self._save_submission()
        start_processing_submission.apply_async(args=args, countdown=delay)

    def submission_store_ended(self, version: str):
        log.info(f"[ASYNC] Submission preparation ended {self.submission.log_name}: "
                 f"{self.submission}")
        self.submission.source_hash = version
        self.reset_task_id(state=SubmissionState.READY, message="Submission preparation ended")

    def download_submission(self):
        file_params = self.params['file_params']
        log.info(f"[ASYNC] Uploading submission: {self.submission.log_name} with {file_params}")

        updated_entity: UploadedEntity = self.storage. \
            submissions.create(dirname=self.submission.storage_dirname, **file_params)
        self.submission_store_ended(version=updated_entity.version)

    def clone(self, target):
        log.info(f"[ASYNC] Cloning submission: {self.submission.log_name} to {target.log_name}")
        self.storage.submissions.clone(self.submission.storage_dirname, target.id)
        self.submission_store_ended(version=self.submission.source_hash)

    def send_to_worker(self):
        # TODO: implement processing
        log.info(f"[ASYNC] Sending submission to worker: {self.submission.log_name}")
        worker = self.schedule_submission_to_worker()
        if worker:
            self.execute_submission(worker)
        else:
            log.warning(f"[EXEC] Worker not available to process submission: "
                        f"{self.submission.log_name}")
        self.abort_submission("No Worker available")

    def upload_result(self, path, file_params):
        log.info(f"[ASYNC] Uploading result for the submission "
                 f"{self.submission.log_name} with {file_params}")
        self.storage.results.create(dirname=self.submission.storage_dirname, **file_params)
        Path(path).unlink()
        self.reset_task_id(SubmissionState.FINISHED, message="Upload result ended")

    def process_submission(self):
        log.info(f"[ASYNC] Processing submission {self.submission.log_name}")
        try:
            self.download_submission()
        except Exception as ex:
            log.error(f"[PROC] Storage submission download aborted due error: {ex}")
            self.abort_submission('Unable to download a submission')
            return None
        self.dispatch_submission_processing()

    def revoke_task(self):
        log.info(f'[ASYNC] Submission processing cancelled {self.submission.log_name}')
        task_id = self.submission.async_task_id
        if task_id:
            self.celery.control.revoke(task_id=task_id, terminate=True)
            self.reset_task_id(state=SubmissionState.CANCELLED, message="Submission cancelled")
        # TODO: Storage clean up

    def _get_avail_workers(self):
        course = self.submission.course
        workers = self._services.workers.find_all()
        return [worker for worker in workers
                if worker.state == WorkerState.READY and course in worker.courses]

    # TODO implement - @mdujava
    # STUB: Select initialized worker
    def schedule_submission_to_worker(self) -> Optional[Worker]:
        """Based on the features (worker tags) and preferences in project config
        schedule submission for the execution on initialized worker

        Returns(Worker): Worker instance on which the submission will be executed
        """
        workers = self._get_avail_workers()
        if not workers:
            self._worker_not_available()
            return None
        worker = secrets.choice(workers)  # randomly select a worker
        log.debug(f"[SCHED] Scheduling submission to worker -> {worker.log_name}")
        return worker

    def execute_submission(self, worker: Worker):
        worker_client = self._services.workers(worker).worker_client
        message = "Executing submission using worker"
        self.submission_service.set_state(SubmissionState.IN_PROGRESS, message=message)
        self._save_submission()
        worker_client.execute_submission(self.submission)

    def _worker_not_available(self):
        log.warning(f"[PROC] Worker is no available for submission: {self.submission.log_name}")

    def process_result(self):
        storage_entity = self.storage.results.get(self.submission.storage_dirname)
        # @mdujava - here put submission processing
        return self._submission_result_processing(storage_entity)

    def _submission_result_processing(self, storage_entity: entities.Entity):
        suite_stats = storage_entity.get('suite-stats.json')
        if not suite_stats.exists():
            log.error(f"[PROC] Suite stats for the {self.submission.log_name} have not been found.")
            raise errors.SuiteStatsNotExists(self.submission.storage_dirname)
        stats = json.loads(suite_stats.read_text('utf-8'))
        return self._parse_stats(stats)

    def _parse_stats(self, stats: dict):
        log.debug(f"[PROC] Processing the stats: {stats}")
        points = stats.get('final_points', 0)
        result = stats.get('result', 'none').lower()
        log.debug(f"[PROC] Processing stats: points={points}, result={result}")
        self.submission.result = result
        self.submission.points = points
        return self.submission

    def abort_submission(self, message: str = 'Unknown error!'):
        self.submission_service.set_state(SubmissionState.ABORTED, message=message)

    def _save_submission(self):
        self._services.submissions.write_entity(self.submission)

from celery import Celery
from flask import Flask

celery_app = Celery()


def init_app(app: Flask) -> Flask:
    """Initializes the async_celery module
    Args:
        app(Flask): Flask application

    Returns(Flask): Flask application

    """
    if app.config.get('BROKER_URL'):
        celery_app.conf.update(app.config)

    # pylint: disable=too-few-public-methods
    class ContextTask(celery_app.Task):
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return self.run(*args, **kwargs)

    # pylint: enable=too-few-public-methods

    celery_app.Task = ContextTask
    return app

import portal.async_celery.periodic_tasks
import datetime

from portal.async_celery import celery_app


@celery_app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    sender.add_periodic_task(30 * 60,
                             abort_non_proc_submissions.s(hours=2),
                             name='Abort non processed submissions after 2 hours')
    sender.add_periodic_task(24 * 60 * 60,
                             delete_cancelled_submissions.s(),
                             name="Delete all cancelled tasks after one day")

    sender.add_periodic_task(24 * 60 * 60,
                             archive_submissions_in_arch_project.s(),
                             name="Archive all submissions in archived project")


@celery_app.task
def abort_non_proc_submissions(**kwargs):
    from portal.async_celery.async_manager import AsyncManager
    limit = datetime.timedelta(**kwargs)
    mgr = AsyncManager()
    mgr.abort_submissions_sched_exceeded(limit)
    mgr.abort_submission_proc_exceeded(limit)
    mgr.abort_submission_checkout_exceeded(limit)


@celery_app.task
def delete_cancelled_submissions():
    from portal.service.services_collection import ServicesCollection
    subm_service = ServicesCollection.get().submissions
    subm_service.delete_cancelled_submissions()


@celery_app.task
def archive_submissions_in_arch_project():
    from portal.service.services_collection import ServicesCollection
    subm_service = ServicesCollection.get().submissions
    subm_service.archive_submissions()

from portal.database import Submission, SubmissionState
from portal.service.services_collection import ServicesCollection
from portal.tools import time


class AsyncManager:
    def __init__(self):
        self._services = ServicesCollection.get()

    @property
    def services(self) -> ServicesCollection:
        return self._services

    def abort_submissions_sched_exceeded(self, limit):
        self.abort_submission_any_exceeded(limit=limit, state=SubmissionState.READY,
                                           param='created_at', msg_add='Schedule phase')

    def abort_submission_checkout_exceeded(self, limit):
        self.abort_submission_any_exceeded(limit=limit, state=SubmissionState.CREATED,
                                           param='scheduled_for', msg_add='Checkout phase')

    def abort_submission_proc_exceeded(self, limit):
        self.abort_submission_any_exceeded(limit=limit, state=SubmissionState.IN_PROGRESS,
                                           param='scheduled_for', msg_add='Process phase')

    def _proc_expired(self, submission: Submission, limit, param='scheduled_for'):
        req_time = getattr(submission, param)
        if not req_time:
            return False
        schedule_time = time.normalize_time(req_time)
        curr = time.current_time()
        exp_time = limit + schedule_time
        return exp_time <= curr

    def _abort_submission(self, sub: Submission, message):
        sub.abort(message=message)
        self.services.submissions.write_entity(sub)

    def abort_submission_any_exceeded(self, limit, state, param, msg_add: str, msg=None):
        msg = msg or "Submission has not been processed in time limit: "
        msg += msg_add
        submissions = Submission.query.filter(Submission.state == state).all()
        submissions = [sub for sub in submissions if self._proc_expired(sub, limit, param=param)]
        for sub in submissions:
            self._abort_submission(sub, msg)

from celery.utils.log import get_task_logger

from portal.async_celery import celery_app, submission_processor
from portal.service.courses import CourseService
from portal.service.find import FindService
from portal.service.is_api_service import IsApiService
from portal.service.projects import ProjectService
from portal.service.submissions import SubmissionsService

log = get_task_logger(__name__)

"""
= Submissions related
"""


@celery_app.task(name='upload-submission-to-storage')
def process_submission(new_submission_id: str):
    find_service = FindService()
    new_submission = find_service.submission(new_submission_id)
    project = new_submission.project
    log.info(f"[SUBMIT] Processing submission: {new_submission.log_name}")
    if not project.config.test_files_commit_hash:
        log.warning(f"Project test files not found: {project.log_name}")
        update_project_test_files(course_id=project.course.id, project_id=project.id)
    new_submission = find_service.submission(new_submission_id)
    processor = submission_processor.SubmissionProcessor(new_submission)
    processor.process_submission()


@celery_app.task(name='delete-submission')
def delete_submission(submission_id: str):
    submission = FindService().submission(submission_id)
    SubmissionsService(submission).delete()


@celery_app.task(name='archive-submission')
def archive_submission(submission_id: str):
    submission = FindService().submission(submission_id)
    SubmissionsService(submission).archive()


@celery_app.task(name='upload-results-to-storage')
def upload_results_to_storage(new_submission_id: str, path: str):
    new_submission = FindService().submission(new_submission_id)
    log.info(f"[SUBMIT] Processing results - upload to the storage for "
             f"{new_submission.log_name}: {path}")
    SubmissionsService(new_submission).upload_results_to_storage(path)


@celery_app.task(name='clone-submission-files')
def clone_submission_files(source_id: str, target_id: str):
    source = FindService().submission(source_id)
    target = FindService().submission(target_id)
    log.info(f"[SUBMIT] Copy submission files: {source.log_name} -> {target.log_name}")
    processor = submission_processor.SubmissionProcessor(source)
    processor.clone(target)


@celery_app.task(name='start-processing-submission')
def start_processing_submission(submission_id: str):
    submission = FindService().submission(submission_id)
    log.info(f"[SUBMIT] Processing submission - send to worker: {submission.log_name}")
    processor = submission_processor.SubmissionProcessor(submission)
    processor.send_to_worker()


"""
= Project related
"""


@celery_app.task(name='update-project-test-files')
def update_project_test_files(course_id: str, project_id: str):
    find_service = FindService()
    course = find_service.course(course_id)
    project = find_service.project(course, project_id)
    log.info(f"[ASYNC] Updating test files for project: {project.log_name}")
    params = {
        'from_dir': project.config.test_files_subdir,
        'source': {
            'type': 'git',
            'url': project.config.test_files_source
        }
    }
    try:
        ProjectService(project).update_project_test_files(params)
    except Exception as ex:
        log.error(f"[ASYNC] Cannot update source files {project.log_name}: {ex}")


@celery_app.task(name='delete-project')
def delete_project(course_id, project_id: str):
    course = FindService().course(course_id)
    submission = FindService().project(course, project_id)
    ProjectService(submission).delete()


@celery_app.task(name='archive-project')
def archive_project(course_id, project_id: str):
    course = FindService().course(course_id)
    project = FindService().project(course, project_id)
    ProjectService(project).archive()


"""
= COURSE RELATED
"""


@celery_app.task(name='delete-course')
def delete_course(course_id):
    course = FindService().course(course_id)
    CourseService(course).delete()


@celery_app.task(name='archive-course')
def archive_course(course_id):
    course = FindService().course(course_id)
    CourseService(course).archive()


"""
= IS MUNI RELATED
"""


@celery_app.task(name='is-sync-course-seminaries')
def is_sync_seminaries(course_id):
    find_service = FindService()
    course = find_service.course(course_id)
    log.info(f"[ASYNC] Sync course seminaries for {course.log_name}")
    IsApiService(course).sync_seminaries()


@celery_app.task(name='is-import-users-for-course')
def is_import_users_for_course(course_id: str, role_name: str, users_type: str):
    find_service = FindService()
    course = find_service.course(course_id)
    log.info(f"[ASYNC] Import course users for {course.log_name}: {users_type} -> {role_name}")
    IsApiService(course).import_users(role_name=role_name, users_type=users_type)

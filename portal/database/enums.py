import enum


class ClientType(enum.Enum):
    """All known client types
    """
    USER = 'user'
    WORKER = 'worker'


class CourseState(enum.Enum):
    """All the states in which the project can be
    """
    ACTIVE = 'active'
    INACTIVE = 'inactive'
    ARCHIVED = 'archived'


class ProjectState(enum.Enum):
    """All the states in which the project can be
    """
    ACTIVE = 1
    INACTIVE = 2
    ARCHIVED = 3


class SubmissionState(enum.Enum):
    """Enum of the submissions states
    """
    CREATED = 1
    READY = 2
    QUEUED = 3
    IN_PROGRESS = 4
    FINISHED = 5
    CANCELLED = 6
    ABORTED = 7
    ARCHIVED = 8


class WorkerState(enum.Enum):
    """Possible worker states
    """
    CREATED = 'created'
    READY = 'ready'
    STOPPED = 'stopped'

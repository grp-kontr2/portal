import json
from enum import Enum

import sqlalchemy
import yaml
from sqlalchemy import TypeDecorator, String
from sqlalchemy.sql import operators


class JSONEncodedDict(TypeDecorator):
    """Represents an immutable structure as a json-encoded string."""

    def process_literal_param(self, value, dialect):
        return self.process_bind_param(value, dialect)

    @property
    def python_type(self):
        return dict

    impl = sqlalchemy.Text

    def process_bind_param(self, value, dialect):
        if value is not None:
            value = json.dumps(value)
        return value

    def process_result_value(self, value, dialect):
        if value is not None:
            value = json.loads(value)
        return value

    def coerce_compared_value(self, op, value):
        if op in (operators.like_op, operators.notlike_op):
            return String()
        else:
            return self


class YAMLEncodedDict(TypeDecorator):
    """Represents an immutable structure as a yaml-encoded string."""

    impl = sqlalchemy.Text

    def process_bind_param(self, value, dialect):
        if value is not None:
            value = yaml.safe_dump(value)
        return value

    def process_result_value(self, value, dialect):
        if value is not None:
            value = yaml.safe_load(value)
        return value


class CustomEnum(TypeDecorator):
    impl = sqlalchemy.String

    def __init__(self, enum_cls, *args, **kwargs):
        self.enum_cls = enum_cls
        super().__init__(*args, **kwargs)

    def process_bind_param(self, value: Enum, dialect):
        if value is not None:
            value = value.value
        return value

    def process_result_value(self, value, dialect):
        if value is not None:
            value = self.enum_cls(value)
        return value

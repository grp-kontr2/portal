"""
Models module where all of the models are specified
"""
import copy
import logging
import uuid
from pathlib import Path
from typing import List

import sqlalchemy as sa
from flask_sqlalchemy import BaseQuery
from sqlalchemy import event
from sqlalchemy.ext.associationproxy import association_proxy
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy_continuum import make_versioned
from werkzeug.security import check_password_hash, generate_password_hash

from portal import db
from portal.database.enums import ClientType, CourseState, ProjectState, SubmissionState, \
    WorkerState
from portal.database.exceptions import PortalDbError
from portal.database.mixins import CodeNameMixin, EntityBase, MetaModelBase, NamedMixin
from portal.database.types import CustomEnum, JSONEncodedDict, YAMLEncodedDict
from portal.tools import time
from portal.tools.time import normalize_time

make_versioned(user_cls=None)
log = logging.getLogger(__name__)


class Client(db.Model, MetaModelBase, CodeNameMixin):
    """Client entity model
    """
    __tablename__ = 'client'
    LISTABLE = ['id', 'type', 'codename']
    UPDATABLE = ['codename']
    id = db.Column(db.String(length=36), default=lambda: str(uuid.uuid4()), primary_key=True)
    type = db.Column(db.Enum(ClientType, name='ClientType'), nullable=False)
    secrets = db.relationship('Secret', back_populates='client', uselist=True,
                              cascade="all, delete-orphan", passive_deletes=True)

    __mapper_args__ = {
        'polymorphic_identity': 'client',
        'polymorphic_on': type
    }

    def __init__(self, client_type: ClientType):
        self.type = client_type

    def verify_secret(self, secret: str) -> bool:
        return any(check_password_hash(item.value, secret) and not item.expired
                   for item in self.secrets)

    def query_permissions_for_course(self, course: 'Course'):
        """Returns the query for client's permissions in the course
        Args:
            course(Course): Course instance
        Returns: Query for the permissions
        """
        from . import queries
        return queries.client_roles_in_course(self, course)

    def get_permissions_for_course(self, course: 'Course') -> List['Role']:
        """Gets list of permissions for the course
        Args:
            course(Course): Course instance
        Returns(list[Permissions]): List of the Permissions
        """
        return self.query_permissions_for_course(course=course).all()

    @property
    def courses(self) -> list:
        """Gets courses the client is a part of

        Returns(list): List of courses
        """
        return self.query_courses().all()

    def query_courses(self) -> BaseQuery:
        """Queries courses the client is a part of

        Returns(BaseQuery): BaseQuery that can be executed
        """
        from . import queries
        return queries.client_courses(self)

    def get_roles_in_course(self, course: 'Course') -> list:
        """Gets user's roles for the course

        Args:
            course(Course): Course instance

        Returns(list): List of roles
        """
        return self.query_roles_in_course(course=course).all()

    def query_roles_in_course(self, course: 'Course') -> BaseQuery:
        """Queries user's roles for the course

        Args:
            course(Course): Course instance

        Returns(BaseQuery): BaseQuery that can be executed
        """
        from . import queries
        return queries.client_roles_in_course(self, course)

    def is_self(self, eid) -> bool:
        return self.id == eid


class Secret(db.Model, EntityBase):
    __tablename__ = 'secret'
    UPDATABLE = ['name', 'expires_at']
    BASE_PARAMS = ['id', *UPDATABLE]

    id = db.Column(db.String(length=36), default=lambda: str(uuid.uuid4()), primary_key=True)
    name = db.Column(db.String(40), nullable=False)
    value = db.Column(db.String(120))
    expires_at = db.Column(db.TIMESTAMP, nullable=True)

    client_id = db.Column(db.String(36),
                          db.ForeignKey('client.id', ondelete='cascade'), nullable=False)
    client = db.relationship("Client", back_populates="secrets", uselist=False)

    def __init__(self, name: str, value: str = None, expires_at=None):
        # TODO: check date/timestamp
        self.name = name
        self.value = generate_password_hash(value)
        self.expires_at = expires_at

    @hybrid_property
    def namespace(self) -> str:
        return f'{self.client.codename}/{self.name}'

    @property
    def expired(self) -> bool:
        """Gets whether the secret is expired or not

        If the expires_at is empty, then it always returns False

        Returns(bool):
        """
        if self.expires_at is None:
            return False
        return time.normalize_time(self.expires_at) < time.current_time()


class User(Client, EntityBase):
    """User entity model

    Attributes:
        id: UUID string
        uco: University identification number
        email: User's email
        username: User's username (xlogin for example)
        name: User's full name
        is_admin: Whether user is administrator
        client: the client part of this user (secrets, roles)
        password_hash: Hashed password
        submissions: Collection of user's submissions
        review_items: Collection of review_items created by user
        groups: Collection of groups the user belongs to
    """
    UPDATABLE = ['name', 'email', 'uco', 'gitlab_username', 'managed']
    BASE_PARAMS = ['username', 'codename', 'is_admin', *UPDATABLE, 'managed', 'id']
    LISTABLE = ['username', 'gitlab_username', 'name', 'uco', 'email']
    __tablename__ = 'user'
    id = db.Column(db.String(length=36), db.ForeignKey('client.id'),
                   default=lambda: str(uuid.uuid4()), primary_key=True)
    uco = db.Column(db.Integer)
    email = db.Column(db.String(50), unique=True, nullable=False)
    name = db.Column(db.String(50))
    is_admin = db.Column(db.Boolean, default=False)
    # optional - after password change
    password_hash = db.Column(db.String(120), default=None)

    _gitlab_username = db.Column('gitlab_username', db.String(50), nullable=True)
    managed = db.Column(db.Boolean, default=False)
    submissions = db.relationship("Submission", back_populates="user", cascade="all, delete-orphan",
                                  passive_deletes=True)
    review_items = db.relationship("ReviewItem", back_populates="user")

    __mapper_args__ = {
        'polymorphic_identity': ClientType.USER,
    }

    @hybrid_property
    def username(self) -> str:
        return self.codename

    @hybrid_property
    def gitlab_username(self) -> str:
        return self._gitlab_username or self.username

    @gitlab_username.setter
    def gitlab_username(self, value: str):
        self._gitlab_username = value

    @username.setter
    def username(self, value: str):
        self.codename = value

    def is_self(self, eid):
        return super().is_self(eid) or self.username == eid

    def set_password(self, password: str):
        """Sets password for the user
        Args:
            password(str): Unhashed password
        """
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password: str) -> bool:
        """Verifies user's password
        Args:
            password(str): Unhashed password

        Returns(bool): True if the password is valid, False otherwise

        """
        return check_password_hash(self.password_hash, password)

    def query_groups_in_course(self, course: 'Course',
                               project: 'Project' = None) -> BaseQuery:
        """Queries user's groups for course and project

        Args:
            course(Course): Course instance
            project(Project): Project Instance (optional)

        Returns(BaseQuery): BaseQuery that can be executed
        """
        from . import queries
        return queries.user_groups_in_course(self, course, project=project)

    def get_groups_in_course(self, course: 'Course',
                             project: 'Project' = None) -> list:
        """Gets user's groups for course and project (optional)

        Args:
            course(Course): Course instance
            project(Project): Project Instance (optional)

        Returns(list): List of groups
        """
        return self.query_groups_in_course(
            course=course, project=project).all()

    def query_projects_by_course(self, course: 'Course') -> BaseQuery:
        from . import queries
        return queries.user_projects_by_course(self, course)

    def query_all_projects(self, course: 'Course' = None):
        from . import queries
        return queries.user_projects(self, course=course)

    def query_all_submissions(self):
        from . import queries
        return queries.user_avail_submissions(self)

    def query_all_submissions_by_group(self):
        from . import queries
        return queries.user_avail_submissions_by_group(self)

    def get_all_submissions(self):
        return self.query_all_submissions().all()

    def query_all_projects_in_group(self):
        from . import queries
        return queries.user_projects_by_group(self)

    def get_all_projects(self):
        return self.query_all_projects().all()

    def get_all_projects_in_group(self):
        return self.query_all_projects_in_group().all()

    def get_projects_by_course(self, course: 'Course') -> List['Project']:
        """Gets projects from the course
        Args:
            course(Course): Course instance
        Returns(List[Project]): filtered list of projects
        """
        return self.query_projects_by_course(course=course).all()

    def __init__(self, uco: int = None, email: str = None, username: str = None,
                 is_admin: bool = False, gitlab_username: str = None, **kwargs) -> None:
        """Creates user instance
        Args:
            uco(int): User's UCO (school identifier)
            email(str): User's Email
            username(str): Username
            is_admin(bool): Whether user is admin
        """
        self.uco = uco
        self.email = email
        self.username = username
        self.is_admin = is_admin
        self.gitlab_username = gitlab_username
        super().__init__(ClientType.USER, **kwargs)

    def __eq__(self, other):
        return self.id == other.id

    def __hash__(self):
        return hash(self.id)


def _get_class_based_on_client_type(client_type):
    klass = Client
    if client_type is not None:
        klass = User if client_type == ClientType.USER else Worker
    return klass


class Course(db.Model, EntityBase, NamedMixin):
    """Course model
    """
    UPDATABLE = ['faculty_id', 'state']
    BASE_PARAMS = ['id', *UPDATABLE]
    LISTABLE = [*BASE_PARAMS]
    __tablename__ = 'course'
    id = db.Column(db.String(length=36), default=lambda: str(uuid.uuid4()), primary_key=True)
    notes_access_token = db.Column(db.String(256))
    faculty_id = db.Column(db.Integer)
    state = db.Column(CustomEnum(CourseState))

    roles = db.relationship("Role", back_populates="course", cascade="all, delete-orphan",
                            passive_deletes=True)
    groups = db.relationship("Group", back_populates="course", cascade="all, delete-orphan",
                             passive_deletes=True)
    projects = db.relationship("Project", back_populates="course", cascade="all, delete-orphan",
                               passive_deletes=True)

    __table_args__ = (
        db.UniqueConstraint('codename', name='course_unique_codename'),
    )

    def is_api_enabled(self) -> bool:
        return self.notes_access_token is not None and self.notes_access_token != ""

    def __init__(self, name: str = None, codename: str = None, notes_access_token: str = None,
                 description: str = None, faculty_id: int = None,
                 state: CourseState = CourseState.INACTIVE) -> None:
        """Creates course instance
        Args:
            state (CourseState): State of the course
            description (str): Course's description
            name(str): Course name
            codename(str): Course codename
            faculty_id(int): Faculty id
            notes_access_token: IS MUNI Notepads access token
        """
        self.name = name
        self.codename = codename or name
        self.description = description
        self.notes_access_token = notes_access_token
        self.faculty_id = faculty_id
        self.state = state

    def __eq__(self, other):
        return self.id == other.id

    def query_roles_by_client(self, client):
        from . import queries
        return queries.client_roles_in_course(client=client, course=self)

    def get_roles_by_client(self, client):
        return self.query_roles_by_client(client).all()

    def get_users_by_role(self, role: 'Role') -> List['User']:
        """Gets all users in the course based on their role
        Args:
            role(Role): The role to filter by
        Returns(List[User]): List of users that have the role
        """
        from . import queries
        return queries.course_clients_by_role(course=self, role=role, klass=User).all()

    def get_clients_filtered(self, groups: List['Group'] = None, roles: List['Role'] = None,
                             client_type=None):
        """Gets all users in the course who are members of at least one of
        the provided groups and have at least one of the specified roles.

        Args:
            client_type:
            groups(List['Group']): A list of groups. A user must be part of at least one to
                be included in the result.
            roles(List['Role']): A list of roles. A user must be part of at
                least one to be included in the result.

        Returns: Users in the course that satisfy constraints
            (are in at least one group AND have at least one role).

        """
        from . import queries
        return queries.course_clients_filtered(course=self, groups=groups,
                                               roles=roles, client_type=client_type).all()


class Project(db.Model, EntityBase, NamedMixin):
    """Project model
    Class Attributes:
        id: UUID of the project
        name: Name of the project
        description: Description of the project
        config: Configuration of the project
        course_id: id of the course associated with the project
        course: course associated with the project
        submissions: list of submissions associated with the project
    """
    UPDATABLE = ['assignment_url', 'submit_instructions', 'submit_configurable']
    BASE_PARAMS = ['id', *UPDATABLE]
    LISTABLE = ['id', 'assignment_url', 'submit_configurable']

    __tablename__ = 'project'
    id = db.Column(db.String(length=36), default=lambda: str(uuid.uuid4()), primary_key=True)
    assignment_url = db.Column(db.Text)
    submit_instructions = db.Column(db.Text, nullable=True)
    submit_configurable = db.Column(db.Boolean, default=True)

    config = db.relationship("ProjectConfig", back_populates="project", uselist=False,
                             cascade="all, delete-orphan", passive_deletes=True)

    course_id = db.Column(db.String(36), db.ForeignKey('course.id', ondelete='cascade'),
                          nullable=False)
    course = db.relationship("Course", back_populates="projects", uselist=False)
    submissions = db.relationship("Submission", back_populates="project",
                                  cascade="all, delete-orphan", passive_deletes=True)

    __table_args__ = (
        db.UniqueConstraint('course_id', 'codename', name='p_course_unique_name'),
    )

    @hybrid_property
    def namespace(self) -> str:
        return f"{self.course.codename}/{self.codename}"

    @hybrid_property
    def storage_dirname(self):
        return f"{self.course.codename}_{self.codename}"

    @hybrid_property
    def state(self) -> ProjectState:
        return self.get_state_by_timestamp()

    def get_state_by_timestamp(self, timestamp=None) -> ProjectState:
        """Gets project state based on the timestamp
               Args:
                   timestamp: Time for which the state should be calculated
               Returns:
        """
        timestamp = timestamp or time.current_time()
        if not (self.config.submissions_allowed_from or
                self.config.submissions_allowed_to or
                self.config.archive_from):
            return ProjectState.INACTIVE
        if self.config.submissions_allowed_from <= timestamp < self.config.submissions_allowed_to:
            return ProjectState.ACTIVE
        elif timestamp >= self.config.archive_from:
            return ProjectState.ARCHIVED
        return ProjectState.INACTIVE

    @hybrid_property
    def is_archived(self) -> bool:
        return self.state == ProjectState.ARCHIVED

    def set_config(self, **kwargs):
        """Sets project configuration
        Keyword Args:
            test_files_source(str): Repository URL
            file_whitelist(str): Filter string for whitelist
            config_subdir(str): Where to look for the configuration (pre, post, scheduler)
            submission_parameters(str):
                Schema that defines all the parameters that should be passed to submission
            submissions_allowed_from(time):
                Time from all the submissions are allowed from
            submissions_allowed_to(time):
                Time to which all submissions are allowed to
            archive_from(time):
                Archive all submissions from
        """
        ALLOWED = ('test_files_source', 'file_whitelist', 'config_subdir', 'config_file',
                   'submission_parameters')
        for k, w in kwargs.items():
            if k in ALLOWED:
                setattr(self.config, k, w)

        if 'submissions_allowed_from' in kwargs.keys():
            self.config.submissions_allowed_from = kwargs['submissions_allowed_from']
        if 'submissions_allowed_to' in kwargs.keys():
            self.config.submissions_allowed_to = kwargs['submissions_allowed_to']
        if 'archive_from' in kwargs.keys():
            self.config.archive_from = kwargs['archive_from']

    def __init__(self, course: Course, codename: str = None, name: str = None,
                 description: str = None, assignment_url: str = None,
                 submit_configurable: bool = True):
        """Creates instance of the project
        Args:
            course(Course): Course instance
            description(des): Project's description
            name(str): Project name
        """
        self.course = course
        self.name = name
        self.codename = codename
        self.description = description
        self.assignment_url = assignment_url
        self.submit_configurable = submit_configurable
        self.config = ProjectConfig(project=self)

    def __eq__(self, other):
        return self.id == other.id

    def __hash__(self):
        return hash(self.id)


class ProjectConfig(db.Model, EntityBase):
    """Project's configuration
    Class Attributes:
        id: UUID of the project's configuration
        project_id: Associated project's id
        project: Associated project
        test_files_source: Test files source location: git repository
        test_files_commit_hash: Full SHA-1 hash of the latest revision of project's test_files
        file_whitelist: whitelisted files
        submission_parameters: Post submit script used in the submissions processing component
    """
    UPDATABLE = ['submissions_cancellation_period', 'test_files_commit_hash', 'config_file',
                 'test_files_source', 'test_files_subdir', 'file_whitelist', 'config_subdir',
                 'submissions_allowed_from', 'submissions_allowed_to', 'archive_from']
    BASE_PARAMS = ['id', *UPDATABLE]
    LISTABLE = ['id', 'assignment_url']
    __tablename__ = 'projectConfig'
    id = db.Column(db.String(length=36), default=lambda: str(uuid.uuid4()), primary_key=True)
    project_id = db.Column(db.String(36), db.ForeignKey(
        'project.id', ondelete='cascade'), nullable=False)
    project = db.relationship("Project", back_populates="config", uselist=False)

    submissions_cancellation_period = db.Column(db.Integer)
    test_files_source = db.Column(db.String(600))  # a git repository URL
    test_files_subdir = db.Column(db.String(50))  # git subdir
    test_files_commit_hash = db.Column(db.String(128))  # hash of the latest revision of test_files
    file_whitelist = db.Column(db.Text)
    config_subdir = db.Column(db.String(50), default='kontr2', nullable=True)
    config_file = db.Column(db.String(50), default='config.yml', nullable=True)
    submission_parameters = db.Column(YAMLEncodedDict)
    _submissions_allowed_from = db.Column(db.TIMESTAMP(timezone=True))
    _submissions_allowed_to = db.Column(db.TIMESTAMP(timezone=True))
    _archive_from = db.Column(db.TIMESTAMP(timezone=True))

    @hybrid_property
    def submissions_allowed_from(self):
        """Gets from which date all the submissions are allowed from

        Returns(time): Time from submissions are allowed from

        """
        seconds = time.strip_seconds(self._submissions_allowed_from)
        return seconds

    @property
    def config_path(self) -> Path:
        cfg_file = self.config_file or 'config.yml'
        cfg_sub = self.config_subdir or 'kontr2'
        return Path(cfg_sub) / cfg_file

    @hybrid_property
    def submissions_allowed_to(self):
        """Gets from which date all the submissions are allowed to

        Returns(time): Time from submissions are allowed to

        """
        seconds = time.strip_seconds(self._submissions_allowed_to)
        return seconds

    @hybrid_property
    def archive_from(self):
        """Gets from which date all the submissions are archived from

        Returns(time): Time from submissions should be archived from

        """
        seconds = time.strip_seconds(self._archive_from)
        return seconds

    @submissions_allowed_from.setter
    def submissions_allowed_from(self, submissions_allowed_from):
        """Set time from which submissions are allowed to submit
        Args:
            submissions_allowed_from(time): Time from which all the submissions are allowed to be
                submitted
        """
        submissions_allowed_from = normalize_time(submissions_allowed_from)
        if self.submissions_allowed_to is not None and \
                submissions_allowed_from > self.submissions_allowed_to:
            raise PortalDbError()
        if self.archive_from is not None and \
                submissions_allowed_from > self.archive_from:
            raise PortalDbError()
        self._submissions_allowed_from = time.strip_seconds(
            submissions_allowed_from)

    @submissions_allowed_to.setter
    def submissions_allowed_to(self, submissions_allowed_to):
        """Set time from which submissions are allowed to submit
        Args:
            submissions_allowed_to(time): Time to which all the submissions are allowed to submit
        """
        submissions_allowed_to = normalize_time(submissions_allowed_to)
        if self.submissions_allowed_from is not None and \
                self.submissions_allowed_from > submissions_allowed_to:
            raise PortalDbError("Submissions allowed from later than to")
        if self.archive_from is not None and \
                self.archive_from < submissions_allowed_to:
            raise PortalDbError("Archive from is lesser than submissions allowed to")
        self._submissions_allowed_to = time.strip_seconds(
            submissions_allowed_to)

    @archive_from.setter
    def archive_from(self, archive_from):
        """Set time from which submissions are allowed to archive
        Args:
            archive_from(time): Time from which all the submissions are allowed to archive
        """
        if archive_from is None:
            self._archive_from = None
            return
        archive_from = normalize_time(archive_from)
        if self.submissions_allowed_from is not None and \
                self.submissions_allowed_from > archive_from:
            raise PortalDbError()
        if self.submissions_allowed_to is not None and \
                self.submissions_allowed_to > archive_from:
            raise PortalDbError()
        self._archive_from = time.strip_seconds(archive_from)

    def __init__(self, project: Project, test_files_source: str = None):
        """Creates instance of the project's config
        Args:
            project(Project): Project instance
            test_files_source: Source files
        """
        self.project = project
        self.test_files_source = test_files_source

    def __eq__(self, other):
        return self.id == other.id


class Role(db.Model, EntityBase, NamedMixin):
    """Role model
    Class Attributes:
        id: UUID of the role
        name: Name of the role
        description: Description of the role
        clients: Clients associated with the role
        course: Course associated with the role
    """
    PERMISSIONS = ('create_submissions', 'create_submissions_other',
                   'view_course_limited', 'view_course_full', 'update_course',
                   'handle_notes_access_token', 'write_roles', 'write_groups',
                   'write_projects', 'resubmit_submissions', 'evaluate_submissions',
                   'read_submissions_all', 'read_submissions_groups',
                   'read_submissions_own', 'read_reviews_all', 'read_reviews_groups',
                   'read_reviews_own', 'write_reviews_all', 'write_reviews_group',
                   'write_reviews_own')
    BASE_PARAMS = [*PERMISSIONS, 'id']
    UPDATABLE = PERMISSIONS
    LISTABLE = ['id']

    __tablename__ = 'role'
    id = db.Column(db.String(length=36), default=lambda: str(
        uuid.uuid4()), primary_key=True)
    clients = db.relationship("Client", secondary="clients_roles")
    course_id = db.Column(db.String(36),
                          db.ForeignKey('course.id', ondelete='cascade'), nullable=False)
    course = db.relationship("Course", back_populates="roles", uselist=False)

    # all default to False, explicit setting via role.set_permissions is
    # required
    view_course_limited = db.Column(db.Boolean, default=False, nullable=False)
    view_course_full = db.Column(db.Boolean, default=False, nullable=False)
    update_course = db.Column(db.Boolean, default=False, nullable=False)
    handle_notes_access_token = db.Column(
        db.Boolean, default=False, nullable=False)

    write_roles = db.Column(db.Boolean, default=False, nullable=False)
    write_groups = db.Column(db.Boolean, default=False, nullable=False)
    write_projects = db.Column(db.Boolean, default=False, nullable=False)

    create_submissions = db.Column(db.Boolean, default=False, nullable=False)
    create_submissions_other = db.Column(db.Boolean, default=False, nullable=False)
    resubmit_submissions = db.Column(db.Boolean, default=False, nullable=False)
    evaluate_submissions = db.Column(db.Boolean, default=False, nullable=False)

    read_submissions_all = db.Column(db.Boolean, default=False, nullable=False)
    read_submissions_groups = db.Column(db.Boolean, default=False, nullable=False)
    read_submissions_own = db.Column(db.Boolean, default=False, nullable=False)
    read_all_submission_files = db.Column(db.Boolean, default=False, nullable=False)

    read_reviews_all = db.Column(db.Boolean, default=False, nullable=False)
    read_reviews_groups = db.Column(db.Boolean, default=False, nullable=False)
    read_reviews_own = db.Column(db.Boolean, default=False, nullable=False)

    write_reviews_all = db.Column(db.Boolean, default=False, nullable=False)
    write_reviews_group = db.Column(db.Boolean, default=False, nullable=False)
    write_reviews_own = db.Column(db.Boolean, default=False, nullable=False)

    def set_permissions(self, **kwargs):
        """Sets permissions for the role
        Args:
            **kwargs: Permissions
        """
        IGNORED_PARAMS = ('id', 'course_id', 'course', 'created_at', 'updated_at')
        for k, w in kwargs.items():
            if hasattr(self, k) and k not in IGNORED_PARAMS:
                setattr(self, k, w)

    __table_args__ = (
        db.UniqueConstraint('course_id', 'codename', name='r_course_unique_name'),
    )

    def __init__(self, course: Course, name: str = None,
                 description: str = None, codename: str = None):
        """Creates instance of the role in a course
        Args:
            course(Course): Course instance
            description(str): Role's description
            name(str): Name of the role
        """
        self.course = course
        self.description = description
        self.name = name
        self.codename = codename

    def __eq__(self, other):
        return self.id == other.id

    def __hash__(self):
        return hash(self.id)


class Group(db.Model, EntityBase, NamedMixin):
    """Group model
    Class Attributes:
        id: UUID of the group
        name: Name of the Group
        course: Course associated with the Group
        users: Collection of users that are in the Group
        projects: Collection of projects that are allowed for the Group
    """
    __tablename__ = 'group'
    LISTABLE = ['id']
    BASE_PARAMS = LISTABLE
    id = db.Column(db.String(length=36), default=lambda: str(
        uuid.uuid4()), primary_key=True)
    course_id = db.Column(db.String(36), db.ForeignKey(
        'course.id', ondelete='cascade'), nullable=False)
    course = db.relationship("Course", back_populates="groups", uselist=False)
    users = db.relationship("User", secondary="users_groups")
    projects = db.relationship(
        "Project", secondary="projects_groups", back_populates="groups")

    __table_args__ = (
        db.UniqueConstraint('course_id', 'codename', name='g_course_unique_name'),
    )

    @hybrid_property
    def namespace(self) -> str:
        return f"{self.course.codename}/{self.codename}"

    def __init__(self, course: Course, name: str = None,
                 description: str = None, codename: str = None):
        """Creates instance of the group
        Args:
            course(Course): Course instance
            name(str): Name of the group
            description(str): Description of the group
        """
        self.course = course
        self.description = description
        self.name = name
        self.codename = codename

    def __eq__(self, other):
        return self.id == other.id

    def __hash__(self):
        return hash(self.id)

    def get_users_based_on_role(self, role: 'Role' = None):
        from . import queries
        return queries.group_users(self, role).all()


class Submission(db.Model, EntityBase):
    """Submission model
    Class Attributes:
        id: UUID of the submission
        scheduled_for: Date and time when the submission should be executed
        parameters: Parameters of the submission
        state: State of the submission
        note: Notes with tags for the submission
        source_hash: The git commit hash of the submitted repository revision

        user: User who created the submission
        project: Associated project for the submission
        review: Review associated with the submission
    """
    LISTABLE = ['id', 'state', 'points', 'result', 'scheduled_for', 'created_at', 'updated_at']
    BASE_PARAMS = ['parameters', 'source_hash', *LISTABLE]
    __tablename__ = 'submission'
    id = db.Column(db.String(length=36), default=lambda: str(
        uuid.uuid4()), primary_key=True)
    scheduled_for = db.Column(db.TIMESTAMP(timezone=True))
    parameters = db.Column(JSONEncodedDict(), nullable=True)
    note = db.Column(JSONEncodedDict(), nullable=True)
    state = db.Column(db.Enum(SubmissionState, name='SubmissionState'), nullable=False)
    source_hash = db.Column(db.String(64))
    user_id = db.Column(db.String(36), db.ForeignKey('user.id', ondelete='cascade'),
                        nullable=False, index=True)
    user = db.relationship("User", back_populates="submissions", uselist=False)
    project_id = db.Column(db.String(36), db.ForeignKey('project.id', ondelete='cascade'),
                           nullable=False, index=True)
    project = db.relationship("Project", uselist=False)
    review = db.relationship("Review", back_populates="submission", cascade="all, delete-orphan",
                             passive_deletes=True, uselist=False)

    async_task_id = db.Column(db.String(length=36))
    points = db.Column(db.Numeric(precision=10, scale=4), default=0.0)
    result = db.Column(db.String(length=10), default='none')
    course = association_proxy('project', 'course')
    course_id = association_proxy('project', 'course_id')

    ALLOWED_TRANSITIONS = {
        SubmissionState.CANCELLED: [SubmissionState.READY, SubmissionState.CREATED,
                                    SubmissionState.QUEUED, SubmissionState.IN_PROGRESS],
        SubmissionState.READY: [SubmissionState.CREATED]
    }

    @property
    def worker_parameters(self) -> dict:
        params = {
            **self.parameters,
            'id': self.id,
            'test_files_hash': self.project.config.test_files_commit_hash,
            'test_files_subdir': self.project.config.test_files_subdir
        }
        return params

    @hybrid_property
    def namespace(self) -> str:
        return f"{self.project.namespace}/{self.user.codename}/{self.created_at}"

    @hybrid_property
    def storage_dirname(self) -> str:
        created = time.simple_fmt(self.created_at)
        return f"{self.user.username}_{created}_{self.course.codename}_{self.project.codename}"

    def change_state(self, new_state, message: str = None):
        # open to extension (state transition validation, ...)
        if new_state in Submission.ALLOWED_TRANSITIONS.keys():
            allow = Submission.ALLOWED_TRANSITIONS[new_state]
            if self.state not in allow:
                return False
        self.state = new_state
        self.make_change(dict(state=new_state.name, message=message))
        return True

    def make_change(self, change):
        self.note = copy.deepcopy(self.note or {})
        if 'changes' not in self.note:
            self.note['changes'] = []
        self.note['changes'].append(change)

    @property
    def changes(self) -> List:
        if self.note and 'changes' in self.note:
            return self.note['changes']
        else:
            return []

    def abort(self, message):
        self.change_state(SubmissionState.ABORTED, message=message)

    @property
    def is_cancelled(self) -> bool:
        return self.state in [SubmissionState.CANCELLED, SubmissionState.ABORTED]

    @property
    def is_finished(self) -> bool:
        return self.state in [SubmissionState.FINISHED, SubmissionState.ARCHIVED]

    @property
    def is_processed(self) -> bool:
        return self.state in [SubmissionState.CREATED, SubmissionState.QUEUED,
                              SubmissionState.READY, SubmissionState.IN_PROGRESS]

    def __init__(self, user: User, project: Project, parameters: dict = None,
                 review: 'Review' = None):
        """Creates a new submission instance
        Args:
            user(User): User instance
            project(Project): Project instance
            parameters(dict): Parameters for the submission
            review(Review): Review instance
        """
        self.user = user
        self.project = project
        self.review = review
        self.parameters = parameters or {}
        self.state = SubmissionState.CREATED
        self.note = {}

    def __eq__(self, other):
        return self.id == other.id


class Review(db.Model, EntityBase):
    """Review model
    Class Attributes:
        id: UUID of the review
        submission: Associated submission for the review
        review_items: Review items (Comments) associated to review and submission
    """
    BASE_PARAMS = ['id']
    LISTABLE = ['id']
    __tablename__ = 'review'
    id = db.Column(db.String(length=36), default=lambda: str(
        uuid.uuid4()), primary_key=True)
    submission_id = db.Column(db.String(36), db.ForeignKey('submission.id', ondelete='cascade'),
                              nullable=False)
    submission = db.relationship("Submission", uselist=False, back_populates="review")
    review_items = db.relationship("ReviewItem", back_populates="review",
                                   cascade="all, delete-orphan", passive_deletes=True)

    def __init__(self, submission: Submission):
        """Creates a review for the submission
        Args:
            submission(Submission):
        """
        self.submission = submission

    def __eq__(self, other):
        return self.id == other.id


class ReviewItem(db.Model, EntityBase):
    """Review item model
    Class Attributes:
        id: UUID of the submission
        review: Associated review
        user: Author of the review
        content: Content of the review (normal text)
        file: File path
        line_start: start line in the file
        line_end: end line in the file
    """
    __versioned__ = {}
    __tablename__ = 'reviewItem'
    UPDATABLE = ['content', 'file', 'line_start', 'line_end', 'line']
    BASE_PARAMS = ['id', *UPDATABLE]
    LISTABLE = BASE_PARAMS
    id = db.Column(db.String(length=36), default=lambda: str(uuid.uuid4()), primary_key=True)
    review_id = db.Column(db.String(36), db.ForeignKey('review.id', ondelete='cascade'),
                          nullable=False)
    review = db.relationship("Review", uselist=False, back_populates="review_items")
    user_id = db.Column(db.String(36), db.ForeignKey('user.id', ondelete='cascade'))
    user = db.relationship("User", uselist=False, back_populates="review_items")
    content = db.Column(db.Text)
    file = db.Column(db.String(256), nullable=True)
    line_start = db.Column(db.Integer, nullable=True)
    line_end = db.Column(db.Integer, nullable=True)

    def __init__(self, user: User, review: Review, content: str = None, file: str = None,
                 line: int = None, line_start: int = None, line_end: int = None):
        """Creates a review item in the review
        Args:
            user(User): The author of the review item
            review(Review): Review the item belongs to
            file(str): File the review item binds to
            line_start(int): line start  of the file
            line_end(int): line end of the file
            content(str): Review item content (text)
        """
        self.review = review
        self.user = user
        self.file = file
        self.line_start = line_start
        self.line_end = line_end
        self.content = content

        if line is not None:
            self.line_start = line
            self.line_end = line

    @hybrid_property
    def line(self) -> int:
        """Get line by default returns line_start
        Returns(int): line_start

        """
        return self.line_start

    @line.setter
    def line(self, line):
        self.line_start = line
        if self.line_end is None:
            self.line_end = line

    def __eq__(self, other):
        return self.id == other.id


class Worker(Client, EntityBase):
    """Worker model:
    Class Attributes:
        id: UUID of the submission
        name: Name of the component
        client: the client part of the worker, attaching secrets and roles
        url: Url at which the worker is accessible
        tags: a string containing tags associated with the worker, denoting its features
        portal_secret: a token used for authenticating portal against the worker
    """
    LISTABLE = ['url', 'id', 'state']
    UPDATABLE = ['url', 'tags', 'portal_secret', 'state']
    BASE_PARAMS = ['id', *UPDATABLE]
    __tablename__ = 'worker'
    id = db.Column(db.String(length=36), db.ForeignKey('client.id'),
                   default=lambda: str(uuid.uuid4()), primary_key=True)
    url = db.Column(db.String(250), nullable=False)
    tags = db.Column(db.Text())  # set by a Worker at init
    portal_secret = db.Column(db.String(64))  # set by a Worker at init
    state = db.Column(db.Enum(WorkerState, name='WorkerState'), nullable=False)

    __mapper_args__ = {
        'polymorphic_identity': ClientType.WORKER,
    }

    @hybrid_property
    def namespace(self) -> str:
        return self.codename

    @hybrid_property
    def name(self):
        return self.codename

    @name.setter
    def name(self, value):
        self.codename = value

    def is_self(self, eid):
        return super().is_self(eid) or self.name == eid

    def __init__(self, name: str, url: str):
        """Creates component
        Args:
            name(str): Name of the component
            url(str) : Url of the component (where portal should send requests)
        """
        self.name = name
        self.url = url
        self.state = WorkerState.CREATED
        super().__init__(client_type=ClientType.WORKER)

    @hybrid_property
    def is_initialized(self):
        return self.url is not None and self.portal_secret is not None

    def __eq__(self, other):
        return self.id == other.id


# source: http://docs.sqlalchemy.org/en/latest/orm/events.html
@event.listens_for(Submission.user_id, 'set', named=True)
def receive_set(**kw):
    submission = kw['target']
    value = kw['value']

    if submission.user_id is not None and submission.user_id != value:
        raise PortalDbError()


# Associations - must come after definitions of the affected classes
users_groups = db.Table("users_groups", db.Model.metadata,
                        db.Column("user_id", db.String(36),
                                  db.ForeignKey('user.id', ondelete='cascade')),
                        db.Column("group_id", db.String(36),
                                  db.ForeignKey('group.id', ondelete='cascade')))

clients_roles = db.Table("clients_roles", db.Model.metadata,
                         db.Column("client_id", db.String(36),
                                   db.ForeignKey('client.id', ondelete='cascade')),
                         db.Column("role_id", db.String(36),
                                   db.ForeignKey('role.id', ondelete='cascade')))

projects_groups = db.Table("projects_groups", db.Model.metadata,
                           db.Column("project_id", db.String(36),
                                     db.ForeignKey('project.id', ondelete='cascade')),
                           db.Column("group_id", db.String(36),
                                     db.ForeignKey('group.id', ondelete='cascade')))

Client.roles = db.relationship("Role", secondary="clients_roles")
User.groups = db.relationship("Group", secondary="users_groups")
Project.groups = db.relationship("Group", secondary="projects_groups",
                                 back_populates='projects')

sa.orm.configure_mappers()

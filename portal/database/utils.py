"""
Utils to help with database
"""

from flask_sqlalchemy import BaseQuery
from sqlalchemy import func


def sql_get_count(q: BaseQuery) -> int:
    """Gets number of elements for query
    Source: https://gist.github.com/hest/8798884
    Args:
        q(BaseQuery): Base query
    Returns(int): Number or rows
    """
    count_q = q.statement.with_only_columns([func.count()]).order_by(None)
    count = q.session.execute(count_q).scalar()
    return count

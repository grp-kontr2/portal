import datetime
import logging

from flask_sqlalchemy import BaseQuery
from sqlalchemy import func

from portal import db
from portal.database.models import Client, Course, Group, Project, Role, Submission, \
    User, Worker
from portal.database import SubmissionState
from portal.database.enums import ClientType

log = logging.getLogger(__name__)


def _get_class_based_on_client_type(client_type):
    klass = Client
    if client_type is not None:
        klass = User if client_type == ClientType.USER else Worker
    return klass


def client_courses(client) -> BaseQuery:
    return Course.query.join(Course.roles).join(Role.clients).filter(Client.id == client.id)


def client_roles_in_course(client, course: Course = None) -> BaseQuery:
    return Role.query.filter_by(course=course).join(Role.clients).filter(Client.id == client.id)


def user_groups_in_course(user, course: Course, project: Project = None):
    query = Group.query.filter_by(course=course)
    if project:
        query = query.join(Group.projects) \
            .filter((Project.id == project.id) & (course.id == Project.course_id))
    return query.join(Group.users).filter(User.id == user.id)


def user_projects(user: User, course: Course = None) -> BaseQuery:
    query = Project.query.join(Project.course).join(Course.roles).join(
        Role.clients).filter(Client.id == user.id)
    if course is not None:
        query = query.filter(course=course)
    return query


def user_projects_by_course(user, course: 'Course') -> BaseQuery:
    # TODO: To query
    groups = user_groups_in_course(user, course=course).all()
    pids = []
    for group in groups:
        for p in group.projects:
            pids.append(p.id)

    return Project.query.filter(Project.id.in_(pids))


def user_projects_by_group(user: User) -> BaseQuery:
    return user_projects(user).join(Course.groups).join(Group.users) \
        .filter(Group.users.contains(user))


def user_avail_submissions(user: User) -> BaseQuery:
    return Submission.query.join(Submission.course).join(Course.roles) \
        .join(Role.clients).filter(Client.id == user.id)


def user_avail_submissions_by_group(user: User) -> BaseQuery:
    return user_avail_submissions(user).join(Course.groups) \
        .filter(Group.users.contains(user))


def course_clients_by_role(course, role, klass=Client) -> BaseQuery:
    return klass.query.join(klass.roles) \
        .filter((Role.id == role.id) & (Role.course == course)).all()


def course_clients_filtered(course, groups=None, roles=None, client_type=None) -> BaseQuery:
    klass = _get_class_based_on_client_type(client_type=client_type)
    query = klass.query.join(klass.roles)
    if client_type is not None:
        query = query.filter(klass.type == client_type)
    query = query.filter(Role.course_id == course.id)
    if roles:
        role_ids = [role.id for role in roles]
        query = query.filter(Role.id.in_(role_ids))
    if groups and client_type == ClientType.USER:
        group_ids = [group.id for group in groups]
        query = query.join(User.groups).filter(Group.id.in_(group_ids)).filter(
            Group.course_id == course.id)
    return query


def group_users(group: Group, role: Role = None) -> BaseQuery:
    """Gets the users in the group, if role is provided,
    it will be selected also by a role
    Args:
        group(Group): Group
        role(Role): Role

    Returns(BaseQuery): Query to get the result
    """
    query = User.query.join(User.groups).filter(Group.id == group.id)
    if role:
        query = query.join(User.roles).filter(Role.id == role.id)
    return query


def user_by_uco(uco: int) -> BaseQuery:
    """Finds user by it's UCO
    Args:
        uco(int): Uco

    Returns(BaseQuery): Query to get the result
    """
    return User.query.filter(User.uco == uco)


def submissions_finished() -> BaseQuery:
    """Returns the collection of finished submissions that are finished
    and the project is archived.

    Returns(BaseQuery): Query to get the result
    """
    return Submission.query.join(Submission.project).filter(
        Submission.state == SubmissionState.FINISHED)


def cancelled_submissions_for_deletion(time_period: datetime.timedelta) -> BaseQuery:
    """Returns the cancelled or aborted submissions, which are older then the time_period

    For example when the time_period is one day, it will select all the submissions
    that are older then a day.

    Args:
        time_period(datetime.timedelta):

    Returns(BaseQuery): Query to get the result
    """
    delete_from = datetime.datetime.now() - time_period
    states = [SubmissionState.CANCELLED, SubmissionState.ABORTED]
    return Submission.query.filter(Submission.state.in_(states) &
                                   (Submission.created_at < delete_from))


def effective_permissions_for_course(client: Client, course: Course) -> BaseQuery:
    def _m(name):
        agg_func = _any_aggregate_by_dialect()
        return agg_func(getattr(Role, name)).label(name)

    props = [_m(prop) for prop in Role.PERMISSIONS]
    result = db.session.query(*props)\
        .filter(Role.course == course)\
        .join(Role.clients)\
        .filter(Client.id == client.id)
    return result


def effective_permissions_for_client(client: Client) -> BaseQuery:
    def _m(name):
        agg_func = _any_aggregate_by_dialect()
        return agg_func(getattr(Role, name)).label(name)

    props = [_m(prop) for prop in Role.PERMISSIONS]
    query = db.session.query(Role.course_id, *props) \
        .filter(Role.clients.contains(client)) \
        .group_by(Role.course_id)
    return query


def list_submissions_universal(user: User = None, project: Project = None,
                               states: list = None) -> BaseQuery:
    query: BaseQuery = Submission.query

    if user:
        query = query.filter(Submission.user == user)

    if project:
        query = query.filter(Submission.project == project)

    if states:
        query = query.filter(Submission.state.in_(states))

    return query


def list_submissions_for_user(client: User = None, user_ids=None,
                              course_id=None, role_ids=None,
                              group_ids=None, project_ids=None,
                              states=None, archived=False) -> BaseQuery:
    query: BaseQuery = Submission.query

    if user_ids:
        user_filter = (User.id.in_(user_ids)) | (User.codename.in_(user_ids))
        query = query.join(Submission.user).filter(user_filter)

    if not archived:
        query = query.filter(Submission.state != SubmissionState.ARCHIVED)

    if states:
        query = query.filter(Submission.state.in_(states))

    if course_id:
        query = _filter_by_course_groups_roles(query, course_id, group_ids, project_ids, role_ids)

    if client is not None and not client.is_admin:
        query = _filter_by_client(query, client)

    log.trace(f"[QUERY] find submissions: {query}")
    return query


def _filter_by_course_groups_roles(query, course_id, group_ids, project_ids, role_ids):
    course_filter = (Course.id == course_id) | (Course.codename == course_id)
    query = query.join(Submission.project).join(Project.course).filter(course_filter)

    if project_ids:
        project_filter = (Project.id.in_(project_ids) | (Project.codename.in_(project_ids)))
        query = query.filter(project_filter)

    if role_ids:
        role_filter = (Role.id.in_(role_ids) | (Role.codename.in_(role_ids)))
        query = query.join(Course.roles).filter(role_filter)

    if group_ids:
        group_filter = (Group.id.in_(group_ids) | (Group.codename.in_(group_ids)))
        query = query.join(Course.groups).filter(group_filter)

    return query


def _filter_by_client(query: BaseQuery, client: User) -> BaseQuery:
    eff_client = effective_permissions_for_client(client).subquery()
    check_course_id = eff_client.c.course_id == Project.course_id
    from sqlalchemy import true
    check_read_all = (check_course_id & (eff_client.c.read_submissions_all == true()))
    check_read_own = (check_course_id & (eff_client.c.read_submissions_own == true()) &
                      (Submission.user_id == client.id))
    query = query.join(Submission.project) \
        .join(eff_client, eff_client.c.course_id == Project.course_id) \
        .filter(check_read_all | check_read_own)
    return query


def _any_aggregate_by_dialect():
    dialect = db.session.bind.dialect.name
    return func.max if dialect == 'sqlite' else func.bool_or
"""
A collection of Mixins specifying common behaviour and attributes of database entities.
"""
from typing import Dict, List

from sqlalchemy.ext.hybrid import hybrid_property

from portal import db
# maybe use server_default, server_onupdate instead of default, onupdate; crashes tests
# (https://stackoverflow.com/questions/13370317/sqlalchemy-default-datetime)
# pylint: disable=too-few-public-methods
from portal.tools.meta import bound_update_class_var
from portal.tools.naming import sanitize_code_name


def _repr(instance) -> str:
    """Repr helper function
    Args:
        instance: Instance of the model

    Returns(str): String representation of the model
    """
    if isinstance(instance, EntityBase):
        params = {key: getattr(instance, key) for key in instance.base_params()}
        return str(params)
    return instance.__dict__


def _str(instance) -> str:
    if hasattr(instance, 'log_name'):
        return instance.log_name
    return _repr(instance=instance)


class MetaModelBase:
    BASE_PARAMS = []
    LISTABLE = []
    UPDATABLE = []

    @classmethod
    def base_params(cls) -> List[str]:
        return bound_update_class_var(cls, 'BASE_PARAMS')

    @classmethod
    def updatable_params(cls) -> List[str]:
        return bound_update_class_var(cls, 'UPDATABLE')

    @classmethod
    def listable_params(cls) -> List[str]:
        return bound_update_class_var(cls, 'LISTABLE')

    def __repr__(self):
        return _repr(self)

    def __str__(self):
        return _str(self)

    def __getitem__(self, item):
        return getattr(self, item)

    @property
    def log_name(self) -> str:
        name = ""
        if hasattr(self, 'namespace'):
            name = f"[{self.namespace}]"
        if hasattr(self, 'id') and self.id is not None:
            name = f"{name} ({self.id})"
        if name == "":
            return str(self.serialize)
        return name

    @property
    def serialize(self) -> Dict:
        """Return object data in easily serializeable format"""
        serialized_obj = {}
        if not hasattr(self, '__table__'):
            return self.__dict__
        for column in self.__table__.columns:
            serialized_obj[column.key] = self[column.key]
        return serialized_obj


class EntityBase(MetaModelBase):
    """Entity mixin for the models

    Class attributes:
        created_at(Column): Date when the entity has been created
        updated_at(Column): Date when the entity has been updated
    """
    BASE_PARAMS = ['created_at', 'updated_at']
    LISTABLE = ['created_at', 'updated_at']
    UPDATABLE = []

    created_at = db.Column(db.TIMESTAMP, default=db.func.now())
    updated_at = db.Column(db.TIMESTAMP, default=db.func.now(), onupdate=db.func.now())


class CodeNameMixin:
    LISTABLE = ['codename', 'namespace']
    UPDATABLE = ['codename']
    BASE_PARAMS = ['namespace', *UPDATABLE]

    _codename = db.Column('codename', db.String(30), nullable=False)

    @hybrid_property
    def namespace(self) -> str:
        return self.codename

    @hybrid_property
    def codename(self) -> str:
        return self._codename

    @codename.setter
    def codename(self, value: str):
        """Sets codename of the entity only the value is provided

        Codename setter will set codename only if value is provided.
        It will also set the name to the value, if the name is empty (None)
        Codename has to be sanitized - it means that it is slugyfied and shortened

        Args:
            value: Codename of the entity
        """
        if value is None:
            return
        self._codename = sanitize_code_name(value)
        if hasattr(self, '_name') and self._name is None:
            self._name = self._codename


class NamedMixin(CodeNameMixin):
    LISTABLE = ['name', 'codename', 'namespace']
    UPDATABLE = ['description', 'name', 'codename']
    BASE_PARAMS = ['namespace', *UPDATABLE]

    _name = db.Column('name', db.String(50), nullable=False)
    description = db.Column(db.Text)

    @hybrid_property
    def name(self) -> str:
        """Gets name
        Returns (str): Name of the entity

        """
        return self._name

    @name.setter
    def name(self, value):
        """Sets the name of the entity
        Args:
            value(str): Name of the entity
        """
        if self._codename is None:
            self.codename = value
        self._name = value

# pylint: enable=too-few-public-methods

import logging
import os
import os.path
import shutil
import zipfile
from pathlib import Path
from typing import Union

log = logging.getLogger(__name__)

AnyPath = Union[str, Path]


def is_forward_path(root: AnyPath, path: AnyPath):
    root = Path(root)
    path = Path(path)
    abs_path = os.path.abspath(root / path)
    abs_path = Path(abs_path)
    try:
        relative = abs_path.relative_to(root)
        relative_str = str(relative)
        return not relative_str.startswith("../")
    except ValueError:
        return False


def zipdir(zip_file_path: AnyPath, src_dir: AnyPath):
    """Zips the directory to the zip file
    Args:
        zip_file_path(Path,str): Zip file path
        src_dir(Path,str): Source directory
    """
    src_dir = str(src_dir)
    log.info(f"[ZIP] Creating archive: {zip_file_path} from {src_dir}")
    with zipfile.ZipFile(zip_file_path, "w",
                         compression=zipfile.ZIP_DEFLATED) as zf:
        base_path = os.path.normpath(src_dir)
        for dirpath, dirnames, filenames in os.walk(src_dir):
            for name in sorted(dirnames):
                path = os.path.normpath(os.path.join(dirpath, name))
                zf.write(path, os.path.relpath(path, base_path))
            for name in filenames:
                path = os.path.normpath(os.path.join(dirpath, name))
                if os.path.isfile(path):
                    zf.write(path, os.path.relpath(path, base_path))


def unzipdir(source_zip_file: AnyPath, destination_directory: AnyPath) -> Path:
    """Unzip the directory
    Args:
        source_zip_file(str, Path):
        destination_directory(Path):

    Returns(Path): to to outout directory
    """
    handle = zipfile.ZipFile(source_zip_file, 'r')
    log.info(f"[ZIP] Extract all: {source_zip_file} -> {destination_directory}")
    handle.extractall(path=destination_directory)
    handle.close()
    return destination_directory


def delete_dir(directory: Path):
    """Deletes directory

    Args:
        directory(Path): Path to the dir
    """
    directory = str(directory)
    log.debug(f"[DEL] Deleting dir: {directory}")
    shutil.rmtree(directory)


def create_dir(directory: Path):
    """Creates a directory

    Args:
        directory: Path to the dir
    """
    if not directory.exists():
        log.debug(f"[MKD] Create dir: {directory}")
        directory.mkdir(parents=True)


def copy_and_overwrite(src: Path, dst: Path):
    """Copies and overrides the files in the directory

    Args:
        src(Path): Source dir
        dst(Path): Destination dir
    """
    if dst.exists():
        shutil.rmtree(str(dst))
    shutil.copytree(str(src), str(dst))


def clone_file(file: Path, dst: Path, src: Path):
    """Clones the file (full path), to the destination dir, src dir is for
    a relative path

    Args:
        file(Path): File path
        dst(Path): Destination path
        src(Path): Base path to which file path is relative to
    """
    rel_path = file.relative_to(src)
    final_path = dst / rel_path
    create_dir(final_path.parent)
    if file.is_dir():
        log.trace(f"[CPD] Copy dir: {file} -> {final_path}")
        copy_and_overwrite(src=file, dst=final_path)
    else:
        log.trace(f"[CPD] Copy file: {file} -> {final_path}")
        shutil.copy2(src=str(file), dst=str(final_path))


def clone_files(src: AnyPath, dst: AnyPath, pattern: str):
    """Clone the files
    Args:
        src(Path): Source directory
        dst(Path): Destination directory
        pattern(str): Patter to be globed

    """
    log.debug(f"[CLN] Cloning files [{pattern}] : {src} -> {dst}")
    src = Path(src)
    remove_git_dir(src)
    dst = Path(dst)
    files = src.rglob(pattern=pattern)
    for fpath in files:
        clone_file(fpath, dst=dst, src=src)


def remove_git_dir(src: Path):
    git_path = src / '.git'
    if git_path.exists():
        delete_dir(git_path)


def dict_dig(dictionary: dict, path: list):
    for item in path:
        if item not in dictionary:
            return None
        dictionary = dictionary[item]
        if not isinstance(dictionary, dict):
            return dictionary
    return dictionary


def get_directory_structure(basedir: AnyPath) -> dict:
    """Creates a nested dictionary that represents the folder structure of rootdir

    Args:
        basedir(str, path): Base directory from which to start walking

    Returns(dict): Dictionary representing the file structure
    """
    dir_content = {}
    basedir = str(basedir)
    basedir = basedir.rstrip(os.sep)

    current = None

    for root, dirs, files in os.walk(basedir):
        rel_path = os.path.relpath(root, basedir)
        rel_path = os.path.normpath(rel_path)
        if root == basedir:
            current = dir_content
        else:
            split_path = rel_path.split(os.sep)
            current = dict_dig(dir_content, split_path)

        for f_name in files:
            current[f_name] = os.path.join(rel_path, f_name)

        for d_name in dirs:
            current[d_name] = {}

    return dir_content

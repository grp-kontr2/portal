from pathlib import Path

import logging

import os

from portal.storage.errors import NotFoundError
from portal.storage.utils import unique_name, delete_dir, unzipdir
from portal.storage.utils.fs import clone_file

log = logging.getLogger(__name__)


def update_dir(src: Path, dst: Path):
    """Updates directory content

    Args:
        src(Path): Source dir
        dst(Path): Destination dir
    """
    log.debug(f"[UPD] Dir update: {src} -> {dst}")
    for root, dirs, files in os.walk(str(src)):
        for file in files:
            src_path = Path(os.path.join(root, file))
            clone_file(file=src_path, src=src, dst=dst)


def entity_update(src: Path, dst: Path):
    if not src.exists():
        raise NotFoundError(dst)
    if src.is_dir():
        __update_dir(src, dst)

    if src.is_file() and src.suffix == 'zip':
        __update_zip(src, dst)


def __update_dir(src: Path, dst: Path):
    log.debug(f"[UPD] Updating {src} -> {dst}")
    update_dir(src=src, dst=dst)


def __update_zip(src: Path, dst: Path):
    tmp_dir = dst / unique_name(src.name)
    log.debug(f"[UPD] Create tmpdir: {tmp_dir}")
    tmp_dir.mkdir()
    log.info(f"[UPD] Unziping to tmpdir: {src} -> {tmp_dir}")
    unzipdir(src, destination_directory=tmp_dir)
    __update_dir(src=tmp_dir)
    delete_dir(tmp_dir)

class KontrStorageError(RuntimeError):
    pass


class NotFoundError(KontrStorageError):
    pass


class InvalidPath(KontrStorageError):
    pass
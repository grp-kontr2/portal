import logging
import shutil
from pathlib import Path
from typing import Union

from portal.storage import errors, utils
from portal.storage.inputs import filter_entity, git_clone
from portal.storage.inputs.zip import unzip_entity
from portal.storage.utils import delete_dir, get_directory_structure
from portal.storage.utils.compress import compress_entity, decompress_entity
from portal.storage.utils.update import entity_update

log = logging.getLogger(__name__)


class Entity:
    def __init__(self, base_dir: Union[Path, str], dirname: str = None):
        """Creates a storage entity

        Args:
            dirname(str): Name of the directory for entity files
            base_dir(Path, str): Base dir for the entity
        """
        self._base_dir = Path(base_dir)
        self._dir_name = dirname

    @property
    def entity_id(self) -> str:
        """Gets an entity id

        Returns(str): Entity id
        """
        return self.dir_name

    @property
    def base_dir(self) -> Path:
        """Gets an base dir where the all entities are being created

        Returns(Path): Base dir of all entities
        """
        return self._base_dir

    @property
    def dir_name(self) -> str:
        return self._dir_name

    @property
    def path(self) -> Path:
        """Gets a path to the entity

        Returns(Path): Entity id
        """
        full_path = self.base_dir / self.dir_name
        return full_path

    @property
    def zip_path(self) -> Path:
        """Gets a path to the zip file of the entity

        Returns(Path): Zip file path
        """
        return self.base_dir / f"{self.dir_name}.zip"

    def get(self, path_query: str):
        """Gets file based on relative path query
        Examples:
            path_query = "src/main.c"
            root = "/storage/submission"
            Result: "/storage/submission/src/main.c"
        Args:
            path_query(str): Path query
        Returns(Path): Absolute path to requested file

        """
        path_query = Path(path_query)
        if not utils.is_forward_path(self.path, path_query):
            log.error(f"Not a valid path: {path_query} for: {self.path}/{path_query}")
            raise errors.InvalidPath(f"Not a valid path: {path_query} "
                                     f"for: {self.path}/{path_query}")
        path = (self.path / path_query)
        abs_path = path.absolute()
        if not abs_path.exists():
            log.error(f"Path {abs_path} not exists!")
            raise errors.NotFoundError(f"Path {abs_path} not exists!")
        return path

    def subdir(self, subdir: str) -> Path:
        """Gets an subdirectory from the path

        Args:
            subdir(str): Subdirectory

        Returns(Path): Full path

        """
        return self.path / subdir

    def compress(self) -> Path:
        """Compresses the entity

        Returns(Path): Path to the zip file
        """
        return compress_entity(self)

    def decompress(self) -> Path:
        """Decompress the entity

        Returns(Path): The entity directory path
        """
        return decompress_entity(self)

    def clean(self):
        self.compress()
        shutil.rmtree(str(self.path))

    def update_subdir(self, source: Union[str, Path], subdir: str) -> Path:
        """Updates subdirectory of the entity with files
        Args:
            source(str, Path): Source subdir
            subdir(str): Subdir

        Returns(Path):

        """
        destination = self.subdir(subdir=subdir)
        entity_update(src=source, dst=destination)
        return destination

    def content(self, file_path: Union[Path, str], subdir='') -> str:
        """Gets an content of the file
        Args:
            file_path(Path,str): Path to the file
            subdir(str): Subdirectory

        Returns(str): String content of the file
        """
        full_path = self.subdir(subdir=subdir) / file_path
        with full_path.open('r') as content_file:
            return content_file.read()

    def tree(self, subdir='') -> dict:
        """Gets a tree representation of file structure of the submission

        Args:
            subdir(str): From which subdir

        Returns(dict):
        """
        return get_directory_structure(self.subdir(subdir))

    def glob(self, pattern: str, subdir=''):
        """Find files in the entity path

        Args:
            pattern(str): Pattern to find in the directory
            subdir(str): Subdirectory

        Returns: Iterable

        """
        full = self.subdir(subdir=subdir)
        return full.glob(pattern=pattern)

    def delete(self):
        if self.zip_path.exists():
            self.zip_path.unlink()
        if self.path.exists():
            delete_dir(self.path)


class TestFiles(Entity):
    pass


class Submission(Entity):
    pass


class Results(Entity):
    pass


class UploadedEntity:
    """Working entity - upload process
    """

    def __init__(self, entity: Entity, workspace: Path, custom_ssh_key=None, **kwargs):
        self._entity = entity
        self._params = {**kwargs}
        self._custom_ssh_key = custom_ssh_key
        self._workspace = workspace

    @property
    def entity(self) -> Entity:
        return self._entity

    @property
    def workspace(self) -> Path:
        return self._workspace

    @property
    def path(self) -> Path:
        return self.entity.path

    @property
    def path(self) -> Path:
        return self.entity.path

    @property
    def type(self) -> str:
        return self.config.get('type')

    @property
    def filters(self) -> str:
        return self._params.get('whitelist') or '*'

    @property
    def from_dir(self) -> Path:
        return self._params.get('from_dir') or ''

    @property
    def config(self) -> dict:
        return self._params.get('source')

    @property
    def version(self) -> str:
        return self._params.get('version')

    @version.setter
    def version(self, value: str):
        self._params['version'] = value

    @property
    def message(self) -> str:
        return self._params.get('message')

    @message.setter
    def message(self, value: str):
        self._params['message'] = value

    @property
    def author(self) -> str:
        return self._params.get('author')

    @author.setter
    def author(self, value: str):
        self._params['author'] = value

    @property
    def additional(self) -> str:
        return self._params.get('additional')

    @additional.setter
    def additional(self, value):
        self._params['additional'] = value

    def download(self):
        if self.type == 'git':
            return git_clone(self, custom_ssh_key=self._custom_ssh_key)
        elif self.type == 'zip':
            return unzip_entity(self)
        else:
            return None

    def filter(self):
        return filter_entity(self)

    def clean(self):
        if self.workspace.exists():
            return delete_dir(self.workspace)

    def process(self) -> 'UploadedEntity':
        self.download()
        self.filter()
        self.clean()
        self.entity.compress()
        return self


class UploadedSubmission(UploadedEntity):
    pass


class UploadedResults(UploadedEntity):
    pass


class UploadedTestFiles(UploadedEntity):
    def filter(self):
        included = ['kontr2', 'kontr_tests', 'tests']
        return filter_entity(self, included=included)

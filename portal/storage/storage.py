import logging
from pathlib import Path
from typing import Dict

from portal.storage import entities
from portal.storage.utils import copy_and_overwrite, unique_name

log = logging.getLogger(__name__)


class AbstractStoragePart:
    def __init__(self, storage: 'Storage', klass=None, upload_class=None):
        """Creates instance of the Abstract storage Factory
        Args:
            storage(Storage):
        """
        self.storage = storage
        self._entity_class = klass
        self._upload_class = upload_class

    @property
    def path(self) -> Path:
        """Path to the base directory

        Returns(Path): Instance of the Path to the base dir
        """
        return Path()

    @property
    def config(self) -> dict:
        """Gets an config

        Returns(dict): Gets an config
        """
        return self.storage.config

    @property
    def workspace_path(self) -> Path:
        """Workspace path

        Returns (Path): Workspace path
        """
        return Path(self.config.get('workspace_dir'))

    @property
    def custom_ssh_key(self) -> str:
        return self.config.get('custom_ssh_key')

    def get(self, dirname: str) -> entities.Entity:
        """Gets an instance of the entity

        Args:
            dirname(str): Dirname of the entity

        Returns(Entity):

        """
        log.debug(f"[GET] {self._entity_class.__name__}: {dirname}")
        return self._entity_class(dirname=dirname, base_dir=self.path)

    def create(self, dirname: str, **kwargs) -> entities.UploadedEntity:
        """Creates an instance of the entity

        Args:
            dirname(str): UUID of the entity
            **kwargs:

        Returns(Entity):
        """
        log.info(f"[CREATE] {self._entity_class.__name__} [{dirname}]: {kwargs}")
        upload = self._create_upload(dirname, **kwargs)
        return upload.process()

    def clone(self, dirname_from: str, dirname_to: str) -> entities.Entity:
        log.info(f"[CLONE] {self._entity_class.__name__}: {dirname_from} -> {dirname_to}")
        from_instance = self.get(dirname_from)
        to_instance = self.get(dirname=dirname_to)
        copy_and_overwrite(src=from_instance.subdir('files'), dst=to_instance.subdir('files'))
        return to_instance

    def _create_upload(self, dirname, **kwargs) -> entities.UploadedEntity:
        log.info(f"[UPLOAD] {self._entity_class.__name__} [{dirname}]: {kwargs}")
        entity = self.get(dirname=dirname)
        uniq = unique_name(entity.dir_name)
        workspace = self.workspace_path / uniq
        return self._upload_class(entity=entity, workspace=workspace,
                                  custom_ssh_key=self.custom_ssh_key,
                                  **kwargs)


class SubmissionStorage(AbstractStoragePart):
    @property
    def path(self) -> Path:
        """Returns the instance of the submission files
        Returns(Path): test path files
        """
        return Path(self.config.get('submissions_dir'))


class TestFilesStorage(AbstractStoragePart):
    @property
    def path(self) -> Path:
        """Returns the instance of the path of the test files
        Returns(Path): test path files
        """
        return Path(self.config.get('test_files_dir'))

    def update(self, dirname: str, **kwargs):
        """Updates content of the test files dir - clean and create
        Args:
            dirname:
            **kwargs:

        Returns:

        """
        log.info(f"[UPDATE] {self._entity_class.__name__} [{dirname}]: {kwargs}")
        test_files = self.get(dirname=dirname)
        if test_files:
            test_files.delete()
        return self.create(dirname=dirname, **kwargs)


class ResultsStorage(AbstractStoragePart):
    @property
    def path(self) -> Path:
        """Returns the path to the results
        Returns(Path): results path
        """
        return Path(self.config.get('results_dir'))


class Storage:
    def init_storage(self, **config):
        """Initializes the storage with configuration
        Args:
            config(dict): Storage config
        Configuration:
            test_files_dir: Directory where the test files should be stored
            submissions_dir: Directory where submission files should be stored
            results_dir: Directory where submission results should be stored
            workspace_dir: Temporary workspace
            custom_ssh_key: Optional - custom ssh key file
        """
        self._config = config

    def __init__(self, **config):
        """Creates an instance of the storage

        Instance contains the configuration

        Args:
            test_files_dir(str, Path): Directory where the test files should be stored
            submissions_dir(str, Path): Directory where submission files should be stored
            workspace_dir(str, Path): Temporary workspace
        Configuration:

        """
        self._config = config
        self.__submissions = SubmissionStorage(self, klass=entities.Submission, upload_class=entities.UploadedSubmission)
        self.__test_files = TestFilesStorage(self, klass=entities.TestFiles, upload_class=entities.UploadedTestFiles)
        self.__results = ResultsStorage(self, klass=entities.Results, upload_class=entities.UploadedResults)

    @property
    def config(self) -> Dict:
        """Gets a instance of the configuration

        Returns(dict): The configuration instance
        """
        return self._config

    @property
    def submissions(self) -> SubmissionStorage:
        """Returns the submissions factory instance

        Returns(SubmissionStorage): Submissions factory instance
        """
        return self.__submissions

    @property
    def test_files(self) -> TestFilesStorage:
        """Returns the test files factory instance

        Returns(TestFilesStorage): Tests files factory instance
        """
        return self.__test_files

    @property
    def results(self) -> ResultsStorage:
        """Returns the results factory instance
        Returns(ResultsStorage): Results storage factory instance

        """
        return self.__results

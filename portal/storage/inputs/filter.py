import logging

from portal.storage.utils import clone_files

log = logging.getLogger(__name__)


def filter_entity(entity, included=None):
    filters = entity.filters.splitlines()
    if included is not None:
        filters += included
    entity.path.mkdir(parents=True)
    from_path = entity.workspace / entity.from_dir
    for pattern in filters:
        log.info(f"[FLT] Filtering using filter for {pattern}")
        clone_files(src=from_path, dst=entity.path, pattern=pattern)
    return entity


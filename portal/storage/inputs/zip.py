import logging

from portal.storage.utils import fs

BUF_SIZE = 65536  # lets read stuff in 64kb chunks!

log = logging.getLogger(__name__)


def calculate_hash(zip_file):
    import hashlib
    sha1 = hashlib.sha1()
    with open(zip_file, 'rb') as hashing:
        while True:
            data = hashing.read(BUF_SIZE)
            if not data:
                break
            sha1.update(data)
    digest = sha1.hexdigest()
    log.info(f"[HASH] SHA1 digest {zip_file}: {digest}")
    return digest


def unzip_entity(uploaded_entity):
    zip_file = uploaded_entity.config['url']
    to_dir = uploaded_entity.workspace
    digest = calculate_hash(zip_file)
    uploaded_entity.version = digest
    log.info(f"[UNZIP] Unziping: {zip_file} -> {to_dir}")

    return fs.unzipdir(zip_file, to_dir)

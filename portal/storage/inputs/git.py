import logging
import os
from pathlib import Path

from git import Repo

log = logging.getLogger(__name__)


def git_clone(entity, **kwargs):
    """Clones the repository
    Args:
        entity: Entity instance
    Returns(Entity): Entity instance
    """
    return GitWrapper(entity=entity, **kwargs).clone()


class GitEntityConfig:
    def __init__(self, entity):
        self.entity = entity

    @property
    def config(self) -> dict:
        """Entity config
        Returns(dict): Entity config
        """
        return self.entity.config

    @property
    def url(self) -> str:
        """Git repo url
        Returns(str): Repository url
        """
        return self.config['url']

    @property
    def branch(self) -> str:
        """Branch name
        Returns(str): Branch name
        """
        return self.config.get('branch')

    @property
    def checkout(self) -> str:
        """Checkout (commit, branch, tag)
        Returns(std): Which version to checkout
        """
        return self.config.get('checkout')

    @property
    def effective_checkout(self) -> str:
        """Effective checkout - whether to checkout branch or tag or commit
        Returns(str): Checkout name
        """
        return self.checkout or self.branch


class GitWrapper:
    def __init__(self, entity, custom_ssh_key: str = None):
        self.entity = entity
        self.config = GitEntityConfig(self.entity)
        self._repo = None
        self._custom_ssh_key = custom_ssh_key

    @property
    def repo(self) -> Repo:
        """Gets an instance of the GitPython Repo object
        Returns(Repo): Instance of the GitPython Repo
        """
        return self._repo

    def clone(self) -> Repo:
        """Clones the repository
        Returns:

        """
        log.info(f"[GIT] Cloning from {self.config.url} -> {self.entity.workspace}")
        params = dict(
            url=self.config.url,
            to_path=self.entity.workspace
        )
        if self.config.branch:
            params['branch'] = self.config.branch

        if self._custom_ssh_key:
            ssh_command = self._create_ssh_command()
            os.environ['GIT_SSH_COMMAND'] = ssh_command
            params['env'] = dict(GIT_SSH_COMMAND=ssh_command)
        log.debug(f"[GIT] Clone: {params}")
        cloned_repo = Repo.clone_from(**params)
        self._process_cloned_repo(cloned_repo)
        self._repo = cloned_repo
        return cloned_repo

    def _process_cloned_repo(self, cloned_repo):
        digest = cloned_repo.head.commit.hexsha
        self.entity.message = cloned_repo.head.commit.message
        self.entity.author = cloned_repo.head.commit.author
        self.entity.version = digest
        self.entity.additional = cloned_repo.head
        log.debug(f"[GIT] Cloned {digest}: {cloned_repo}")

    def _create_ssh_command(self):
        ssh_key_path = str(Path(self._custom_ssh_key).absolute())
        log.debug(f"[GIT] Using custom ssh key: {ssh_key_path}")
        ssh_cmd = f"ssh -i {ssh_key_path}"
        log.debug(f"[GIT] Using custom ssh command: {ssh_cmd}")
        return ssh_cmd

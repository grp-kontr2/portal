"""
Errors in the service layer
"""
import json
import logging
from typing import Union

from portal.database import Course, Project, User
from portal.tools import time

log = logging.getLogger(__name__)


class PortalError(Exception):
    """Base exception class
    """


class PortalServiceError(PortalError):
    def __init__(self, message):
        self.message = message


class PortalConfigurationLoadError(PortalServiceError):
    """Error thrown when configuration is missing or is invalid
    """
    pass


class PortalAPIError(PortalError):
    """Portal API Error thrown when error in the API happened
    """

    def __init__(self, code: int, message: Union[str, dict]):
        """Creates instance of the API portal error
        Args:
            code(int): Status code
            message(Union[str, dict]): Message
        """
        super().__init__()
        self._code = code
        self._message = message
        log.error(f"[ERR] API ERROR[{code}]: {message}")

    @property
    def code(self) -> int:
        """Gets the status code that should be returned
        Returns(int): Status code

        """
        return self._code

    @property
    def message(self) -> Union[str, dict]:
        """Gets the error message
        Returns(Union[str, dict]): Message
        """
        return self._message

    @property
    def json(self):
        """Json serialized message
        Returns:

        """
        return json.dumps(self.message)

    def __str__(self):
        return self.__class__.__name__ + ': ' + self.json


class DataMissingError(PortalAPIError):
    """Data missing error thrown when data is missing i.e. (update)
    """

    def __init__(self, action: str, resource: str):
        """Creates data missing error instance
        Args:
            action(str): Name of the action
            resource(str): Name of the resource
        """
        message = dict(action=action, resource=resource,
                       message='Data is missing')
        super(DataMissingError, self).__init__(code=400, message=message)


class ResourceNotFoundError(PortalAPIError):
    """Resource not found error
    """

    def __init__(self, resource: str, res_id=None, note=None):
        """Creates instance of the error
        Args:
            resource(str): Name of the resource
            res_id(str): UUID of the resource
            note(str): Note
        """
        message = dict(
            resource=resource,
            id=res_id,
            message=f"Cannot find {resource} with identifier: {res_id}",
        )
        if note:
            message['note'] = note

        super().__init__(code=404, message=message)


class UnauthorizedError(PortalAPIError):
    def __init__(self, note=None):
        message = dict(message=f"You are not authorized.", )
        if note:
            message['note'] = note

        super().__init__(code=401, message=message)


class ForbiddenError(PortalAPIError):
    # could use a resource identification (like 404)
    def __init__(self, client=None, note=None):
        user_message = f"Forbidden for {client.type}: {client.id} ({client.codename})" if client else \
            'Forbidden action.'
        message = dict(uid=client.id, message=user_message)
        if note:
            message['note'] = note

        super().__init__(code=403, message=message)


class IncorrectCredentialsError(UnauthorizedError):
    def __init__(self):
        super().__init__(note="Incorrect username or password.")


class InvalidGitlabAccessTokenError(UnauthorizedError):
    def __init__(self):
        super().__init__(note='Invalid Gitlab Access token.')


class ValidationError(PortalAPIError):  # currently not used
    def __init__(self, schema, data, failures: list):
        message = dict(
            schema=schema,
            data=data,
            failures=failures,
        )

        super().__init__(code=400, message=message)


class SubmissionRefusedError(PortalAPIError):
    def __init__(self, reason):
        super().__init__(code=400, message=reason)


class WorkerNotAvailable(PortalServiceError):
    def __init__(self, message: str = None):
        self.message = message or "Worker is not available"


class SubmissionDiffTimeError(PortalAPIError):
    def __init__(self, project: Project, diff_time, user: User = None):
        log_user = f'by {user.log_name} ' if user else ''
        when = time.current_time() + diff_time
        message = f"Submission cannot be created for {project.log_name}, {log_user}" \
            f"you can create submission in {diff_time} (when)."
        full_message = {
            'text': message,
            'log_user': log_user,
            'project': project.codename,
            'course': project.course.codename,
            'author': user.username,
            'diff_time': str(diff_time),
            'when': when.strftime(time.DEFAULT_FORMAT)
        }

        super().__init__(code=429, message=full_message)


class SuiteStatsNotExists(PortalServiceError):
    def __init__(self, submission):
        super().__init__(f"Suite stats have not been found "
                         f"for the submission: {submission.log_name}")


class ResultsAreNotUploaded(PortalAPIError):
    def __init__(self, submission):
        message = f"Submission {submission.log_name} results are not ready."
        super().__init__(code=404, message=message)


class AccessTokenForCourseMissingError(PortalServiceError):
    def __init__(self, course: Course):
        message = f"Course's {course.log_name} is api access token has not been set."
        super().__init__(message=message)


class GitlabIsNotEnabledError(PortalAPIError):
    def __init__(self, message: str = None):
        message = message or f"Gitlab integration is not enabled"
        super().__init__(code=404, message=message)


class GitlabOAuthIsNotEnabledError(GitlabIsNotEnabledError):
    def __init__(self):
        message = f"Gitlab OAuth integration is not enabled"
        super().__init__(message=message)


class GitlabAuthorizationError(PortalAPIError):
    def __init__(self, message: str = None):
        message = f"You are not authorized to use gitlab, " \
            f"check your GL token: {message}"
        super().__init__(code=401, message=message)


class SchemaParseError(PortalServiceError):
    pass


def error(message, error_class=PortalServiceError):
    log.error(f"{error_class}: {message}")
    raise error_class(message)


def api_error(message, code=400, error_class=PortalAPIError):
    log.error(f"{error_class}[{code}]: {message}")
    raise error_class(message=message, code=code)

import time
from typing import Optional, Union, Dict

import flask
import yaml

from portal import logger, storage_wrapper, tools
from portal.database import Project, Submission
from portal.service import errors
from portal.service.general import GeneralService
from portal.storage import Storage, entities

log = logger.get_logger(__name__)


class StorageService(GeneralService):
    def _set_data(self, entity, data=None):
        return None

    @property
    def storage(self) -> Storage:
        return storage_wrapper

    def __call__(self, submission: Submission = None, project: Project = None):
        self._project = project
        self._submission = submission
        return self

    @property
    def storage_entity(self) -> entities.Entity:
        if self._submission is not None:
            return self._get_entity(self.submission, type='submission')
        if self._project is not None:
            return self._get_entity(self.project, type='project')

    def _get_entity(self, entity, type=None) -> Union[entities.Entity, entities.TestFiles,
                                                      entities.Results, entities.Submission]:
        OPTIONS = dict(project=self.storage.test_files,
                       submission=self.storage.submissions,
                       result=self.storage.results,
                       results=self.storage.results,
                       test_files=self.storage.test_files)

        if type is not None and type in OPTIONS.keys():
            return OPTIONS[type].get(entity.storage_dirname)

        if isinstance(entity, Project):
            return self.storage.test_files.get(entity.storage_dir_name)
        if isinstance(entity, Submission):
            return self.storage.test_files.get(entity.storage_dirname)

    @property
    def submission(self) -> Submission:
        return self._submission

    @property
    def project(self) -> Optional[Project]:
        if self._project is not None:
            return self._project
        if self.submission is not None:
            return self.submission.project
        return None

    def __init__(self):
        super().__init__()
        self._submission = None
        self._project = None

    def send_zip(self, storage_entity: entities.Submission):
        storage_entity = storage_entity or self.storage_entity
        path = storage_entity.zip_path
        klass = storage_entity.__class__.__name__
        if not path.exists():
            raise errors.PortalAPIError(400, f"Requested path does not exist: {path}")
        log.debug(f"[SEND] Sending zip file {klass} "
                  f"({self.submission.id}): {path}")
        return flask.send_file(str(path), attachment_filename=path.name)

    def send_selected_file(self, storage_entity: entities.Submission, path_query: str):
        storage_entity = storage_entity or self.storage_entity
        klass = storage_entity.__class__.__name__
        log_name = self.submission.log_name if self.submission else self.project.log_name
        log.debug(f"[SEND] Sending file {klass} {log_name}: {path_query}")
        path = storage_entity.get(path_query)
        return flask.send_file(str(path), attachment_filename=path.name)

    def send_file_or_zip(self, path: str = None, storage_entity=None):
        storage_entity = storage_entity or self.storage_entity
        if path is None:
            return self.send_zip(storage_entity)
        return self.send_selected_file(storage_entity, path)

    def send_files_tree(self, storage_entity=None):
        storage_entity = storage_entity or self.storage_entity
        tree = storage_entity.tree()
        log.debug(f"[TREE] Tree for the storage entity {storage_entity.dir_name}: {tree}")
        return tree

    def get_test_files_entity_from_storage(self, project=None):
        project = project or self.project
        if not project.config.test_files_commit_hash:
            log.warning(f"Test files are not present "
                        f"for the project {project.log_name}")
            from .projects import ProjectService
            ProjectService(project).update_project_test_files()
            self.__wait_for_test_files()
        return self._get_entity(project, type='project')

    def __wait_for_test_files(self):
        project = self.project
        wait_interval = 60
        log.debug("[WAIT] Waiting for test files to be present")
        while True:
            self.refresh(project)
            wait_interval -= 1
            if wait_interval <= 0:
                raise TimeoutError("Waiting for the test files timed out")
            log.debug(f"[WAIT] Project Files Hash: {project.config.test_files_commit_hash}")
            if project.config.test_files_commit_hash is not None:
                break
            time.sleep(1)

    def project_config(self, project: Project = None) -> Optional[Dict]:
        project = project or self.project
        project_entity: entities.TestFiles = self._get_entity(project, type='project')
        cfg_path = project.config.config_path
        config_path = project_entity.get(cfg_path)
        if config_path.exists():
            log.debug(f"[STORAGE] Project config found: {config_path}")
            return tools.load_yaml_file(config_path)

        log.warning(f"[STORAGE] Project config not found: {config_path}")
        return None

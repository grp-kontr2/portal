"""
Authentication service
"""
import logging

import flask
from flask_jwt_extended import get_jwt_identity

from portal.database.models import Client
from portal.logger import AUTH, LOGIN
from portal.service import errors
from portal.service.find import FindService
from portal.service.gitlab import GitlabService
from portal.tools import request_helpers

log = logging.getLogger(__name__)


def log_auth(message, level='info', logger=AUTH):
    req = flask.request
    message = f"[AUTH] {message}  | IP: {request_helpers.get_ip()}, UA: {req.user_agent}"
    method = getattr(logger, level)
    method(message)


def log_auth_w(message):
    log_auth(message=message, level='warning')


class AuthService:
    def __init__(self):
        self._find = FindService()

    @property
    def find(self) -> FindService:
        return self._find

    @property
    def client(self) -> Client:
        return self.find_client()

    def login_client(self, **data) -> Client:
        if not data.get('type'):
            raise errors.PortalAPIError(400, message="Missing login type.")

        login_type = data['type']
        types = dict(gitlab=self.login_gitlab,
                     username_password=self.login_username_password,
                     secret=self.login_secret)

        if login_type not in types.keys():
            raise errors.PortalAPIError(400, message="Invalid login type.")
        identifier = data.get('identifier', None)
        secret = data.get('secret', None)
        req = flask.request
        LOGIN.info(f"[LOGIN] Login using \"{login_type}\", "
                   f"client: \"{identifier}\", "
                   f"IP: \"{request_helpers.get_ip()}\", "
                   f"UA: \"{req.user_agent}\"")
        return types[login_type](identifier, secret)

    def login_gitlab(self, identifier: str, secret: str) -> Client:
        """Verifies GitLab access token and username combination.

        Args:
            identifier(str): username of the user attempting to log in
            secret(str): access token string from gitlab

        Returns(User): the authenticated user
        """
        if secret is None:
            log_auth_w(f"Gitlab: No access token for {identifier}")
            raise errors.PortalAPIError(400, 'No gitlab access token found.')

        self.validate_gitlab_token(secret, username=identifier)

        user = self.find.user(identifier, throws=False)
        if user is None:
            log_auth_w(f"Gitlab: Invalid access token for {identifier}")
            raise errors.InvalidGitlabAccessTokenError()
        return user

    def login_username_password(self, identifier: str, secret: str) -> Client:
        """Verifies user login credentials.
        Args:
            identifier(str): username to log in
            secret(str): user's password

        Returns(Client): the authenticated user

        """
        user = self.find.user(identifier, throws=False)
        if user is None or secret is None:
            log_auth_w(f"Login: Invalid user or secret for {identifier}")
            raise errors.IncorrectCredentialsError()

        if user.verify_password(password=secret):
            log_auth(f"Login successful with password for {identifier}: {user.log_name}")
            return user
        log_auth_w(f"Login: Invalid credentials for {identifier}")
        raise errors.IncorrectCredentialsError()

    def login_secret(self, identifier: str, secret: str) -> Client:
        """Verifies the provided Client secret.

        Args:
            identifier: identifier of the Client to log in
            secret: the provided secret

        Returns:

        """
        client = self._find_client_helper(identifier)
        if client.verify_secret(secret):
            log_auth(f"Login successful with secret for {identifier}: {client.log_name}")
            return client
        log_auth_w(f"Login: Invalid credentials for {identifier}")
        raise errors.UnauthorizedError(f"[LOGIN] Invalid secret.")

    def validate_gitlab_token(self, token: str, username: str, throws: bool = True):
        """Validates gitlab access token using the gitlab client
        Args:
            token(str): Gitlab access token
            username(str): Username
            throws(bool): Throws an exception if the token is not valid
        Returns(Bool):
        """
        user = GitlabService(token=token, token_type='oauth').current_user
        if not user or user.username != username:
            if throws:
                log_auth_w(f"Login: gitlab authorization failed for - {username}")
                raise errors.InvalidGitlabAccessTokenError()
            return False
        log_auth(f"Login: gitlab authorization success - {username}")
        return True

    def find_client(self, throw=True) -> Client:
        identifier = get_jwt_identity()
        return self._find_client_helper(identifier, throw=throw)

    def _find_client_helper(self, identifier: str, throw=True) -> Client:
        log.trace(f"[FIND] Finding client using identifier: {identifier}")
        client = self.find.client(identifier, throws=False)
        if not client and throw:
            raise errors.UnauthorizedError(f"[LOGIN] Unknown client identifier {identifier}.")
        return client

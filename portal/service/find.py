from typing import Union

from portal import logger
from portal.database import Course, Group, Project, Role, Submission, User, Worker
from portal.database.models import Client, ReviewItem, Secret
from portal.database.enums import ClientType
from portal.service import errors
from portal.tools.naming import sanitize_code_name

log = logger.get_logger(__name__)


class FindService:
    def client_owner(self, client: Client) -> Union[User, Worker]:
        if client.type == ClientType.USER:
            return self.user(client.id)
        if client.type == ClientType.WORKER:
            return self.worker(client.id)
        raise errors.UnauthorizedError(f"[LOGIN] Unknown client type {client.type}.")

    def resource(self, identifier: str, resource: str, query, throws: bool = True):
        """Gets an instance of the resource

           Args:
               identifier(str): ID or some other identifier of the resource
               resource(str): Resource name
               query: SQLAlchemy query
               throws(bool): True if an exception should be thrown if no instance is found

           Returns: An entity instance or null if none is found

        """
        instance = None
        if identifier is not None and query is not None:
            instance = query.first()

        if not instance:
            log.debug(f"[FIND] Not found {resource.capitalize()}: {identifier}")
            if throws:
                raise errors.ResourceNotFoundError(resource=resource, res_id=identifier)
            else:
                return None
        log.trace(
            f"[FIND] Found {resource.capitalize()} for identifier [{identifier}]: {instance}")
        return instance

    def group(self, course: Union[Course, str], identifier: str, throws=True) -> Group:
        """Gets a Group instance

        Args:
            course(Course): Course instance
            identifier(str): Id of the resource
            throws(bool): Throws an exception if true

        Returns(Group): Group entity instance
        """
        query = Group.query.filter(
            ((Group.id == identifier) | (Group.codename == sanitize_code_name(identifier)))
            & (Group.course == course)
        )

        return self.resource(
            resource='group',
            identifier=identifier,
            query=query,
            throws=throws
        )

    def worker(self, identifier: str, throws=True) -> Worker:
        """Gets a Component instance

        Args:
            identifier(str): Id of the component
            throws(bool): Throws an exception if true

        Returns(Component): Component entity instance
        """
        query = Worker.query.filter(
            (Worker.id == identifier) | (Worker.name == sanitize_code_name(identifier))
        )
        return self.resource(
            resource='worker',
            identifier=identifier,
            query=query,
            throws=throws
        )

    def course(self, identifier: str, throws=True) -> Course:
        """Gets a Course instance

        Args:
            identifier(str): Id of the resource
            throws(bool): Throws an exception if true

        Returns(Course): Course entity instance
        """
        query = Course.query.filter((Course.id == identifier) | (Course.codename == identifier))

        return self.resource(
            resource='course',
            identifier=identifier,
            query=query,
            throws=throws
        )

    def project(self, course, identifier: str, throws=True) -> Project:
        """Gets a Project instance

        Args:
            course(Course): Course instance
            identifier(str): Id of the resource
            throws(bool): Throws an exception if true

        Returns(Project): Project entity instance
        """
        query = Project.query.filter(
            ((Project.id == identifier) | (Project.codename == sanitize_code_name(identifier))) &
            (Project.course == course)
        )

        return self.resource(
            resource='project',
            identifier=identifier,
            query=query,
            throws=throws
        )

    def secret(self, client: Union[str, Client, User, Worker], identifier, throws=True) -> Secret:
        query = self._get_client(Secret.query, client)
        query = query.filter(
            (Secret.id == identifier) | (Secret.name == sanitize_code_name(identifier))
        )

        return self.resource(
            resource='secret',
            identifier=identifier,
            query=query,
            throws=throws
        )

    def role(self, course: Course, identifier: str, throws=True) -> Role:
        """Gets a Role instance

        Args:
            course(Course): Course instance
            identifier(str): Id of the resource
            throws(bool): Throws an exception if true

        Returns(Role): Role entity instance
        """
        query = Role.query.filter(
            (Role.id == identifier) | (Role.codename == sanitize_code_name(identifier))
            & (Role.course == course)
        )

        return self.resource(
            resource='role',
            identifier=identifier,
            query=query,
            throws=throws
        )

    def submission(self, identifier: str, throws=True) -> Submission:
        """Gets a Submission instance

        Args:
            identifier(str): Id of the resource
            throws(bool): Throws an exception if true

        Returns(Submission): Submission entity instance
        """
        return self.resource(
            resource='submission',
            identifier=identifier,
            query=Submission.query.filter_by(id=identifier),
            throws=throws
        )

    def review_item(self, identifier: str, throws=True) -> ReviewItem:
        return self.resource(
            resource='review_item',
            identifier=identifier,
            query=ReviewItem.query.filter_by(id=identifier),
            throws=throws
        )

    def user(self, identifier: str, throws=True) -> User:
        """Gets a User instance

        Args:
            identifier(str): Id of the resource
            throws(bool): Throws an exception if true

        Returns(User): User entity instance
        """
        query = User.query.filter(
            ((User.id == identifier) | (User.username == identifier) | (User.email == identifier))
        )

        return self.resource(
            resource='user',
            identifier=identifier,
            query=query,
            throws=throws
        )

    def client(self, identifier: str, throws=False, client_type=None) -> Client:
        """Gets a Client instance

            Args:
                client_type(str): Client type can be user or worker
                identifier(str): Id of the resource
                throws(bool): Throws an exception if true

            Returns(Client): User entity instance
            """
        register = dict(user=User, client=Client, worker=Worker)
        klass = register.get(client_type) or Client
        query = klass.query.filter((klass.id == identifier) | (klass.codename == identifier))

        return self.resource(
            resource=client_type or 'client',
            identifier=identifier,
            query=query,
            throws=throws
        )

    def _get_client(self, query, client: Union[Client, str]):
        if isinstance(client, str):
            query = query.filter((Client.id == client) | (Client.codename == client))
        else:
            query = query.filter_by(client=client)
        return query

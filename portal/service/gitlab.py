import logging
from typing import List, Optional
from urllib.parse import urlparse

import gitlab
from gitlab.v4 import objects

from portal import tools
from portal.database import Submission, models
from portal.service import errors
from portal.service.general import GeneralService

log = logging.getLogger(__name__)


class GitlabService(GeneralService):
    def __init__(self, token: str = None, token_type: str = 'oauth'):
        super().__init__()
        self._authorized = False
        self._token = token
        self._token_type = token_type
        self._client = None

    def __call__(self, token: str, token_type: str = 'oauth') -> 'GitlabService':
        self._authorized = False
        self._token = token
        self._token_type = token_type
        self._client = None
        return self

    @property
    def is_authorized(self):
        return self._authorized

    @property
    def gl_client(self):
        if self._client is None:
            log.debug(f"[GL_CLIENT] Creating GL client for token type - [{self.token_type}]")
            self._client = self.get_client(token=self._token, token_type=self._token_type)
        return self._client

    @property
    def token(self) -> str:
        return self._token

    @property
    def token_type(self) -> str:
        return self._token_type

    def get_client(self, token=None, token_type='oauth', **kwargs) -> gitlab.Gitlab:
        params = {**kwargs}
        if token:
            params[f'{token_type}_token'] = token
        gitlab_url = self._config.get('GITLAB_URL')
        instance: gitlab.Gitlab = gitlab.Gitlab(gitlab_url, **params)
        reducted_params = {(k, v) for (k, v) in params.items() if not k.endswith('_token')}
        log.debug(f"[CL_CLIENT] Instance {instance.url} - {reducted_params}")

        try:
            if token:
                instance.auth()
                self._authorized = True
        except gitlab.GitlabError as ex:
            log.error(f"[GL_CLIENT] Client authentication error: {ex}")
            raise errors.GitlabAuthorizationError(f"{ex}")
        return instance

    @property
    def current_user(self):
        return self.gl_client.user

    def list_projects(self, gl_client=None, **kwargs) -> List[objects.Project]:
        """List gitlab projects

        Returns:
        DOC: https://python-gitlab.readthedocs.io/en/stable/gl_objects/projects.html
        GL_DOC: https://docs.gitlab.com/ee/api/projects.html
        """
        gl_client = self._resolve_client(gl_client)
        projects: objects.ProjectManager = gl_client.projects
        return projects.list(**kwargs)

    def list_members(self, project_name: str, gl_client=None, **kwargs):
        members = self._project_members(project_name=project_name, gl_client=gl_client)
        return members.list(**kwargs)

    def _project_members(self, project_name: str, gl_client=None) \
            -> Optional[objects.ProjectMemberManager]:
        project = self.get_project(project_name=project_name, gl_client=gl_client)
        if not project:
            return None
        return project.members

    def get_project(self, project_name, gl_client=None, **kwargs) -> objects.Project:
        gl_client = gl_client or self.gl_client
        projects: objects.ProjectManager = gl_client.projects
        log.debug(f"[GL_PROJ] Getting gitlab project: {project_name}")
        project: objects.Project = projects.get(project_name, **kwargs)
        return project

    def tag_submission(self, submission: Submission, **kwargs) -> objects.ProjectTag:
        p_name = self._extract_project_name(submission)
        if not p_name:
            log.info("[GL_TAG] SKIPPED tagging due project name has not been extracted.")
            return None
        gl_project = self.gl_client.projects.get(p_name)
        ref = kwargs.get('ref') or 'master'
        name = str(submission.created_at)
        message = f"Submission {submission.id} at {submission.created_at}"
        params = {'tag_name': name, 'ref': ref, 'message': message}
        log.info(f"[GL_TAG] Tag for {submission.log_name} - project {gl_project.path}"
                 f": {params}")
        tag = gl_project.tags.create(**params)
        return tag

    def add_member_to_project(self, project_name: str,
                              member_id=None, access_level=None, **kwargs):
        members = self._project_members(project_name=project_name)
        member_id = member_id or self.kontr_gl_user.id
        access_level = access_level or gitlab.REPORTER_ACCESS
        params = {'user_id': member_id, 'access_level': access_level}
        log.info(f"[GL_MEMBER] Adding member to a project {project_name} - {params}")
        try:
            return members.create(params, **kwargs)
        except gitlab.exceptions.GitlabCreateError as ex:
            log.warning(f"[GL_MEMBER] User is already a member: {ex}")
            raise errors.PortalAPIError(
                400, f"User is already a member of a project {project_name}")

    @property
    def kontr_gl_user(self) -> objects.CurrentUser:
        from portal.service.services_collection import ServicesCollection
        return ServicesCollection.get().kontr_gitlab.current_user

    def _resolve_client(self, gl_client: gitlab.Gitlab = None):
        return gl_client or self.gl_client

    def _extract_project_name(self, submission: Submission):
        url: str = tools.dict_dig(submission.parameters, ['file_params', 'source', 'url'])
        if not url:
            log.debug(f"[SKIP] Unable to extract git URL from "
                      f"subm. parameters {submission.parameters}")
            return None
        parsed = tools.extract_git_info(url)
        full_name = parsed.owner + '/' + parsed.name
        gl_base = tools.extract_git_info(self.gl_client.url).resource
        if parsed.resource != gl_base:
            log.debug(f"[SKIP] Git base URL are not matching: "
                      f"{parsed.resource} != {gl_base}")
            return None
        return full_name

    @property
    def git_domain(self) -> str:
        return self._config.get('GITLAB_BASE_DOMAIN') or urlparse(self.gl_client.url).hostname

    def build_ssh_url(self, client: models.Client, project: models.Project):
        return f"git@{self.git_domain}:{self.build_repo_name(client, project)}.git"

    def build_repo_name(self, client: models.Client, project: models.Project):
        return f"{client.codename}/{project.course.codename}"


class KontrGitlabService(GitlabService):
    def __init__(self):
        kontr_token = self._config.get('GITLAB_KONTR_TOKEN')
        super().__init__(token_type='private', token=kontr_token)

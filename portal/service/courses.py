"""
Courses service
"""

from typing import List

from portal import logger
from portal.database.enums import CourseState
from portal.database.models import Client, Course, Group, Role
from portal.service.general import GeneralService
from portal.service.groups import GroupService
from portal.service.projects import ProjectService

log = logger.get_logger(__name__)


class CourseService(GeneralService):
    @property
    def _entity_class(self):
        return Course

    def query_find_all(self, archived=False, **kwargs):
        query = super(CourseService, self).query_find_all(**kwargs)
        if not archived:
            query = query.filter(Course.state != CourseState.ARCHIVED)
        return query

    @property
    def course(self):
        return self._entity

    def create(self, **data) -> Course:
        """Creates new course

        Keyword Args:
            name(str): name of the course
            codename(str): codename of the course

        Returns(Course): Course instance
        """
        course = Course()
        self._entity = course
        self._set_data(entity=course, **data)
        log.info(f"[CREATE] Course {course.log_name}: {course.log_name}")
        return course

    def set_state(self, state: CourseState):
        log.info(f"[STATE] Course set state {self.course.log_name}: {state}")
        self.course.state = state
        self.write_entity(self.course)

    def activate(self):
        self.set_state(CourseState.ACTIVE)

    def deactivate(self):
        self.set_state(CourseState.INACTIVE)

    def archive(self):
        self.set_state(CourseState.ARCHIVED)
        # TODO archive all projects and submissions

    def copy_course(self, target: Course, config: dict) -> Course:
        """Copies course and it's other resources (roles, groups, projects)
        The other resources that should be copied are specified in the config

        Args:
            target(Course): To which course the resources will be copied
            config(dict): Configuration to specify which resource should be copied

        Returns(Course): Copied course instance (target)
        """
        log.info(f"[COPY] Copy course from {self.course.log_name} -> {target.log_name}: {config}")
        if config.get('roles'):
            for role in self.course.roles:
                from portal.service.roles import RoleService
                RoleService(role).copy_role(target, with_clients=config['roles'])
        if config.get('groups'):
            for group in self.course.groups:
                GroupService(group).copy_group(target, with_users=config['groups'])
        if config.get('projects'):
            for project in self.course.projects:
                ProjectService(project).copy_project(target)

        self.write_entity(target)
        return target

    def update_notes_token(self, token: str) -> Course:
        """Updates notes access token of a course.

        Args:
            token(str): The new token

        Returns(Course): Course instance
        """
        log.info(f"[UPDATE] Notes access token for {self.course.log_name}")
        self.course.notes_access_token = token
        self.write_entity(self.course)
        return self.course

    def get_clients_filtered(self, groups: List[str], roles: List[str], client_type=None) \
            -> List["Client"]:
        """Get all users for course filtered
        Args:
            groups(list): Group names list
            roles(list):  Role names list
            client_type(ClientType): Client type
        Returns(List[User]):
        """
        groups_entities = Group.query.filter(Group.id.in_(groups)).all() if groups else None
        roles_entities = Role.query.filter(Role.id.in_(roles)).all() if roles else None
        return self.course.get_clients_filtered(groups_entities, roles_entities,
                                                client_type=client_type)

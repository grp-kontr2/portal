"""
Emails service
"""

import logging
from pathlib import Path

import yaml
from emails.template import JinjaTemplate
from flask_emails import Message

from portal.database.models import User
from portal.service import errors
from portal.service.general import GeneralService
from portal.tools import paths

log = logging.getLogger(__name__)

EMAILS_DIR = paths.RESOURCES_DIR / 'emails'


class EmailMessage:
    def __init__(self, subject: str, body: str, **kwargs):
        """Create email message
        Args:
            subject(str): Subject of the the mail
            body(str): Body of the mail
            **kwargs:
        """
        self._subject = JinjaTemplate(subject)
        self._body = JinjaTemplate(body)
        self._params = kwargs
        self._message = None

    @property
    def subject(self) -> JinjaTemplate:
        """Gets subject of a mail
        Returns(str): Subject of the mail
        """
        return self._subject

    @property
    def body(self) -> JinjaTemplate:
        """Gets body content
        Returns: Email body content
        """
        return self._body

    def create_message(self) -> Message:
        """Creates new message

        Returns(Message): Message instance
        """
        message = Message(
            subject=self.subject,
            html=self.body,
            **self._params
        )
        return message

    @property
    def message(self) -> Message:
        if self._message is None:
            self._message = self.create_message()
        return self._message

    def __str__(self):
        return str(dict(subject=self.subject.template_text))


def _read_yml(full_path):
    with open(str(full_path), 'r') as stream:
        try:
            return yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            log.error(f"[EMAIL] Yaml parser has failed: {exc}")
            raise errors.PortalServiceError(f"Cannot load email template from {full_path}")


class EmailMessages:
    def __init__(self, email_service: 'EmailService'):
        self._emails = email_service

    @property
    def emails(self) -> 'EmailService':
        return self._emails

    def get(self, name) -> EmailMessage:
        return self.emails.create_message(name)

    def __getitem__(self, item):
        return self.get(name=item)

    @property
    def user_created(self):
        return self.get('user_created')

    @property
    def user_password_update(self):
        return self.get('user_password_update')

    @property
    def user_role_membership(self):
        return self.get('user_role_membership')


class EmailService(GeneralService):
    def __init__(self):
        super().__init__()
        self._messages = EmailMessages(self)

    @property
    def is_enabled(self) -> bool:
        return bool(self._config.get('EMAIL_HOST', False))

    @property
    def messages(self) -> EmailMessages:
        return self._messages

    @property
    def emails_dir(self) -> Path:
        return EMAILS_DIR

    def load_template(self, template: str, template_type='general') -> dict:
        """Loads template data
        Args:
            template(str): template name
            template_type(str): Template type - default is 'general'

        Returns(dict): message template
        """
        full_path = self.emails_dir / template_type / f"{template}.yml"
        if not full_path.exists():
            log.error(f"[EMAIL] Emails template do not exists: {full_path}")
            raise errors.PortalServiceError(f"Cannot load email template from {full_path}")
        return _read_yml(full_path=full_path)

    def create_message(self, name, template_type='general') -> EmailMessage:
        """Creates message for the given template name
        Args:
            name: template name
            template_type: template type (default: general)

        Returns(EmailMessage): Email message instance
        """
        template = self.load_template(name, template_type=template_type)
        return EmailMessage(**template)

    def notify_user(self, user: User, template: str, context: dict, **kwargs):
        """Notify user by email
        Args:
            user(User): User instance
            template(template): template name
            context(dict): Message context
            **kwargs: Update context by kwargs
        """
        if not self.is_enabled:
            log.error("[EMAILS] Emails are not enabled, not sending")
            return

        context.update(kwargs)
        message = self.messages[template]
        if not message:
            log.error(f"[EMAILS] Cannot find template: {template}")
            raise errors.PortalServiceError(f"Cannot find template: {template}")
        email = user.email
        if email:
            to = (user.name, email)
            log.info(f"[EMAIL] Send message to user [{user.log_name} :: {email}]: {message}")
            message.create_message().send(to=to, render=context)

"""
Users service
"""

from typing import List, Optional

from portal import ldap_wrapper, logger
from portal.database.models import Course, Group, Project, Review, Role, Submission, User
from portal.rest.schemas import SCHEMAS
from portal.service.errors import IncorrectCredentialsError
from portal.service.general import GeneralService

log = logger.get_logger(__name__)


class UserLdapService:
    def __init__(self, service: 'UserService'):
        self._service = service

    @property
    def service(self):
        return self._service

    @property
    def ldap(self):
        return ldap_wrapper

    @property
    def is_enabled(self) -> bool:
        return self.ldap.is_enabled

    @property
    def user(self):
        return self.service.user

    def extract_uco(self) -> Optional[int]:
        """Get uco from the ldap
        Returns(int): User's uco
        """
        username = self.user.username
        log.info(f"[LDAP] Extracting uco for: {self.user.log_name}")
        user_entity = self.ldap.search(f"uid={username},ou=People")
        log.debug(f"[LDAP] User ({username}): {user_entity}")
        if not user_entity:
            return None
        if 'description' not in user_entity.entry_attributes:
            return None
        desc = user_entity.description
        uco = self._parse_uco_from_desc(desc)
        log.debug(f"[LDAP] User's UCO ({username}): {uco}")
        return uco

    def _parse_uco_from_desc(self, desc):
        values: List[str] = desc.values
        uco_entry = None
        for item in values:
            if item.startswith('UCO='):
                uco_entry = item
        if not uco_entry:
            return None
        uco = int(uco_entry[len('UCO='):])
        return uco


class UserService(GeneralService):
    @property
    def _entity_class(self):
        return User

    @property
    def user(self) -> User:
        return self._entity

    @property
    def ldap(self) -> UserLdapService:
        if not hasattr(self, '_ldap'):
            setattr(self, '_ldap', UserLdapService(self))
        return getattr(self, '_ldap')

    def _set_user_data(self, data: dict) -> User:
        self.__update_uco_using_ldap(data)
        params = ['name', 'email', 'uco', 'gitlab_username', 'managed']
        return self.update_entity(self.user, data, allowed=params)

    def __update_uco_using_ldap(self, data):
        if self.ldap.is_enabled:
            log.debug("[LDAP] Ldap is enabled, extracting UCO")
            uco = self.ldap.extract_uco()
            if uco:
                data['uco'] = uco
            else:
                log.warning(f'[LDAP] Cannot extract UCO for user: {self.user.log_name}')

    def create(self, **data):
        """Creates a new user

            Keyword Args:
                username(str): User's name
                uco(str): User's Uco

            Returns(User): Created user instance
            """
        data['is_admin'] = data.get('is_admin', False)
        username = data['username']
        new_user = User(username=username)
        self._entity = new_user
        self._set_user_data(data)
        log.info(f"[CREATE] User ({new_user.log_name}): {new_user}")
        return new_user

    def update(self, **data):
        """Updates a user

        Args:
            data(dict): User's data

        Returns(User): Updated user
        """
        self._set_user_data(data)
        log.info(f"[UPDATE] User {self.user.log_name}: {self.user}.")
        return self.user

    def update_password(self, data: dict):
        log.info(f"[UPDATE] User password for {self.user.log_name}")
        self.user.set_password(data['new_password'])
        self.write_entity(self.user)
        return self.user

    def check_old_password(self, data):
        old_password = data['old_password']
        if not self.user.verify_password(old_password):
            raise IncorrectCredentialsError()

    def find_users(self, ids: List[str]) -> List[User]:
        """Finds all users based on their ids

        Args:
            ids(List[str]): List of user ids

        Returns(List[User]): List of users
        """
        return [self.find.user(i) for i in ids]

    def find_users_filtered(self, course_id: str = None,
                            group_id: str = None) -> List[User]:
        """Find all users, optionally filtered by course and group
        Args:
            course_id(str): Course id (optional)
            group_id(str): Group id (optional)

        Returns(List[User]): Filtered list of  users
        """
        users = User.query
        if course_id:
            users = users.join(User.roles) \
                .join(Course).filter((Course.id == course_id) | (Course.codename == course_id))
        if group_id:
            course = self.find.course(course_id)
            users = users.join(User.groups).filter(
                ((Group.id == group_id) | (Group.name == group_id)) & (
                        course.id == Group.course_id)
            )

        return users.all()

    def find_groups_filtered(self, course_id: str = None) -> List[Group]:
        """Find all groups for the user, optionally filtered by course.

        Args:
            course_id(str): Course id (optional)

        Returns(List[Group]): List of all groups
        """
        if course_id:
            return self.user.get_groups_in_course(self.find.course(course_id))
        return self.user.groups

    def find_roles_filtered(self, course_id: str = None) -> List[Role]:
        """Get all roles for the user, optionally filtered by course

        Args:
            course_id(str): Course id (optional)

        Returns(List[Role]):
        """
        roles = self.user.roles
        if course_id:
            roles = self.user.get_roles_in_course(self.find.course(course_id))
        return roles

    def find_reviews(self) -> List[Review]:
        """Gets reviews the user has contributed to.

        Returns(List[Review]):
        """
        reviews = []
        for item in self.user.review_items:
            if item not in reviews:
                reviews.append(item.review)
        return list(reviews)

    def find_submissions_filtered(self, course_id: str = None,
                                  project_ids: List[str] = None) -> List[Submission]:
        """Get all submissions of a user, optionally filtered by course and project.

        Args:
            course_id(str): Course id (optional)
            project_ids(List[str]): List of project ids (optional)

        Returns(List[Submission]): submissions in the course and projects
        """
        submissions = Submission.query.filter_by(user=self.user)
        # user.submissions cannot be used as the base query
        if course_id:
            submissions = submissions.join(Project)
            if project_ids:
                course = self.find.course(course_id)
                submissions = submissions.filter(
                    ((Project.id.in_(project_ids)) | (Project.codename.in_(project_ids))) &
                    (Project.course == course)
                )
            submissions = submissions.join(Course).filter(
                (Course.id == course_id) | (Course.codename == course_id))
        return submissions

    def find_all_projects_for_user(self):
        all_projects = self.user.get_all_projects()
        return all_projects

    def dump_projects_with_wait(self):
        all_projects = self.find_all_projects_for_user()
        projects_dump = []
        for project in all_projects:
            dump = SCHEMAS.dump('project', project)
            from portal.service.projects import ProjectService
            dump['next_time'] = ProjectService(project).calculate_wait_time(self.user).isoformat()
            projects_dump.append(dump)
        return projects_dump

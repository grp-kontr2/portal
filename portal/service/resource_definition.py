import logging
from datetime import datetime
from typing import Dict, List

import marshmallow

from portal.database import Course, Group, Project, Role
from portal.database.enums import CourseState
from portal.service.general import GeneralService

log = logging.getLogger(__name__)


class ResourceDefinitionService(GeneralService):
    def sync_course(self, definition: Dict, save: bool = True):
        """Synchronize course based on definition
        Args:
            definition(Dict): Course definition schema
            save(bool): Whether to save to database
        Returns(Course): Synchronized course
        """
        definition = definition.get('course') or definition
        if 'state' in definition:
            try:
                definition['state'] = CourseState(definition['state'])
            except Exception as ex:
                log.error(f"State error: {ex}")

        course = self._get_course(definition['codename'])
        params = self._course_params

        _update_params(course, definition, params)
        projects = definition.get('projects')
        if projects:
            _sync_collection(course, projects, self.sync_project)
        groups = definition.get('groups')
        if projects:
            _sync_collection(course, groups, self.sync_group)

        roles = definition.get('roles')
        if roles:
            _sync_collection(course, roles, self.sync_role)

        if save:
            self.write_entity(course)
        return course

    def sync_project(self, course: Course, definition: Dict, save: bool = True):
        """Sync project
        Args:
            course(Course): Course instance
            definition(Dict): Project definition
            save(bool): Whether to save to DB
        Returns(Project): Synchronized project
        """
        definition = definition.get('project') or definition
        project = self._get_project(course, definition['codename'])
        params = self._project_params
        _update_params(project, definition, params)
        config = definition.get('config')
        if config:
            self.sync_project_config(project, config)
        if save:
            self.write_entity(project)
        return project

    def sync_project_config(self, project: Project, definition: Dict, save=False):
        for timed in self._timed_params:
            time_from_def = definition.get(timed)
            if time_from_def:
                parsed = marshmallow.utils.from_iso(time_from_def)
                definition[timed] = parsed
        project.set_config(**definition)
        if save:
            self.write_entity(project)
        return project

    def sync_role(self, course: Course, definition: Dict, save: bool = True):
        """Sync role
        Args:
           course(Course): Course instance
           definition(Dict): Role definition
           save(bool): Whether to save to DB
        Returns(Role): Synchronized role
       """
        definition = definition.get('role') or definition
        role = self._get_role(course, definition['codename'])
        params = self._role_params
        _update_params(role, definition, params)
        permissions = definition.get('permissions')
        clients = definition.get('clients')
        if permissions:
            role.set_permissions(**permissions)
        if clients:
            for client in clients:
                instance = self.find.client(client, throws=False)
                if instance:
                    role.clients.append(instance)
        if save:
            self.write_entity(role)
        return role

    def sync_group(self, course: Course, definition: Dict, save: bool = True):
        """Sync group
        Args:
            course(Course): Course instance
            definition(Dict): Group definition
            save(bool): Whether to save to DB
        Returns(Group): Synchronized group
        """
        definition = definition.get('group') or definition
        group = self._get_group(course, definition['codename'])
        params = self._group_params
        _update_params(group, definition, params)
        users = definition.get('users')
        projects = definition.get('projects')
        if users:
            for client in users:
                instance = self.find.client(client, throws=False)
                if instance:
                    group.users.append(instance)
        if projects:
            for project in projects:
                instance = self.find.project(group.course, project)
                if instance:
                    group.projects.append(instance)
        if save:
            self.write_entity(group)
        return group

    def dump_course(self, course: Course, with_users=False, with_projects=False) -> Dict:
        """Dumps the course schema for specified course
        Args:
            course(Course): Course instance
            with_users(bool): Whether to dump role and group with clients/users
            with_projects(bool): Whether to dump group with projects
        Returns(Dict): Course dump
        """
        schema = _extract_params(course, self._course_params)
        schema['state'] = course.state.value if course.state else None
        schema['projects'] = [self.dump_project(project) for project in course.projects]
        schema['roles'] = [self.dump_role(role, with_clients=with_users) for role in course.roles]
        schema['groups'] = [
            self.dump_group(group, with_projects=with_projects, with_users=with_users)
            for group in course.groups
        ]
        return schema

    def dump_project(self, project: Project) -> Dict:
        """Dumps the project schema
        Args:
            project(Project): Project instance
        Returns(Dict): Project dump
        """
        schema = _extract_params(project, self._project_params)
        config_params = ['test_files_source', 'file_whitelist', 'config_subdir',
                         'test_files_subdir', 'submission_parameters']
        config = project.config
        schema['config'] = _extract_params(config, config_params)
        for param in self._timed_params:
            value: datetime = getattr(config, param)
            if value:
                schema['config'][param] = value.isoformat()
        return schema

    @property
    def _timed_params(self):
        return ['submissions_allowed_from', 'submissions_allowed_to', 'archive_from']

    def dump_role(self, role: Role, with_clients=False) -> Dict:
        """Dumps the role's schema
        Args:
            role(Role): Role instance
            with_clients(bool): Whether to dump role with client ids
        Returns(Dict): Role dump
        """
        schema = _extract_params(role, self._role_params)
        schema['permissions'] = _extract_params(role, Role.PERMISSIONS)
        if with_clients:
            schema['clients'] = [client.id for client in role.clients]
        return schema

    def dump_group(self, group: Group, with_users=False, with_projects=False):
        """Dumps the group's schema
        Args:
            group(Group): Group instance
            with_users(bool): Whether to dump the group with user ids
            with_projects(bool): Whether to dump the group with project ids

        Returns:

        """
        schema = _extract_params(group, self._group_params)
        if with_users:
            schema['users'] = [client.id for client in group.users]
        if with_projects:
            schema['projects'] = [project.id for project in group.project]
        return schema

    def _get_course(self, codename):
        entity = self.find.course(codename, throws=False)
        instance = entity if entity else Course(codename=codename)
        self.db_session.add(instance)
        return instance

    def _get_project(self, course, codename):
        entity = self.find.project(course, codename, throws=False)
        instance = entity if entity else Project(codename=codename, course=course)
        self.db_session.add(instance)
        return instance

    def _get_group(self, course, codename):
        entity = self.find.group(course, codename, throws=False)
        return entity if entity else Group(codename=codename, course=course)

    def _get_role(self, course, codename):
        entity = self.find.role(course, codename, throws=False)
        instance = entity if entity else Role(codename=codename, course=course)
        self.db_session.add(instance)
        return instance

    @property
    def _course_params(self) -> List[str]:
        params = Course.updatable_params() + ['notes_access_token']
        params.remove('state')
        return params

    @property
    def _project_params(self) -> List[str]:
        return ['codename', 'name', 'description', 'submit_configurable',
                'assignment_url', 'submit_instructions']

    @property
    def _role_params(self) -> List[str]:
        return Role.updatable_params()

    @property
    def _group_params(self) -> List[str]:
        return Group.updatable_params()


def _update_params(entity, schema: Dict, params):
    for param in params:
        value = schema.get(param)
        setattr(entity, param, value)
    return entity


def _sync_collection(course, collection: List, sync_entity):
    for item in collection:
        sync_entity(course, item)


def _extract_params(entity, params: List) -> Dict:
    dump = dict()
    for param in params:
        value = getattr(entity, param)
        if value is not None:
            dump[param] = value
    return dump

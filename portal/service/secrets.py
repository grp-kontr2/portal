import secrets

from portal import logger
from portal.database.models import Secret
from portal.service.general import GeneralService

log = logger.get_logger(__name__)


class SecretsService(GeneralService):
    @property
    def _entity_class(self):
        return Secret

    def __call__(self, entity=None, client=None) -> 'SecretsService':
        super(SecretsService, self).__call__(entity=entity)
        if self._client is None and self._entity:
            self._client = self._entity.client
        self._client = client
        return self

    def __init__(self):
        super().__init__()
        self._client = None

    @property
    def secret(self) -> Secret:
        return self._entity

    @property
    def client(self):
        return self._client

    def create(self, client=None, **data):
        client = client or self.client
        value = secrets.token_urlsafe(64)
        secret = Secret(name=data['name'], value=value)
        client.secrets.append(secret)
        self._set_data(secret, **data)
        log.info(f"[CREATE] Secret {secret.log_name} by {client.log_name}: {secret}")
        return secret, value

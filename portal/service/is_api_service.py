import logging
from typing import Collection, Dict, List, Set

from is_api import entities
from is_api.entities import NotepadContent

from portal.database import Course, Group, Project, User, queries
from portal.service import errors
from portal.service.general import GeneralService
from portal.tools.is_api_adapter import IsApiFactory, IsApiWrapper

log = logging.getLogger(__name__)


def _users_to_ucos_set(users: List[User]) -> Set[int]:
    return set(user.uco for user in users)


class IsApiService(GeneralService):
    @property
    def roles(self):
        from portal.service.roles import RoleService
        return RoleService

    @property
    def groups(self):
        from portal.service.groups import GroupService
        return GroupService

    @property
    def is_factory(self) -> IsApiFactory:
        from portal import is_api_factory
        return is_api_factory

    @property
    def course(self) -> Course:
        return self._entity

    @property
    def is_api(self) -> IsApiWrapper:
        return self.is_factory.get_instance(course=self.course)

    def import_students(self, role_name: str = 'student'):
        """Import students to course from IS MU
        Args:
            role_name: Name of the role for the students
        """
        students = self.is_api.course_students
        ucos = [student.uco for student in students.students]
        self._import_users(ucos, role_name)

    def import_teachers(self, role_name: str = 'teacher'):
        """Import teachers to course from IS MU
        Args:
            role_name: Name of the role for teachers
        """
        teachers = self.is_api.course_teachers
        ucos = [teacher.uco for teacher in teachers]
        self._import_users(ucos, role_name)

    def import_users(self, role_name: str, users_type: str):
        """Import users to course from IS MU
        Args:
            role_name(str): Role name
            users_type(str): Users type
        """
        u_types = dict(students=self.import_students, teachers=self.import_teachers)
        method = u_types.get(users_type)
        if not method:
            raise errors.PortalAPIError(400, "Users type is not recognized")
        return method(role_name)

    def sync_seminaries(self):
        """Synchronisation for the seminaries: for given course
        """
        log.info(f"[IS_API] Syncing groups and users in course {self.course.log_name}")
        seminaries = self.is_api.course_seminars_list
        for seminary in seminaries:
            self._sync_seminary(seminary)

    def write_note(self, user: User, content: str, shortcut: str = None, project: Project = None):
        """Write the notepad note for provided user with provided content
        Args:
            user(User): User instance
            content(str): Notepad content
            shortcut(str): notepad shortcut (Optional if not project)
            project(Project): Project instance (Optional if not shortcut)

        Returns:

        """
        shortcut = shortcut or project.codename
        log.info(f"[IS_API] Writing note for {self.course.log_name} - notepad {shortcut} "
                 f"for {user.log_name}: {content}")
        return self.is_api.write_notepad(shortcut=shortcut, user=user, content=content)

    def read_note(self, user: User, shortcut: str = None, project: Project = None) \
            -> Dict[str, str]:
        """Read the notepad token
        Args:
            user(User): User instance
            shortcut(str): Notepad shortcut (Optional if not project)
            project(Project): Project instance (Optional if not shortcut)

        Returns:

        """
        shortcut = shortcut or project.codename
        log.info(f"[IS_API] Reading note for {self.course.log_name} - notepad: {shortcut} "
                 f"for {user.log_name}")
        result: NotepadContent = self.is_api.read_notepad(shortcut=shortcut, users=[user])
        log.debug(f"[IS_API] Content: {result}")
        return {student.uco: student.content for student in result.students}

    def create_notepad(self, shortcut: str = None, name: str = None,
                       project: Project = None, **kwargs):
        """Create new note
        Args:
            shortcut(str): Note shortcut (Optional if not project)
            name(str): Note name (Optional if not project)
            project(Project): project instance (Optional if not shortcut and name)
            **kwargs:
        Returns:
        """
        log.info(f"[IS_API] Creating notepad for {self.course.log_name} - notepad {shortcut} "
                 f"with name {name} and params {kwargs}")
        name = name or project.name
        shortcut = shortcut or project.codename
        created = self.is_api.create_note(shortcut=shortcut, name=name)
        log.debug(f"[IS_API] Notepad: {created}")
        return created

    def _import_users(self, uco_list: List[int], role_name: str):
        log.info(f"[IS_API] Importing users to course {self.course.log_name} "
                 f"with role {role_name}: {uco_list}")
        role = self.find.role(course=self.course, identifier=role_name)
        for uco in uco_list:
            user = queries.user_by_uco(uco).first()
            if not user:
                log.info(f"User with uco: {uco} does not exists - SKIPPING.")
                continue
            self.roles(role).add_client(user)

    def _sync_seminary(self, seminary: entities.Seminar):
        group = self._get_or_create_group(seminary)
        s_uco = self._extract_ucos_students(seminary)
        t_uco = self._extract_ucos_teachers(seminary)
        current_is = set(*s_uco, *t_uco)
        current_portal = _users_to_ucos_set(group.users)
        remove = current_portal - current_is
        add = current_is - current_portal
        log.info(f"[IS_API] Seminary SYNC - remove: {remove}, add: {add}")
        self._mod_person_in_group(group, add, 'add_user')
        self._mod_person_in_group(group, remove, 'remove_user')

    def _get_or_create_group(self, seminary: entities.Seminar):
        group = self.find.group(self.course, seminary.label)
        if not group:
            params = dict(codename=seminary.label, name=f"Seminary {seminary.label}")
            group = self.groups.create(course=self.course, **params)
        return group

    def _extract_ucos_students(self, seminary: entities.Seminar):
        items = self.is_api.is_client.seminar_list_students(seminars=[seminary.label])
        return [item.uco for item in items.seminars[0].students]

    def _mod_person_in_group(self, group: Group, collection: Collection[int], action: str):
        for uco in collection:
            user: User = queries.user_by_uco(uco).first()
            if not user:
                log.info(f"[IS_API] User with uco: {uco} does not exists - SKIPPING.")
                continue
            log.info(f"User with uco {uco} ({user.log_name}), "
                     f"action - {action} on group: {group.log_name}")
            group_service = self.groups(group)
            action_method = getattr(group_service, action)
            action_method(user)

    def _extract_ucos_teachers(self, seminary: entities.Seminar):
        items = self.is_api.is_client.seminar_list_teachers(seminars=[seminary.label])
        return [item.uco for item in items.seminars[0].teachers]

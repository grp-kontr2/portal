"""
Projects service
"""
import datetime
import logging
from datetime import timedelta
from typing import List

from flask_sqlalchemy import BaseQuery

import portal.database.enums
from portal import storage, storage_wrapper
from portal.database import enums, models, queries
from portal.service import errors
from portal.service.general import GeneralService, get_new_name
from portal.storage import entities
from portal.tools import time

log = logging.getLogger(__name__)


class ProjectService(GeneralService):
    @property
    def storage(self):
        return storage

    @property
    def _entity_class(self):
        return models.Project

    @property
    def project(self) -> models.Project:
        return self._entity

    def copy_project(self, target: models.Course) -> models.Project:
        """Copies a project to the target course

        Args:
            target(Course): Course instance

        Returns(Project): Copied project
        """
        new_name = get_new_name(self.project, target)
        new_project = models.Project(target, codename=new_name)
        new_project.description = self.project.description
        new_project.name = self.project.name
        new_project.set_config(**vars(self.project.config))
        log.info(f"[COPY] Project from {self.project.log_name}: {new_project.log_name}")
        return new_project

    def can_create_submission(self, user: models.User) -> bool:
        """Checks if a user may create a new submission.

        A user is permitted to create a new submission if there are no submissions
        created or queued for him in the given project.

        Args:
            user(User): the user who wants to crete a new submission

        Returns: whether the user can create a new submission in the project

        """
        conflicting_states = [portal.database.enums.SubmissionState.CREATED,
                              portal.database.enums.SubmissionState.READY,
                              portal.database.enums.SubmissionState.QUEUED]
        conflict = queries.list_submissions_universal(
            user=user, project=self.project, states=conflicting_states
        ).first()
        log.debug(f"[CONFLICT] Found conflicting submission: {conflict}")
        return conflict is None

    def create(self, course: models.Course, **data) -> models.Project:
        """Creates a new project

        Args:
            course(Course): Course instance
            data(dict): Project data

        Returns(Project): Project instance
        """
        new_project = models.Project(course=course)
        self._entity = new_project
        self._set_data(new_project, **data)
        log.info(f"[CREATE] Project  {new_project.log_name}: {new_project}")
        return new_project

    def update_project_config(self, data: dict) -> models.ProjectConfig:
        """Updates project config

        Args:
            data(dict): Config data

        Returns(ProjectConfig): Project config instance
        """
        self.project.set_config(**data)
        self.write_entity(self.project)
        log.info(f"[UPDATE] Configuration for project {self.project.log_name}: {data}.")
        return self.project.config

    def query_submissions(self, user: models.User) -> BaseQuery:
        """Queries submissions for the project and filters them by user.

        Args:
            user(User): User instance
        """
        return queries.list_submissions_universal(user=user, project=self.project)

    def find_project_submissions(self, user_id: str = None) -> List[models.Submission]:
        """Finds all project submissions. If a user id is specified, returns
        only submissions created
        by that user.

        Args:
            user_id(str): User id (optional)

        Returns(List[Submission]): A list of all submissions in a project if no user_id is specified
        Otherwise a list of submissions created in the project by the specified user.

        """
        user = self.find.user(user_id) if user_id else None
        query = queries.list_submissions_universal(user, project=self.project)
        return query.all()

    def check_submission_create(self, client=None):
        if client.type != portal.database.enums.ClientType.USER:
            raise errors.SubmissionRefusedError(f"Worker cannot create submission.")
        user = self.find.user(client.id)
        # TODO: Might be good to add check whether it is in DEBUG Mode
        if user.is_admin:
            return True
        if self.project.state != portal.database.enums.ProjectState.ACTIVE:
            raise errors.SubmissionRefusedError(f"Project {self.project.log_name} not active.")
        if not ProjectService(self.project).can_create_submission(client):
            log.warning(f"[WARN] Already active submission for: {self.project.log_name}")
            raise errors.SubmissionRefusedError(
                f"User {user.log_name} has already active "
                f"submission for project {self.project.log_name}")
        self._check_latest_submission(user)
        return True

    def _check_latest_submission(self, user: models.User) -> bool:
        latest_submission = self.get_latest_submission(user, ignore_cancel=True)
        if not latest_submission:
            return True
        latest_time = latest_submission.created_at
        now_time = datetime.datetime.utcnow()
        diff_time = now_time - latest_time
        delta = timedelta(minutes=30)  # TODO this should be configurable @mdujava
        log.info(f"Submission for project {self.project.log_name} by user {user.log_name},"
                 f" time delta: {diff_time} < {delta} = {diff_time < delta} created")
        if diff_time < delta:
            log.debug(f"Submission for {self.project.log_name} by {user.log_name} "
                      f"rejected based on the time delta: {diff_time}")
            raise errors.SubmissionDiffTimeError(self.project, diff_time, user=user)
        return True

    def get_latest_submission(self, user, ignore_cancel: bool = False) -> models.Submission:
        query = queries.list_submissions_universal(user=user, project=self.project)
        if ignore_cancel:
            query = query.filter((models.Submission.state != enums.SubmissionState.CANCELLED) &
                                 (models.Submission.state != enums.SubmissionState.ABORTED))
        return query.order_by(models.Submission.created_at.desc()).first()

    def find_all(self, course: models.Course, state=None, states=None,
                 with_archived=False, **kwargs) -> List[models.Project]:
        """List of all projects
        Args:
            state: Project state
            states: Project states
            with_archived: Whether to list also archived projects
            course(Course): Course instance
        Returns(list): list of projects
        """
        projects = self.query_find_all(**kwargs).filter(models.Project.course == course).all()
        if not with_archived:
            projects = [project for project in projects if
                        project.state != enums.ProjectState.ARCHIVED]
        if not states and state:
            states = [state]
        if states:
            projects = [project for project in projects if project.state in states]
        return projects

    def update_project_test_files(self, params=None):
        """ Sends a request to Storage to update the project's test_files to the newest version.
        """
        log.info(f"[SERVICE] Updating project files for {self.project.log_name}")
        params = params or []
        updated_entity: entities.UploadedEntity = storage_wrapper. \
            test_files.update(dirname=self.project.storage_dirname, **params)
        version = updated_entity.version
        self.project.config.test_files_commit_hash = version
        log.debug(f"Updated project config {self.project.log_name}: {self.project.config}")
        self.write_entity(self.project)

    def calculate_wait_time(self, user: models.User) -> datetime.datetime:
        latest_submission = self.get_latest_submission(user, ignore_cancel=True)
        if not latest_submission:
            return time.current_time()
        latest_time = latest_submission.created_at
        delta = datetime.timedelta(minutes=30)
        return latest_time + delta

    def archive(self):
        log.info(f"[SERVICE] Archive project's test files: {self.project.log_name}")
        st_entity = storage_wrapper.projects.get(self.project.storage_dirname)
        if st_entity:
            st_entity.clean()

    def delete(self):
        log.info(f"[SERVICE] Archive project's test files: {self.project.log_name}")
        st_entity: entities.TestFiles = storage_wrapper.test_files.get(self.project.storage_dirname)
        if st_entity:
            st_entity.delete()
        super(ProjectService, self).delete()

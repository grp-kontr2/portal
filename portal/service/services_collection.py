import flask

from portal.service.auth import AuthService
from portal.service.courses import CourseService
from portal.service.emails import EmailService
from portal.service.find import FindService
from portal.service.general import GeneralService
from portal.service.gitlab import GitlabService, KontrGitlabService
from portal.service.groups import GroupService
from portal.service.is_api_service import IsApiService
from portal.service.permissions import PermissionsService
from portal.service.projects import ProjectService
from portal.service.resource_definition import ResourceDefinitionService
from portal.service.reviews import ReviewService
from portal.service.roles import RoleService
from portal.service.secrets import SecretsService
from portal.service.storage import StorageService
from portal.service.submissions import SubmissionsService
from portal.service.users import UserService
from portal.service.workers import WorkerService


class ServicesCollection:
    _collection = None

    @classmethod
    def get(cls) -> 'ServicesCollection':
        if cls._collection is None:
            cls._collection = cls()
        return cls._collection

    def __init__(self):
        self._auth = AuthService()
        self._find = FindService()
        self._email = EmailService()
        self._general = GeneralService()
        self._kontr_gl_service = None
        self._res_def = ResourceDefinitionService()

    @property
    def general(self):
        return self._general

    @property
    def flask_app(self) -> flask.Flask:
        return flask.current_app

    @property
    def emails(self) -> EmailService:
        return self._email

    @property
    def find(self) -> FindService:
        return self._find

    @property
    def auth(self) -> AuthService:
        return self._auth

    @property
    def users(self) -> UserService:
        return UserService()

    @property
    def workers(self) -> WorkerService:
        return WorkerService()

    @property
    def courses(self) -> CourseService:
        return CourseService()

    @property
    def roles(self) -> RoleService:
        return RoleService()

    @property
    def groups(self) -> GroupService:
        return GroupService()

    @property
    def projects(self) -> ProjectService:
        return ProjectService()

    @property
    def permissions(self) -> PermissionsService:
        return PermissionsService()

    @property
    def secrets(self) -> SecretsService:
        return SecretsService()

    @property
    def submissions(self) -> SubmissionsService:
        return SubmissionsService()

    @property
    def reviews(self) -> ReviewService:
        return ReviewService()

    @property
    def storage(self) -> StorageService:
        return StorageService()

    @property
    def is_api(self) -> IsApiService:
        return IsApiService()

    @property
    def gitlab(self) -> GitlabService:
        return GitlabService()

    @property
    def kontr_gitlab(self) -> GitlabService:
        if self._kontr_gl_service is None:
            self._kontr_gl_service = KontrGitlabService()
        return self._kontr_gl_service

    @property
    def resource_definition(self) -> ResourceDefinitionService:
        return self._res_def

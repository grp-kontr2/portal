"""
Roles service
"""

import logging
from typing import List

from portal.database.models import Client, Course, Role
from portal.service import errors, general
from portal.service.general import GeneralService

log = logging.getLogger(__name__)


class RoleService(GeneralService):
    @property
    def _entity_class(self):
        return Role

    @property
    def role(self) -> Role:
        return self._entity

    def copy_role(self, target: Course, with_clients: str) -> Role:
        """Copies role to the target course
        Args:
            target(Course): Target Course resource
            with_clients(str): With users flag

        Returns(Role): Copied role
        """
        source = self.role
        log.info(f"[COPY] Role: {self.role.log_name} to course {target} ")
        new_role = self.__copy_new_role(target)
        if with_clients == "with_users":
            for clients in source.clients:
                clients.roles.append(new_role)
        return new_role

    def __copy_new_role(self, target):
        source = self.role
        new_name = general.get_new_name(source, target)
        new_role = Role(target, codename=new_name, description=source.description, name=source.name)
        new_role.set_permissions(**vars(source))
        return new_role

    def create(self, course: Course, **data) -> Role:
        """Creates new role
        Args:
            course(Course): Course instance

        Keyword Args:
            name(str): Name of the role
            description(str): Description of the role
            codename(str): Codename of the role

        Returns(Role): New role instance
        """
        new_role = Role(course=course)
        self._entity = new_role
        self._set_data(new_role, **data)
        log.info(f"[CREATE] New {self._entity_class.__name__} {new_role.log_name}: {new_role}")
        return new_role

    def update_permissions(self, perm: dict) -> Role:
        """Updates role permissions
        Args:
            perm(dict): Permissions dictionary

        Returns(RolePermissions): Role Permissions instance
        """
        self.role.set_permissions(**perm)
        self.write_entity(self.role)
        log.info(f"[UPDATE] Permissions for role {self.role.log_name}: {perm}.")
        return self.role

    def find_clients(self, ids: List[str]) -> List[Client]:
        """Finds all clients based on their ids

        Args:
            ids(List[str]): List of clients ids

        Returns(List[Client]): List of clients
        """
        return [self.find.client(i) for i in ids]

    def update_clients_membership(self, data: dict) -> Role:
        """Updates client membership in the role
        Args:
            data(dict): Data provided to update role membership

        Returns(Role): Updated role
        """
        clients_old = set(self.role.clients)
        clients = self.__add_users_to_role(clients_old, data)
        clients = self.__remove_users_from_role(clients, clients_old, data)
        self.role.clients = list(clients)
        self.write_entity(self.role)
        log.info(f"[UPDATE] Clients in Role {self.role.log_name}: {data}.")
        return self.role

    def __remove_users_from_role(self, clients, clients_old, data):
        clients_remove = data.get('remove')
        if clients_remove:
            to_remove = self.find_clients(data['remove'])
            clients = clients_old.difference(to_remove)
        return clients

    def __add_users_to_role(self, clients_old, data):
        clients_add = data.get('add')
        clients = set()
        if clients_add:
            to_add = self.find_clients(data['add'])
            clients = clients_old.union(to_add)
        return clients

    def add_client(self, client: Client):
        """Adds single client to the role
        Args:
            client(Client): Client instance
        Returns:
        """
        if client not in self.role.clients:
            self.role.clients.append(client)
            self.write_entity(self.role)
            log.info(f"[ADD] Client {client.log_name} added to role {self.role.log_name}")
        else:
            log.info(f"[ADD] Client {client.log_name} is already "
                     f"in role {self.role.log_name}: no change.")

    def remove_client(self, client: Client) -> Role:
        """Removes single client from the role
        Args:
            client(Client): Client instance
        Returns(Role): Updated role
        """
        try:
            self.role.clients.remove(client)
            self.write_entity(self.role)
        except ValueError:
            message = f"Could not remove client {client.log_name} " \
                f"from role {self.role.log_name}: " \
                f"role does not contain client."
            raise errors.PortalAPIError(400, message=message)
        log.info(f"[REMOVE] Client {client.log_name} from role {self.role.log_name}.")
        return self.role

    def find_all(self, course: Course, **kwargs) -> List[Role]:
        """List of all roles
        Args:
            course(Course): Course instance
        Returns(list): List of roles

        """
        return self.query_find_all(**kwargs).filter(Role.course == course)

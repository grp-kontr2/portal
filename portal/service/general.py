"""
General service functions
"""
import logging

import flask
from flask_sqlalchemy import BaseQuery, SQLAlchemy

from portal import db
from portal.database.models import Course
from portal.database.utils import sql_get_count

log = logging.getLogger(__name__)


class GeneralService:
    def __call__(self, entity=None):
        self._entity = entity
        return self

    @property
    def _entity_class(self):
        return self._entity.__class__ if self._entity else self.__class__

    def __init__(self, entity=None):
        self._entity = entity
        from portal.service.find import FindService
        self._find = FindService()

    @property
    def find(self):
        return self._find

    @property
    def db(self) -> SQLAlchemy:
        return db

    def _set_data(self, entity, **data):
        cls = entity.__class__
        allowed = cls.updatable_params()
        return self.update_entity(entity, data, allowed=allowed)

    @property
    def db_session(self):
        return self.db.session

    @property
    def _flask_app(self) -> flask.Flask:
        return flask.current_app

    @property
    def _config(self) -> flask.Config:
        return self._flask_app.config

    def query_find_all(self, **kwargs) -> BaseQuery:
        query: BaseQuery = self._entity_class.query
        if kwargs:
            kw_subset = set(kwargs.keys())
            col_subset = self._entity_class.BASE_PARAMS
            intersect = kw_subset.intersection(col_subset)
            if intersect:
                params = {key: kwargs[key] for key in intersect}
                query = query.filter(**params)
        return query

    def find_all(self, *args, **kwargs):
        log.debug(f"[GET] Find all {self._entity_class.__name__}: {args} {kwargs}")
        return self.query_find_all(**kwargs).all()

    def delete(self):
        log.info(f"[DELETE] {self._entity_class.__name__} "
                 f"{self._entity.log_name}")
        self.delete_entity(self._entity)

    def update(self, **data):
        """Updates Entity

        Args:
            data(dict): Dictionary containing data which should be changed

        Returns: Updated instance instance
        """
        self._set_data(self._entity, **data)
        log.info(f"[UPDATE] {self._entity_class.__name__} {self._entity.log_name}: {self._entity}.")
        return self._entity

    def update_entity(self, entity, data: dict, allowed: list, write: bool = True):
        """Updates any entity.

           Args:
               entity: Entity that should be updated
               data(dict): Data that should be used to update entity
               allowed(list): List of allowed attributes
               write(bool): if True writes the entity into the database

           Returns: Updated entity
           """
        for param in allowed:
            if hasattr(entity, param) and param in data.keys():
                setattr(entity, param, data[param])

        if write:
            self.write_entity(entity=entity)
        return entity

    def write_entity(self, entity):
        """Writes an entity to the database.

        Args:
            entity(db.Model): Database entity instance
        """
        log.trace(f"[WRITE] Entity {entity.__class__.__name__}: {entity}")
        self.db_session.add(entity)
        self.db_session.commit()
        return entity

    def delete_entity(self, entity):
        """Deletes an entity from the database.

        Args:
            entity(db.Model): Database entity instance
        """
        log.trace(f"[DELETE] Entity {entity.__class__.__name__}: {entity}")
        self.db_session.delete(entity)
        self.db.session.commit()

    def refresh(self, entity) -> object:
        """Refreshes the object
        Args:
            entity: Any db model
        Returns: Instance of the same object

        """
        log.trace(f"[REFRESH] Entity {entity.__class__.__name__}: {entity}")
        self.db.session.refresh(entity)
        return entity

    def expire(self, entity) -> object:
        """Expires the object attributes
        Args:
            entity: Any db model
        Returns: Instance of the same object
        """
        log.trace(f"[EXPIRE] Entity {entity.__class__.__name__}: {entity}")
        self.db_session.expire(entity)
        return entity


def get_new_name(source, target: Course, count_func=None) -> str:
    """Gets new name based on the count
    Args:
        source: Source entity
        target(course): Target entity
        count_func: Count func
    Returns(str): New name with suffix

    """

    def __count_general(target_course, name):
        klass = source.__class__
        query = klass.query.filter_by(course=target_course).filter(
            klass.codename.like(f"%{name}%"))
        return sql_get_count(query)

    count_func = count_func or __count_general
    new_name = source.codename
    number = count_func(target, new_name)
    if number > 0:
        new_name += f"_{number + 1}"
    return new_name

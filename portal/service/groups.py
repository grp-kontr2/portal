"""
Groups service
"""

import logging
from typing import List

from portal.database.models import Course, Group, Project, User
from portal.service.errors import PortalAPIError
from portal.service.general import GeneralService, get_new_name

log = logging.getLogger(__name__)


class GroupService(GeneralService):
    @property
    def _entity_class(self):
        return Group

    @property
    def group(self) -> Group:
        return self._entity

    def copy_group(self, target: Course, with_users: str):
        """Copies group to a specified course

        Args:
            target(Course): Target course instance
            with_users(str): Whether it should copy with users

        Returns: the new group
        """
        source = self.group
        log.info(f"[COPY] Group: {source.log_name} to course {target.log_name}"
                 f"with config: {with_users}")
        new_name = get_new_name(source, target)
        new_group = Group(target, codename=new_name)
        new_group.description = source.description
        new_group.name = source.name
        if with_users == "with_users":
            for user in source.users:
                user.groups.append(new_group)
        return new_group

    def create(self, course: Course, **data) -> Course:
        """Creates a new group

        Args:
            course(Course): Course instance

        Keyword Args:
            name(str): Name of the group
            description(str): Description of the group
            codename(str): Codename of the group

          Returns(Group): Created group instance
          """
        new_group = Group(course=course)
        self._entity = new_group
        self._set_data(new_group, **data)
        log.info(f"[CREATE] New group {new_group.log_name}: {new_group}")
        return new_group

    def update_membership(self, add: list = None, remove: list = None):
        """Updates the list of members of the group
        """
        from portal.service.users import UserService
        users_old = set(self.group.users)
        users_add = add
        users_remove = remove
        users = set()
        if users_add:
            to_add = UserService().find_users(users_add)
            users = users_old.union(to_add)
        if users_remove:
            to_remove = UserService().find_users(users_remove)
            users = users_old.difference(to_remove)
        self.group.users = list(users)
        self.write_entity(self.group)
        log.info(f"[UPDATE] Users of group {self.group.log_name} "
                 f" to {self.group.users}.")

    def find_users_by_role(self, course: Course = None, role_id=None) -> List[User]:
        """Finds users in the group by role

        Args:
            course(Course): Course instance
            role_id(str): Group id

        Returns(list[User]): List of the users
        """
        course = course or self.group.course
        role = self.find.role(course, role_id) if role_id else None
        return self.group.get_users_based_on_role(role=role)

    def add_user(self, user: User) -> Group:
        """Adds a single user to the group

        Args:
            user(User): User instance

        Returns(Group): updated group instance
        """
        # TODO: Check that user belongs to Course using the Role relationship
        if user not in self.group.users:
            self.group.users.append(user)
            self.write_entity(self.group)
            log.info(
                f"[ADD] Added user {user.log_name} to group {self.group.log_name} ")
        else:
            log.info(f"[ADD] User {user.log_name} is already in group {self.group.log_name}: "
                     f"no change.")
        return self.group

    def remove_user(self, user: User) -> Group:
        """Removes single user from the group

       Args:
           user(User): User instance

       Returns(Group): updated group instance
        """
        try:
            self.group.users.remove(user)
            self.write_entity(self.group)
        except ValueError:
            message = f"[REMOVE] Could not remove user " \
                f"{user.log_name} from group {self.group.log_name}: " \
                f"group does not contain user."
            raise PortalAPIError(400, message=message)
        log.info(f"[REMOVE] User {user.log_name} removed from group "
                 f"{self.group.log_name}")
        return self.group

    def import_group(self, data: dict, course: Course = None) -> Group:
        """Imports Group from a course

        Args:
            data(dict): import configuration
            course(Course): Course instance
        """
        course = course or self.group.course
        source_course = self.find.course(data['source_course'])
        source_group = self.find.group(source_course, data['source_group'])
        self._entity = source_group
        new_group = self.copy_group(course, data['with_users'])
        self.write_entity(new_group)
        log.info(f"[IMPORT] Group {source_group.log_name} to course {course.log_name}"
                 f" as {new_group.log_name}")
        return new_group

    def find_all(self, course: Course, **kwargs) -> List[Group]:
        """List all groups
        Args:
            course(Course): Course instance

        Returns(list): List of all groups
        """
        return self.query_find_all(**kwargs).filter(Group.course == course).all()

    def remove_project(self, project: Project) -> Group:
        """Removes projects from the group
        Args:
            project(Project): Project instance
        Returns:
        """

        try:
            self.group.projects.remove(project)
            self.write_entity(self.group)
        except ValueError:
            message = f"[REMOVE] Could not remove project " \
                f"{project.log_name} from group {self.group.log_name}: " \
                f"group does not contain project."
            raise PortalAPIError(400, message=message)
        log.info(f"[REMOVE] Project {project.log_name} from group"
                 f" {self.group.log_name}")
        return self.group

    def add_project(self, project: Project) -> Group:
        """Adds project to the group
        Args:
=            project(Project): Project instance

        Returns:
        """
        if project not in self.group.projects:
            self.group.projects.append(project)
            self.write_entity(self.group)
            log.info(f"[ADD] Added project {project.log_name} to group {self.group.log_name} ")
        else:
            log.info(f"[ADD] Project {project.log_name} is already in group {self.group.log_name}: "
                     f"no change.")
        return self.group

    def find_projects(self) -> List[Project]:
        """Lists all the projects in the group
        Returns:
        """
        return self.group.projects

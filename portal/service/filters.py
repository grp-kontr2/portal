"""
Filters for the services
"""

import logging

from portal.database.models import Client, Course, User
from portal.database import ProjectState
from portal.tools import time

log = logging.getLogger(__name__)


def filter_projects_from_course(course: Course, projects: list = None,
                                user: User = None,
                                state: ProjectState = ProjectState.ACTIVE) -> list:
    """Filters projects for the course based on their state.

    Args:
        user(User): User instance
        state(ProjectState): State the project must be in to be included in the result.
            Defaults to ACTIVE.
        projects(list): Project list
        course(Course): Course instance

    Returns(list): Filtered list of  projects
    """

    def is_active(project): return project.state == state

    filtered = course.projects
    if user:
        filtered = user.get_projects_by_course(course=course)

    if projects:
        active_projects_id = [p.id for p in filtered if is_active(p)]
        projects = [p for p in projects if p['id'] in active_projects_id]
    else:
        projects = [p for p in filtered if is_active(p)]
    return projects


def filter_roles_from_course(
        course: Course, roles: list = None, client: Client = None) -> list:
    """Gets roles in a course the user has

    Args:
        roles(list): List of roles to filter
        course(Course): Course instance
        client(Client): Client instance

    Returns(list): Filtered list of roles
    """
    client_roles = client.get_roles_in_course(course=course)
    if roles:
        client_roles_id = [role.id for role in client_roles]
        return [r for r in roles if r['id'] in client_roles_id]
    return client_roles


def filter_groups_from_course(
        course: Course, groups: list = None, user: User = None) -> list:
    """Filters groups of a course. Returns only groups the user is part of.

    Args:
        groups(list): List of groups to filter
        course(Course): Course instance
        user(User): User instance

    Returns(list): Filtered list of groups
    """
    user_groups = user.get_groups_in_course(course=course)
    if groups:
        user_groups_id = [g.id for g in user_groups]
        return [g for g in groups if g['id'] in user_groups_id]

    return user_groups


def filter_course_dump(course: Course, dump: dict, user: User) -> dict:
    """Filters course dump
    Args:
        course(Course): Course instance
        dump(dict): Dump
        user(User): User instance

    Returns(dict): Filtered dump

    """
    log.debug(f"[FILTER] Course: {dump}")
    filtered_dump = {
        'projects': filter_projects_from_course(projects=dump['projects'], course=course,
                                                user=user),
        'roles': filter_roles_from_course(roles=dump['roles'], course=course, client=user),
        'groups': filter_groups_from_course(groups=dump['groups'], course=course, user=user)
    }
    return {**dump, **filtered_dump}

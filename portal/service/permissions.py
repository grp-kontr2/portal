"""
Permissions service
"""

import logging
from typing import Union

from portal.database import queries
from portal.database.models import Course, Group, Role, Submission, \
    User, Worker
from portal.database.enums import ClientType
from portal.service.errors import ForbiddenError
from portal.service.general import GeneralService

FILTER_PERMISSION_ATTRS = [
    'id', 'course_id', 'clients', 'course',
    'description', 'created_at', 'updated_at']

log = logging.getLogger(__name__)


def _resolve_permissions(permissions_required_all: list, effective_permissions: dict):
    if isinstance(permissions_required_all, str):
        permissions_required_all = [permissions_required_all]
    return all(
        effective_permissions.get(permission, False) for permission in permissions_required_all)


class PermissionServiceCheck:
    def __init__(self, service: 'PermissionsService'):
        self.service = service

    def sysadmin_or_self(self, eid):
        return self.any_check(self.sysadmin(), self.service.client.is_self(eid))

    def sysadmin(self) -> bool:
        if self.service.client.type == ClientType.USER:
            return self.service.client.is_admin
        return False

    def permissions(self, permissions) -> bool:
        client = self.service.client
        log.trace(f"[PERM] Client {client.log_name} in course "
                  f"\"{self.service.client_name}\": {permissions}")
        if self.sysadmin():
            return True

        effective_permissions = self.service.effective_permissions_for_course()
        if not effective_permissions:
            return False

        res = any(_resolve_permissions(conjunction, effective_permissions) for conjunction in
                  permissions)

        log.debug(f"[PERM] {permissions} for {client.log_name}: {res}")
        return res

    def any_check(self, *params) -> bool:
        return any(params)

    def view_course_full(self) -> bool:
        return self.permissions(['view_course_full'])

    def view_course_limited(self) -> bool:
        return self.permissions(['view_course_limited'])

    def view_course_any(self) -> bool:
        return self.permissions(['view_course_limited', 'view_course_full'])

    def read_submissions_all(self) -> bool:
        result = self.permissions(['read_submissions_all'])
        log.debug(f"[TRY] Read sub all for {self.service.submission} "
                  f"by {self.service.auth.client.log_name}: {result}")
        return result

    def read_submissions_group(self, submission: Submission = None) -> bool:
        submission = submission or self.service.submission
        result = self.read_submissions_all() or self.service.submission_access_group(submission, [
            'read_submissions_groups'])
        log.debug(f"[TRY] Read sub group for {submission.log_name} "
                  f"by {self.service.client_name}: {result}")
        return result

    def read_submissions_own(self, submission: Submission = None) -> bool:
        submission = submission or self.service.submission
        result = self.read_submissions_all() or self.permissions(
            ['read_submissions_own']) and submission.user == self.service.client_owner
        log.debug(f"[TRY] Read Sub Own for {submission.log_name} "
                  f"by {self.service.client_name}: {result}")
        return result

    def read_submission(self, submission=None):
        submission = submission or self.service.submission
        checks = [
            self.read_submissions_group(submission),
            self.read_submissions_own(submission=submission)
        ]
        return self.any_check(*checks)

    def create_submission(self, user):
        if user != self.service.client:
            self.permissions(['create_submissions_other'])

        from portal.service.services_collection import ServicesCollection
        user_service = ServicesCollection().permissions(course=self.service.course, client=user)
        return user_service.check.permissions(['create_submissions'])

    def create_submission_other(self):
        return self.permissions(['create_submissions_other'])

    def evaluate_submissions(self):
        return self.permissions(['evaluate_submissions'])


class PermissionServiceRequire:
    def __init__(self, service: 'PermissionsService'):
        self.service = service

    @property
    def _check(self) -> PermissionServiceCheck:
        return self.service.check

    def sysadmin_or_self(self, eid):
        self.any_check(self._check.sysadmin_or_self(eid))

    def sysadmin(self):
        self.any_check(self._check.sysadmin())

    def permissions(self, permissions):
        self.any_check(self._check.permissions(permissions))

    def any_check(self, *checks):
        if not self._check.any_check(*checks):
            raise ForbiddenError(self.service.client)

    def update_course(self):
        self.permissions(['update_course'])

    def write_projects(self):
        self.permissions(['update_course', 'write_projects'])

    def write_roles(self):
        self.permissions(['update_course', 'write_roles'])

    def write_groups(self):
        self.permissions(['update_course', 'write_groups'])

    def view_course(self):
        self._check.view_course_any()

    def view_course_full(self):
        self.permissions(['update_course', 'view_course_full'])

    def belongs_to_group(self, group):
        checks = [
            self._check.view_course_full(),
            (self._check.view_course_limited()
             and self.service.client_owner in group.users)
        ]
        self.any_check(*checks)

    def belongs_to_role(self, role: Role):
        checks = [
            self._check.view_course_full(),
            (self._check.view_course_limited()
             and self.service.client_owner in role.clients)
        ]
        self.any_check(*checks)

    def evaluate_submissions(self):
        self.permissions(['evaluate_submissions'])

    def read_submission(self, submission=None):
        submission = submission or self.service.submission
        checks = [
            self._check.read_submissions_group(submission),
            self._check.read_submissions_own(submission=submission)
        ]
        self.any_check(*checks)

    def read_submission_group(self, submission=None):
        submission = submission or self.service.submission
        checks = [
            self._check.read_submissions_group(submission=submission)
        ]
        self.any_check(*checks)

    def write_review_for_submission(self, submission):
        checks = [
            self._check.permissions(['write_reviews_all']),
            self.service.submission_access_group(submission, ['write_reviews_group']),
            (self._check.permissions(['write_reviews_own']) and
             submission.user == self.service.client_owner)
        ]
        self.any_check(*checks)

    def create_submission(self, user):
        if user != self.service.client:
            self.permissions(['create_submissions_other'])

        from portal.service.services_collection import ServicesCollection
        user_service = ServicesCollection().permissions(course=self.service.course, client=user)
        user_service.require.permissions(['create_submissions'])

    def create_submission_other(self):
        return self.permissions(['create_submissions_other'])

    def course_access_token(self):
        self.permissions(['handle_notes_access_token'])


class PermissionsService(GeneralService):
    def _set_data(self, entity, data=None):
        pass

    def __call__(self, client=None, course=None, submission=None):
        self._client = client
        self._course = course
        if course is None and submission is not None:
            self._course = submission.course
        self._submission = submission
        return self

    @property
    def auth(self):
        from portal.service.auth import AuthService
        return AuthService()

    @property
    def client_name(self):
        if self.client:
            return self.client.log_name
        return 'Unknown client'

    @property
    def _entity_class(self):
        return Role

    def __init__(self, client=None, course=None, submission=None):
        super().__init__()
        self._requires = PermissionServiceRequire(self)
        self._checks = PermissionServiceCheck(self)
        self._client = client
        self._course = course
        self._submission = submission

    @property
    def require(self):
        return self._requires

    @property
    def check(self):
        return self._checks

    @property
    def client(self):
        return self._client or self.auth.client

    @property
    def client_owner(self) -> Union[Worker, User]:
        return self.find.client_owner(self.client)

    @property
    def course(self) -> Course:
        return self._course

    @property
    def submission(self) -> Submission:
        return self._submission

    def get_effective_permissions(self, course_id: str = None) -> dict:
        """Gets effective permissions for a course in course.
        If no course is specified, returns the client's
        effective permissions in all courses he is a part of.

        Args:
            course_id(str): Course ID

        Returns(dict): Effective permissions for the course. Keys are course IDs, values
        dictionaries of permissions with their values.
        """
        course = self.find.course(course_id, throws=False)
        courses = [course] if course else self.client.courses
        return self.build_effective_permissions(*courses)

    def build_effective_permissions(self, *courses) -> dict:
        """Builds effective permissions in a list of courses for the client

        Args:
            *courses: List of courses

        Returns(dict): Effective permissions
        """
        result = {}
        for course in courses:
            result[course.id] = self.effective_permissions_for_course(course)
        return result

    def effective_permissions_for_course(self, course: Course = None) -> dict:
        """Extracts effective permissions for a client in one course

        Returns(dict): Effective permissions dictionary
        """
        course = course or self.course
        query = queries.effective_permissions_for_course(client=self.client, course=course)
        result = query.first()
        result = dict(zip(Role.PERMISSIONS, result))
        log.trace(f"[PERM] Effective permissions for client {self.client_name} "
                  f"in course {course.log_name}: {result}")

        return result

    def submission_access_group(self, submission, perm):
        if not self.check.permissions(perm):
            return False
        # Filter groups that contains current user and submission users
        query = Group.query.join(Group.users).filter(
            Group.users.contains(submission.user) & Group.users.contains(self.client_owner)
        )
        # Filter Groups that also has active projects
        query = query.join(Group.projects).filter(Group.projects.contains(submission.project))
        return len(query.all()) > 0

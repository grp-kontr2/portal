"""
Components service
"""
import portal.tools.worker_client

from portal import logger
from portal.database.models import Worker
from portal.service.general import GeneralService

log = logger.get_logger(__name__)


class WorkerService(GeneralService):
    @property
    def _entity_class(self):
        return Worker

    @property
    def worker(self):
        return self._entity

    @property
    def worker_client(self) -> portal.tools.worker_client.WorkerClient:
        return portal.tools.worker_client.WorkerClient(worker=self.worker)

    def create(self, **data):
        """Create worker
        Keyword Args:
            name(str): Name of the submission
            url(str): Address where the worker is accessible
        """
        worker = Worker(name=data['name'], url=data['url'])
        self._entity = worker
        self._set_data(worker, **data)
        log.info(f"[CREATE] Worker {worker.log_name}: {data}")
        return worker

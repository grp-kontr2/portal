"""
Reviews service
"""

import logging
from typing import List

from portal.database.models import Review, ReviewItem, Submission, User
from portal.service.general import GeneralService

log = logging.getLogger(__name__)


class ReviewService(GeneralService):
    @property
    def _entity_class(self):
        return Review

    def _set_data(self, entity, **data):
        allowed = []
        return self.update_entity(entity, data, allowed=allowed)

    def _set_item_data(self, entity: ReviewItem, data):
        allowed = ReviewItem.updatable_params()
        return self.update_entity(entity=entity, data=data, allowed=allowed)

    def __call__(self, entity: Review = None, submission: Submission = None):
        super(ReviewService, self).__call__(entity=entity)
        self._submission = submission
        return self

    @property
    def review(self) -> Review:
        return self._entity or self.submission.review

    @property
    def submission(self):
        return self._submission or self._entity.submission

    def create_review_items(self, author: User, items: List[dict]) -> Review:
        """Creates review items for the review

        Args:
            author(User): Review's author
            items(List[dict]): Review items to append to the review

        Returns(Review): review instance with review items
        """
        for item in items:
            rw_item = ReviewItem(user=author, review=self.review)
            self._set_item_data(rw_item, item)

        log.info(f"Added review items {items} to review {self.review.id} "
                 f"for submission {self.submission.log_name}")
        self.write_entity(self.review)
        return self.review

    def delete(self, item: ReviewItem):
        log.info(f"Delete review item {item} to review {self.review.id} "
                 f"for submission {self.submission.log_name}")
        self.delete_entity(item)

    def create(self) -> Review:
        """Creates instance of the review

        Args:

        Returns(Review): Review instance
        """
        log.info(f"[CREATE] Review for submission: {self.submission.log_name}")
        review = Review(submission=self.submission)
        self.write_entity(review)
        return review

    def update_item(self, item: ReviewItem, **data) -> Review:
        """Update review item
        Args:
            item(ReviewItem): Review item instance
            **data: Provided data

        Returns(Review): Review instance

        """
        log.info(f"[UPDATE] Review item for submission {self.submission.log_name}: {data}")
        self._set_item_data(item, data=data)
        return item.review

"""
Submissions service
"""
import datetime
import json
import logging
from pathlib import Path
from typing import Union

from werkzeug.datastructures import FileStorage
from werkzeug.utils import secure_filename

from portal import storage_wrapper
from portal.async_celery.submission_processor import SubmissionProcessor
from portal.database import queries, SubmissionState
from portal.database.models import Project, Submission, User
from portal.service import errors
from portal.service.general import GeneralService

log = logging.getLogger(__name__)


class SubmissionsService(GeneralService):
    @classmethod
    def delete_cancelled_submissions(cls):
        one_day = datetime.timedelta(hours=24)
        query_filter = queries.cancelled_submissions_for_deletion(one_day)
        log.info(f"[DEL] Cancelled submissions for deletion "
                 f"({query_filter}): {query_filter.all()}")

        for submission in query_filter.all():
            SubmissionsService(submission).delete()

    @classmethod
    def archive_submissions(cls):
        query_filter = queries.submissions_finished().all()
        filtered = [submission for submission in query_filter if submission.project.is_archived]
        log.info(f"[ARCH] Archiving submissions "
                 f": {[s.log_name for s in filtered]}")
        for submission in filtered:
            SubmissionsService(submission).archive()

    @classmethod
    def get_upload_file_path(cls, file: FileStorage) -> str:
        if upload_file_is_allowed(file):
            path = upload_files_to_storage(file)
            return str(path)
        return None

    def processor(self) -> SubmissionProcessor:
        return SubmissionProcessor(self.submission)

    @property
    def _entity_class(self):
        return Submission

    def _set_data(self, entity, **data):
        allowed = []
        return self.update_entity(entity, data, allowed=allowed)

    @property
    def storage_results(self):
        return self.storage.results.get(self.submission.storage_dirname)

    def create(self, user: User = None, project: Project = None, submission_params: dict = None,
               allow_override=False) -> Submission:
        """Creates a new submission in the database and downloads its files.

          Zip and git sources are processed differently.

          Args:
              allow_override: Whether override is possible
              user(User): the submission's author
              project(Project): the project the submission falls under
              submission_params(dict): custom parameters specified by
                  project with their values as a JSON-encoded string

          Returns(Submission): the created submission entity

          """
        # adding review at submission create is not supported
        if not allow_override:
            submission_params['file_params'] = None
        submission_params = self.process_submission_params(submission_params, project, user=user)
        log.info(f"[SUBMIT] Create new submission for user {user.log_name} "
                 f"for project {project.log_name} "
                 f"with params: {submission_params}")
        new_submission = Submission(user=user, project=project, parameters=submission_params)
        self.write_entity(new_submission)
        self._entity = new_submission
        return new_submission

    @property
    def submission(self) -> Submission:
        return self._entity

    @property
    def project(self) -> Project:
        return self.submission.project

    @property
    def storage(self):
        return storage_wrapper

    def copy_submission(self, note: str) -> Submission:
        """Copies a submission. Used at resubmitting

        Args:
            note(str): Note for the submission. Typically contains a reason for the resubmit.

        Returns(Submission): Copied submission
        """
        log.info(f"[COPY] Submission {self.submission.log_name} to "
                 f"same project {self.submission.project.log_name}: {note}")
        new_submission = Submission(user=self.submission.user, project=self.project,
                                    parameters=self.submission.parameters)
        new_submission.note = note
        self.write_entity(new_submission)
        return new_submission

    def delete(self):
        """Deletes a submission

        """
        log.info(f"[DEL] Submission delete submission files: {self.submission.log_name}")
        st_entity = storage_wrapper.submissions.get(self.submission.storage_dirname)
        if st_entity:
            st_entity.delete()
        log.info(f"[DEL] Submission delete result files: {self.submission.log_name}")
        st_entity = storage_wrapper.results.get(self.submission.storage_dirname)
        if st_entity:
            st_entity.delete()
        super(SubmissionsService, self).delete()

    def update_submission_state(self, **data) -> Submission:
        """Updates submission's state

        Args:
            data(dict): should contain one key, 'state' with the new value

        Returns(Submission): Updated submission
        """
        new_state = data['state']

        return self.set_state(state=new_state, message='Manual state update')

    def cancel_submission(self):
        """Cancels the submission

        """
        self.processor().revoke_task()

    def upload_results_to_storage(self, path):
        processor = self.processor()
        file_params = dict(source=dict(url=path, type='zip'))
        processor.upload_result(path=path, file_params=file_params)
        submission = processor.process_result()
        SubmissionsService().write_entity(submission)

    def query_find_all(self, **kwargs):
        return queries.list_submissions_for_user(**kwargs)

    def process_submission_params(self, params: dict, project: Project, user: User):
        params['project_params'] = params.get('project_params') or {}
        params['file_params'] = params.get('file_params') or {}
        file_params: dict = params['file_params']

        SOURCE_TYPES = dict(git=self._process_git_submission_params,
                            zip=self._process_zip_subm_params)

        file_params['whitelist'] = project.config.file_whitelist
        file_params['source'] = file_params.get('source') or {}
        file_params['source']['type'] = file_params['source'].get('type') or 'git'
        if file_params['source']['type'] not in SOURCE_TYPES.keys():
            file_params['source']['type'] = 'git'

        params = SOURCE_TYPES[file_params['source']['type']](params, project, user)
        log.debug(f"[PROC] Processed submission params: {params}")
        return params

    def _process_zip_subm_params(self, params, project: Project, user: User):
        return params

    def _process_git_submission_params(self, params, project: Project, user: User):
        file_params = params['file_params']
        source = file_params['source']
        source['url'] = source.get('url') or self.__get_default_git_url(project, user)
        file_params['from_dir'] = params.get('from_dir') or project.codename
        return params

    def __get_default_git_url(self, project: Project, user: User):
        git_base = self._config.get('GIT_REPO_BASE', 'git@gitlab.fi.muni.cz')
        return f"{git_base}:{user.gitlab_username}/{project.course.codename}.git"

    def get_stats(self):
        """Gets the statistics for the submission
        Returns: JSON representation
        """
        results = self.storage_results
        if not results.path.exists():
            raise errors.ResultsAreNotUploaded(self.submission)
        stats = results.get('suite-stats.json')
        if not stats.exists():
            raise errors.SuiteStatsNotExists(self.submission)
        return json.loads(stats.read_text('utf-8'))

    def archive(self):
        log.info(f"[ARCHIVE] Submission archive submission files: {self.submission.log_name}")
        st_entity = storage_wrapper.submissions.get(self.submission.storage_dirname)
        if st_entity:
            st_entity.clean()
        log.info(f"[ARCHIVE] Submission archive result files: {self.submission.log_name}")
        st_entity = storage_wrapper.results.get(self.submission.storage_dirname)
        if st_entity:
            st_entity.clean()

    def set_state(self, state: Union[str, SubmissionState], message: str = None):
        if isinstance(state, str):
            state = SubmissionState[state]

        if self.submission.change_state(state, message=message):
            self.write_entity(self.submission)
            log.info(f"[UPDATE] Submission state {self.submission.log_name} "
                     f"to {self.submission.state}")
        else:
            log.warning("[UPDATE] Submission state change is not possible")
        return self.submission


def nonempty_intersection(provided: list, required: list):
    if not required:
        return True
    return list(set(provided) & set(required))


def upload_files_to_storage(file: FileStorage):
    filename = secure_filename(file.filename)
    uploads = storage_wrapper.results.workspace_path / 'uploads'
    if not uploads.exists():
        uploads.mkdir(parents=True)
    path = uploads / filename
    file.save(str(path))
    log.info(f"[UPLOAD] Uploading file to {filename} to {path}")
    return path


def upload_file_is_allowed(file, throws: bool = True):
    extension = Path(file.filename).suffix
    log.debug(f"[ZIP] Extension for {file.filename}: {extension}")
    if not extension == '.zip':
        if throws:
            message = f"File with extension \"{extension}\" is not allowed: {file.filename}"
            raise errors.PortalAPIError(400, message)
        return False
    return True

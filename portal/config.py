"""
Main Configuration module
Defines all the configuration types for the Flask Application
Types:
    - Development
    - Production
    - Test
"""

import datetime
import os
import tempfile

from portal import logger
from portal.tools import paths, s2b_env

log = logger.get_logger(__name__)


# pylint: disable=too-few-public-methods

class Config(object):
    """Base configuration
    """
    ENV = os.getenv('ENV', 'development')
    FLASK_ENV = os.getenv('FLASK_ENV', ENV)

    # Base dirs
    PROJECT_ROOT = paths.ROOT_DIR
    APP_DIR = paths.ROOT_DIR / 'portal'

    # FRONTEND
    FRONTEND_URL = os.getenv('FRONTEND_URL', 'http://localhost:4200')
    FRONTEND_LOCAL_REPO = os.getenv('FRONTEND_LOCAL_REPO', PROJECT_ROOT / '..' / 'portal-frontend')

    # Secrets and JWT Config
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'you-will-never-guess'
    JWT_SECRET_KEY = os.environ.get('JWT_SECRET_KEY') or 'super-safe'
    JWT_REFRESH_TOKEN_EXPIRES = datetime.timedelta(days=30)
    JWT_ACCESS_TOKEN_EXPIRES = datetime.timedelta(hours=1)

    # CORS Config
    CORS_SUPPORT_CREDENTIALS = True
    CORS_ORIGINS = '*'

    # API config
    API_PREFIX = os.environ.get('PORTAL_API_PREFIX', default="/api/v1.0/")

    # Celery config
    BROKER_URL = os.getenv('CELERY_BROKER_URL', 'redis://localhost:6379/0')
    CELERY_RESULT_BACKEND = os.getenv('CELERY_RESULT_BACKEND', BROKER_URL)

    # Database config
    SQLALCHEMY_DATABASE_URI = os.getenv('SQLALCHEMY_DATABASE_URI',
                                        f"sqlite:///{paths.ROOT_DIR}/devel.db")
    # Worker config
    PORTAL_DATA_WORKER_DOMAIN = os.getenv('PORTAL_DATA_WORKER_DOMAIN', 'localhost')
    LDAP_URL = os.getenv('LDAP_URL', None)

    # Gitlab config
    GITLAB_BASE_DOMAIN = os.getenv('GITLAB_BASE_DOMAIN', "gitlab.fi.muni.cz")
    GITLAB_KONTR_TOKEN = os.getenv('GITLAB_KONTR_TOKEN')
    GITLAB_KONTR_USERNAME = os.getenv('GITLAB_KONTR_USERNAME')
    GIT_REPO_BASE = os.getenv('GIT_REPO_BASE', f"git@{GITLAB_BASE_DOMAIN}")
    GITLAB_URL = os.getenv('GITLAB_URL', None)

    # Logging config
    LOG_LEVEL_GLOBAL = os.getenv('LOG_LEVEL_GLOBAL', 'INFO')
    LOG_LEVEL_FILE = os.getenv('LOG_LEVEL_FILE', LOG_LEVEL_GLOBAL)
    LOG_LEVEL_CONSOLE = os.getenv('LOG_LEVEL_CONSOLE', LOG_LEVEL_GLOBAL)

    # Emails configuration
    EMAIL_ENABLED = s2b_env('PORTAL_EMAIL_ENABLED', 'false')
    EMAIL_HOST = os.getenv('PORTAL_EMAIL_HOST', 'localhost')
    EMAIL_PORT = os.getenv('PORTAL_EMAIL_PORT', '1025')
    EMAIL_DEFAULT_FROM = os.getenv('PORTAL_EMAIL_DEFAULT_FROM', 'kontr.fi.muni.cz')

    # Storage config
    PORTAL_STORAGE_BASE_DIR = os.getenv('PORTAL_STORAGE_BASE_DIR',
                                        f"{tempfile.gettempdir()}/kontr-portal")
    PORTAL_STORAGE_TEST_FILES_DIR = os.getenv('PORTAL_STORAGE_TEST_FILES_DIR',
                                              f"{PORTAL_STORAGE_BASE_DIR}/test-files")
    PORTAL_STORAGE_WORKSPACE_DIR = os.getenv('PORTAL_STORAGE_WORKSPACE_DIR',
                                             f"{PORTAL_STORAGE_BASE_DIR}/workspace")
    PORTAL_STORAGE_SUBMISSIONS_DIR = os.getenv('PORTAL_STORAGE_SUBMISSIONS_DIR',
                                               f"{PORTAL_STORAGE_BASE_DIR}/submissions")
    PORTAL_STORAGE_RESULTS_DIR = os.getenv('PORTAL_STORAGE_RESULTS_DIR',
                                           f"{PORTAL_STORAGE_BASE_DIR}/results")
    UPLOAD_FOLDER = os.getenv('PORTAL_UPLOAD_FOLDER', f"{PORTAL_STORAGE_BASE_DIR}/upload")
    GITPYTHON_CUSTOM_SSH_KEY = os.getenv('GITPYTHON_CUSTOM_SSH_KEY', None)

    # IS MUNI NOTES Integration
    IS_API_DOMAIN = os.getenv('IS_API_DOMAIN', None)


class DevelopmentConfig(Config):
    """Development configuration
    """
    SQLALCHEMY_DATABASE_URI = os.getenv('SQLALCHEMY_DATABASE_URI',
                                        f"sqlite:///{paths.ROOT_DIR}/devel.db")
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    PORTAL_ENV = 'dev'
    DEBUG = True


class ProductionConfig(Config):
    """Production configuration
    """
    DEBUG = False
    TESTING = False
    LOG_LEVEL_GLOBAL = os.getenv('LOG_LEVEL_GLOBAL', 'INFO')
    LOG_LEVEL_FILE = os.getenv('LOG_LEVEL_FILE', LOG_LEVEL_GLOBAL)
    LOG_LEVEL_CONSOLE = os.getenv('LOG_LEVEL_CONSOLE', LOG_LEVEL_GLOBAL)
    FLASK_ENV = os.getenv('FLASK_ENV', 'production')


class TestConfig(Config):
    """Test configuration
    """
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    TESTING = True
    ENV = 'development'
    FLASK_ENV = ENV
    DEBUG = False
    SQLALCHEMY_DATABASE_URI = 'sqlite://'
    # SQLALCHEMY_ECHO = True
    PORTAL_ENV = 'test'
    PORTAL_STORAGE_BASE_DIR = tempfile.mkdtemp()
    PORTAL_STORAGE_TEST_FILES_DIR = f"{PORTAL_STORAGE_BASE_DIR}/test-files"
    PORTAL_STORAGE_WORKSPACE_DIR = f"{PORTAL_STORAGE_BASE_DIR}/workspace"
    PORTAL_STORAGE_SUBMISSIONS_DIR = f"{PORTAL_STORAGE_BASE_DIR}/submissions"
    PORTAL_STORAGE_RESULTS_DIR = f"{PORTAL_STORAGE_BASE_DIR}/results"
    EMAIL_BACKEND = 'tests.utils.email_backend.EmailBackend'
    EMAIL_HOST = "localhost"
    EMAIL_PORT = "1025"
    EMAIL_DEFAULT_FROM = "kontr@example.com"
    BROKER_URL = 'redis://'
    CELERY_RESULT_BACKEND = BROKER_URL
    LOG_LEVEL_GLOBAL = 'DEBUG'
    LOG_LEVEL_FILE = LOG_LEVEL_GLOBAL
    LOG_LEVEL_CONSOLE = LOG_LEVEL_GLOBAL

    GITLAB_BASE_DOMAIN = 'gitlab.local'
    GITLAB_KONTR_TOKEN = os.getenv('GITLAB_KONTR_TOKEN', 'kontr-private-token')
    GITLAB_KONTR_USERNAME = os.getenv('GITLAB_KONTR_USERNAME', 'admin')
    GIT_REPO_BASE = os.getenv('GIT_REPO_BASE', f"git@{GITLAB_BASE_DOMAIN}")
    GITLAB_URL = f'https://{GITLAB_BASE_DOMAIN}'
    IS_API_DOMAIN = 'is.muni.local'


# pylint: enable=too-few-public-methods


CONFIGURATIONS = dict(
    dev=DevelopmentConfig(),
    test=TestConfig(),
    production=ProductionConfig()
)

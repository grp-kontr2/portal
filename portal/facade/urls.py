import logging

import flask

from portal.database import Submission

log = logging.getLogger(__name__)


class UrlService:
    @property
    def flask_app(self) -> flask.Flask:
        return flask.current_app

    @property
    def frontend_url(self) -> str:
        return self.flask_app.config.get('FRONTEND_URL')

    def submission(self, submission: Submission):
        url = self.frontend_url + f"/submissions/{submission.id}"
        return url

    def submission_review(self, submission: Submission):
        return self.submission(submission) + "/review"

import logging

from portal.database import Worker
from portal.facade.general_facade import GeneralCRUDFacade
from portal.service.workers import WorkerService

log = logging.getLogger(__name__)


class WorkersFacade(GeneralCRUDFacade):
    def __init__(self):
        super().__init__(WorkerService, 'Worker')

    def get_worker_status(self, worker: Worker):
        """Gets current worker status
        Args:
            worker: Worker instance
        """
        return self.services.workers(worker).worker_client.status()

    def list_images_available(self, worker: Worker):
        """List all images available on the worker
        Args:
            worker: Worker instance
        """
        return self.services.workers(worker).list_images()

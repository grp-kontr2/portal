import logging

from werkzeug.datastructures import FileStorage

from portal.async_celery import tasks
from portal.database import Project, Submission, SubmissionState, User
from portal.facade.general_facade import GeneralCRUDFacade
from portal.logger import SUBMIT
from portal.service import errors
from portal.service.submissions import SubmissionsService

log = logging.getLogger(__name__)


class SubmissionsFacade(GeneralCRUDFacade):
    def __init__(self):
        super().__init__(SubmissionsService, 'Submission')

    def create(self, project, user, evaluate=True, **data):
        from portal.tools import request_helpers
        SUBMIT.info(f'[SUBMIT] Submission for project: "{project.log_name}", '
                    f'user: "{user.log_name}", created: "{self.client_name}", '
                    f'params: {data}, UA: {self._request.user_agent} '
                    f'({request_helpers.get_ip()})')
        user = user or self.client
        allow_override = self.permissions(course=project.course).check.create_submission_other()
        created_submission: Submission = super(SubmissionsFacade, self) \
            .create(user=user, project=project, allow_override=allow_override, **data)
        if evaluate:
            log.info(f"[SUBMIT] Evaluation of submission: {created_submission.log_name}")
            tasks.process_submission.delay(created_submission.id)
        return created_submission

    def get_source_files(self, submission: Submission):
        service = self.services.storage(submission=submission)
        path = self._request.args.get('path')
        return service.send_file_or_zip(path=path)

    def get_result_files(self, submission: Submission):
        service = self.services.storage(submission)
        storage_entity = self.storage.results.get(submission.storage_dirname)
        path = self._request.args.get('path')
        return service.send_file_or_zip(path=path, storage_entity=storage_entity)

    def get_test_files(self, submission: Submission):
        service = self.services.storage(submission)
        path = self._request.args.get('path')
        storage_entity = service.get_test_files_entity_from_storage()
        return service.send_file_or_zip(storage_entity=storage_entity, path=path)

    def result_files_tree(self, submission: Submission):
        storage_entity = self.storage.results.get(submission.storage_dirname)
        service = self.services.storage(submission)
        return service.send_files_tree(storage_entity)

    def test_files_tree(self, submission: Submission):
        service = self.services.storage(project=submission.project)
        storage_entity = service.get_test_files_entity_from_storage()
        return service.send_files_tree(storage_entity)

    def update_submission_state(self, submission: Submission, **data):
        log.info(f"[UPDATE] Submission state {submission.log_name} "
                 f"by {self.client_name}: {data}")

        new_state = data.get('state')
        self.normal_user_can_only_cancel(new_state)
        return self._service(submission).update_submission_state(**data)

    def normal_user_can_only_cancel(self, new_state):
        if isinstance(self.client_name, User) and \
                new_state != SubmissionState.CANCELLED:
            raise errors.ForbiddenError(self.client,
                                        note=f"User {self.client.log_name} cannot update "
                                        f"state to other than CANCELLED.")

    def get_stats(self, submission: Submission):
        return self._service(submission).get_stats()

    def send_sources_tree(self, submission: Submission):
        service = self.services.storage(submission)
        return service.send_files_tree()

    def upload_results_to_storage(self, submission: Submission):
        log.info(f"[UPLOAD] Uploading results to storage for "
                 f"{submission.log_name} by {self.client_name}")
        file: FileStorage = self._request.files['file']
        submission_service: SubmissionsService = self._service(submission)
        path = submission_service.get_upload_file_path(file)
        return tasks.upload_results_to_storage.delay(submission.id, path)

    def copy_submission(self, source_submission: Submission, **params):
        """Copies a submission. Used at resubmitting

        Args:
            source_submission: Source submission instance

        Returns(Submission): Copied submission
        """
        log.info(f"[COPY] Submission {source_submission.log_name} to "
                 f"same project {source_submission.project.log_name} by {self.client_name}")
        submission = self._service(source_submission).copy_submission(**params)
        tasks.clone_submission_files.delay(source_id=source_submission.id,
                                           target_id=submission.id)
        return submission

    def resend_submission(self, submission: Submission):
        """ Resends the submission to the worker
        """
        log.info(f"[SUBMIT] Resending submission task for {submission.log_name}")
        return tasks.start_processing_submission.delay(submission.id)

    def cancel_submission(self, submission: Submission):
        log.info(f"[CANCEL] Cancelling the submission {submission.log_name} "
                 f"by {self.client_name}")
        self._service(submission).cancel_submission()
        tasks.delete_submission.delay(submission.id)
        return

    def find_all(self, *args, **kwargs):
        role_ids = self._request.args.getlist('roles')
        group_ids = self._request.args.getlist('groups')
        project_ids = self._request.args.getlist('projects')
        states = self._request.args.getlist('states')
        user_ids = self._request.args.getlist('users')
        course = self._request.args.get('course')
        return super(SubmissionsFacade, self).find_all(*args, **kwargs, client=self.client,
                                                       course_id=course, states=states,
                                                       user_ids=user_ids, project_ids=project_ids,
                                                       role_ids=role_ids, group_ids=group_ids)

    def is_read_notepad(self, submission: Submission = None,
                        project: Project = None, user: User = None):
        """Read IS MUNI notepad for the author's submission
        Args:
            user(User): User instance
            project(Project): project instance
            submission(Submission): Submission instance
        """
        log.debug(f"[IS] Reading notepad content for {submission.log_name} "
                  f"by {self.client_name}")

        if submission is not None:
            project = project or submission.project
            user = user or submission.user

        note = self._services.is_api(submission.course).read_note(project=project, user=user)
        return note

    def is_write_notepad(self, submission: Submission, data: dict = None):
        """Write IS MUNI notepad content for the submission
        Args:
            data: Notepad data
            submission(Submission): Submission instance
        """
        log.info(f"[IS] Writing notepad content for {submission.log_name} "
                 f"by {self.client_name}: {data}")
        content = data.get('content') if isinstance(data, dict) else data
        response = self._services.is_api(submission.course) \
            .write_note(project=submission.project, user=submission.user, content=content)
        self._submission_email(context=dict(content=content), submission=submission)
        return response

    def _submission_email(self, context, submission):
        self._services.emails.notify_user(
            user=submission.user,
            template='is_notepad_updated',
            context=context,
            submission=submission,
            submission_url=self.urls.submission(submission=submission)
        )

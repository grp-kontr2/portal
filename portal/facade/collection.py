from portal import facade


class FacadesCollection:
    _collection = None

    @classmethod
    def get(cls) -> 'FacadesCollection':
        if cls._collection is None:
            cls._collection = cls()
        return cls._collection

    def __init__(self):
        self._reviews = facade.ReviewsFacade()
        self._users = facade.UsersFacade()
        self._courses = facade.CoursesFacade()
        self._roles = facade.RolesFacade()
        self._groups = facade.GroupsFacade()
        self._secrets = facade.SecretsFacade()
        self._workers = facade.WorkersFacade()
        self._submissions = facade.SubmissionsFacade()
        self._projects = facade.ProjectsFacade()
        self._gitlab = facade.GitlabFacade()

    @property
    def users(self) -> facade.UsersFacade:
        return self._users

    @property
    def courses(self) -> facade.CoursesFacade:
        return self._courses

    @property
    def groups(self) -> facade.GroupsFacade:
        return self._groups

    @property
    def roles(self) -> facade.RolesFacade:
        return self._roles

    @property
    def secrets(self) -> facade.SecretsFacade:
        return self._secrets

    @property
    def submissions(self) -> facade.SubmissionsFacade:
        return self._submissions

    @property
    def projects(self) -> facade.ProjectsFacade:
        return self._projects

    @property
    def workers(self) -> facade.WorkersFacade:
        return self._workers

    @property
    def reviews(self) -> facade.ReviewsFacade:
        return self._reviews

    @property
    def gitlab(self) -> facade.GitlabFacade:
        return self._gitlab
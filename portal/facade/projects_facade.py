import logging
from typing import Dict

from gitlab.v4 import objects

from portal.async_celery import tasks
from portal.database import Project
from portal.database.models import Client, Course
from portal.facade.general_facade import GeneralCRUDFacade
from portal.service import errors, filters
from portal.service.projects import ProjectService

log = logging.getLogger(__name__)


class ProjectsFacade(GeneralCRUDFacade):
    def __init__(self):
        super().__init__(ProjectService, 'Project')

    def _project_config(self, project: Project) -> Dict:
        return self._services.storage.project_config(project=project)

    def find_all(self, course: Course, *args, **kwargs):
        course_perm = self.permissions(course=course)
        if course_perm.check.view_course_full():
            return self._service.find_all(*args, course=course, **kwargs)
        elif course_perm.check.view_course_limited():
            return filters.filter_projects_from_course(course=course, user=self.client)
        raise errors.ForbiddenError(self.client)

    def update_project_config(self, project: Project, **params):
        """Updates the project configuration for the project
        Args:
            project: Project Instance
            **params:
        """
        log.info(f"[UPDATE] Configuration for project {project.log_name}"
                 f" by {self.client_name}: {params}.")
        result = self._service(project).update_project_config(params)
        return result

    def update_project_test_files(self, project: Project):
        """Update the project test files

        Downloads the new version from the gitlab
        Args:
            project: project instance

        """
        # TODO: Wrap tasks call to service
        log.info(f"[UPDATE] Project files for project {project}")
        return tasks.update_project_test_files.delay(project.course.id, project.id)

    def find_project_submissions(self, project: Project, user_id: str):
        """Finds all project submissions. If a user id is specified, returns only submissions created
        by that user.

        Args:
            project(Project): Project
            user_id(str): User id (optional)

        Returns(List[Submission]): A list of all submissions in a project if no user_id is specified
        Otherwise a list of submissions created in the project by the specified user.

        """
        result = self._service(project).find_project_submissions(user_id)
        return result

    def check_submission_create(self, project: Project, client: Client):
        """Check whether client can create a submission
        Args:
            project: project instance
            client: client instance
        """
        client = client or self.client
        result = self._service(project).check_submission_create(client)
        if self._services.kontr_gitlab.is_authorized and not project.submit_configurable:
            log.debug("[CHECK] Checking gitlab params")
            result = self._check_gitlab_params(client, project)
        return result

    def get_test_files(self, project: Project):
        """Gets the test files for the project
        Args:
            project: project instance
        """
        log.debug(f"Getting test files for the project: {project.log_name}"
                  f" by {self.client.log_name}")
        service = self.services.storage(project=project)
        storage_entity = service.get_test_files_entity_from_storage()
        path = self._request.args.get('path')
        return service.send_file_or_zip(storage_entity=storage_entity, path=path)

    def copy_project(self, project: Project, target_course: Course):
        """Copies the project
        Args:
            project(Project): Project instance
            target_course(Course): Course instance
        """
        log.info(f"[COPY] Project from {project.log_name} to {target_course.log_name} by"
                 f"{self.client_name}")
        result = self._service(project).copy_project(target_course)
        return result

    def is_create_notepad(self, project: Project):
        """Creates notepad for the project
        """

        log.info(f"[IS] Create notepad for project {project.log_name}"
                 f"{self.client_name}")
        result = self._services.is_api(project.course).create_notepad(project=project)
        return result

    def _check_gitlab_params(self, client: Client, project: Project):
        gl_repo = self.services.kontr_gitlab.build_repo_name(client, project)
        gl_project = self._services.kontr_gitlab.get_project(gl_repo)
        self._check_gl_repo_size(gl_project, project)
        self._check_gl_repo_visibility(gl_project, project)
        return True

    def delete(self, project: Project):
        for submission in project.submissions:
            self._facades.submissions.delete(submission)
        super(ProjectsFacade, self).delete(project)

    def archive(self, project: Project):
        for submission in project.submissions:
            self._facades.submissions.archive(submission)
        self._service(project).archive()

    def _check_gl_repo_size(self, gl_project, project: Project):
        config = self._project_config(project=project)
        if not config or 'scheduler' not in config.keys():
            log.debug("[CHECK] Scheduler config not set or config is None - skip")
            return True
        max_repo_size = config['scheduler'].get('repo_max_size')
        if max_repo_size:
            repo_size = gl_project.statistics['repository_size']
            if repo_size > max_repo_size:
                raise errors.SubmissionRefusedError(f"Your repository is bigger than the limit:"
                                                    f" {repo_size} > {max_repo_size}")
            else:
                log.debug(f"[CHECK] Your repository size is within the limit: "
                          f"{repo_size} > {max_repo_size}")
        else:
            log.debug("[CHECK] Max repo size has not been set - skip")
        return True

    def _check_gl_repo_visibility(self, gl_project: objects.Project, project: Project):
        # TODO: CRITICAL - Rewrite
        config = self._project_config(project=project)
        if not config or 'scheduler' not in config.keys():
            log.debug("[CHECK] Scheduler config not set or config is None - skip")
            return True
        required_repo_visibility = config['scheduler'].get('repo_visibility')
        if required_repo_visibility:
            repo_visibility = gl_project.visibility
            if required_repo_visibility != repo_visibility:
                raise errors.SubmissionRefusedError(f"Your repository visibility "
                                                    f"is not {required_repo_visibility}."
                                                    f" Actual: {repo_visibility}")
            else:
                log.debug(f"[CHECK] Your repository visibility is correct - "
                          f"{repo_visibility}")
        else:
            log.debug("[CHECK] Required repo visibility has not been set - skip")
        return True

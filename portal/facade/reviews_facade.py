import logging
from typing import List

from portal.database import Review, ReviewItem, Submission, User
from portal.facade.general_facade import GeneralCRUDFacade
from portal.service.reviews import ReviewService

log = logging.getLogger(__name__)


class ReviewsFacade(GeneralCRUDFacade):
    def __init__(self):
        super().__init__(ReviewService, 'Review')

    def create(self, submission: Submission, **data) -> Review:
        if not submission.review:
            self._service(submission=submission).create()
        log.info(f"[REVIEW] Create review by {self.client.log_name} "
                 f"for {submission.log_name}: {data}")
        params = dict(items=data['review_items'], author=self.client)
        result = self._service(submission=submission).create_review_items(**params)
        return result

    def create_review_item(self, submission: Submission, author: User, items: List[dict]) \
            -> Review:
        log.info(f"[REVIEW] Create review by {author} for {submission.log_name}: {items}")
        return self._service(submission=submission) \
            .create_review_items(author=author, items=items)

    def update_item(self, item: ReviewItem, **data):
        return self._service(item.review).update_item(item, **data)

    def delete_item(self, item: ReviewItem):
        return self._service(item.review).delete(item)

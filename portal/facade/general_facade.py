import logging
from typing import List

import flask

from portal.database import models
from portal.facade.urls import UrlService
from portal.service import errors
from portal.service.auth import AuthService
from portal.service.find import FindService
from portal.service.general import GeneralService
from portal.service.services_collection import ServicesCollection

log = logging.getLogger(__name__)


class GeneralFacade:
    def __init__(self, main_service=None, entity_name: str = None):
        """Creates new instance of the general facade and sets the variables
        Args:
            main_service: Main service that is used for managing the resources
            entity_name: Entity name - for logging purposes
        """
        service_klass = main_service or GeneralService
        self._service = service_klass()
        self._entity_name = entity_name
        self._services = ServicesCollection()
        self._urls = UrlService()

    @property
    def _facades(self):
        from portal.facade.collection import FacadesCollection
        return FacadesCollection.get()

    @property
    def urls(self):
        return self._urls

    @property
    def services(self) -> ServicesCollection:
        return self._services

    @property
    def client(self) -> models.Client:
        return self.auth.find_client(throw=False)

    @property
    def client_name(self):
        if self.client:
            return self.client.log_name
        return 'Unknown client'

    @property
    def _request(self) -> flask.Request:
        return flask.request

    @property
    def flask_app(self) -> flask.Flask:
        return flask.current_app

    @property
    def _config(self) -> flask.Config:
        return self.flask_app.config

    @property
    def permissions(self):
        return self.services.permissions

    @property
    def auth(self) -> AuthService:
        return self.services.auth

    @property
    def find(self) -> FindService:
        return self.services.find

    @property
    def storage(self):
        from portal import storage_wrapper
        return storage_wrapper

    def _require_params(self, dictionary, *params):
        for param in params:
            if not dictionary.get(param):
                raise errors.PortalAPIError(400, f'missing param: "{param}" is required')


class GeneralCRUDFacade(GeneralFacade):
    def find_all(self, *args, **kwargs) -> List:
        """Gets a collection of all the entities
        Args:
            *args:
            **kwargs:

        Returns(List): List of entities
        """
        log.debug(f"[GET] Find all ({self.__class__.__name__}): {args} {kwargs}")
        return self._service.find_all(*args, **kwargs)

    def create(self, **data):
        """Creates new instance of the entity

        Returns(User): Created user instance
        """
        log.info(f"[CREATE] New {self._entity_name} by {self.client_name}: {data}")
        new_entity = self._service.create(**data)
        return new_entity

    def delete(self, entity):
        """General delete method called on facade - calls the service delete method
        Args:
            entity: General entity instance (ex. User)

        """
        log.info(f"[DELETE] {entity.__class__.__name__} "
                 f"{entity.log_name} by {self.client_name}")

        self._service(entity).delete()

    def update(self, entity, **data):
        """Updates Entity

        Args:
            entity: Entity instance
            data(dict): Dictionary containing data which should be changed

        Returns: Updated instance instance
        """
        log.info(f"[UPDATE] {entity.__class__.__name__} {entity.log_name} "
                 f"by {self.client_name}: {entity}.")
        updated = self._service(entity).update(**data)
        return updated

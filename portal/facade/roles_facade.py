import logging
from typing import List

from portal.database import Course, Role
from portal.database.models import Client
from portal.facade.general_facade import GeneralCRUDFacade
from portal.service import errors, filters
from portal.service.roles import RoleService

log = logging.getLogger(__name__)


class RolesFacade(GeneralCRUDFacade):
    def __init__(self):
        super().__init__(RoleService, 'Role')

    def find_all(self, course: Course, *args, **kwargs) -> List[Role]:
        """List of all roles
        Args:
            course(Course): Course instance
        Returns(list): List of roles

        """
        course_perm = self.permissions(course=course)
        if course_perm.check.view_course_full():
            return self._service.find_all(*args, course=course, **kwargs)
        elif course_perm.check.view_course_limited():
            return filters.filter_roles_from_course(course=course, client=self.client)
        raise errors.ForbiddenError(self.client)

    def update_permissions(self, role: Role, **params):
        """Updates role permissions
        Args:
            role: Role instance
            params: Permissions dictionary

        Returns(RolePermissions): Role Permissions instance
        """
        log.info(f"[UPDATE] Permissions for role {role.log_name} "
                 f"by {self.client_name}: {params}.")
        result = self._service(role).update_permissions(params)
        return result

    def update_clients_membership(self, role: Role, **data):
        """Updates client membership in the role
        Args:
            role: Role instance
            data(dict): Data provided to update role membership

        Returns(Role): Updated role
        """
        log.info(f"[UPDATE] Membership for role {role.log_name} "
                 f"by {self.client_name}: {data}.")
        result = self._service(role).update_clients_membership(data)
        return result

    def add_client(self, role: Role, client: Client):
        """Adds single client to the role
        Args:
            role: Role instance
            client(Client): Client instance
        Returns:
        """
        log.info(f"[REMOVE] Client {client.log_name} to role {role.log_name} by "
                 f"{self.client.log_name}.")
        result = self._service(role).add_client(client)
        return result

    def remove_client(self, role: Role, client: Client):
        """Removes single client from the role
        Args:
            role: Role instance
            client(Client): Client instance
        Returns(Role): Updated role
        """
        log.info(f"[REMOVE] Client {client.log_name} from role {role.log_name} by "
                 f"{self.client.log_name}.")
        result = self._service(role).remove_client(client)
        return result

    def copy_role(self, source: Role, target_course: Course, **params):
        """Copies role to the target course
        Args:
            source: Source role
            target_course(Course): Target Course resource

        Returns(Role): Copied role
        """
        log.info(f"[COPY] Role {source.log_name} to course {target_course} by "
                 f"{self.client_name}: {params}")
        return self._service(source).copy_role(target_course, params)

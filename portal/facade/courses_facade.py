import logging
from typing import List

from portal.async_celery import tasks
from portal.database import Course
from portal.database.enums import ClientType
from portal.database.models import Client
from portal.facade.general_facade import GeneralCRUDFacade
from portal.service.courses import CourseService

log = logging.getLogger(__name__)


class CoursesFacade(GeneralCRUDFacade):
    def __init__(self):
        super().__init__(CourseService, entity_name='Course')

    def copy_course(self, source: Course, target: Course, **config) -> Course:
        """Copies course and it's other resources (roles, groups, projects)
        The other resources that should be copied are specified in the config

        Args:
            source(Course): Source course which resources will be copied
            target(Course): Target course
            config(dict): Configuration to specify which resource should be copied

        Returns(Course): Copied course instance (target)
        """
        log.info(f"[IMPORT] From {source.log_name} "
                 f"to {target.log_name} by {self.client_name}.")

        result = self._service(source).copy_course(target, config)
        return result

    def update_notes_token(self, course: Course, token: str) -> Course:
        """Updates notes access token of a course.

        Args:
            course(Course): Course instance
            token(str): The new token

        Returns(Course): Course instance
        """
        log.info(f"[UPDATE] Notes access token {course.log_name} "
                 f"by {self.client_name}")
        result = self._service(course).update_notes_token(token)
        return result

    def get_clients_filtered(self, course: Course, groups: List[str],
                             roles: List[str], client_type: ClientType = None) -> List[Client]:
        """Get all users for course filtered
        Args:
            course(Course): Course instance
            groups(list): Group names list
            roles(list):  Role names list
            client_type(ClientType): Client type
        Returns(List[User]):
        """
        clients = self._service(course).get_clients_filtered(groups, roles, client_type)
        return clients

    def import_users(self, course: Course, role_name: str, users_type: str):
        log.info(f"[IMPORT] Import users {course.log_name} {users_type} -> {role_name}"
                 f"by {self.client_name}")
        task = tasks.is_import_users_for_course.delay(course_id=course.id,
                                                      role_name=role_name,
                                                      users_type=users_type)
        log.debug(f"[TASK] Created import task: {task}")
        return task

    def sync_seminaries(self, course):
        log.info(f"[SYNC] Sync seminaries for course {course.log_name}"
                 f"by {self.client_name}")

        # Create async task to synchronize seminaries
        task = tasks.is_sync_seminaries.delay(course.id)
        log.debug(f"[TASK] Created debug task: {task}")
        return task

    def delete(self, course: Course):
        for project in course.projects:
            self._facades.projects.delete(project)
        super(CoursesFacade, self).delete(course)

    def archive(self, course: Course):
        for project in course.projects:
            self._facades.projects.archive(project)
        self._service(course).archive()


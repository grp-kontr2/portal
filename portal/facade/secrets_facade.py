import logging

from portal.facade.general_facade import GeneralCRUDFacade
from portal.service.secrets import SecretsService

log = logging.getLogger(__name__)


class SecretsFacade(GeneralCRUDFacade):
    def __init__(self):
        super().__init__(SecretsService, 'Secret')

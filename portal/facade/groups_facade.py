import logging

from portal.database import Course, Group, Project, User
from portal.facade.general_facade import GeneralCRUDFacade
from portal.service import filters, errors
from portal.service.groups import GroupService

log = logging.getLogger(__name__)


class GroupsFacade(GeneralCRUDFacade):
    def __init__(self):
        super().__init__(GroupService, 'Group')

    def find_all(self, course: Course, *args, **kwargs):
        course_perm = self.permissions(course=course)
        if course_perm.check.view_course_full():
            return self._service.find_all(*args, course=course, **kwargs)
        if course_perm.check.view_course_limited():
            return filters.filter_groups_from_course(course=course, user=self.client)
        raise errors.ForbiddenError(self.client)

    def find_users_by_role(self, group: Group, role_id: str):
        return self._service(group).find_users_by_role(role_id=role_id)

    def update_membership(self, group: Group, **data):
        log.info(f"[UPDATE] Group membership {group.log_name} "
                 f" by {self.client_name}: {data}")
        return self._service(group).update_membership(**data)

    def add_user(self, group: Group, user: User):
        log.info(f"[ADD] User to group {group.log_name} "
                 f"by {self.client_name}: {user.log_name}")
        return self._service(group).add_user(user)

    def remove_user(self, group: Group, user: User):
        log.info(f"[REMOVE] User from group {group.log_name} "
                 f"by {self.client_name}: {user.log_name}")
        return self._service(group).remove_user(user)

    def find_projects(self, group: Group):
        return self._service(group).find_projects()

    def add_project(self, group: Group, project: Project):
        log.info(f"[ADD] Project {project.log_name} to group"
                 f" {group.log_name} by {self.client_name}")
        return self._service(group).add_project(project)

    def remove_project(self, group: Group, project: Project):
        log.info(f"[REMOVE] Project {project.log_name} from group"
                 f" {group.log_name} by {self.client_name}")
        return self._service(group).remove_project(project)

    def import_groups(self, target_course: Course, **data):
        log.info(f"[IMPORT] Group to course {target_course.log_name}"
                 f"  by {self.client_name}: {data}")
        return self._service.import_group(course=target_course, data=data)

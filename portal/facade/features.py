import logging

from portal.database import Course
from portal.service import errors

log = logging.getLogger(__name__)


def is_api_enabled(course: Course, **kwargs):
    if not course.is_api_enabled():
        feature_not_enabled_error(name=f'IS MUNI API for course {course.log_name}', **kwargs)


def feature_not_enabled_error(name: str, error_klass=None,
                              message: str = None, code: int = 400, **kwargs):
    """Will raise an exception if the feature is not enabled
    Args:
        name: Name of the feature
        error_klass: Which error class should be used - default: portal API Error
        message(str): Message
        code(int): Response status code
        **kwargs:

    Returns:

    """
    error_klass = error_klass or errors.PortalAPIError
    default_message = f"[FEAT] Feature is not enabled: {name}"
    log.warning(default_message)
    message = message or default_message
    params = {'message': message, **kwargs}
    raise error_klass(code=code, message=params)

import logging
from typing import List

from portal.database import Group, Project, Review, Role, Submission, User
from portal.facade.general_facade import GeneralCRUDFacade
from portal.service.users import UserService

log = logging.getLogger(__name__)


class UsersFacade(GeneralCRUDFacade):
    def __init__(self):
        super().__init__(UserService, 'User')

    def create(self, **data):
        new_user = super(UsersFacade, self).create(**data)
        self.services.emails.notify_user(new_user, 'user_created', context=data)
        return new_user

    def update(self, user: User, **data):
        """Update the user params, but it is required to check whether the user is admin
        if the user is not admin, the is_admin param will be removed
        Args:
            user:
            **data:

        Returns:

        """
        params = {**data}
        log.info(f"[UPDATE] User {user.log_name} by {self.client_name}: {data}")
        params = self._remove_params_to_update(params, user)
        updated = self._service(user).update(**params)
        return updated

    def _remove_params_to_update(self, params, user):
        if self.client.is_admin:
            return params
        params.pop('is_admin', None)
        params.pop('managed', None)
        if user.managed and user.is_self(self.client.id):
            params.pop('gitlab_username', None)
        return params

    def update_password(self, user: User, **data):
        """Updates user's password

        if the user is admin, it will not require the old password
        Args:
            user:
            **data:

        Returns:

        """
        log.info(f"[UPDATE] User password for {user.log_name} by {self.client_name}")
        self._require_params(data, 'new_password')
        if self.client == user and not user.is_admin:
            self._require_params(data, 'old_password')
            self._service.check_old_password()
        result = self._service(user).update_password(data)
        self.services.emails.notify_user(user, 'user_password_update',
                                         context=dict(username=user.username))
        return result

    def find_users(self, ids: List[str]) -> List[User]:
        """Finds all users based on their ids

        Args:
            ids(List[str]): List of user ids

        Returns(List[User]): List of users
        """
        found = self._service.find_users(ids=ids)
        return found

    def find_users_filtered(self, course_id: str = None,
                            group_id: str = None) -> List[User]:
        """Find all users, optionally filtered by course and group
        Args:
            course_id(str): Course id (optional)
            group_id(str): Group id (optional)

        Returns(List[User]): Filtered list of  users
        """
        found_users = self._service.find_users_filtered(course_id=course_id, group_id=group_id)
        return found_users

    def find_groups_filtered(self, user, course_id: str = None) -> List[Group]:
        """Find all groups for the user, optionally filtered by course.

        Args:
            user: User instance
            course_id(str): Course id (optional)

        Returns(List[Group]): List of all groups
        """
        found_groups = self._service(user).find_groups_filtered(course_id=course_id)
        return found_groups

    def find_roles_filtered(self, user, course_id: str = None) -> List[Role]:
        """Get all roles for the user, optionally filtered by course

        Args:
            user: User instance
            course_id(str): Course id (optional)

        Returns(List[Role]):
        """
        found_roles = self._service(user).find_roles_filtered(course_id=course_id)
        return found_roles

    def find_reviews(self, user: User) -> List[Review]:
        """Gets reviews the user has contributed to.

        Returns(List[Review]):
        """
        reviews = self._service(user).find_reviews()
        return reviews

    def find_submissions_filtered(self, user: User, course_id: str = None,
                                  project_ids: List[str] = None) -> List[Submission]:
        """Get all submissions of a user, optionally filtered by course and project.

        Args:
            user: User instance
            course_id(str): Course id (optional)
            project_ids(List[str]): List of project ids (optional)

        Returns(List[Submission]): submissions in the course and projects
        """
        find_submissions = self._service(user).find_submissions_filtered(course_id=course_id,
                                                                         project_ids=project_ids)
        return find_submissions

    def find_all_projects_for_user(self, user: User) -> List[Project]:
        """Get list of all projects for specified user
        Args:
            user(User): User instance

        Returns(List[Project]): List of all projects
        """
        all_projects = self._service(user).find_all_projects_for_user()
        return all_projects

    def dump_projects_with_wait(self, user: User) -> List['Project']:
        all_projects = self._service(user).dump_projects_with_wait()
        return all_projects

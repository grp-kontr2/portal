import logging
from typing import List

import flask
from flask import make_response
from gitlab.v4 import objects
from werkzeug.utils import redirect

from portal.database import User
from portal.facade.general_facade import GeneralFacade
from portal.service import errors
from portal.service.gitlab import GitlabService, KontrGitlabService

log = logging.getLogger(__name__)


class GitlabFacade(GeneralFacade):
    def __init__(self):
        super().__init__(GitlabService)

    @property
    def user_gl_cred(self):
        return dict(token=self._get_credential('token'),
                    token_type=self._get_credential('token_type'))

    def _get_cookie(self, name, prefix='gitlab'):
        return self._request.cookies.get(f'{prefix}_{name}')

    def _get_header(self, name, prefix='gitlab'):
        full_name = f'{prefix}_{name}'.upper()
        return self._request.headers.get(full_name)

    def _get_credential(self, name, prefix='gitlab'):
        return self._get_cookie(name, prefix=prefix) or self._get_header(name, prefix=prefix)

    def check_gl_enabled(self):
        if not self.is_enabled():
            raise errors.GitlabIsNotEnabledError()

    def check_oauth_enabled(self):
        if not self.is_enabled():
            raise errors.GitlabOAuthIsNotEnabledError()

    def is_enabled(self) -> bool:
        return bool(self._config.get('GITLAB_URL'))

    def is_oauth_enabled(self) -> bool:
        return self.is_enabled() and bool(self._config.get('GITLAB_CLIENT_ID'))

    @property
    def gl_service(self) -> GitlabService:
        credentials = self.user_gl_cred
        return self._service(**credentials)

    @property
    def kontr_gl_service(self) -> KontrGitlabService:
        return self._services.kontr_gitlab

    def list_projects(self, **kwargs) -> List[objects.Project]:
        log.debug(f"[GL_PROJ] List projects in gitlab: {kwargs}")
        return self.gl_service.list_projects(**kwargs)

    def list_members(self, project_name, **kwargs):
        log.debug(f"[GL_PROJ] List members for project({project_name}): {kwargs}")
        return self.gl_service.list_members(project_name=project_name, **kwargs)

    def get_project(self, project_name, **kwargs):
        log.debug(f"[GL_PROJ] Get project({project_name}): {kwargs}")
        return self.gl_service.get_project(project_name=project_name, **kwargs)

    def add_member_to_project(self, **kwargs):
        log.info(f"[GL_PROJ] Add member project in gitlab: {kwargs}")
        return self.gl_service.add_member_to_project(**kwargs)

    def user_by_token(self, token=None, token_type='oauth') -> objects.CurrentUser:
        log.debug("[GL_USER] Get user by token")
        return self.services.gitlab(token, token_type=token_type).current_user

    def user_login(self, token=None, token_type='oauth', redir=True):
        log.debug(f"Logging in to gitlab with token[{token_type}]: {token} ")
        user_info = self.user_by_token(token=token, token_type=token_type)
        user = self.find.user(user_info.username, throws=False)
        if not user:
            log.debug(f"[GL_LOGIN] User not found: {user_info.username}")
            self.user_oauth_register(user_info)
        log.debug(f"[GL_LOGIN] User found: {user_info.username}")
        params = self._extracted_params(token=token, token_type=token_type, user_info=user_info,)
        return flask.jsonify(params) if not redir else self._make_login_response(params)

    def user_oauth_register(self, user_info: objects.CurrentUser) -> User:
        new_user = self.services.users.create(
            uco=None,
            username=user_info.username,
            gitlab_username=user_info.username,
            name=user_info.name,
            email=user_info.email,
            is_admin=False,
            managed=True,
        )
        log.info(f"[GITLAB] Created user after GL login: {new_user}")
        return new_user

    def _extracted_params(self, user_info, token, token_type):
        params = {
            'gitlab_token': token,
            'gitlab_token_type': token_type,
            'gitlab_username': user_info.username,
            'username': user_info.username,
            'gitlab_url': self._config.get('GITLAB_URL'),
            'gitlab_domain': self._config.get('GITLAB_BASE_DOMAIN')
        }
        return params

    def _make_login_response(self, params):
        frontend = self._config.get('FRONTEND_URL') or 'http://localhost:4200'
        front_gl_login = frontend + '/gitlabLogin'
        login_response = make_response(redirect(front_gl_login))
        for (key, val) in params.items():
            login_response.set_cookie(key, val)
        return login_response

from datetime import timedelta

from flask import Flask
from flask_sqlalchemy import SQLAlchemy

from management.data.shared import DataFactory
from portal.database.models import Course, Review, Secret, Submission
from portal.database import SubmissionState
from portal.tools import time


def get_project(factory: DataFactory, course: Course, num: int):
    project_config = dict(
        file_whitelist="main.cpp",
        config_subdir='kontr2',
        submission_parameters="{\"type\":\"text\"}",
        submissions_allowed_from=time.current_time(),
        submissions_allowed_to=time.current_time() + timedelta(days=2),
        archive_from=time.current_time() + timedelta(days=30)
    )
    return factory.create_project(
        course=course, name=f"HW0{num}", config=project_config)


def init_dev_data(app: Flask, db: SQLAlchemy):
    with app.app_context():
        factory = DataFactory(db)
        # users
        student1 = factory.create_user(username='student1', uco=111)
        student2 = factory.create_user(username='student2', uco=222)
        teacher1 = factory.create_user(username='teacher1', password='123123',
                                       name='Teacher And Student', uco=1001)
        teacher2 = factory.create_user(username='teacher2', password='123123',
                                       name='Teacher Only', uco=1002)
        lecturer1 = factory.create_user(
            username='lecturer1',
            name='Courses Owner',
            password='654321',
            uco=1010
        )

        db.session.commit()

        # courses
        test_course1 = factory.create_course(
            codename='testcourse1', name='Test Course One')
        test_course2 = factory.create_course(
            codename='testcourse2', name='test Course Two')

        # groups
        tc1_students = factory.create_group(
            course=test_course1, name="seminar01")
        tc1_teachers = factory.create_group(
            course=test_course1, name="teachers")

        tc2_students = factory.create_group(
            course=test_course2, name="seminar01")
        tc2_teachers = factory.create_group(
            course=test_course2, name="teachers")

        tc1_hw01 = factory.create_project(
            course=test_course1, name="HW01", codename='hw01', config=dict(
                file_whitelist="*.*",
                test_files_source='https://gitlab.fi.muni.cz/grp-kontr2/testing/hello-test-files',
                test_files_subdir='',
                config_subdir='kontr2',
                submission_parameters="{\"type\":\"text\"}",
                submissions_allowed_from=time.current_time() - timedelta(days=1),
                submissions_allowed_to=time.current_time() + timedelta(days=10),
                archive_from=time.current_time() + timedelta(days=30)
            )
        )

        tc1_hw03 = get_project(factory=factory, course=test_course1, num=3)
        tc1_hw02 = factory.create_project(
            course=test_course1, name="HW02", codename='hw02', config=dict(
                file_whitelist="main.cpp\nfunc.hpp\nfunc.cpp",
                test_files_source='https://gitlab.fi.muni.cz/grp-kontr2/testing/hello-test-files',
                test_files_subdir='',
                config_subdir='kontr2',
                submission_parameters="{\"type\":\"text\"}",
                submissions_allowed_from=time.current_time() - timedelta(days=1),
                submissions_allowed_to=time.current_time() + timedelta(days=10),
                archive_from=time.current_time() + timedelta(days=30)
            )
        )

        tc2_hw01 = factory.create_project(
            course=test_course2, name="HW01", codename='hw01', config=dict(
                file_whitelist="main.c",
                test_files_source='https://gitlab.fi.muni.cz/grp-kontr2/testing/test-repo',
                test_files_subdir='',
                config_subdir='kontr2',
                submission_parameters="{\"type\":\"text\"}",
                submissions_allowed_from=time.current_time() - timedelta(days=1),
                submissions_allowed_to=time.current_time() - timedelta(minutes=1),
                archive_from=time.current_time() + timedelta(days=30)
            )
        )

        # roles
        tc1_student = factory.scaffold_role(role_type='student', course=test_course1)
        tc1_teacher = factory.scaffold_role(role_type='teacher', course=test_course1)
        tc1_owner = factory.scaffold_role(role_type='owner', course=test_course1)

        tc1_worker = factory.scaffold_role(role_type='worker', course=test_course1)

        tc2_student = factory.scaffold_role(role_type='student', course=test_course2)
        tc2_teacher = factory.scaffold_role(role_type='teacher', course=test_course2)
        tc2_owner = factory.scaffold_role(role_type='owner', course=test_course2)
        tc2_worker = factory.scaffold_role(role_type='worker', course=test_course2)

        #   roles
        lecturer1.roles.append(tc1_owner)
        lecturer1.roles.append(tc2_owner)
        teacher1.roles.append(tc1_teacher)
        teacher1.roles.append(tc2_student)
        teacher2.roles.append(tc1_teacher)
        teacher2.roles.append(tc2_teacher)
        student1.roles.append(tc1_student)
        student2.roles.append(tc1_student)
        student2.roles.append(tc2_student)
        #   groups
        teacher1.groups.append(tc1_teachers)
        teacher1.groups.append(tc2_students)
        teacher2.groups.append(tc1_teachers)
        teacher2.groups.append(tc2_teachers)
        student1.groups.append(tc1_students)
        student2.groups.append(tc1_students)
        student2.groups.append(tc2_students)

        # submissions
        tc2_sub1 = factory.create_submission(user=teacher1, project=tc2_hw01, parameters={})
        tc2_sub1.state = SubmissionState.FINISHED
        tc2_sub2 = factory.create_submission(user=student2, project=tc2_hw01, parameters={})
        tc2_sub2.state = SubmissionState.ABORTED

        tc1_sub_p1_cancel = factory.create_submission(
            user=student2, project=tc1_hw01, parameters={})
        tc1_sub_p1_cancel.state = SubmissionState.CANCELLED
        tc1_sub_p1_abort = factory.create_submission(
            user=student1, project=tc1_hw01, parameters={})
        tc1_sub_p1_abort.state = SubmissionState.ABORTED
        tc1_sub_p1_finished = factory.create_submission(
            user=student2, project=tc1_hw01, parameters={})
        tc1_sub_p1_finished.state = SubmissionState.FINISHED
        tc1_sub_p1_in_progress = factory.create_submission(
            user=student1, project=tc1_hw01, parameters={})
        tc1_sub_p1_in_progress.state = SubmissionState.IN_PROGRESS

        # Projects in groups
        tc1_students.projects.append(tc1_hw01)
        tc1_students.projects.append(tc1_hw03)
        tc2_students.projects.append(tc2_hw01)

        db.session.add_all(
            [tc2_sub1, tc2_sub2, tc1_sub_p1_cancel, tc1_sub_p1_abort, tc1_sub_p1_finished,
             tc1_sub_p1_in_progress])

        # reviews
        review = Review(tc2_sub1)
        factory.create_review_item(author=teacher2, review=review, line=1,
                                   content="problem here")
        factory.create_review_item(author=teacher2, review=review, line=5,
                                   content="good stuff")
        factory.create_review_item(author=teacher1, review=review,
                                   line=1, content="oops")

        db.session.add_all([review])

        # components
        worker_domain = app.config.get('PORTAL_DATA_WORKER_DOMAIN', 'localhost')
        executor = factory.create_worker(name='executor', url=f'http://{worker_domain}:8080')
        executor.secrets.append(Secret('executor_secret', 'executor_secret'))
        executor.roles.append(tc1_worker)
        executor.roles.append(tc2_worker)
        db.session.add_all([executor])

        # Commit to the DB
        db.session.commit()

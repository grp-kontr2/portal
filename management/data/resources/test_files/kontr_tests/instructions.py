from ktdk.asserts import matchers
from ktdk.asserts.checks.executable import *
from ktdk.asserts.checks.general import *
from ktdk.core import Test
from ktdk import KTDK

from ktdk.tasks.cpp.compiler import RawCompilerTask
from ktdk.tasks.cpp.valgrind import ValgrindCommand
from ktdk.tasks.fs.tasks import MakeDir, ExistFiles, CopyFiles
from ktdk.tasks.fs.tools import *
from ktdk.tasks.raw.executable import ExecutableTask


ktdk = KTDK.get_instance()
ktdk.register_tags('naostro')

naostro = Test(name="naostro", desc="Test nanecisto", tags=['naostro'])
ktdk.suite.add_test(naostro)

ft = FileTasks()
ft.workspace('src').add_task(MakeDir())
ft.workspace('results').add_task(MakeDir())
ft.submission().require_that(ExistFiles('main.c'))
ft.submission().add_task(CopyFiles('main.c'))
ft.workspace().check_that(ExistFiles("main.c"))

ktdk.suite.add_task(ft)

compile_task = RawCompilerTask(compiler='gcc', executable='hello', files=['main.c'])
compile_task.check_that(TestResultCheck(matcher=matchers.ResultPassed()))
ktdk.suite.add_task(compile_task)

hello_test = Test(name="hello_test", desc="Super hello test")
hello = ExecutableTask(executable='hello', executor=ValgrindCommand)
hello_test.add_task(hello)

hello.check_that(ReturnCodeMatchesCheck(matcher=matchers.Equals(0)))
hello.check_that(StdOutMatchesCheck(matcher=matchers.Equals("Hello world!\n")))
hello.check_that(StdErrMatchesCheck(matcher=matchers.IsEmpty()))

naostro.add_test(hello_test)



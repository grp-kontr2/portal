#include <stdio>
#define UNUSED(x) (void*) x

unsigned long long fact(unsigned long long x) {
    if(x == 0) { return 1; }
    return fact(x-1) * x;
}

unsigned long long sum_n(unsigned long long x) {
    if(x == 0) { return 0; }
    return fact(x-1) + x;
}

import csv
import datetime
import sys

import yaml
from flask import Flask
from flask_sqlalchemy import SQLAlchemy

from management.data import shared
from management.data.data_c import init_c_data
from management.data.data_cpp import init_cpp_data
from management.data.data_dev import init_dev_data
from management.data.data_prod import init_prod_data
from management.data.data_test import init_test_data
from portal import logger
from portal.database import SubmissionState
from portal.database.models import Course, Role, Secret, Submission, User
from portal.service.find import FindService
from portal.service.users import UserService
from portal.tools import time

log = logger.get_logger(__name__)


class DataManagement:
    DATASETS = dict(prod=init_prod_data,
                    test=init_test_data,
                    cpp=init_cpp_data,
                    c=init_c_data,
                    dev=init_dev_data)

    def __init__(self, app: Flask, db: SQLAlchemy):
        """Creates instance of th data manager
        Args:
            app(Flask): Flask application
            db(SQLAlchemy): SqlAlchemy Db instance
        """
        self.app = app
        self.db = db
        self.creator = shared.DataFactory(self.db)
        from portal.service.services_collection import ServicesCollection
        self.services = ServicesCollection.get()

    @property
    def find(self):
        return FindService()

    def reset_db(self):
        """Resets the database
        """
        self.db.drop_all(app=self.app)
        self.db.create_all(app=self.app)

    def init_data(self, env=None):
        """Init sample data
        Args:
            env(str): Name of the environment
        """
        log.info(f"[INIT] Data: {env}")
        func = DataManagement.DATASETS.get(env) or init_dev_data
        log.debug(f"[INIT] calling function: {func.__name__}")
        func(db=self.db, app=self.app)

    def delete_user(self, name: str):
        """Deletes user
        Args:
            name(str): name of the user
        """
        with self.app.app_context():
            user = self.find.user(name)
            UserService()(user).delete()

    def delete_course(self, name: str):
        """Deletes course
        Args:
            name(str): name of the course
        """
        with self.app.app_context():
            course = self.services.find.course(name)
            self.services.courses(course).delete()

    def update_users_password(self, name: str, password: str) -> User:
        """Updates user's password
        Args:
            name(str): Name of the user
            password(str): User's password
        Returns(User): Updated user
        """
        with self.app.app_context():
            user = self.services.find.user(name)
            user.set_password(password=password)
            self.db.session.add(user)
            self.db.session.commit()
            log.debug(f"[DATA] User's password updated: {user.log_name}")
            return user

    def create_admin_user(self, name: str, password: str, secret: str = None, gitlab: str = None) \
            -> User:
        """Creates new admin user with username
        Args:
            name(str): Username of the user
            password(str): User's password
            secret(str): User's secret
        Returns(User): Created user
        """
        with self.app.app_context():
            user = self.creator.create_user(
                username=name,
                name=name,
                admin=True,
                gitlab_username=gitlab,
                password=password)
            if secret:
                user.secrets.append(Secret(name='admin-secret', value=secret))
            log.debug(f'[DATA] Created user: {user.log_name}')
            self.db.session.commit()
            return user

    def create_course(self, name: str) -> Course:
        """Creates course with associated roles
        Args:
            name(str): Codename of the course
        Returns(Course): Course instance
        """
        with self.app.app_context():
            course = self.creator.scaffold_course(codename=name)
            self.db.session.commit()
            log.debug(f'[DATA] Created course: {course.log_name}')
            return course

    def create_project(self, course: str, name):
        with self.app.app_context():
            course = self.services.find.course(course)
            role = self.creator.scaffold_project(course, name)
            self.db.session.commit()
            log.debug(f'[DATA] Created project: {role.log_name}')
            return role

    def create_role(self, course_name: str, role_type: str, name: str) -> Role:
        """Creates role in the course based on type
        Args:
            name(str): Name of the role
            course_name(str): Name of the course
            role_type(str): Type of the role
        Returns(Role): Created role
        """
        with self.app.app_context():
            course = self.services.find.course(course_name)
            role = self.creator.scaffold_role(
                course, role_type=role_type, name=name)
            self.db.session.commit()
            log.debug(f'[DATA] Created role: {role.log_name}')
            return role

    def list_users(self):
        with self.app.app_context():
            for user in User.query.all():
                print(f"{user.username} ({user.id}) - {user.created_at}")

    def activate_project(self, course: str, project: str):
        with self.app.app_context():
            course = self.services.find.course(course)
            project = self.services.find.project(course, project)
            days_allow_to = time.current_time() + datetime.timedelta(days=1000)
            project.config.archive_from = None
            project.config.submissions_allowed_to = days_allow_to
            self.services.projects.write_entity(project)
            log.info(f"[CMD] Project has been activated: {project.log_name}")
            return project

    def list_projects(self, course: str):
        with self.app.app_context():
            course = self.services.find.course(course)
            for project in course.projects:
                print(f"{project.name}: {project.description}")

    def list_courses(self):
        with self.app.app_context():
            for course in Course.query.all():
                print(f"{course.codename}: {course.name}")

    def list_all_submissions(self):
        with self.app.app_context():
            for submission in Submission.query.all():
                print(f"{submission.id} [{submission.state}]: {submission.user.username}")

    def cancel_all_submissions(self):
        with self.app.app_context():
            for submission in Submission.query.all():
                self._cancel_one(submission)
            self.db.session.commit()

    def _cancel_one(self, submission):
        message = "Cancelling submission using command line"
        self.services.submissions(submission).set_state(SubmissionState.CANCELLED,
                                                        message=message)

    def cancel_submission(self, sid):
        with self.app.app_context():
            submission = self.services.find.submission(sid)
            self._cancel_one(submission)
            self.db.session.commit()

    def clean_all_submissions(self):
        with self.app.app_context():
            for submission in Submission.query.all():
                Submission.query.filter_by(id=submission.id).delete()
            self.db.session.commit()

    def add_user_secret(self, name, secret):
        with self.app.app_context():
            user = self.services.find.user(name)
            user.secrets.append(Secret(name='user-cli-secret', value=secret))
            log.debug(f'[DATA] Created secret for user: {user.log_name}')
            self.db.session.commit()

    def import_users(self, file, course=None, role=None, group=None):
        with self.app.app_context():
            users = self._parse_csv_users(file)
            created_users = []
            for user in users:
                found_user = self.services.find.user(user['username'], throws=False)
                if not found_user:
                    log.debug(f"[IMPORT] Adding user {user['username']}: {user}")
                    found_user = self.services.users.create(**user, admin=False)
                else:
                    log.debug(f"[IMPORT] skipping already existing user {user['username']}: {user}")
                created_users.append(found_user)

            self._add_users_to_course(created_users, course, role, group)

    def _get_role_group_instance(self, course, group, role):
        course = self.services.find.course(course)
        if role:
            role = self.services.find.role(course, role)
        if group:
            group = self.services.find.group(course, group)
        return group, role

    def _parse_csv_users(self, file=None) -> list:
        fields = ['username', 'uco', 'email', 'name']

        def __read_users(reader):
            return [user for user in reader]

        if file is None:
            reader = csv.DictReader(sys.stdin, fieldnames=fields)
            return __read_users(reader)
        with open(file) as f:
            reader = csv.DictReader(f, fieldnames=fields)
            return __read_users(reader)

    def _add_users_to_course(self, created_users, course, role, group):
        for user in created_users:
            self.add_user_to_course(user, course, role, group)

    def promote_user(self, name):
        with self.app.app_context():
            user = self.services.find.user(name)
            user.is_admin = True
            self.services.users.write_entity(user)

    def add_user_to_course(self, user, course, role, group=None):
        if isinstance(user, str):
            user = self.services.find.user(user)
        group, role = self._get_role_group_instance(course, group, role)
        if role:
            self.services.roles(role).add_client(user)
        if group:
            self.services.groups(group).add_user(user)

    def is_import_users(self, course: str, role_name: str, users_type: str):
        course = self.find.course(course)
        return self.services.is_api(course).import_users(
            role_name=role_name, users_type=users_type
        )

    def is_sync_seminaries(self, course: str):
        course = self.find.course(course)
        self.services.is_api(course).sync_seminaries()

    def dump_definition(self, name: str):
        course = self.find.course(name)
        schema = self.services.resource_definition.dump_course(course=course)
        print(yaml.safe_dump(schema, sort_keys=False))

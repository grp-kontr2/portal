from datetime import timedelta

from flask import Flask
from flask_sqlalchemy import SQLAlchemy

from management.data import shared
from portal.database import Worker
from portal.tools import time


def create_project(factory, test_course, name: str, submit_configurable=False):
    return factory.create_project(
        course=test_course, name=name.upper(), codename=name,
        submit_configurable=submit_configurable, config=dict(
            file_whitelist="*.*",
            test_files_source='git@gitlab.fi.muni.cz:xstanko2/pb071-mini-template.git',
            test_files_subdir=name,
            config_subdir='kontr2',
            submission_parameters="{\"type\":\"text\"}",
            submissions_allowed_from=time.current_time() - timedelta(days=1),
            submissions_allowed_to=time.current_time() + timedelta(days=300),
            archive_from=time.current_time() + timedelta(days=330)
        )
    )


def init_c_data(app: Flask, db: SQLAlchemy):
    with app.app_context():
        factory = shared.DataFactory(db)
        # course
        c = factory.create_course(codename='PB071', name='Programování v jazyce C')

        # groups
        students_group = factory.create_group(course=c, name="students")
        teachers_group = factory.create_group(course=c, name="teachers")

        for num in range(2, 13):
            name = f"mini0{num}"
            project = create_project(factory, c, name, submit_configurable=(num % 2 == 0))
            students_group.projects.append(project)
            teachers_group.projects.append(project)

        # roles
        students = factory.scaffold_role(role_type='student', course=c)
        teachers = factory.scaffold_role(role_type='teacher', course=c)
        owner = factory.scaffold_role(role_type='owner', course=c)
        workers = factory.scaffold_role(role_type='worker', course=c)

        # users
        c_student = factory.create_user(username='c_student', admin=False, password='123456')
        students.clients.append(c_student)
        students_group.users.append(c_student)
        c_teacher = factory.create_user(username='c_teacher', admin=False, password='123456')
        teachers.clients.append(c_teacher)
        students_group.users.append(c_teacher)
        teachers_group.users.append(c_teacher)

        # Projects in groups
        db.session.add_all([students, teachers, owner, teachers_group, students_group])

        # workers
        worker = Worker.query.filter_by(codename='executor').first()
        worker.roles.append(workers)
        db.session.add_all([worker])
        # Commit to the DB
        db.session.commit()

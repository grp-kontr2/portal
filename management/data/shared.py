"""
Factory to create sample data
"""

import random
import shutil
import string
from datetime import timedelta
from pathlib import Path

from flask_sqlalchemy import SQLAlchemy

from portal import logger, storage_wrapper
from portal.database.models import Course, Group, Project, ReviewItem, Role, Submission, User, \
    Worker
from portal.tools import time

log = logger.get_logger(__name__)

DATA_DIR = Path(__file__).parent
RESOURCES = DATA_DIR / 'resources'
RESULTS_DIR = RESOURCES / 'results'
SOURCES_DIR = RESOURCES / 'sources'
TESTS_DIR = RESOURCES / 'test_files'


def password_generator(size=16, chars=string.ascii_letters + string.digits):
    """
    Returns a string of random characters, useful in generating temporary
    passwords for automated password resets.

    size: default=8; override to provide smaller/larger passwords
    chars: default=A-Za-z0-9; override to provide more/less diversity

    Credit: Ignacio Vasquez-Abrams
    Source: http://stackoverflow.com/a/2257449
    """
    return ''.join(random.choice(chars) for _ in range(size))


PERM_TEACHER = dict(
    view_course_full=True,
    update_course=True,
    handle_notes_access_token=False,
    write_groups=True,
    write_projects=True,
    create_submissions_other=True,
    create_submissions=True,
    resubmit_submissions=True,
    evaluate_submissions=True,
    write_reviews_all=True,
    read_submissions_groups=True,
    read_submissions_all=True
)

PERM_STUDENT = dict(
    view_course_limited=True,
    create_submissions=True,
    read_submissions_own=True,
    write_reviews_own=True
)

PERM_OWNER = dict(
    view_course_full=True,
    update_course=True,
    handle_notes_access_token=True,
    write_roles=True,
    write_groups=True,
    write_projects=True,
    resubmit_submissions=True,
    evaluate_submissions=True,
    read_submissions_all=True,
    read_all_submission_files=True,
    write_reviews_all=True
)

PERM_WORKER = dict(
    view_course_full=True,
    resubmit_submissions=True,
    evaluate_submissions=True,
    read_submissions_all=True,
    read_all_submission_files=True,
    write_reviews_all=True,
    create_submissions=True,
)

PERMISSION_TYPES = dict(
    teacher=PERM_TEACHER,
    student=PERM_STUDENT,
    owner=PERM_OWNER,
    worker=PERM_WORKER,
)


class DataFactory:
    def __init__(self, db: SQLAlchemy):
        self._db = db

    @property
    def session(self):
        return self._db.session

    def create_user(self, username: str, uco=0, name=None, password=None,
                    email=None, admin=False, gitlab_username: str = None) -> User:
        email = email or f"{username}@example.com"
        password = password or "123456"
        user = self.__create_entity(
            User, uco=uco, username=username, is_admin=admin, email=email,
            gitlab_username=gitlab_username)
        user.name = name or f"{username.capitalize()} User"
        user.set_password(password=password)
        return user

    def create_test_admin_user(self) -> User:
        return self.create_user(
            username='admin',
            password='789789',
            email='admin@example.com',
            name='Kontr Admin',
            uco=0,
            admin=True,
        )

    def __create_entity(self, klass, *args, **kwargs):
        entity = klass(*args, **kwargs)
        self.session.add(entity)
        log.info(f"[SAMPLE] Create {klass.__name__}: {entity}")
        return entity

    def create_worker(self, name: str, url: str) -> Worker:
        return self.__create_entity(Worker, name=name, url=url)

    def create_course(self, codename, name=None, token=None, faculty_id=None) \
            -> Course:
        name = name or codename
        desc = name + "'s description"
        course = self.__create_entity(
            Course, codename=codename, name=name, description=desc)
        course.notes_access_token = token or f"{codename}_token"
        if faculty_id is not None:
            course.faculty_id = faculty_id
        return course

    def create_group(self, course: Course, name: str) -> Group:
        desc = name + "'s description"
        return self.__create_entity(
            Group, course=course, name=name, description=desc)

    def create_role(self, course: Course, name: str,
                    permissions: dict = None, desc: str = None) -> Role:
        permissions = permissions or {}
        role = self.__create_entity(Role, course=course, name=name)
        role.set_permissions(**permissions)
        role.description = desc or f"{name.capitalize()}'s role"
        return role

    def create_project(self, course, name=None, codename=None, config=None,
                       submit_configurable=False) -> Project:
        config = config or {}
        desc = name + "'s description"
        name = name or codename
        codename = codename or name
        project = self.__create_entity(Project, course=course, name=name,
                                       codename=codename, description=desc,
                                       submit_configurable=submit_configurable)
        log.debug(f"[CREATE] Project config: {project.log_name}: {config}")
        project.set_config(**config)
        self.session.flush()
        test_path = storage_wrapper.test_files.path / project.storage_dirname
        if test_path.exists():
            shutil.rmtree(str(test_path))
        shutil.copytree(str(TESTS_DIR), str(test_path))
        return project

    def create_submission(self, project, user, **kwargs):
        submission = self.__create_entity(Submission, project=project, user=user, **kwargs)
        self.session.flush()
        subm_path = storage_wrapper.submissions.path / submission.storage_dirname
        if not subm_path.exists():
            shutil.copytree(str(SOURCES_DIR), str(subm_path))
        res_path = storage_wrapper.results.path / submission.storage_dirname
        if not res_path.exists():
            shutil.copytree(str(RESULTS_DIR), str(res_path))
        return submission

    def create_review_item(self, review, author, file="main.c", line=1,
                           content="problem here", **kwargs) -> ReviewItem:
        return self.__create_entity(ReviewItem, review=review, user=author, file=file,
                                    line=line, content=content, **kwargs)

    def scaffold_course(self, codename: str, name: str = None) -> Course:
        name = name or codename
        course = self.create_course(codename=codename, name=name)
        for role in ('teacher', 'student', 'owner', 'worker'):
            self.scaffold_role(course, role)
        return course

    def scaffold_role(self, course: Course, role_type: str, name=None) -> Role:
        name = name or role_type
        permissions = PERMISSION_TYPES[role_type]
        role = self.create_role(course=course, name=name,
                                permissions=permissions)
        return role

    def scaffold_project(self, course: Course, name: str):
        project_config = dict(
            file_whitelist="*.*",
            test_files_source='https://gitlab.fi.muni.cz/grp-kontr2/testing/hello-test-files',
            test_files_subdir='',
            config_subdir='kontr2',
            submission_parameters="{\"type\":\"text\"}",
            submissions_allowed_from=time.current_time() - timedelta(days=1),
            submissions_allowed_to=time.current_time() + timedelta(days=10),
            archive_from=time.current_time() + timedelta(days=30)
        )
        return self.create_project(
            course=course, name=name, config=project_config)

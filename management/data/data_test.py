from datetime import timedelta

from flask import Flask
from flask_sqlalchemy import SQLAlchemy

from management.data.shared import DataFactory
from portal.database.models import Review, Secret
from portal.database import SubmissionState
from portal.tools import time


def init_test_data(app: Flask, db: SQLAlchemy):
    with app.app_context():
        factory = DataFactory(db)
        # users
        student1 = factory.create_user(username='student1', uco=111)
        student2 = factory.create_user(username='student2', uco=222)
        teacher1 = factory.create_user(username='teacher1', password='123123',
                                       name='Teacher And Student', uco=1001)
        teacher2 = factory.create_user(username='teacher2', password='123123',
                                       name='Teacher Only', uco=1002)

        teacher1.secrets.append(Secret('teacher1_secret', 'ultimate_teacher_secret'))
        lecturer1 = factory.create_user(username='lecturer1', name='Courses Owner', uco=1010)

        # courses
        test_course1 = factory.create_course(codename='testcourse1', name='Test Course One',
                                             faculty_id=100)
        test_course2 = factory.create_course(codename='testcourse2', name='test Course Two',
                                             faculty_id=100)

        # groups
        tc1_students = factory.create_group(course=test_course1, name="seminar01")
        tc1_teachers = factory.create_group(course=test_course1, name="teachers")

        tc2_students = factory.create_group(course=test_course2, name="seminar01")
        tc2_teachers = factory.create_group(course=test_course2, name="teachers")

        tc1_hw01 = factory.scaffold_project(test_course1, 'hw01')
        tc1_hw02 = factory.scaffold_project(test_course1, 'hw02')

        tc2_hw01 = factory.scaffold_project(test_course2, 'hw01')
        tc2_hw01 = factory.scaffold_project(test_course2, 'hw02')

        # roles
        tc1_student = factory.scaffold_role(role_type='student', course=test_course1)
        tc1_teacher = factory.scaffold_role(role_type='teacher', course=test_course1)
        tc1_owner = factory.scaffold_role(role_type='owner', course=test_course1)
        tc1_workers = factory.scaffold_role(role_type='worker', course=test_course1)

        tc2_student = factory.scaffold_role(role_type='student', course=test_course2)
        tc2_teacher = factory.scaffold_role(role_type='teacher', course=test_course2)
        tc2_owner = factory.scaffold_role(role_type='owner', course=test_course2)
        tc2_workers = factory.scaffold_role(role_type='worker', course=test_course2)

        # relationships
        teacher_student = teacher1
        teacher_both = teacher2
        student_tc1 = student1
        student_both = student2
        #   roles
        lecturer1.roles.append(tc1_owner)
        lecturer1.roles.append(tc2_owner)
        teacher_student.roles.append(tc1_teacher)
        teacher_student.roles.append(tc2_student)
        teacher_both.roles.append(tc1_teacher)
        teacher_both.roles.append(tc2_teacher)
        student_tc1.roles.append(tc1_student)
        student_both.roles.append(tc1_student)
        student_both.roles.append(tc2_student)
        #   groups
        teacher_student.groups.append(tc1_teachers)
        teacher_student.groups.append(tc2_students)
        teacher_both.groups.append(tc1_teachers)
        teacher_both.groups.append(tc2_teachers)
        student_tc1.groups.append(tc1_students)
        student_both.groups.append(tc1_students)
        student_both.groups.append(tc2_students)

        # submissions
        tc2_sub1 = factory.create_submission(user=teacher_student, project=tc2_hw01,
                                             parameters={})
        tc2_sub1.state = SubmissionState.FINISHED
        tc2_sub2 = factory.create_submission(user=student_both, project=tc2_hw01,
                                             parameters={})
        tc2_sub2.state = SubmissionState.ABORTED

        tc1_sub_p1_cancel = factory.create_submission(user=student_both, project=tc1_hw01,
                                                      parameters={})
        tc1_sub_p1_cancel.state = SubmissionState.CANCELLED
        tc1_sub_p1_abort = factory.create_submission(user=student_tc1, project=tc1_hw01,
                                                     parameters={})
        tc1_sub_p1_abort.state = SubmissionState.ABORTED
        tc1_sub_p1_finished = factory.create_submission(user=student_both, project=tc1_hw01,
                                                        parameters={})
        tc1_sub_p1_finished.state = SubmissionState.FINISHED
        tc1_sub_p1_in_progress = factory.create_submission(user=student_tc1, project=tc1_hw01,
                                                           parameters={})
        tc1_sub_p1_in_progress.state = SubmissionState.IN_PROGRESS

        # Projects in groups
        tc1_students.projects.append(tc1_hw01)
        tc2_students.projects.append(tc2_hw01)

        db.session.add_all(
            [tc2_sub1, tc2_sub2, tc1_sub_p1_cancel, tc1_sub_p1_abort, tc1_sub_p1_finished,
             tc1_sub_p1_in_progress])

        # reviews
        review = Review(tc2_sub1)
        factory.create_review_item(author=teacher_both, review=review, line=1,
                                   content="problem here")
        factory.create_review_item(author=teacher_both, review=review, line=5, content="good stuff")
        factory.create_review_item(author=teacher_student, review=review, line=1, content="oops")

        db.session.add_all([review])

        # workers
        executor = factory.create_worker(name='executor', url="foo/url")
        executor.secrets.append(Secret('executor_secret', "executor_secret",
                                       time.current_time() + timedelta(hours=1)))
        processing = factory.create_worker(name='processing', url="bar/url")
        processing.secrets.append(Secret('processing_secret', "processing_secret",
                                         time.current_time() + timedelta(hours=1)))
        executor.roles.append(tc1_workers)
        processing.roles.append(tc2_workers)
        db.session.add_all([executor, processing])
        # Commit to the DB
        db.session.commit()

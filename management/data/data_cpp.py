from datetime import timedelta

from flask import Flask
from flask_sqlalchemy import SQLAlchemy

from management.data import shared
from portal.database import models
from portal.tools import time


def create_project(factory, test_course, name: str):
    factory.create_project(
        course=test_course, name=name.upper(), codename=name, config=dict(
            file_whitelist="*.*",
            test_files_source='git@gitlab.fi.muni.cz:xstanko2/kontr2-tests.git',
            test_files_subdir=name,
            config_subdir="kontr2",
            submission_parameters="{\"type\":\"text\"}",
            submissions_allowed_from=time.current_time() - timedelta(days=1),
            submissions_allowed_to=time.current_time() + timedelta(days=10),
            archive_from=time.current_time() + timedelta(days=30)
        )
    )


def init_cpp_data(app: Flask, db: SQLAlchemy):
    with app.app_context():
        factory = shared.DataFactory(db)
        # course
        cpp = factory.create_course(codename='PB161', name='Programování v jazyce C++')

        # groups
        seminar_kontr = factory.create_group(course=cpp, name="seminar_kontr")
        teachers_group = factory.create_group(course=cpp, name="teachers")

        projects = []
        for num in range(2, 5):
            name = f"hw0{num}"
            projects.append(create_project(factory, cpp, name))

        for num in range(4, 9):
            name = f"mini0{num}"
            projects.append(create_project(factory, cpp, name))

        # roles
        students = factory.scaffold_role(role_type='student', course=cpp)
        teachers = factory.scaffold_role(role_type='teacher', course=cpp)
        owner = factory.scaffold_role(role_type='owner', course=cpp)
        workers = factory.scaffold_role(role_type='worker', course=cpp)

        # Projects in groups
        db.session.add_all([students, teachers, owner, teachers_group, seminar_kontr])

        # workers
        worker = factory.create_worker(name='worker', url='http://localhost:8080')
        worker.secrets.append(models.Secret('executor_secret', 'executor_secret'))
        worker.roles.append(workers)
        db.session.add_all([worker])
        # Commit to the DB
        db.session.commit()

from pathlib import Path

import pytest

from portal.storage import Submission, UploadedEntity
from portal.storage.inputs.filter import filter_entity


def tmp_file(src, i, ext):
    file = src.join(f'test-{i}.{ext}')
    file.write(f'test-{i}.{ext}')


def tmp_dir(work, name):
    src = work.mkdir(name)
    for i in range(10):
        tmp_file(src, i, 'c')
        tmp_file(src, i, 'txt')


@pytest.fixture
def workspace(tmpdir):
    work = tmpdir.mkdir('work')
    tmp_dir(work=work, name='src')
    tmp_dir(work=work, name='data')
    work.mkdir('noot')
    dest = tmpdir.mkdir('dest')
    return Path(work), Path(dest)


@pytest.fixture
def uploaded_entity(workspace):
    work, dest = workspace
    submission = Submission(dirname='1', base_dir=dest)
    return UploadedEntity(
        entity=submission,
        workspace=work,
        whitelist="**/*.c\ndata"
    )


def test_filtered_files(uploaded_entity):
    entity = filter_entity(uploaded_entity)

    assert (entity.path / 'src').exists()
    assert (entity.path / 'data').exists()
    assert not (entity.path / 'noot').exists()
    assert (entity.path / 'src/test-1.c').exists()
    assert (entity.path / 'data/test-1.c').exists()
    assert (entity.path / 'data/test-1.txt').exists()

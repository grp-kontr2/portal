import shutil
from pathlib import Path

import pytest

from portal.storage import Submission
from portal.storage.utils.compress import decompress_entity, compress_entity
from portal.storage.utils import zipdir


def tmp_file(src, i, ext):
    file = src.join(f'test-{i}.{ext}')
    file.write(f'test-{i}.{ext}')


def tmp_dir(work, name):
    src = work.mkdir(name)
    for i in range(10):
        tmp_file(src, i, 'c')
        tmp_file(src, i, 'txt')


@pytest.fixture
def subm_dir(tmpdir):
    subm = tmpdir.mkdir('sub')
    work = subm.mkdir('1')
    tmp_dir(work=work, name='src')
    tmp_dir(work=work, name='data')

    work2 = subm.mkdir('2')
    tmp_dir(work=work2, name='src')
    tmp_dir(work=work2, name='data')
    zipdir(zip_file_path=f'{subm}/2.zip', src_dir=work2)
    shutil.rmtree(work2)
    return Path(subm)


@pytest.fixture
def submission(subm_dir):
    return Submission(base_dir=subm_dir, dirname='1')


@pytest.fixture
def submission2(subm_dir):
    return Submission(base_dir=subm_dir, dirname='2')


def test_compressed_files(submission: Submission):
    compress_entity(submission)
    assert submission.zip_path.exists


def test_decompressed_files(submission2: Submission):
    assert not submission2.path.exists()
    decompress_entity(submission2)
    assert submission2.path.exists()
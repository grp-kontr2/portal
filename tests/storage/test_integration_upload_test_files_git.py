import pytest

from portal.storage import Storage, Submission, UploadedEntity


@pytest.fixture
def git_config():
    return dict(
        type='git',
        url='https://gitlab.fi.muni.cz/grp-kontr2/testing/test-repo.git',
        branch='master'
    )


@pytest.fixture
def test_request(git_config):
    return dict(
        dirname="updated-git",
        source=git_config,
        whitelist="src/*",
        from_dir='hw02'
    )


@pytest.fixture
def updated_request(git_config):
    return dict(
        dirname="updated-git",
        source=git_config,
        whitelist="main.c",
        from_dir='hw01'
    )


@pytest.fixture
def uploaded(test_request: dict, storage_instance: Storage) -> UploadedEntity:
    return storage_instance.test_files.create(**test_request)


@pytest.fixture()
def updated(updated_request: dict, storage_instance: Storage, uploaded) -> UploadedEntity:
    return storage_instance.test_files.update(**updated_request)


@pytest.fixture()
def submission(updated):
    return updated.entity


def test_repo_will_be_cloned_successfully(submission: Submission, storage_instance: Storage,
                                          uploaded):
    assert uploaded.version
    assert submission.path.exists()
    assert (submission.path / 'main.c').exists()
    assert not (submission.path / 'src').exists()
    assert not (submission.path / 'src/main.c').exists()
    assert not (submission.path / 'src/foo.c').exists()
    assert not (submission.path / 'src/bar.c').exists()


def test_repo_will_be_zipped(submission: Submission, storage_instance: Storage):
    assert submission.path.exists()
    assert submission.compress()
    assert submission.zip_path.exists()
    submission.clean()
    assert not submission.path.exists()
    assert submission.decompress()
    assert submission.path.exists()

import logging
import os
from pathlib import Path

import pytest

from portal import create_app
from portal.storage import Storage

TESTS_PATH = Path(__file__).parent
RESOURCES_PATH = TESTS_PATH / 'resources'


@pytest.fixture(scope='session')
def app():
    os.environ["PORTAL_CONFIG_TYPE"] = 'test'
    _app = create_app(environment='test')
    with _app.app_context():
        yield _app


@pytest.fixture
def log():
    return logging.getLogger(__name__)


@pytest.fixture
def workspace_dir(tmpdir):
    return tmpdir.mkdir('workspace')


@pytest.fixture
def test_files_dir(tmpdir):
    return tmpdir.mkdir('test_files')


@pytest.fixture()
def submissions_dir(tmpdir):
    return tmpdir.mkdir('submissions')


@pytest.fixture()
def results_dir(tmpdir):
    return tmpdir.mkdir('results')


@pytest.fixture
def storage_instance(app, test_files_dir, workspace_dir,
                     submissions_dir, results_dir) -> Storage:
    return Storage(test_files_dir=test_files_dir,
                   workspace_dir=workspace_dir,
                   submissions_dir=submissions_dir,
                   results_dir=results_dir)

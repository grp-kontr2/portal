import pytest

from portal.storage import Storage, Submission, UploadedEntity


@pytest.fixture
def git_config():
    return dict(
        type='git',
        url='https://gitlab.fi.muni.cz/grp-kontr2/testing/test-repo.git',
        branch='master'
    )


@pytest.fixture
def submission_request(git_config):
    return dict(
        dirname="11111111111111111111-git",
        source=git_config,
        whitelist="src/*",
        from_dir='hw02'
    )


@pytest.fixture
def uploaded(submission_request: dict, storage_instance: Storage) -> UploadedEntity:
    return storage_instance.submissions.create(**submission_request)


@pytest.fixture()
def submission(uploaded):
    return uploaded.entity


def test_repo_will_be_cloned_successfully(submission: Submission, storage_instance: Storage, uploaded):
    assert uploaded.version
    assert submission.path.exists()
    assert (submission.path / 'src').exists()
    assert (submission.path / 'src/main.c').exists()
    assert (submission.path / 'src/foo.c').exists()
    assert (submission.path / 'src/bar.c').exists()


def test_repo_will_be_ziped(submission: Submission, storage_instance: Storage):
    assert submission.path.exists()
    assert submission.compress()
    assert submission.zip_path.exists()
    submission.clean()
    assert not submission.path.exists()
    assert submission.decompress()
    assert submission.path.exists()

from pathlib import Path

import pytest

from portal.storage import Submission, UploadedEntity
from portal.storage.inputs.git import GitWrapper


@pytest.fixture
def workspace(tmpdir):
    return tmpdir.mkdir('workspace')


@pytest.fixture
def git_config():
    return dict(
        type='git',
        url='https://github.com/pestanko/example-repo',
        branch='master'
    )


@pytest.fixture
def submission():
    return Submission(dirname='1', base_dir='/tmp')


@pytest.fixture
def uploaded_entity(submission, workspace, git_config):
    return UploadedEntity(
        entity=submission,
        source=git_config,
        workspace=workspace.mkdir('repo')
    )


@pytest.fixture
def subm_proc(uploaded_entity, app):
    return GitWrapper(uploaded_entity)


def test_will_successfully_clone_dir(subm_proc: GitWrapper):
    cloned = subm_proc.clone()
    assert cloned
    working_dir = Path(cloned.working_dir)
    readme = working_dir / 'README.md'
    assert readme.exists()

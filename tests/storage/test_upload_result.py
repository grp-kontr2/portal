import pytest

from portal.storage import Storage
from portal.storage.entities import Results, UploadedEntity
from tests.storage.conftest import RESOURCES_PATH


@pytest.fixture
def git_config():
    return dict(
        type='zip',
        url=str(RESOURCES_PATH / 'result.zip'),
    )


@pytest.fixture
def result_request(git_config):
    return dict(
        dirname="11111111111111111111-result-zip",
        source=git_config,
    )


@pytest.fixture
def uploaded(result_request: dict, storage_instance: Storage) -> UploadedEntity:
    return storage_instance.results.create(**result_request)


@pytest.fixture()
def result(uploaded):
    return uploaded.entity


def test_repo_will_be_unziped_successfully(result: Results, storage_instance: Storage):
    assert result.path.exists()
    assert (result.path / 'test_result_reduced.json').exists()
    assert (result.path / 'tests_result.json').exists()


def test_repo_will_be_ziped(result: Results, storage_instance: Storage):
    assert result.path.exists()
    assert result.compress()
    assert result.zip_path.exists()
    result.clean()
    assert not result.path.exists()
    assert result.decompress()
    assert result.path.exists()

import pytest

from portal.storage import Storage, Submission, UploadedEntity
from tests.storage.conftest import RESOURCES_PATH


@pytest.fixture
def git_config():
    return dict(
        type='zip',
        url=str(RESOURCES_PATH / 'hw02.zip'),
    )


@pytest.fixture
def submission_request(git_config):
    return dict(
        dirname="11111111111111111111-zip",
        source=git_config,
        whitelist="src/*",
        from_dir='hw02'
    )


@pytest.fixture
def uploaded(submission_request: dict, storage_instance: Storage) -> UploadedEntity:
    return storage_instance.submissions.create(**submission_request)


@pytest.fixture()
def submission(uploaded):
    return uploaded.entity


def test_repo_will_be_unziped_successfully(submission: Submission, storage_instance: Storage):
    assert submission.path.exists()
    assert (submission.path / 'src').exists()
    assert (submission.path / 'src/main.c').exists()
    assert (submission.path / 'src/foo.c').exists()
    assert (submission.path / 'src/bar.c').exists()


def test_repo_will_be_ziped(submission: Submission, storage_instance: Storage):
    assert submission.path.exists()
    assert submission.compress()
    assert submission.zip_path.exists()
    submission.clean()
    assert not submission.path.exists()
    assert submission.decompress()
    assert submission.path.exists()

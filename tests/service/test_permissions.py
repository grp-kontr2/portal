import pytest

from portal import logger
from portal.service.permissions import PermissionsService
from portal.service.services_collection import ServicesCollection

log = logger.get_logger(__name__)


@pytest.fixture()
def perm():
    return PermissionsService()


def test_project_list_with_admin_permissions(perm, course, admin_user):
    service = perm(admin_user, course)
    assert service.check.permissions(['view_course_full'])


def test_project_list_with_non_admin_permissions(perm, student_user, course):
    service = perm(student_user, course)
    assert not service.check.permissions(['view_course_full'])


def test_project_list_with_non_admin_permissions_limited(perm, student_user, course):
    service = perm(student_user, course)
    assert service.check.permissions(['view_course_limited'])

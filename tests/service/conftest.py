import pytest


@pytest.fixture
def admin_user(app, portal_services):
    return portal_services.find.user('admin')


@pytest.fixture
def student_user(app, portal_services):
    return portal_services.find.user('student1')


@pytest.fixture
def teacher_user(app, portal_services):
    return portal_services.find.user('teacher1')


@pytest.fixture
def course(app, portal_services):
    return portal_services.find.course('testcourse1')


@pytest.fixture
def project(app, portal_services, course):
    return portal_services.find.project(course, 'hw01')

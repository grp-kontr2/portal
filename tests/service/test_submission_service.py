import pytest

from portal.database import Submission, SubmissionState


@pytest.fixture
def submission(project, student_user, portal_services) -> Submission:
    params = {
        "project_params": {'tags': 'docker'},
        "file_params": {
            "source": {
                "type": "git",
                "url": "https://gitlab.fi.muni.cz/xkompis/test-hello-world.git",
                "branch": "master",
                "checkout": "master"
            }
        }
    }
    return portal_services.submissions.create(user=student_user, project=project,
                                              submission_params=params)


def test_submission_has_been_created(submission: Submission, project, student_user):
    assert submission.id is not None
    assert submission.project == project
    assert submission.user == student_user


def test_submission_set_state(submission: Submission, portal_services):
    message = "Archiving submission"
    portal_services.submissions(submission).set_state(SubmissionState.ARCHIVED, message=message)

    updated = portal_services.find.submission(submission.id)
    assert updated.id == submission.id
    assert updated.note is not None
    assert updated.changes is not None
    assert isinstance(updated.changes, list)
    assert updated.changes[0]['state'] == 'ARCHIVED'


import copy

import pytest
import yaml


@pytest.fixture()
def just_course():
    return yaml.safe_load("""
name: Programování v jazyce C
codename: pb071
description: Programování v jazyce C desc
faculty_id: 1433
notes_access_token: random-token-not-supported
""")


@pytest.fixture()
def just_project():
    return yaml.safe_load("""
    name: HW01 - Calculator
    codename: hw01
    description: Calculator homework
    assignment_url: https://localhost.local/assignment
    submit_configurable: false
    config:
        file_whitelist: *.*
        test_files_source: git@gitlab.fi.muni.cz:xstanko2/pb071-mini-template.git
        test_files_subdir: hw01
        submissions_allowed_from=
    """)


def test_resource_definition_will_create_just_course(just_course, portal_services):
    course = portal_services.resource_definition.sync_course(just_course, save=False)
    assert course.codename == 'pb071'
    assert course.name == 'Programování v jazyce C'
    assert course.description == 'Programování v jazyce C desc'
    assert course.faculty_id == 1433
    assert course.notes_access_token == 'random-token-not-supported'


def test_resource_definition_will_serialize_and_deserialize_course(portal_services):
    course1 = portal_services.find.course('testcourse1')
    c_schema = portal_services.resource_definition.dump_course(course1)
    c2_schema = copy.deepcopy(c_schema)
    c2_schema['codename'] = c2_schema['codename'] + "-new"
    c2 = portal_services.resource_definition.sync_course(c2_schema, save=True)
    assert c2.codename == c2_schema['codename']
    for p in course1.projects:
        p2 = portal_services.find.project(c2, p.codename)
        assert p.config.submissions_allowed_from == p2.config.submissions_allowed_from
        assert p.config.submissions_allowed_to == p2.config.submissions_allowed_to
        assert p.config.archive_from == p2.config.archive_from

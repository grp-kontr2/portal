from portal.logger import Logging

Logging(global_level='DEBUG').load_config()

import os

import pytest

from management.data import DataManagement
from portal import create_app, db
from portal.database import ProjectConfig, Review, Submission
from tests.utils.ent_mocker import EntitiesMocker


@pytest.fixture(scope='function')
def app():
    os.environ["PORTAL_CONFIG_TYPE"] = 'test'
    flask_app = create_app(environment='test')
    from tests import init_db
    init_db(flask_app)
    with flask_app.app_context():
        DataManagement(flask_app, db).create_admin_user('admin', '789789')
        DataManagement(flask_app, db).init_data('test')
        yield flask_app
        db.session.remove()
        db.drop_all()


@pytest.fixture(scope='function')
def client(app):
    with app.test_client() as _client:
        yield _client


@pytest.fixture()
def portal_services(app):
    from portal.service.services_collection import ServicesCollection
    return ServicesCollection.get()


@pytest.fixture()
def portal_facades(app):
    from portal.facade.collection import FacadesCollection
    return FacadesCollection.get()


@pytest.fixture()
def ent_mocker(app):
    from tests.utils.ent_mocker import EntitiesMocker
    return EntitiesMocker(app, db=db)


@pytest.fixture()
def mocked_submission(ent_mocker: EntitiesMocker, portal_services):
    submission = ent_mocker.create_submission()
    portal_services.submissions.write_entity(submission)
    project_config: ProjectConfig = submission.project.config
    project_config.test_files_commit_hash = 'some-random-hash'
    return submission


@pytest.fixture()
def created_course(portal_services):
    return portal_services.find.course('testcourse1')


@pytest.fixture()
def created_course2(portal_services):
    return portal_services.find.course('testcourse2')


@pytest.fixture()
def created_project(portal_services, created_course):
    return portal_services.find.project(created_course, 'hw01')


@pytest.fixture()
def created_role_student(portal_services, created_course):
    return portal_services.find.role(created_course, 'student')


@pytest.fixture()
def created_role_teacher(portal_services, created_course):
    return portal_services.find.role(created_course, 'teacher')


@pytest.fixture()
def created_group(portal_services, created_course):
    return portal_services.find.group(created_course, 'seminar01')


@pytest.fixture()
def created_student(portal_services):
    return portal_services.find.user('student1')


@pytest.fixture()
def created_submission(portal_services, created_project) -> Submission:
    return created_project.submissions[0]


@pytest.fixture()
def created_review(portal_services) -> Review:
    return Review.query.all()[0]


@pytest.fixture()
def created_admin(portal_services):
    return portal_services.find.user('admin')


@pytest.fixture()
def created_teacher(portal_services):
    return portal_services.find.user('teacher1')


@pytest.fixture()
def created_worker(portal_services):
    return portal_services.find.worker('executor')

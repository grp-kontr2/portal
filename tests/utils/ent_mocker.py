import secrets
from pathlib import Path

from management.data import DataManagement
from portal.database import Group, Submission
from portal.service.services_collection import ServicesCollection
from portal.storage import Entity


class EntitiesMocker:
    def __init__(self, app, db):
        self.mgmt = DataManagement(app=app, db=db)

    @property
    def data(self):
        return self.mgmt.creator

    @property
    def services(self) -> ServicesCollection:
        return self.mgmt.services

    @property
    def random_suffix(self):
        return secrets.token_urlsafe(5)

    def rand_name(self, what):
        return f'test_{what}_{self.random_suffix}'

    def create_course(self):
        return self.data.create_course(self.rand_name('course'))

    def create_project(self, course=None):
        course = course or self.create_course()
        return self.data.scaffold_project(course, self.rand_name('project'))

    def create_role(self, course=None):
        course = course or self.create_course()
        return self.data.create_role(course, self.rand_name('role'))

    def create_group(self, course=None) -> Group:
        course = course or self.create_course()
        return self.data.create_group(course, self.rand_name('group'))

    def create_submission(self, project=None, course=None, user=None):
        project = project or self.create_project(course=course)
        user = user or self.services.find.user('admin')
        return self.data.create_submission(project=project, user=user)

    def create_submission_storage(self, submission: Submission):
        storage = self.services.storage.storage

        def __create_content(location: Entity, *files):
            path = location.path
            path.mkdir(parents=True)
            self._create_files(path, *files)
            location.compress()

        subm = storage.submissions.get(submission.storage_dirname)
        results = storage.results.get(submission.storage_dirname)
        test_files = storage.test_files.get(submission.project.storage_dirname)
        __create_content(subm, 'src/main.c', 'src/foo.c')
        __create_content(results, 'results.json', 'student.json')
        __create_content(test_files, 'test_main.c')

    def _create_files(self, path: Path, *files):
        for fpath in files:
            full_path: Path = path / fpath
            if not full_path.parent.exists():
                full_path.parent.mkdir(parents=True)
            full_path.write_text(f'This is a test file: {fpath}')

    def create_user_in_course(self, username, role_type='student'):
        user = self.create_user(username)
        course = self.create_course()
        project = self.create_project(course=course)
        role = self.data.scaffold_role(course=course, role_type=role_type)
        group = self.create_group(course=course)
        group.users.append(user)
        role.clients.append(user)
        group.projects.append(project)
        self.data.session.commit()
        return user, course, project

    def create_user(self, username, **kwargs):
        user = self.data.create_user(username=username, **kwargs)
        return user

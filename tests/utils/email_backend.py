class DummyResponse(object):
    status_code = 250


class EmailBackend(object):
    messages = []

    def __init__(self, **kw):
        self.config = kw

    def sendmail(self, **kwargs):
        EmailBackend.messages.append(kwargs)
        return DummyResponse()

from portal import db


def init_db(app):
    db.drop_all(app=app)
    db.create_all(app=app)

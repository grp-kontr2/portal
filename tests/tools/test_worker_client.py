import urllib.parse

import pytest

from portal.database import Worker
from portal.tools.worker_client import WorkerClient


@pytest.fixture()
def mocked_worker() -> Worker:
    worker = Worker(name='test-worker', url='http://localhost')
    worker.portal_secret = 'test-worker-secret'
    return worker


@pytest.fixture()
def worker_client(mocked_worker):
    return WorkerClient(worker=mocked_worker)


def test_worker_client_init_with_worker(mocked_worker, worker_client):
    worker_client = worker_client
    assert worker_client.worker == mocked_worker
    assert worker_client.worker_url == mocked_worker.url

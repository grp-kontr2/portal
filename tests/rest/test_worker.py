from portal.database.models import Worker
from tests.rest import rest_tools
from tests.rest.rest_tools import assert_response


def test_list(client):
    response = rest_tools.make_request(client, '/workers')
    assert_response(response)

    workers = rest_tools.extract_data(response)
    assert len(workers) == 2
    db_workers = Worker.query.all()
    rest_tools.assert_worker_in(db_workers, workers[0])
    rest_tools.assert_worker_in(db_workers, workers[1])


def test_create(client):
    request_dict = dict(
        name="new_executor",
        url="127.0.0.1/worker",
    )
    db_workers_number = len(Worker.query.all())

    response = rest_tools.make_request(client, '/workers', json=request_dict, method='post')
    assert_response(response, 201)

    resp_new_worker = rest_tools.extract_data(response)
    db_workers = Worker.query.all()
    assert len(db_workers) == db_workers_number + 1
    rest_tools.assert_worker_in(db_workers, resp_new_worker)


def test_read_by_id(client, created_worker):
    response = rest_tools.make_request(client, f"/workers/{created_worker.id}")
    assert_response(response)

    worker = rest_tools.extract_data(response)

    rest_tools.assert_worker(created_worker, worker)


def test_read_by_codename(client, created_worker):
    response = rest_tools.make_request(client, f"/workers/{created_worker.codename}")
    assert_response(response)

    worker = rest_tools.extract_data(response)

    rest_tools.assert_worker(created_worker, worker)


def test_update(client, created_worker):
    request_dict = dict(url="new url", portal_secret="such safe")

    url = f'/workers/{created_worker.id}'
    response = rest_tools.make_request(client, url, json=request_dict, method='put')

    assert_response(response, 204)
    worker_after_update: Worker = Worker.query.filter_by(name=created_worker.codename).first()
    assert worker_after_update.url == "new url"
    assert worker_after_update.portal_secret == "such safe"


def test_delete(client, created_worker):
    response = rest_tools.make_request(client, f'/workers/{created_worker.id}', method='delete')
    assert_response(response, 204)
    assert not Worker.query.filter_by(name=created_worker.codename).first()

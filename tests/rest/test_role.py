from portal.database.models import Course, Role, User
from tests.rest.rest_tools import assert_response
from . import rest_tools


def test_list(client, created_course):
    cpp_roles = len(created_course.roles)
    response = rest_tools.make_request(client, f'/courses/{created_course.codename}/roles')
    assert_response(response, 200)
    roles = rest_tools.extract_data(response)
    assert len(roles) == cpp_roles
    cpp_updated = Course.query.filter_by(codename=created_course.codename).first()
    for r in roles:
        rest_tools.assert_role_in(cpp_updated.roles, r)


def test_create(client, created_course):
    cpp = created_course
    cpp_roles = len(cpp.roles)

    request_dict = {
        "name": "new role",
        'codename': 'new-role',
        "description": "new role desc"
    }
    url = f"/courses/{created_course.codename}/roles"
    response = rest_tools.make_request(client, url, json=request_dict,
                                       method='post')
    assert_response(response, 201)

    new_role = rest_tools.extract_data(response)
    cpp_updated = Course.query.filter_by(codename=created_course.codename).first()
    assert len(cpp_updated.roles) == cpp_roles + 1
    assert new_role['name'] == "new role"
    assert new_role['description'] == "new role desc"
    assert new_role['id']
    rest_tools.assert_role_in(cpp_updated.roles, new_role)


def test_read(client, created_course, created_role_student):
    url = f"/courses/{created_course.codename}/roles/{created_role_student.codename}"
    response = rest_tools.make_request(client, url)
    assert_response(response, 200)
    role = rest_tools.extract_data(response)
    rest_tools.assert_role(created_role_student, role)


def test_update(client, created_course, created_role_student, portal_services):
    request_dict = dict(
        name="new role name",
        description="new role desc",
    )
    url = f"/courses/{created_course.codename}/roles/{created_role_student.name}"
    response = rest_tools.make_request(client, url, json=request_dict, method='put')
    assert_response(response, 204)

    r_updated = portal_services.find.role(created_course, created_role_student.id)
    assert r_updated
    assert r_updated.description == "new role desc"


def test_delete(client, created_course, created_role_student, portal_services):
    cpp_roles = len(created_course.roles)
    url = f"/courses/{created_course.codename}/roles/{created_role_student.name}"
    response = rest_tools.make_request(client, url, method='delete')

    assert_response(response, 204)

    course_updated = portal_services.find.course(created_course.id)
    assert len(course_updated.roles) == cpp_roles - 1
    assert not portal_services.find.role(created_course, created_role_student.id, throws=False)


def test_users_list(client, created_course, created_role_student, portal_services):
    url = f"/courses/{created_course.codename}/roles/{created_role_student.id}/clients?type=user"
    response = rest_tools.make_request(client, url)

    assert_response(response, 200)

    users = rest_tools.extract_data(response)
    assert len(users) == len(created_role_student.clients)
    for user in users:
        rest_tools.assert_user_in(created_role_student.clients, user)


def test_users_update_add(client, created_course2, created_role_student, portal_services,
                          created_student):
    roles = [role for role in created_course2.roles if role.name == "student"]
    role = roles[0]
    r_users = len(role.clients)
    user = created_student
    assert user not in role.clients

    request_dict = dict(add=[user.id])
    url = f"/courses/{created_course2.codename}/roles/{role.name}/clients"
    response = rest_tools.make_request(client, url, json=request_dict, method='put')

    assert_response(response, 204)

    r_updated = portal_services.find.role(created_course2, role.id)
    assert len(r_updated.clients) == r_users + 1
    assert user in r_updated.clients


def test_users_update_add_duplicate(client, created_course2, created_role_student, portal_services,
                                    created_student):
    roles = [role for role in created_course2.roles if role.name == "student"]
    role = roles[0]
    r_users = len(role.clients)
    user = role.clients[0]
    assert user in role.clients

    request_dict = dict(add=[user.id])
    url = f"/courses/{created_course2.codename}/roles/{role.name}/clients"
    response = rest_tools.make_request(client, url, json=request_dict, method='put')

    assert_response(response, 204)

    r_updated = portal_services.find.role(created_course2, role.id)
    assert len(r_updated.clients) == r_users
    assert user in r_updated.clients


def test_users_update_remove(client, portal_services, created_course2, created_student):
    roles = [role for role in created_course2.roles if role.name == "student"]
    role = roles[0]
    r_users = len(role.clients)
    user = role.clients[0]
    assert user in role.clients

    request_dict = dict(remove=[user.id])
    url = f"/courses/{created_course2.codename}/roles/{role.name}/clients"
    response = rest_tools.make_request(client, url,
                                       json=request_dict, method='put')

    assert_response(response, 204)

    r_updated = portal_services.find.role(created_course2, role.id)
    assert len(r_updated.clients) == r_users - 1
    assert user not in r_updated.clients


def test_users_update_remove_user_not_in(client):
    c = Course.query.filter_by(codename="testcourse2").first()
    roles = [role for role in c.roles if role.name == "student"]
    r = roles[0]
    r_users = len(r.clients)
    user = User.query.filter_by(username="student1").first()
    assert user not in r.clients

    request_dict = dict(
        remove=[user.id]
    )
    response = rest_tools.make_request(client, f"/courses/{c.codename}/roles/{r.name}/clients",
                                       json=request_dict, method='put')

    assert_response(response, 204)

    r_updated = Role.query.filter(
        Role.course_id == c.id).filter(Role.id == r.id).first()
    assert len(r_updated.clients) == r_users
    assert user not in r_updated.clients


def test_add_user(client):
    c = Course.query.filter_by(codename="testcourse2").first()
    roles = [role for role in c.roles if role.name == "student"]
    r = roles[0]
    r_users = len(r.clients)

    u = User.query.filter_by(username="student1").first()
    assert r not in u.roles

    response = rest_tools.make_request(
        client, f"/courses/{c.codename}/roles/{r.name}/clients/{u.id}", method='put')
    assert_response(response, 204)

    r_updated = Role.query.filter(
        Role.course_id == c.id).filter(Role.id == r.id).first()
    assert u in r_updated.clients
    assert len(r_updated.clients) == r_users + 1


def test_add_worker(client, portal_services):
    course = portal_services.find.course('testcourse2')
    role = portal_services.find.role(course, 'worker')
    role_clients = len(role.clients)

    worker = portal_services.workers.create(codename='new-test-worker',
                                            name='new-test-worker',
                                            url='https://test-worker:443')
    assert role not in worker.roles

    response = rest_tools.make_request(
        client, f"/courses/{course.codename}/roles/{role.name}/clients/{worker.codename}",
        method='put')
    assert_response(response, 204)

    r_updated = portal_services.find.role(course, 'worker')
    assert worker in r_updated.clients
    assert len(r_updated.clients) == role_clients + 1


def test_add_user_duplicate(client):
    c = Course.query.filter_by(codename="testcourse2").first()
    roles = [role for role in c.roles if role.name == "student"]
    r = roles[0]
    r_users = len(r.clients)
    u = r.clients[0]
    assert u in r.clients

    response = rest_tools.make_request(
        client, f"/courses/{c.codename}/roles/{r.name}/clients/{u.id}", method='put')
    assert_response(response, 204)
    r_updated = Role.query.filter(
        Role.course_id == c.id).filter(Role.id == r.id).first()
    assert u in r_updated.clients
    assert len(r_updated.clients) == r_users


def test_remove_user(client):
    c = Course.query.filter_by(codename="testcourse2").first()
    roles = [role for role in c.roles if role.name == "student"]
    r = roles[0]
    r_users = len(r.clients)
    u = r.clients[0]

    response = rest_tools.make_request(
        client, f"/courses/{c.codename}/roles/{r.name}/clients/{u.id}", method='delete')
    assert_response(response, 204)

    r_updated = Role.query.filter(
        Role.course_id == c.id).filter(Role.id == r.id).first()
    assert u not in r_updated.clients
    assert len(r_updated.clients) == r_users - 1


def test_remove_user_not_in(client):
    c = Course.query.filter_by(codename="testcourse2").first()
    roles = [role for role in c.roles if role.name == "student"]
    r = roles[0]
    r_users = len(r.clients)
    u = User.query.filter_by(username="student1").first()
    assert r not in u.roles

    response = rest_tools.make_request(
        client, f"/courses/{c.codename}/roles/{r.name}/clients/{u.id}", method='delete')
    assert_response(response, 400)

    r_updated = Role.query.filter(
        Role.course_id == c.id).filter(Role.id == r.id).first()
    assert u not in r_updated.clients
    assert len(r_updated.clients) == r_users


def test_permissions_list(client):
    cpp = Course.query.filter_by(codename="testcourse1").first()
    r = cpp.roles[0]

    response = rest_tools.make_request(
        client, f"/courses/{cpp.codename}/roles/{r.name}/permissions")
    assert_response(response, 200)

    data = rest_tools.extract_data(response)
    rest_tools.assert_role_permissions(r, data)


def test_permissions_update(client):
    cpp = Course.query.filter_by(codename="testcourse1").first()
    r = cpp.roles[0]
    r_name = r.name

    request_dict = dict(
        view_course_full=True,
        write_roles=True,
        read_submissions_all=True
    )

    response = rest_tools.make_request(client,
                                       f"/courses/{cpp.codename}/roles/{r.name}/permissions",
                                       json=request_dict, method='put')
    assert_response(response, 200)

    r_updated = Role.query.filter(
        Role.course_id == cpp.id).filter_by(name=r_name).first()
    assert r_updated.view_course_full
    assert r_updated.write_roles
    assert r_updated.read_submissions_all

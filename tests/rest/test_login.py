# tests for basic login

import pytest

from portal.database.models import Secret
from portal.tools import time
from tests.rest import rest_tools
from .rest_tools import assert_response


@pytest.fixture
def tokens(client):
    request_dict = {
        "type": "username_password",
        "identifier": "admin",
        "secret": "789789"
    }
    response = client.post("/api/v1.0/auth/login", json=request_dict)
    assert_response(response, 200)
    data = rest_tools.extract_data(response)
    return data


def test_login_user_valid(client):
    request_dict = {
        "type": "username_password",
        "identifier": "admin",
        "secret": "789789"
    }
    response = client.post("/api/v1.0/auth/login", json=request_dict)
    assert_response(response, 200)

    tokens = rest_tools.extract_data(response)
    assert tokens.get('access_token')
    assert tokens.get('refresh_token')


def test_login_component_valid(client):
    request_dict = {
        "type": "secret",
        "identifier": "executor",
        "secret": "executor_secret"
    }
    response = client.post("/api/v1.0/auth/login", json=request_dict)
    assert_response(response, 200)


def test_refresh_token(client, tokens: dict):
    headers = {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + tokens['refresh_token']
    }
    response = client.post("/api/v1.0/auth/refresh", headers=headers)
    assert_response(response, 200)
    assert tokens.get('access_token')


def test_user_logout(client, tokens):
    headers = {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + tokens['access_token']
    }
    response = client.post("/api/v1.0/auth/logout", headers=headers)
    assert_response(response)


def test_login_with_expired_secret(client, portal_services):
    secret1 = Secret(name='user_secret1', value='test-1', expires_at=None)
    secret2 = Secret(name='user_secret2', value='test-2', expires_at=time.current_time())
    student = portal_services.find.user('student1')
    student.secrets.append(secret1)
    student.secrets.append(secret2)
    portal_services.general.write_entity(student)

    request_dict1 = dict(type="secret", identifier="student1", secret="test-1")
    request_dict2 = dict(type="secret", identifier="student1", secret="test-2")

    response = client.post("/api/v1.0/auth/login", json=request_dict1)
    assert_response(response)

    response = client.post("/api/v1.0/auth/login", json=request_dict2)
    assert_response(response, 401)

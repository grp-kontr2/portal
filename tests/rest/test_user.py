import json

from portal.database.models import Course, Role, User
from tests.rest.rest_tools import assert_response
from . import rest_tools

"""Basic tests to verify functionality of the API for User.

These tests do not mock the database or care about authentication/authorization.

"""


def test_create(client):
    users_before = len(User.query.all())
    request_dict = {
        "uco": 445574,
        "email": "baz@foo.com",
        "username": "xuser",
    }
    response = rest_tools.make_request(client, '/users', json=request_dict, method='post')
    assert_response(response, 201)

    resp_new_user = rest_tools.extract_data(response)
    db_users = User.query.all()
    assert len(db_users) == users_before + 1
    rest_tools.assert_user_in(db_users, resp_new_user)


def test_create_extra_key(client):
    users_before = len(User.query.all())
    request_dict = {
        "uco": 456789,
        "email": "baz@foo.com",
        "username": "xuser",
        "redundant key": True,
    }

    response = rest_tools.make_request(client, '/users', json=request_dict, method='post')
    assert_response(response, 201)

    resp_new_user = rest_tools.extract_data(response)
    db_users = User.query.all()
    assert len(db_users) == users_before + 1
    rest_tools.assert_user_in(db_users, resp_new_user)


def test_post_data_error_invalid_value(client):
    users_before = len(User.query.all())
    request_dict = {
        "uco": "should be a number",
        "email": "baz@foo.com",
        "username": "xuser",
    }
    response = rest_tools.make_request(client, '/users', json=request_dict, method='post')
    assert_response(response, 400)
    db_users = User.query.all()
    assert len(db_users) == users_before


def test_post_data_error_missing_uco_for_user(client):
    users_before = len(User.query.all())
    request_dict = {
        "email": "baz@foo.com",
        "username": "xuser",
    }

    response = rest_tools.make_request(client, '/users', json=request_dict, method='post')
    assert_response(response, 400)

    db_users = User.query.all()
    assert len(db_users) == users_before


def test_list(client):
    users_before = len(User.query.all())
    response = rest_tools.make_request(client, '/users')
    assert_response(response)

    users = rest_tools.extract_data(response)

    assert len(users) == users_before
    db_users = User.query.all()
    rest_tools.assert_user_in(db_users, users[0])


def test_list_filter_course(client):
    codename = 'testcourse1'
    cpp = Course.query.filter_by(codename=codename).first()
    users_before = [user for user in User.query.all() if cpp in user.courses]

    response = rest_tools.make_request(client, f'/users?course={codename}')
    assert_response(response)

    users = rest_tools.extract_data(response)
    assert len(users) == len(users_before)

    rest_tools.assert_user_in(users_before, users[0])


def test_list_filter_group(client):
    codename = 'testcourse1'
    cpp = Course.query.filter_by(codename=codename).first()
    g = [group for group in cpp.groups if group.name == "seminar01"][0]
    users_before = g.users

    response = rest_tools.make_request(
        client, f'/users?course={codename}&group=seminar01')
    assert_response(response)

    users = rest_tools.extract_data(response)
    assert len(users) == len(users_before)

    rest_tools.assert_user_in(users_before, users[0])


def test_list_filter_missing_course(client):
    response = rest_tools.make_request(client, '/users?group=g1')
    assert_response(response, 400)


def test_read(client):
    user = User.query.all()[0]
    response = rest_tools.make_request(client, f'/users/{user.id}')
    assert_response(response)

    data = rest_tools.extract_data(response)
    rest_tools.assert_user(user, data)


def test_update(client):
    users_before = len(User.query.all())
    user = User.query.all()[0]
    request_dict = {
        "name": "new_name",
        "email": "new_email@domain.net",
        "uco": 456789,
        "is_admin": True,
    }
    request_json = json.dumps(request_dict)
    response = rest_tools.make_request(client, f'/users/{user.id}', data=request_json,
                                       headers={"content-type": "application/json"}, method='put')

    assert_response(response, 204)

    updated_entity = User.query.filter_by(id=user.id).first()
    assert updated_entity.name == request_dict["name"]
    assert updated_entity.email == request_dict["email"]
    assert updated_entity.uco == request_dict["uco"]
    assert updated_entity.is_admin == request_dict["is_admin"]
    assert len(User.query.all()) == users_before


def test_delete(client):
    users_before = len(User.query.all())
    user = User.query.all()[0]

    response = rest_tools.make_request(client, f"/users/{user.id}", method='delete')
    assert_response(response, 204)

    db_users = User.query.all()
    assert len(db_users) == users_before - 1
    assert user not in db_users


def test_read_submissions(client):
    user = User.query.filter_by(username="student1").first()
    user_submissions = user.submissions

    response = rest_tools.make_request(client, f"/users/{user.id}/submissions")
    assert_response(response)

    # check if the result is paginated?
    submissions = rest_tools.extract_data(response)
    assert len(submissions) == len(user_submissions)
    rest_tools.assert_submission_in(user_submissions, submissions[0])


def test_read_submissions_filter_course(client):
    user = User.query.filter_by(username="student2").first()
    user_submissions = [
        s for s in user.submissions if s.project.course.codename == "testcourse1"]

    response = rest_tools.make_request(client, f"/users/{user.id}/submissions?course=testcourse1")
    assert_response(response)

    submissions = rest_tools.extract_data(response)
    assert len(submissions) == len(user_submissions)
    rest_tools.assert_submission_in(user_submissions, submissions[0])


def test_read_submissions_filter_course_project(portal_services, client):
    user = portal_services.find.user('student1')
    user_submissions = [s for s in user.submissions if
                        s.project.codename == "hw01" and s.course.codename == "testcourse1"]

    url = f"/users/{user.id}/submissions?course=testcourse1&project=hw01"
    response = rest_tools.make_request(client, url)
    assert_response(response)

    submissions = rest_tools.extract_data(response)
    assert len(submissions) == len(user_submissions)
    rest_tools.assert_submission_in(user_submissions, submissions[0])


def test_read_submissions_filter_missing_course(client):
    user = User.query.filter_by(username="student1").first()
    path = f"/users/{user.id}/submissions?project=p1"
    response = rest_tools.make_request(client, path)
    assert_response(response, 400)


def test_read_roles(client):
    user = User.query.filter_by(username="student2").first()
    roles = user.roles

    response = rest_tools.make_request(
        client, f"/users/{user.id}/roles")

    assert_response(response)

    user_roles = rest_tools.extract_data(response)
    assert len(user_roles) == len(roles)
    rest_tools.assert_role_in(roles, user_roles[0])


def test_read_roles_filter_course(client):
    user = User.query.filter_by(username="student2").first()
    roles = [role for role in user.roles if role.course.codename == "testcourse1"]
    response = rest_tools.make_request(client, f"/users/{user.id}/roles?course=testcourse1")

    assert_response(response)

    user_roles = rest_tools.extract_data(response)
    assert len(user_roles) == len(roles)
    rest_tools.assert_role_in(roles, user_roles[0])


def test_read_courses(client):
    user = User.query.filter_by(username="student2").first()
    courses = user.courses

    response = rest_tools.make_request(client, f"/users/{user.id}/courses")
    assert_response(response)

    user_courses = rest_tools.extract_data(response)
    assert len(user_courses) == len(courses)

    roles = Role.query.all()
    roles_filtered = [role for role in roles if user in role.clients]

    courses2 = [role.course for role in roles_filtered]
    assert len(courses2) == len(courses)
    rest_tools.assert_course_in(courses2, user_courses[0])


def test_read_groups(client):
    user = User.query.filter_by(username="student2").first()
    groups = user.groups
    response = rest_tools.make_request(client, f"/users/{user.id}/groups")

    assert_response(response)

    user_groups = rest_tools.extract_data(response)
    assert len(user_groups) == len(groups)
    rest_tools.assert_group_in(groups, user_groups[0])


def test_read_groups_filter_course(client):
    user = User.query.filter_by(username="student2").first()
    groups = [
        group for group in user.groups if group.course.codename == "testcourse1"]
    response = rest_tools.make_request(client, f"/users/{user.id}/groups?course=testcourse1")

    assert_response(response)

    user_groups = rest_tools.extract_data(response)
    assert len(user_groups) == len(groups)

    rest_tools.assert_group_in(groups, user_groups[0])


def test_user_reviews(client):
    user = User.query.filter_by(username="teacher1").first()
    reviews = []
    for item in user.review_items:
        if item.review not in reviews:
            reviews.append(item.review)

    response = rest_tools.make_request(client, f"/users/{user.id}/reviews")

    assert_response(response)

    user_reviews = rest_tools.extract_data(response)
    assert len(user_reviews) == len(reviews)
    

def test_user_projects(client):
    user = User.query.filter_by(username="student2").first()
    response = rest_tools.make_request(client, f"/users/{user.id}/projects")
    assert_response(response)
    user_projects = rest_tools.extract_data(response)
    for proj in user_projects:
        assert proj['next_time']


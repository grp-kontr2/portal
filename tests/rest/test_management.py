from tests.rest import rest_tools
from tests.rest.rest_tools import assert_response


def test_status_endpoint(client):
    headers = {"Content-Type": "application/json"}
    response = client.get("/api/v1.0/management/status", headers=headers)
    assert_response(response, 200)

    status = rest_tools.extract_data(response=response)
    assert status['status'] == 'works'

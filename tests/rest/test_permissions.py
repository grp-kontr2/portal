import pytest

from tests.rest import rest_tools
from tests.rest.rest_tools import assert_response, get_user_credentials, get_user_secret


@pytest.fixture
def teacher_secret() -> str:
    return get_user_secret('teacher1', 'ultimate_teacher_secret')


def test_project_detail_using_admin_cred(client):
    path = f'/courses/testcourse1'
    response = rest_tools.make_request(client, path)
    assert_response(response, 200)


def test_project_detail_using_student_cred(client, student_credentials):
    path = f'/courses/testcourse1'
    response = rest_tools.make_request(client, path, credentials=student_credentials)
    assert_response(response, 200)


def test_project_detail_using_teacher_cred(client, teacher_credentials):
    path = f'/courses/testcourse1/projects'
    response = rest_tools.make_request(client, path, credentials=teacher_credentials)
    assert_response(response, 200)


def test_project_detail_using_teacher_secret(client, teacher_secret):
    path = f'/courses/testcourse1'
    response = rest_tools.make_request(client, path, credentials=teacher_secret)
    assert_response(response, 200)

import pytest

from tests.rest.rest_tools import get_user_credentials


@pytest.fixture
def student_credentials() -> str:
    return get_user_credentials('student1')


@pytest.fixture
def teacher_credentials() -> str:
    return get_user_credentials('teacher2', '123123')


@pytest.fixture
def teacher1_credentials() -> str:
    return get_user_credentials('teacher1', '123123')

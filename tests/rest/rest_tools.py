import json as json_parser
from datetime import datetime

import flask
from flask import Response
from flask.testing import FlaskClient

from portal import logger
from portal.database.models import Course, Group, Project, ProjectConfig, Review, ReviewItem, Role, \
    Submission, User, Worker


def get_user_credentials(username, password='123456'):
    return json_parser.dumps({
        "type": "username_password",
        "identifier": username,
        "secret": password
    })


def get_user_secret(username, secret):
    return json_parser.dumps({
        "type": "secret",
        "identifier": username,
        "secret": secret
    })


DEFAULT_USER_CREDENTIALS = get_user_credentials('admin', '789789')

log = logger.get_logger(__name__)

API_PREFIX = "/api/v1.0"


def extract_data(response: Response) -> dict:
    data = response.get_json()
    log.debug(f"[EXTRACT] Data: {data}")
    return data


def make_request(client: FlaskClient, url: str, method: str = 'get',
                 credentials: str = None, headers: dict = None,
                 data: str = None, json: dict = None, cookies: dict = None) -> Response:
    """ Creates an authenticated request to an endpoint.

    Args:
        client: Flask's test client
        url: request url
        method: get, post, put
        credentials: json data for login
        headers: headers to send with the request
        data: json data to send in the request

    Returns(Response): a Werkzeug response

    """
    headers = {'content-type': 'application/json', **(headers or {})}
    credentials = credentials or DEFAULT_USER_CREDENTIALS
    access_token = __get_access_token(client, credentials=credentials)
    headers['Authorization'] = f"Bearer {access_token}"
    full_url = f"{API_PREFIX}{url}"
    log.debug(f"[REQ] ({method}) - \"{full_url}\"")
    if cookies is not None:
        for (k, v) in cookies.items():
            client.set_cookie('localhost', k, v)
    params = dict(headers=headers, follow_redirects=True, method=method)
    if json is not None and data is None:
        params['data'] = json_parser.dumps(json)
    else:
        params['data'] = data
    log.debug(f"[REQ] {full_url}: {params}")
    return client.open(full_url, **params)


def __get_access_token(client, credentials):
    log.debug(f"[REQ] Login credentials: {credentials}")
    login_resp = client.post(f"{API_PREFIX}/auth/login", data=credentials,
                             headers={"content-type": "application/json"})
    log.debug(f"[RESP] {login_resp}")
    resp_data = json_parser.loads(str(login_resp.get_data().decode("utf-8")))
    return resp_data['access_token']


def assert_user(expected: User, actual: dict):
    assert expected.id == actual['id']
    assert expected.uco == actual['uco']
    assert expected.email == actual['email']
    assert expected.username == actual['username']
    assert expected.name == actual['name']
    assert expected.is_admin == actual['is_admin']


def compare_user(expected: User, actual: dict) -> bool:
    return expected.id == actual['id'] and \
           expected.uco == actual['uco'] and \
           expected.email == actual['email']


def assert_user_in(user_list: list, user: dict) -> bool:
    return any(compare_user(u, user) for u in user_list)


def assert_submission(expected: Submission, actual: dict):
    assert expected.id == actual['id']
    assert expected.state.name == actual['state']
    assert expected.project.course_id == actual['project']["course"]["id"]
    assert expected.scheduled_for == actual['scheduled_for']
    assert expected.project_id == actual['project']['id']


def compare_submission(expected: Submission, actual: dict) -> bool:
    return expected.id == actual['id'] \
           and expected.state == actual['state'] \
           and expected.project.id == actual['project']['id'] \
           and expected.scheduled_for == actual['scheduled_for']


def assert_submission_in(submission_list: list, submission: dict):
    return any(compare_submission(s, submission) for s in submission_list)


def compare_worker(expected: Worker, actual: dict) -> bool:
    return expected.id == actual['id'] and \
           expected.url == actual['url'] and \
           expected.name == actual['name']


def assert_worker_in(worker_list: list, worker: dict):
    return any(compare_worker(c, worker) for c in worker_list)


def assert_worker(expected: Worker, actual: dict):
    assert expected.id == actual['id']
    assert expected.name == actual['name']
    assert expected.url == actual['url']


def assert_role(expected: Role, actual: dict):
    assert expected.id == actual['id']
    assert expected.name == actual['name']
    assert expected.description == actual['description']
    assert expected.course_id == actual['course']['id']


def assert_course(expected: Course, actual: dict):
    assert expected.id == actual['id']
    assert expected.name == actual['name']
    assert expected.codename == actual['codename']


def compare_course(expected: Course, actual: dict) -> bool:
    return expected.id == actual['id'] and \
           expected.codename == actual['codename']


def assert_course_in(course_list: list, course: dict) -> bool:
    return any(compare_course(c, course) for c in course_list)


def assert_group(expected: Group, actual: dict):
    assert expected.id == actual['id']
    assert expected.name == actual['name']
    assert expected.course_id == actual['course']['id']


def compare_group(expected: Group, actual: dict) -> bool:
    return expected.id == actual['id'] and expected.name == actual['name'] \
           and expected.course_id == actual['course']['id']


def assert_group_in(group_list: list, group: dict) -> bool:
    return any(compare_group(g, group) for g in group_list)


def compare_project(expected: Project, actual: dict) -> bool:
    return expected.id == actual['id'] and \
           expected.name == actual['name'] and \
           expected.course_id == actual['course']['id']


def assert_project(expected: Project, actual: dict):
    assert expected.id == actual['id']
    assert expected.name == actual['name']
    assert expected.course_id == actual['course']['id']


def assert_project_in(project_list: list, project: dict) -> bool:
    return any(compare_project(p, project) for p in project_list)


def assert_project_config(expected: ProjectConfig, actual: dict):
    assert expected.id == actual['id']
    assert expected.project.id == actual['project']['id']
    assert expected.test_files_source == actual['test_files_source']
    assert expected.file_whitelist == actual['file_whitelist']
    assert expected.config_subdir == actual['config_subdir']
    assert expected.submission_parameters == actual['submission_parameters']


def assert_role_permissions(expected: Role, actual: dict):
    assert expected.id == actual['id']

    assert expected.view_course_limited == actual['view_course_limited']
    assert expected.view_course_full == actual['view_course_full']
    assert expected.update_course == actual['update_course']
    assert expected.handle_notes_access_token == actual['handle_notes_access_token']

    assert expected.write_roles == actual['write_roles']
    assert expected.write_groups == actual['write_groups']
    assert expected.write_projects == actual['write_projects']

    assert expected.create_submissions == actual['create_submissions']
    assert expected.resubmit_submissions == actual['resubmit_submissions']
    assert expected.evaluate_submissions == actual['evaluate_submissions']

    assert expected.read_submissions_all == actual['read_submissions_all']
    assert expected.read_submissions_groups == actual['read_submissions_groups']
    assert expected.read_submissions_own == actual['read_submissions_own']
    assert expected.read_all_submission_files == actual['read_all_submission_files']

    assert expected.read_reviews_all == actual['read_reviews_all']
    assert expected.read_reviews_groups == actual['read_reviews_groups']
    assert expected.read_reviews_own == actual['read_reviews_own']

    assert expected.write_reviews_all == actual['write_reviews_all']
    assert expected.write_reviews_group == actual['write_reviews_group']
    assert expected.write_reviews_own == actual['write_reviews_own']


def compare_role(expected: Role, actual: dict) -> bool:
    return expected.id == actual['id'] \
           and expected.name == actual['name'] \
           and expected.description == actual['description'] \
           and expected.course_id == actual['course']['id']


def assert_role_in(role_list: list, role: dict) -> bool:
    return any(compare_role(r, role) for r in role_list)


def compare_review_items(expected: ReviewItem, actual: dict) -> bool:
    return expected.id == actual['id'] \
           and expected.user_id == actual['user']['id'] \
           and expected.content == actual['content'] \
           and expected.file == actual['file'] \
           and expected.line == actual['line'] \
           and expected.line_start == actual['line'] \
           and expected.line_end == actual['line']


def assert_review_item_in(item_list: list, item: dict) -> bool:
    return any(compare_review_items(i, item) for i in item_list)


def assert_review_items(expected: list, actual: dict):
    assert len(expected) == len(actual)
    for item in actual:
        assert_review_item_in(expected, item)


def assert_review(expected: Review, actual: dict):
    assert expected is not None
    assert expected.id == actual['id']
    assert expected.submission_id == actual['submission']['id']
    assert_review_items(expected.review_items, actual['review_items'])


# source:
# https://stackoverflow.com/questions/11875770/how-to-overcome-datetime-datetime-not-json-serializable
class DateTimeEncoder(json_parser.JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()

        return json_parser.JSONEncoder.default(self, o)


def assert_response(response: flask.Response, code=200, content_type='application/json'):
    assert response.status_code == code
    assert response.mimetype == content_type

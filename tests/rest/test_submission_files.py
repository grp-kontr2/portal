from pathlib import Path

from . import rest_tools


def assert_path(data: dict, path: str):
    current = data
    path = Path(path)
    for part in path.parts:
        current = current.get(part)

    assert Path(current) == path


def test_submission_sources_are_available(client, mocked_submission):
    url = f'/submissions/{mocked_submission.id}/files/sources?path=main.c'
    response = rest_tools.make_request(client, url)
    assert response.status_code == 200
    assert response.data
    assert response.data.decode('utf-8')


def test_submission_sources_are_not_available(client, mocked_submission):
    url = f'/submissions/{mocked_submission.id}/files/sources?path=nonexisting.c'
    response = rest_tools.make_request(client, url)
    assert response.status_code == 404


def test_submission_test_files_are_not_available(client, mocked_submission):
    url = f'/submissions/{mocked_submission.id}/files/test_files?path=nonexisting.c'
    response = rest_tools.make_request(client, url)
    assert response.status_code == 404


def test_submission_results_are_not_available(client, mocked_submission):
    url = f'/submissions/{mocked_submission.id}/files/results?path=nonexisting.c'
    response = rest_tools.make_request(client, url)
    assert response.status_code == 404


def test_submission_results_are_available(client, mocked_submission):
    s = mocked_submission
    url = f'/submissions/{s.id}/files/results?path=suite-stats.json'
    response = rest_tools.make_request(client, url)
    assert response.status_code == 200
    assert response.data
    assert response.data.decode('utf-8')


def test_submission_test_files_are_available(client, mocked_submission):
    s = mocked_submission
    url = f'/submissions/{s.id}/files/test_files?path=kontr_tests/instructions.py'
    response = rest_tools.make_request(client, url)
    assert response.status_code == 200
    assert response.data
    assert response.data.decode('utf-8')


def test_project_test_files_are_available(client, mocked_submission):
    s = mocked_submission
    url = f'/courses/{s.course.id}/projects/{s.project.id}/files?path=Dockerfile'
    response = rest_tools.make_request(client, url)
    assert response.status_code == 200
    assert response.data
    assert response.data.decode('utf-8')


def test_submission_sources_tree(client, mocked_submission):
    s = mocked_submission
    response = rest_tools.make_request(client, f'/submissions/{s.id}/files/sources/tree')
    assert response.status_code == 200
    assert response.data
    data = rest_tools.extract_data(response=response)
    assert_path(data, 'main.c')
    assert_path(data, 'main.cpp')


def test_submission_test_files_tree(client, mocked_submission):
    s = mocked_submission
    response = rest_tools.make_request(client, f'/submissions/{s.id}/files/test_files/tree')
    assert response.status_code == 200
    assert response.data
    data = rest_tools.extract_data(response=response)
    assert_path(data, 'Dockerfile')
    assert_path(data, 'Pipfile')


def test_submission_results_tree(client, mocked_submission):
    s = mocked_submission
    response = rest_tools.make_request(client, f'/submissions/{s.id}/files/results/tree')
    assert response.status_code == 200
    assert response.data
    data = rest_tools.extract_data(response)
    paths = ['suite-files.json', 'suite-stats.json', 'outputs/suite::gcc.stdout']
    for path in paths:
        assert_path(data, path)

import json
from datetime import timedelta

import pytest
import yaml
from flask_jwt_extended import create_access_token

from portal.database.models import Course, Project, User
from portal.tools.time import current_time, strip_seconds
from tests.rest.rest_tools import assert_response, get_user_credentials
from . import rest_tools


def test_list(client):
    cpp = Course.query.filter_by(codename="testcourse2").first()
    cpp_projects = len(cpp.projects)

    response = rest_tools.make_request(client, f'/courses/{cpp.codename}/projects')
    assert_response(response)

    projects = rest_tools.extract_data(response)
    assert len(projects) == cpp_projects
    cpp_updated = Course.query.filter_by(codename="testcourse1").first()
    for p in projects:
        rest_tools.assert_project_in(cpp_updated.projects, p)


def test_create(portal_services, client):
    cpp = portal_services.find.course('testcourse1')
    cpp_projects = len(cpp.projects)

    request_dict = {
        "name": "new_project",
        'codename': 'new-project',
        'description': 'ultimate project blah'
    }
    request_json = json.dumps(request_dict)
    response = rest_tools.make_request(client, f"/courses/{cpp.codename}/projects", method='post',
                                       data=request_json)
    assert_response(response, 201)

    new_project = rest_tools.extract_data(response)
    cpp_updated = portal_services.find.course('testcourse1')
    assert len(cpp_updated.projects) == cpp_projects + 1
    assert new_project['name'] == "new_project"
    assert new_project['id']
    assert new_project['submit_configurable'] == True
    rest_tools.assert_project_in(cpp_updated.projects, new_project)


def test_read(client):
    cpp = Course.query.filter_by(codename="testcourse1").first()
    p = cpp.projects[0]
    response = rest_tools.make_request(client, f"/courses/{cpp.codename}/projects/{p.name}")
    assert_response(response)

    project = rest_tools.extract_data(response)

    rest_tools.assert_project(p, project)


def test_update(portal_services, client):
    cpp = portal_services.find.course("testcourse1")
    p = cpp.projects[0]
    p_name = p.name
    request_dict = dict(name="new project name")
    request_json = json.dumps(request_dict)
    response = rest_tools.make_request(client, f"/courses/{cpp.codename}/projects/{p.name}",
                                       data=request_json, method='put')
    assert_response(response, 204)

    assert Project.query.filter(Project.course_id == cpp.id).filter(
        Project.name == "new project name").first()
    assert not Project.query.filter(Project.course_id == cpp.id).filter(
        Project.name == p_name).first()


def test_delete(client):
    cpp = Course.query.filter_by(codename="testcourse1").first()
    p = cpp.projects[0]
    p_name = p.name
    cpp_projects = len(cpp.projects)
    response = rest_tools.make_request(client, f"/courses/{cpp.codename}/projects/{p.name}",
                                       method="delete")

    assert_response(response, 204)
    cpp_updated = Course.query.filter_by(codename="testcourse1").first()
    assert len(cpp_updated.projects) == cpp_projects - 1
    assert not Project.query.filter(Project.course_id == cpp.id).filter(
        Project.name == p_name).first()


def test_config_read(client):
    cpp = Course.query.filter_by(codename="testcourse1").first()
    p = cpp.projects[0]

    response = rest_tools.make_request(client, f"/courses/{cpp.codename}/projects/{p.name}/config",
                                       method='get')
    assert_response(response)

    data = rest_tools.extract_data(response)
    rest_tools.assert_project_config(p.config, data)


def test_process_config_read(client):
    cpp = Course.query.filter_by(codename="testcourse1").first()
    p = cpp.projects[0]

    response = rest_tools.make_request(client,
                                       f"/courses/{cpp.codename}/projects/{p.name}/process_config",
                                       method='get')
    assert_response(response, content_type='application/yaml')

    config = yaml.safe_load(response.data)
    assert 'pre' in config.keys()
    assert 'post' in config.keys()
    assert 'scheduler' in config.keys()


def test_config_update(client):
    cpp = Course.query.filter_by(codename="testcourse1").first()
    p = cpp.projects[0]
    p_name = p.name

    new_time = current_time() + timedelta(days=2)
    request_dict = dict(
        test_files_source="new source",
        config_subdir='kontr2',
        submissions_allowed_to=new_time
    )
    request_json = json.dumps(request_dict, cls=rest_tools.DateTimeEncoder)

    response = rest_tools.make_request(client, f"/courses/{cpp.codename}/projects/{p.name}/config",
                                       data=request_json,
                                       method='put')
    assert_response(response, 204)

    p_updated = Project.query.filter(
        Project.course_id == cpp.id).filter_by(name=p_name).first()
    assert p_updated.config.test_files_source == "new source"
    assert p_updated.config.submissions_allowed_to == strip_seconds(new_time)


def test_list_submissions(client):
    cpp = Course.query.filter_by(codename="testcourse1").first()
    p = cpp.projects[0]

    user = User.query.filter_by(username="student2").first()
    access_token = create_access_token(identity=user.username)
    response = rest_tools.make_request(client,
                                       f"/courses/{cpp.codename}/projects/{p.name}/submissions",
                                       headers={"Authorization": f"Bearer {access_token}"},
                                       method='get')
    assert_response(response)
    submissions = rest_tools.extract_data(response)
    assert len(submissions) == len(p.submissions)
    for s in submissions:
        rest_tools.assert_submission_in(p.submissions, s)


def test_list_submissions_filter(client):
    cpp = Course.query.filter_by(codename="testcourse1").first()
    p = cpp.projects[0]
    user = User.query.filter_by(username="student2").first()
    user_submissions = len(
        [submission for submission in user.submissions if submission.project.course == cpp])
    access_token = create_access_token(identity=user.username)
    response = rest_tools.make_request(
        client,
        f"/courses/{cpp.codename}/projects/{p.name}/submissions?user=student2",
        headers={"Authorization": f"Bearer {access_token}"})
    assert_response(response)

    submissions = rest_tools.extract_data(response)
    assert len(submissions) == user_submissions
    for s in submissions:
        rest_tools.assert_submission_in(p.submissions, s)
        assert s['user']['id'] == user.id


@pytest.fixture
def request_dict() -> dict:
    return {
        "project_params": {},
        "file_params": {
            "source": {
                "type": "git",
                "url": "https://gitlab.fi.muni.cz/xkompis/test-hello-world.git",
                "branch": "master",
                "checkout": "master"
            }
        }
    }


@pytest.mark.celery(result_backend='redis://')
def test_create_submission(client, request_dict):
    cpp = Course.query.filter_by(codename="testcourse1").first()
    p = cpp.projects[0]
    p_submissions = len(p.submissions)

    path = f"/courses/{cpp.codename}/projects/{p.name}/submissions"
    response = rest_tools.make_request(client, path, json=request_dict, method='post')

    assert_response(response, code=201)

    new_submission = rest_tools.extract_data(response)
    p_updated = Project.query.filter(Project.course_id == cpp.id) \
        .filter_by(name=p.name).first()
    user_updated = User.query.filter_by(username="admin").first()

    assert len(p_updated.submissions) == p_submissions + 1
    rest_tools.assert_submission_in(p_updated.submissions, new_submission)
    rest_tools.assert_submission_in(user_updated.submissions, new_submission)


@pytest.mark.celery(result_backend='redis://')
def test_check_create_submission_as_admin(client, request_dict):
    cpp = Course.query.filter_by(codename="testcourse1").first()
    p = cpp.projects[0]

    path = f"/courses/{cpp.codename}/projects/{p.name}/submissions/check"
    response = rest_tools.make_request(client, path)

    assert_response(response, code=200)


@pytest.mark.celery(result_backend='redis://')
def test_create_submission_as_different_user(client, portal_services, request_dict):
    cpp = portal_services.find.course("testcourse1")

    project = cpp.projects[0]
    p_submissions = len(project.submissions)

    path = f"/courses/{cpp.codename}/projects/{project.codename}/submissions?user=student1"
    response = rest_tools.make_request(client, path, json=request_dict, method='post')

    assert_response(response, code=201)

    new_submission = rest_tools.extract_data(response)

    project = portal_services.projects.refresh(project)
    user_updated = portal_services.find.user("student1")

    assert len(project.submissions) == p_submissions + 1
    rest_tools.assert_submission_in(project.submissions, new_submission)
    rest_tools.assert_submission_in(user_updated.submissions, new_submission)


@pytest.mark.celery(result_backend='redis://')
def test_create_submission_as_student(client, ent_mocker, portal_services, request_dict):
    user, course, project = ent_mocker.create_user_in_course('testuser', role_type='student')
    path = f"/courses/{course.codename}/projects/{project.name}/submissions"
    user_credentials = get_user_credentials(user.username)
    response = rest_tools.make_request(client, path, json=request_dict, method='post',
                                       credentials=user_credentials)
    assert_response(response, code=201)

    new_submission = rest_tools.extract_data(response)
    assert new_submission['id']


@pytest.mark.celery(result_backend='redis://')
def test_check_create_submission_as_student(client, ent_mocker, portal_services, request_dict):
    user, course, project = ent_mocker.create_user_in_course('testuser', role_type='student')
    path = f"/courses/{course.codename}/projects/{project.name}/submissions/check"
    project
    user_credentials = get_user_credentials(user.username)
    response = rest_tools.make_request(client, path, credentials=user_credentials)
    assert_response(response, code=200)


@pytest.mark.celery(result_backend='redis://')
def test_create_submission_second_should_fail(client, ent_mocker, portal_services, request_dict):
    user, course, project = ent_mocker.create_user_in_course('testuser', role_type='student')
    path = f"/courses/{course.codename}/projects/{project.name}/submissions"
    user_credentials = get_user_credentials(user.username)
    response = rest_tools.make_request(client, path, json=request_dict, method='post',
                                       credentials=user_credentials)
    assert_response(response, code=201)

    new_submission = rest_tools.extract_data(response)
    assert new_submission['id']

    response = rest_tools.make_request(client, path, json=request_dict, method='post',
                                       credentials=user_credentials)

    assert_response(response, code=400)
    assert response.json['message']


@pytest.fixture()
def defaults_dict():
    return {
        "project_params": {},
        "file_params": {
            "source": {
                "type": "git",
                "branch": "master",
                "checkout": "master"
            }
        }
    }


@pytest.mark.celery(result_backend='redis://')
def test_create_submission_as_with_default_params(client, ent_mocker, portal_services,
                                                  defaults_dict):
    user, course, project = ent_mocker.create_user_in_course('testuser', role_type='student')
    path = f"/courses/{course.codename}/projects/{project.name}/submissions"
    user_credentials = get_user_credentials(user.username)
    response = rest_tools.make_request(client, path, json=defaults_dict, method='post',
                                       credentials=user_credentials)
    assert_response(response, code=201)
    new_submission = rest_tools.extract_data(response)
    assert new_submission['id']
    submission = portal_services.find.submission(new_submission['id'])
    assert submission.project == project
    assert submission.course == course
    assert submission.user == user
    assert submission.parameters['file_params']['from_dir'] == project.codename
    gitlab_url = f'git@gitlab.local:{user.username}/{course.codename}.git'
    assert submission.parameters['file_params']['source']['url'] == gitlab_url


@pytest.mark.celery(result_backend='redis://')
def test_refresh_test_files(client, ent_mocker, portal_services):
    course = portal_services.find.course('testcourse1')
    project = portal_services.find.project(course, 'hw01')
    path = f'/courses/{project.course.id}/projects/{project.id}/test-files-refresh'
    response = rest_tools.make_request(client, path, method='post')

    assert_response(response, code=204)

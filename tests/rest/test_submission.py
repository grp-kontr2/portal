import pytest
import responses

from portal import storage_wrapper
from portal.database import SubmissionState
from portal.database.models import Review, ReviewItem, Submission
from portal.tools.is_api_adapter import IsApiWrapper
from tests.rest.rest_tools import assert_response
from . import rest_tools


@pytest.fixture
def created_submission(portal_services, client) -> Submission:
    cpp = portal_services.find.course("testcourse1")
    p = cpp.projects[0]

    request_dict = {
        "project_params": {'tags': 'docker'},
        "file_params": {
            "source": {
                "type": "git",
                "url": "https://gitlab.fi.muni.cz/xkompis/test-hello-world.git",
                "branch": "master",
                "checkout": "master"
            }
        }
    }

    url = f"/courses/{cpp.codename}/projects/{p.name}/submissions"
    response = rest_tools.make_request(client, url, json=request_dict, method='post')
    assert_response(response, 201)
    new_submission = rest_tools.extract_data(response)
    return portal_services.find.submission(new_submission['id'])


def test_list_all_avail(client):
    db_subm = Submission.query.all()
    response = rest_tools.make_request(client, f'/submissions')
    assert_response(response, 200)

    resp_submissions = rest_tools.extract_data(response)
    assert len(resp_submissions) == len(db_subm)


def test_list_all_avail_for_user(portal_services, client):
    student = portal_services.find.user('student1')
    db_subm = Submission.query.filter(Submission.user == student).all()
    response = rest_tools.make_request(client, f'/submissions?users=student1')
    assert_response(response, 200)

    resp_submissions = rest_tools.extract_data(response)
    assert len(resp_submissions) == len(db_subm)


def test_list_all_avail_for_user_as_user(portal_services, client, student_credentials):
    student = portal_services.find.user('student1')
    db_subm = Submission.query.filter(Submission.user == student).all()
    response = rest_tools.make_request(client, f'/submissions', credentials=student_credentials)
    assert_response(response, 200)

    resp_submissions = rest_tools.extract_data(response)
    assert len(resp_submissions) == len(db_subm)


def test_list_all_avail_for_user_as_teacher(portal_services, client, teacher1_credentials):
    response = rest_tools.make_request(client, f'/submissions', credentials=teacher1_credentials)
    assert_response(response, 200)

    resp_submissions = rest_tools.extract_data(response)
    assert len(resp_submissions) == 5


def test_list_all_avail_for_course(portal_services, client):
    course = portal_services.find.course('testcourse1')
    db_subm = Submission.query.filter(Submission.course == course).all()
    response = rest_tools.make_request(client, f'/submissions?course=testcourse1', )
    assert_response(response, 200)
    resp_submissions = rest_tools.extract_data(response)
    num_of_submissions = len(resp_submissions)
    assert num_of_submissions == len(db_subm)
    assert num_of_submissions == 4


def test_list_all_avail_for_project(portal_services, client):
    course = portal_services.find.course('testcourse2')
    project = portal_services.find.project(course, 'hw01')
    db_subm = Submission.query.filter(Submission.project == project).all()
    response = rest_tools.make_request(client, f'/submissions?course=testcourse2&projects=hw01')
    assert_response(response, 200)

    resp_submissions = rest_tools.extract_data(response)
    assert len(resp_submissions) == len(db_subm)


# TODO: missing some tests (filter by group, role)
def test_list_all_avail_for_group(portal_services, client):
    course = portal_services.find.course('testcourse1')
    project = portal_services.find.project(course, 'hw01')
    group = portal_services.find.group(project.course, 'seminar01')
    db_subm = [sub for sub in Submission.query.filter(Submission.project == project).all() if
               group in sub.user.groups]
    response = rest_tools.make_request(client, f'/submissions?course=testcourse1&groups=seminar01')
    assert_response(response, 200)
    resp_submissions = rest_tools.extract_data(response)
    assert len(resp_submissions) == len(db_subm)


# missing tests for working with zip
def test_read(client):
    submissions = Submission.query.all()
    submission = submissions[0]
    response = rest_tools.make_request(client, f'/submissions/{submission.id}', )
    assert_response(response, 200)

    submission = rest_tools.extract_data(response)
    rest_tools.assert_submission_in(submissions, submission)


def test_read_stats(client):
    submissions = Submission.query.all()
    submission = submissions[0]
    response = rest_tools.make_request(client, f'/submissions/{submission.id}/stats')
    assert_response(response, 200)

    stats = rest_tools.extract_data(response)
    assert stats is not None


# missing tests for working with zip
def test_worker_params(client):
    submission: Submission = Submission.query.first()
    response = rest_tools.make_request(client, f'/submissions/{submission.id}/worker_params', )
    assert_response(response, 200)

    params = rest_tools.extract_data(response)
    assert params['test_files_hash'] == submission.project.config.test_files_commit_hash
    assert params['id'] == submission.id


def test_student_is_able_to_access_own_submission(client, portal_services, student_credentials):
    student = portal_services.find.user('student1')
    submission = student.submissions[0]
    response = rest_tools.make_request(client, f'/submissions/{submission.id}',
                                       credentials=student_credentials)

    assert_response(response, 200)


def test_delete(client):
    submissions = Submission.query.all()
    submissions_len = len(submissions)
    sub: Submission = submissions[0]
    assert sub.review
    reviews_len = len(Review.query.all())
    response = rest_tools.make_request(client, f'/submissions/{sub.id}', method='delete')
    assert_response(response, 204)
    submissions_updated = Submission.query.all()
    assert len(submissions_updated) == submissions_len - 1
    assert len(Review.query.all()) == reviews_len - 1

    # Submission sources should not exists
    storage_dir = sub.storage_dirname
    assert not storage_wrapper.submissions.get(storage_dir).path.exists()
    assert not storage_wrapper.submissions.get(storage_dir).zip_path.exists()
    assert not storage_wrapper.results.get(storage_dir).path.exists()
    assert not storage_wrapper.results.get(storage_dir).zip_path.exists()


def test_read_state(client):
    submissions = Submission.query.all()
    s = submissions[0]
    response = rest_tools.make_request(
        client, f'/submissions/{s.id}/state', )
    assert_response(response, 200)

    state = rest_tools.extract_data(response)['state']
    assert state == s.state.name


# needs more tests for state transitions
def test_update_state(client):
    request_dict = {"state": SubmissionState.CANCELLED.name}
    submission = Submission.query.filter_by(state=SubmissionState.IN_PROGRESS).first()
    response = rest_tools.make_request(client, f'/submissions/{submission.id}/state',
                                       json=request_dict,
                                       method='put')
    assert_response(response, 204)
    sub_up = Submission.query.filter_by(id=submission.id).first()
    assert sub_up.state == SubmissionState.CANCELLED


# does not pass - needs a Storage mock or config
def test_resubmit(client, created_submission):
    submission = created_submission

    request_dict = {"note": '{"message": "resubmit reason"}'}
    response = rest_tools.make_request(client, f'/submissions/{submission.id}/resubmit',
                                       json=request_dict, method='post')

    assert_response(response, 201)
    submission = rest_tools.extract_data(response)

    submissions_updated = Submission.query.all()
    rest_tools.assert_submission_in(submissions_updated, submission)


def test_read_review(client):
    submissions = Submission.query.all()
    s = submissions[0]
    assert s.review

    response = rest_tools.make_request(client, f'/submissions/{s.id}/review')
    assert_response(response, 200)

    review = rest_tools.extract_data(response)
    rest_tools.assert_review(expected=s.review, actual=review)


def test_create_review(client, created_submission):
    len_reviews = len(Review.query.all())
    len_review_items = len(ReviewItem.query.all())

    request_dict = {
        "review_items": [
            {
                "file": "src/main.c",
                "line": 10,
                "content": "This is wrong!"
            },
            {
                "file": "src/main.c",
                "line": 15,
                "content": "Nice job!"
            }
        ]
    }
    url = f'/submissions/{created_submission.id}/review'
    response = rest_tools.make_request(client, url, json=request_dict,
                                       method='post')

    assert_response(response, 201)

    review = rest_tools.extract_data(response)
    s_updated = Submission.query.filter_by(id=created_submission.id).first()
    assert len(Review.query.all()) == len_reviews + 1
    assert len(ReviewItem.query.all()) == len_review_items + 2
    rest_tools.assert_review(s_updated.review, review)


def test_delete_review(created_review, client):
    submission = created_review.submission
    item = created_review.review_items[0]
    url = f'/submissions/{submission.id}/review/{item.id}'
    response = rest_tools.make_request(client, url, method='delete')
    assert_response(response=response, code=204)
    assert not ReviewItem.query.filter_by(id=item.id).all()


def test_update_review(created_review, client):
    submission = created_review.submission
    item: ReviewItem = created_review.review_items[0]
    url = f'/submissions/{submission.id}/review/{item.id}'
    body = {
        "content": "Updated content"
    }
    response = rest_tools.make_request(client, url, json=body, method='put')
    assert_response(response=response, code=204)
    updated: ReviewItem = ReviewItem.query.filter_by(id=item.id).one()
    assert updated
    assert updated.content == 'Updated content'
    assert updated.line == item.line
    assert updated.file == item.file


def url_base(is_wrapper: IsApiWrapper):
    course = is_wrapper.course
    is_client = is_wrapper.is_client
    url = f"{is_client.url}?" \
        f"klic={course.notes_access_token};" \
        f"fakulta={is_client.faculty};" \
        f"kod={course.codename}"
    return url


def gen_url(is_wrapper: IsApiWrapper, operation: str):
    url = url_base(is_wrapper=is_wrapper)
    url += f"{operation}"
    return url


@responses.activate
def test_write_is_muni_notepad_for_submission(created_submission, client, portal_services):
    is_wrapper: IsApiWrapper = portal_services.is_api(created_submission.course).is_api
    content = "Foo points *2"
    operation = f";operace=blok-pis-student-obsah;zkratka={created_submission.project.codename};" \
        f"uco={created_submission.user.uco};obsah={content};prepis=a"
    url = gen_url(is_wrapper=is_wrapper, operation=operation)
    responses.add(responses.GET, url, body="<ZAPIS>Úspěšně uloženo.</ZAPIS>", status=200)

    surl = f'/submissions/{created_submission.id}/review/is_muni/notepad'
    response = rest_tools.make_request(client, surl, json=dict(content=content), method='post')
    assert_response(response=response, code=200)


@responses.activate
def test_read_is_muni_notepad_for_submission(created_submission, client, portal_services):
    BLOCKS_CONTENT = """
<BLOKY_OBSAH>
 <STUDENT>
  <OBSAH>25 bodu</OBSAH>
  <UCO>0</UCO>
  <ZMENENO>20160111104208</ZMENENO>
  <ZMENIL>444111222</ZMENIL>
 </STUDENT>
</BLOKY_OBSAH>
"""
    is_wrapper: IsApiWrapper = portal_services.is_api(created_submission.course).is_api
    content = "Foo points *2"
    operation = f";operace=blok-dej-obsah;zkratka={created_submission.project.codename};" \
        f"uco={created_submission.user.uco};"
    url = gen_url(is_wrapper=is_wrapper, operation=operation)
    responses.add(responses.GET, url, body=BLOCKS_CONTENT, status=200)

    surl = f'/submissions/{created_submission.id}/review/is_muni/notepad'
    response = rest_tools.make_request(client, surl, json=dict(content=content), method='get')
    assert_response(response=response, code=200)
    data = response.json
    assert isinstance(data, dict)
    content = data.get(str(created_submission.user.uco))
    assert content
    assert content == '25 bodu'

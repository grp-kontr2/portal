import json

import pytest

from portal.database.models import Client, Secret
from tests.rest import rest_tools
from tests.rest.rest_tools import assert_response


@pytest.fixture
def worker_credentials():
    return json.dumps({
        "type": "secret",
        "identifier": "executor",
        "secret": "executor_secret"
    })


def test_client_detail_using_student_cred(client, student_credentials):
    path = f'/client'
    response = rest_tools.make_request(
        client, path, credentials=student_credentials)
    assert_response(response, 200)
    data = response.json
    assert data['username'] == 'student1'
    assert data['type'] == 'ClientType.USER'


def test_client_detail_using_executor_cred(client, worker_credentials):
    path = f'/client'
    response = rest_tools.make_request(
        client, path, credentials=worker_credentials)
    assert_response(response, 200)

    data = response.json
    assert data['name'] == 'executor'
    assert data['type'] == 'ClientType.WORKER'


def test_create_secret(client):
    instance = Client.query.filter_by(codename="executor").first()
    request_dict = dict(name="new_secret")
    response = rest_tools.make_request(client, f'/clients/{instance.id}/secrets',
                                       json=request_dict,
                                       method='post')

    assert_response(response, 201)
    parsed = response.json
    assert parsed['value'] is not None
    assert parsed['id'] is not None

    secrets = Secret.query.filter_by(client=instance)
    assert len(secrets.all()) == 2
    assert secrets.filter_by(name='new_secret').first()


def test_list_secret(client):
    instance = Client.query.filter_by(codename="executor").first()
    response = rest_tools.make_request(client, f'/clients/{instance.id}/secrets')

    assert_response(response, 200)
    secrets = rest_tools.extract_data(response)

    assert len(secrets) == 1


def test_delete_secret(client):
    worker = Client.query.filter_by(codename="executor").first()
    secret = Secret.query.filter_by(client=worker).first()
    response = rest_tools.make_request(client, f'/clients/{worker.id}/secrets/{secret.id}',
                                       method='delete')

    assert response.status_code == 204

    assert Secret.query.filter_by(client=worker).count() == 0


def test_read_secret(client):
    instance = Client.query.filter_by(codename="executor").first()
    secret = Secret.query.filter_by(client=instance).first()
    response = rest_tools.make_request(client, f'/clients/{instance.id}/secrets/{secret.id}')

    assert_response(response, 200)
    response_secret = rest_tools.extract_data(response)
    assert response_secret['id'] == secret.id
    assert response_secret['name'] == secret.name

    assert Secret.query.filter_by(client=instance).count() == 1


def test_update_secret(client):
    instance = Client.query.filter_by(codename="executor").first()
    request_dict = dict(name="new_name")
    secret = Secret.query.filter_by(client=instance).first()
    response = rest_tools.make_request(client, f'/clients/{instance.id}/secrets/{secret.id}',
                                       json=request_dict,
                                       method='put')

    assert_response(response, 204)
    updated_secret = Secret.query.filter_by(client=instance).first()
    assert updated_secret.id == secret.id
    assert updated_secret.name == "new_name"

    assert Secret.query.filter_by(client=instance).count() == 1

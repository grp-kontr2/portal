import yaml

from portal.database import Course
from tests.rest import rest_tools


def test_read_courses_definition(client, portal_services):
    response = rest_tools.make_request(client, f"/definition/courses")
    rest_tools.assert_response(response, 200, content_type='text/yaml')
    definition = yaml.safe_load(response.data)
    assert definition.get('courses') is not None
    assert len(definition['courses']) > 0


def test_read_projects_definition(client, portal_services):
    course: Course = portal_services.find.course('testcourse1')
    url = f"/definition/courses/{course.id}/projects"
    response = rest_tools.make_request(client, url)
    rest_tools.assert_response(response, 200, content_type='text/yaml')
    definition = yaml.safe_load(response.data)
    assert definition.get('projects') is not None
    assert len(definition['projects']) > 0


def test_read_roles_definition(client, portal_services):
    course: Course = portal_services.find.course('testcourse1')
    url = f"/definition/courses/{course.id}/roles"
    response = rest_tools.make_request(client, url)
    rest_tools.assert_response(response, 200, content_type='text/yaml')
    definition = yaml.safe_load(response.data)
    assert definition.get('roles') is not None
    assert len(definition['roles']) > 0


def test_read_groups_definition(client, portal_services):
    course: Course = portal_services.find.course('testcourse1')
    url = f"/definition/courses/{course.id}/groups"
    response = rest_tools.make_request(client, url)
    rest_tools.assert_response(response, 200, content_type='text/yaml')
    definition = yaml.safe_load(response.data)
    assert definition.get('groups') is not None
    assert len(definition['groups']) > 0


def test_read_course_definition(client, portal_services):
    course: Course = portal_services.find.course('testcourse1')
    response = rest_tools.make_request(client, f"/definition/courses/{course.id}")
    rest_tools.assert_response(response, 200, content_type='text/yaml')
    definition = yaml.safe_load(response.data)
    assert definition['codename'] == course.codename
    assert definition['name'] == course.name


def test_read_project_definition(client, portal_services):
    course: Course = portal_services.find.course('testcourse1')
    entity = course.projects[0]
    url = f"/definition/courses/{course.id}/projects/{entity.id}"
    response = rest_tools.make_request(client, url)
    rest_tools.assert_response(response, 200, content_type='text/yaml')
    definition = yaml.safe_load(response.data)
    assert definition['codename'] == entity.codename
    assert definition['name'] == entity.name


def test_read_role_definition(client, portal_services):
    course: Course = portal_services.find.course('testcourse1')
    entity = course.roles[0]
    url = f"/definition/courses/{course.id}/roles/{entity.id}"
    response = rest_tools.make_request(client, url)
    rest_tools.assert_response(response, 200, content_type='text/yaml')
    definition = yaml.safe_load(response.data)
    assert definition['codename'] == entity.codename
    assert definition['name'] == entity.name


def test_read_group_definition(client, portal_services):
    course: Course = portal_services.find.course('testcourse1')
    entity = course.groups[0]
    url = f"/definition/courses/{course.id}/groups/{entity.id}"
    response = rest_tools.make_request(client, url)
    rest_tools.assert_response(response, 200, content_type='text/yaml')
    definition = yaml.safe_load(response.data)
    assert definition['codename'] == entity.codename
    assert definition['name'] == entity.name


def test_create_course_from_definition(client, portal_services):
    course: Course = portal_services.find.course('testcourse1')
    course_def = portal_services.resource_definition.dump_course(course)
    course_def['codename'] = course_def['codename'] + '-new'
    yaml_dump = yaml.safe_dump(course_def)
    response = rest_tools.make_request(client, f"/definition/courses",
                                       headers={'Content-type': 'text/yaml'},
                                       method='post', data=yaml_dump)
    rest_tools.assert_response(response, 201)
    created = rest_tools.extract_data(response)
    assert created['codename'] == course_def['codename']
    assert created['name'] == course_def['name']

    c_course: Course = portal_services.find.course('testcourse1-new')
    rest_tools.assert_course(c_course, created)


def test_create_project_from_definition(client, portal_services):
    course: Course = portal_services.find.course('testcourse1')
    project = course.projects[0]
    project_def = portal_services.resource_definition.dump_project(project)
    project_def['codename'] = project_def['codename'] + '-new'
    yaml_dump = yaml.safe_dump(project_def)
    url = f"/definition/courses/{course.id}/projects"
    response = rest_tools.make_request(client, url, headers={'Content-type': 'text/yaml'},
                                       method='post', data=yaml_dump)
    rest_tools.assert_response(response, 201)
    created = rest_tools.extract_data(response)
    assert created['codename'] == project_def['codename']
    assert created['name'] == project_def['name']

    c_entity = portal_services.find.project(course, created['codename'])
    rest_tools.assert_project(c_entity, created)


def test_create_role_from_definition(client, portal_services):
    course: Course = portal_services.find.course('testcourse1')
    role = course.roles[0]
    role_def = portal_services.resource_definition.dump_role(role)
    role_def['codename'] = role_def['codename'] + '-new'
    yaml_dump = yaml.safe_dump(role_def)
    url = f"/definition/courses/{course.id}/roles"
    response = rest_tools.make_request(client, url, headers={'Content-type': 'text/yaml'},
                                       method='post', data=yaml_dump)
    rest_tools.assert_response(response, 201)
    created = rest_tools.extract_data(response)
    assert created['codename'] == role_def['codename']
    assert created['name'] == role_def['name']

    c_entity = portal_services.find.role(course, created['codename'])
    rest_tools.assert_role(c_entity, created)


def test_create_group_from_definition(client, portal_services):
    course: Course = portal_services.find.course('testcourse1')
    group = course.roles[0]
    group_def = portal_services.resource_definition.dump_group(group)
    group_def['codename'] = group_def['codename'] + '-new'
    yaml_dump = yaml.safe_dump(group_def)
    url = f"/definition/courses/{course.id}/groups"
    response = rest_tools.make_request(client, url, headers={'Content-type': 'text/yaml'},
                                       method='post', data=yaml_dump)
    rest_tools.assert_response(response, 201)
    created = rest_tools.extract_data(response)
    assert created['codename'] == group_def['codename']
    assert created['name'] == group_def['name']

    c_entity = portal_services.find.group(course, created['codename'])
    rest_tools.assert_group(c_entity, created)


def test_update_course_from_definition(client, portal_services):
    course: Course = portal_services.find.course('testcourse1')
    course_def = portal_services.resource_definition.dump_course(course)
    old_def = {**course_def}
    course_def['name'] = 'New course name'
    course_def['description'] = 'new course description'
    yaml_dump = yaml.safe_dump(course_def)
    response = rest_tools.make_request(client, f"/definition/courses",
                                       headers={'Content-type': 'text/yaml'},
                                       method='post', data=yaml_dump)
    rest_tools.assert_response(response, 201)
    updated = rest_tools.extract_data(response)

    assert updated['codename'] == course_def['codename']
    assert updated['name'] == course_def['name']

    assert updated['codename'] == old_def['codename']
    assert updated['name'] != old_def['name']
    assert updated['description'] != old_def['description']

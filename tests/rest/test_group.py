import json

from portal.database.models import Course, Group, Role, User
from tests.rest.rest_tools import assert_response
from . import rest_tools


def test_list(client):
    cpp = Course.query.filter_by(codename="testcourse1").first()
    response = rest_tools.make_request(
        client, f'/courses/{cpp.codename}/groups')
    assert_response(response, 200)

    groups = rest_tools.extract_data(response)
    assert len(groups) == len(cpp.groups)
    db_courses = Group.query.all()
    rest_tools.assert_group_in(db_courses, groups[0])


def test_create(client):
    cpp = Course.query.filter_by(codename="testcourse1").first()
    groups_before = len(cpp.groups)

    request_dict = dict(
        name="newgrp",
        codename='newgrp'
    )
    response = rest_tools.make_request(client, f"/courses/{cpp.codename}/groups", json=request_dict,
                                       method='post')
    assert_response(response, 201)

    resp_new_group = rest_tools.extract_data(response)

    cpp_updated = Course.query.filter_by(codename="testcourse1").first()
    assert len(cpp_updated.groups) == groups_before + 1
    rest_tools.assert_group_in(cpp_updated.groups, resp_new_group)
    assert Group.query.filter(Group.course_id == cpp_updated.id).filter(
        Group.name == "newgrp").first()


def test_read(client):
    cpp = Course.query.filter_by(codename="testcourse1").first()
    g = cpp.groups[0]
    response = rest_tools.make_request(
        client, f"/courses/{cpp.codename}/groups/{g.name}")
    assert response.status_code == 200
    assert response.mimetype == 'application/json'

    group = rest_tools.extract_data(response)
    rest_tools.assert_group(g, group)


def test_update(client):
    cpp = Course.query.filter_by(codename="testcourse1").first()
    g = cpp.groups[0]
    request_dict = dict(
        name="newgrp",
    )
    request_json = json.dumps(request_dict)
    response = rest_tools.make_request(client, f"/courses/{cpp.codename}/groups/{g.name}",
                                       data=request_json, method='put')
    assert response.status_code == 204
    assert response.mimetype == 'application/json'

    g_updated = Group.query.filter(
        Group.course_id == cpp.id).filter(Group.id == g.id).first()
    assert g_updated.name == "newgrp"


def test_delete(client):
    cpp = Course.query.filter_by(codename="testcourse1").first()
    cpp_groups = len(cpp.groups)
    len_groups = len(Group.query.all())
    g = cpp.groups[0]
    response = rest_tools.make_request(client, f"/courses/{cpp.codename}/groups/{g.name}",
                                       method='delete')

    assert response.status_code == 204
    assert response.mimetype == 'application/json'
    cpp_updated = Course.query.filter_by(codename="testcourse1").first()
    assert len(Group.query.all()) == len_groups - 1
    assert len(cpp_updated.groups) == cpp_groups - 1


def test_list_users(client):
    cpp = Course.query.filter_by(codename="testcourse1").first()
    g = cpp.groups[0]
    response = rest_tools.make_request(client, f"/courses/{cpp.codename}/groups/{g.id}/users")

    assert response.status_code == 200
    assert response.mimetype == 'application/json'
    users = rest_tools.extract_data(response)
    assert len(users) == len(g.users)
    for user in users:
        rest_tools.assert_user_in(g.users, user)


def test_list_users_filter(client):
    cpp = Course.query.filter_by(codename="testcourse1").first()
    g = cpp.groups[0]
    role = Role.query.filter(Role.course_id == cpp.id).filter_by(
        name="student").first()
    students = [user for user in g.users if role in user.roles]
    response = rest_tools.make_request(client,
                                       f"/courses/{cpp.codename}/groups/{g.id}/users?role={role.name}")
    assert response.status_code == 200
    assert response.mimetype == 'application/json'
    users = rest_tools.extract_data(response)
    assert len(users) == len(students)
    for user in users:
        rest_tools.assert_user_in(students, user)


def test_update_users_add(client):
    tc2 = Course.query.filter_by(codename="testcourse2").first()
    groups = [group for group in tc2.groups if group.name == "seminar01"]
    g = groups[0]

    g_users = len(g.users)
    user = User.query.filter_by(username="student1").first()
    assert user not in g.users

    request_dict = dict(
        add=[user.id]
    )
    request_json = json.dumps(request_dict)
    response = rest_tools.make_request(client, f"/courses/{tc2.codename}/groups/{g.name}/users",
                                       data=request_json, method='put')

    assert response.status_code == 204
    assert response.mimetype == 'application/json'

    g_updated = Group.query.filter(
        Group.course_id == tc2.id).filter(Group.id == g.id).first()
    assert len(g_updated.users) == g_users + 1
    assert user in g_updated.users


def test_update_users_add_duplicate(client):
    cpp = Course.query.filter_by(codename="testcourse1").first()
    g = cpp.groups[0]
    g_users = len(g.users)
    user = g.users[0]
    assert user in g.users

    request_dict = dict(
        add=[user.id]
    )
    request_json = json.dumps(request_dict)
    response = rest_tools.make_request(client, f"/courses/{cpp.codename}/groups/{g.name}/users",
                                       data=request_json, method='put')

    assert response.status_code == 204
    assert response.mimetype == 'application/json'

    g_updated = Group.query.filter(
        Group.course_id == cpp.id).filter(Group.id == g.id).first()
    assert len(g_updated.users) == g_users
    assert user in g_updated.users


def test_update_users_remove(client):
    cpp = Course.query.filter_by(codename="testcourse1").first()
    g = cpp.groups[0]
    g_users = len(g.users)
    user = g.users[0]
    assert user in g.users

    request_dict = dict(
        remove=[user.id]
    )
    request_json = json.dumps(request_dict)
    response = rest_tools.make_request(client, f"/courses/{cpp.codename}/groups/{g.name}/users",
                                       data=request_json,
                                       method='put')

    assert response.status_code == 204
    assert response.mimetype == 'application/json'

    g_updated = Group.query.filter(
        Group.course_id == cpp.id).filter(Group.id == g.id).first()
    assert len(g_updated.users) == g_users - 1
    assert user not in g_updated.users


def test_update_users_remove_user_was_not_in(client):
    c = Course.query.filter_by(codename="testcourse2").first()
    groups = [group for group in c.groups if group.name == "seminar01"]
    g = groups[0]

    g_users = len(g.users)
    user = User.query.filter_by(username="student1").first()
    assert user not in g.users

    request_dict = dict(
        remove=[user.id]
    )
    request_json = json.dumps(request_dict)
    response = rest_tools.make_request(client, f"/courses/{c.codename}/groups/{g.name}/users",
                                       data=request_json, method='put')

    assert response.status_code == 204
    assert response.mimetype == 'application/json'

    g_updated = Group.query.filter(
        Group.course_id == c.id).filter(Group.id == g.id).first()
    assert len(g_updated.users) == g_users
    assert user not in g_updated.users


def test_add_user(client):
    tc2 = Course.query.filter_by(codename="testcourse2").first()
    groups = [group for group in tc2.groups if group.name == "seminar01"]
    g = groups[0]

    group_users = len(g.users)
    u = User.query.filter_by(username="student1").first()
    assert g not in u.groups

    path = f"/courses/{tc2.codename}/groups/{g.name}/users/{u.username}"
    response = rest_tools.make_request(client, path, method='put')
    assert response.status_code == 204
    assert response.mimetype == 'application/json'

    g_updated = Group.query.filter(
        Group.course_id == tc2.id).filter(Group.id == g.id).first()
    assert u in g_updated.users
    assert len(g_updated.users) == group_users + 1


def test_add_user_duplicate(client):
    cpp = Course.query.filter_by(codename="testcourse1").first()
    g = cpp.groups[0]
    group_users = len(g.users)
    u = g.users[0]

    path = f"/courses/{cpp.codename}/groups/{g.name}/users/{u.username}"
    response = rest_tools.make_request(client, path, method='put')
    assert response.status_code == 204
    assert response.mimetype == 'application/json'

    g_updated = Group.query.filter(
        Group.course_id == cpp.id).filter(Group.id == g.id).first()
    assert u in g_updated.users
    assert group_users == len(g_updated.users)


def test_remove_user(client):
    cpp = Course.query.filter_by(codename="testcourse1").first()
    g = cpp.groups[0]
    group_users = len(g.users)
    u = g.users[0]

    path = f"/courses/{cpp.codename}/groups/{g.name}/users/{u.username}"
    response = rest_tools.make_request(client, path, method='delete')
    assert response.status_code == 204
    assert response.mimetype == 'application/json'

    g_updated = Group.query.filter(
        Group.course_id == cpp.id).filter(Group.id == g.id).first()
    assert u not in g_updated.users
    assert len(g_updated.users) == group_users - 1


def test_remove_user_not_in_group(client):
    tc2 = Course.query.filter_by(codename="testcourse2").first()
    groups = [group for group in tc2.groups if group.name == "seminar01"]
    g = groups[0]

    group_users = len(g.users)
    u = User.query.filter_by(username="student1").first()
    assert g not in u.groups

    path = f"/courses/{tc2.codename}/groups/{g.name}/users/{u.username}"
    response = rest_tools.make_request(client, path, method='delete')
    response.status_code == 400

    g_updated = Group.query.filter(
        Group.course_id == tc2.id).filter(Group.id == g.id).first()
    assert u not in g_updated.users
    assert len(g_updated.users) == group_users


# only basic test, should be expanded
def test_import(client):
    c = Course.query.filter_by(codename="testcourse2").first()
    c_groups = len(c.groups)
    request_dict = {
        "source_course": "testcourse1",
        "source_group": "seminar01",
        "with_users": "with_users",
    }
    request_json = json.dumps(request_dict)
    response = rest_tools.make_request(client, f"/courses/{c.codename}/groups/import",
                                       data=request_json, method='put')

    assert response.status_code == 201
    assert response.mimetype == "application/json"
    c_updated = Course.query.filter_by(codename="testcourse2").first()
    assert len(c_updated.groups) == c_groups + 1

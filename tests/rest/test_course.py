import json

import yaml

from portal.database.models import Course
from tests.rest.rest_tools import assert_response
from . import rest_tools


def test_list(client):
    response = rest_tools.make_request(client, '/courses')
    assert_response(response)

    courses_response = rest_tools.extract_data(response)
    assert len(courses_response) == 2
    db_courses = Course.query.all()
    rest_tools.assert_course_in(db_courses, courses_response[0])
    rest_tools.assert_course_in(db_courses, courses_response[1])


def test_create(client):
    request_dict = dict(name="java", codename="PA165")
    db_courses_number = len(Course.query.all())

    response = rest_tools.make_request(client, '/courses', json=request_dict, method='post')
    assert_response(response, 201)
    resp_new_course = rest_tools.extract_data(response)
    db_courses = Course.query.all()
    assert len(db_courses) == db_courses_number + 1
    rest_tools.assert_course_in(db_courses, resp_new_course)


def test_create_extra_key(client):
    request_dict = dict(
        name="java",
        codename="PA165",
        extra="should not be here"
    )
    db_courses_number = len(Course.query.all())

    response = rest_tools.make_request(client, '/courses', json=request_dict, method='post')
    assert_response(response, 201)

    resp_new_course = rest_tools.extract_data(response)
    db_courses = Course.query.all()
    assert len(db_courses) == db_courses_number + 1
    rest_tools.assert_course_in(db_courses, resp_new_course)


def test_create_missing_name(client):
    request_dict = dict(codename="PA165")
    db_courses_number = len(Course.query.all())

    response = rest_tools.make_request(client, '/courses', json=request_dict, method='post')
    assert_response(response, 400)

    assert len(Course.query.all()) == db_courses_number


def test_create_invalid_value(client):
    request_dict = dict(
        name=456,
        codename="PA165",
    )
    db_courses_number = len(Course.query.all())

    response = rest_tools.make_request(client, '/courses', json=request_dict, method='post')
    assert_response(response, 400)
    assert len(Course.query.all()) == db_courses_number


def test_read(client):
    c = Course.query.filter_by(codename="testcourse1").first()
    response = rest_tools.make_request(client, f"/courses/{c.id}")
    assert_response(response, 200)
    course = rest_tools.extract_data(response)

    rest_tools.assert_course(c, course)


def test_read_definition(client):
    course: Course = Course.query.filter_by(codename="testcourse1").first()
    response = rest_tools.make_request(client, f"/courses/{course.id}/definition")
    assert_response(response, 200, content_type='text/yaml')
    definition = yaml.safe_load(response.data)
    assert definition['codename'] == course.codename
    assert definition['name'] == course.name


def test_read_course_clients(client):
    course = Course.query.filter_by(codename="testcourse2").first()
    response = rest_tools.make_request(client, f"/courses/{course.id}/clients")
    assert_response(response, 200)
    users = rest_tools.extract_data(response)
    course_users = set()
    for role in course.roles:
        course_users.update([c.id for c in role.clients])
    assert users
    assert len(users) == 5
    assert len(course_users) == len(users)


def test_read_course_client_type_workers(client):
    course = Course.query.filter_by(codename="testcourse1").first()
    response = rest_tools.make_request(client, f"/courses/{course.id}/clients?type=worker")
    assert_response(response, 200)
    workers = rest_tools.extract_data(response)
    assert workers
    assert len(workers) == 1


def test_read_course_client_type_users(portal_services, client):
    course = Course.query.filter_by(codename="testcourse1").first()
    response = rest_tools.make_request(client, f"/courses/{course.id}/clients?type=user")
    assert_response(response, 200)
    users = rest_tools.extract_data(response)
    assert users
    assert len(users) == 5


def test_update(client):
    c = Course.query.filter_by(codename="testcourse1").first()
    request_dict = dict(
        name="new_cpp",
        codename="testcourse1_new",
    )
    request_json = json.dumps(request_dict)
    response = rest_tools.make_request(client, f'/courses/{c.id}', data=request_json, method='put')

    assert_response(response, 204)

    assert not Course.query.filter_by(codename="testcourse1").first()
    course_from_db = Course.query.filter_by(codename="testcourse1-new").first()
    assert course_from_db.name == "new_cpp"


def test_delete(client):
    c = Course.query.filter_by(codename="testcourse2").first()
    response = rest_tools.make_request(client, f'/courses/{c.id}', method='delete')
    assert_response(response, 204)
    assert not Course.query.filter_by(codename="testcourse2").first()


def test_notes_token_read(client):
    c = Course.query.filter_by(codename="testcourse2").first()
    response = rest_tools.make_request(
        client, f"/courses/{c.id}/notes_access_token")
    assert_response(response, 200)

    data = rest_tools.extract_data(response)
    assert data == "testcourse2_token"


def test_notes_token_create(client):
    c = Course.query.filter_by(codename="testcourse2").first()
    response = rest_tools.make_request(client, f"/courses/{c.id}/notes_access_token",
                                       json={"token": "new_token"}, method='put')
    assert_response(response, 204)

    updated = Course.query.filter_by(codename="testcourse2").first()
    assert updated.notes_access_token == "new_token"


def test_notes_token_update(client):
    c = Course.query.filter_by(codename="testcourse1").first()
    response = rest_tools.make_request(client, f"/courses/{c.id}/notes_access_token",
                                       json={"token": "new_token"}, method='put')
    assert_response(response, 204)

    updated = Course.query.filter_by(codename="testcourse1").first()
    assert updated.notes_access_token == "new_token"


def test_copy_from_course(portal_services, client):
    source: Course = portal_services.find.course('testcourse1')
    target = portal_services.courses.create(name='test_course', codename='test')
    request_dict = {
        "source_course": "testcourse2",
        "config": {
            "roles": "with_clients",
            "groups": "with_users",
            "projects": "bare",
        }
    }
    response = rest_tools.make_request(client, f'/courses/{target.codename}/import',
                                       json=request_dict, method='put')
    assert_response(response, 200)

    updated_course: Course = portal_services.find.course(target.codename)

    assert_collections(source, updated_course, 'projects')
    assert_collections(source, updated_course, 'roles')
    assert_collections(source, updated_course, 'groups')


def assert_collections(expected: Course, actual: Course, collection: str):
    def extract_names(obj, coll_name: str):
        coll = getattr(obj, coll_name)
        return [e.name for e in coll]

    expected_names = extract_names(expected, collection)
    actual_names = extract_names(actual, collection)
    for elem in actual_names:
        assert elem in expected_names

import logging

import gitlab
import pytest
import responses

from tests.rest import rest_tools
from tests.rest.rest_tools import assert_response

log = logging.getLogger(__name__)


def gl_gen_project(pid, gl_url):
    pname = f"project{pid}"
    uname = 'admin'
    bname = f'{uname}/{pname}'
    links_base = f"{gl_url}/api/v4/projects/{pid}"
    pdict = {
        "id": pid,
        "description": "",
        "name": pname,
        "name_with_namespace": f"Admin User / {pname}",
        "path": pname,
        "path_with_namespace": bname,
        "created_at": "2018-02-20T16:20:10.853+01:00",
        "default_branch": "master",
        "tag_list": [],
        "ssh_url_to_repo": f"git@gitlab.fi.muni.cz:{bname}.git",
        "http_url_to_repo": f"{gl_url}/{bname}.git",
        "web_url": f"{gl_url}/{bname}",
        "readme_url": None,
        "avatar_url": None,
        "star_count": 0,
        "forks_count": 0,
        "last_activity_at": "2019-03-14T21:26:14.678+01:00",
        "namespace": {
            "id": 31,
            "name": uname,
            "path": uname,
            "kind": "user",
            "full_path": uname,
            "parent_id": None
        },
        "_links": {
            "self": links_base,
            "issues": f"{links_base}/issues",
            "merge_requests": f"{links_base}/merge_requests",
            "repo_branches": f"{links_base}/repository/branches",
            "labels": f"{links_base}/labels",
            "events": f"{links_base}/events",
            "members": f"{links_base}/members"
        },
        "archived": False,
        "visibility": "private",
        "owner": {
            "id": 26,
            "name": "Admin User",
            "username": uname,
            "state": "active",
            "avatar_url": f"{gl_url}/uploads/-/system/user/avatar/26/avatar.png",
            "web_url": gl_url + "/admin"
        },
        "resolve_outdated_diff_discussions": False,
        "container_registry_enabled": True,
        "issues_enabled": True,
        "merge_requests_enabled": True,
        "wiki_enabled": True,
        "jobs_enabled": True,
        "snippets_enabled": True,
        "shared_runners_enabled": True,
        "lfs_enabled": True,
        "creator_id": 26,
        "import_status": "none",
        "import_error": None,
        "open_issues_count": 0,
        "runners_token": f"token-{pname}",
        "public_jobs": True,
        "ci_config_path": None,
        "shared_with_groups": [],
        "only_allow_merge_if_pipeline_succeeds": False,
        "request_access_enabled": False,
        "only_allow_merge_if_all_discussions_are_resolved": False,
        "printing_merge_request_link_enabled": True,
        "merge_method": "merge",
        "permissions": {
            "project_access": {
                "access_level": 40,
                "notification_level": 3
            },
            "group_access": None
        },
        "approvals_before_merge": 0,
        "mirror": False,
        "external_authorization_classification_label": None,
        "packages_enabled": None
    }
    return pdict


@pytest.fixture()
def gl_user():
    gl_url = "https://gitlab.local"
    uname = 'admin'
    return {
        "id": 26,
        "name": "Test user",
        "username": uname,
        "state": "active",
        "avatar_url": f"{gl_url}/uploads/-/system/user/avatar/26/avatar.png",
        "web_url": f"{gl_url}/{uname}",
        "created_at": "2016-04-25T16:46:02.862+02:00",
        "bio": "",
        "location": "",
        "public_email": "",
        "skype": "",
        "linkedin": "",
        "twitter": "",
        "website_url": "",
        "organization": "",
        "last_sign_in_at": "2019-03-29T10:02:25.337+01:00",
        "confirmed_at": "2017-10-12T15:56:15.152+02:00",
        "last_activity_on": "2019-04-02",
        "email": f"{uname}@example.com",
        "theme_id": 2,
        "color_scheme_id": 5,
        "projects_limit": 25,
        "current_sign_in_at": "2019-04-02T17:18:31.223+02:00",
        "identities": [
            {
                "provider": "ldapmain",
                "extern_uid": f"uid={uname},ou=people,dc=example,dc=com"
            }
        ],
        "can_create_group": False,
        "can_create_project": True,
        "two_factor_enabled": False,
        "external": False,
        "private_profile": None,
        "shared_runners_minutes_limit": None
    }


@pytest.fixture()
def gl_kontr_client(portal_services) -> gitlab.Gitlab:
    return portal_services.kontr_gitlab.gl_client


@pytest.fixture()
def gl_url(portal_services) -> str:
    return portal_services.flask_app.config.get('GITLAB_URL')


@pytest.fixture()
def gl_api_url(gl_url) -> str:
    return gl_url + '/api/v4'


@pytest.fixture()
def gl_cookies(portal_services) -> dict:
    cookies = dict(
        gitlab_token=portal_services.kontr_gitlab.token,
        gitlab_token_type=portal_services.kontr_gitlab.token_type,
        username='admin',
        gitlab_username='admin',
    )
    return cookies


@pytest.fixture()
def data_gl_projects(gl_url) -> list:
    projects = [gl_gen_project(gl_url=gl_url, pid=i) for i in range(100, 110)]
    log.debug(f"[GEN] Generated projects: {projects}")
    return projects


@pytest.mark.gitlab
@responses.activate
def test_list_gitlab_projects(client, gl_api_url, data_gl_projects, gl_cookies, gl_user):
    responses.add(responses.GET, f"{gl_api_url}/user", json=gl_user, status=200)
    responses.add(responses.GET, f"{gl_api_url}/projects",
                  json=data_gl_projects, status=200)
    # 'https://gitlab.local/api/v4/user'
    response = rest_tools.make_request(client, '/gitlab/projects', cookies=gl_cookies)
    assert_response(response)

    gl_projects = rest_tools.extract_data(response)
    assert len(gl_projects) > 0
    assert data_gl_projects == data_gl_projects


@pytest.mark.gitlab
@responses.activate
def test_get_gitlab_project(client, gl_api_url, data_gl_projects, gl_cookies, gl_user):
    project = data_gl_projects[0]
    pname: str = project['path_with_namespace']
    namespace = pname.replace('/', '%2F')
    responses.add(responses.GET, f"{gl_api_url}/user", json=gl_user, status=200)
    responses.add(responses.GET, f"{gl_api_url}/projects/{namespace}", json=project, status=200)
    # 'https://gitlab.local/api/v4/user'
    response = rest_tools.make_request(client, f'/gitlab/project?project={pname}',
                                       cookies=gl_cookies)
    assert_response(response)

    res_proj = rest_tools.extract_data(response)
    assert res_proj
    assert res_proj == project


@pytest.mark.gitlab
@responses.activate
def test_enable_member_for_gl_project(client, gl_api_url, data_gl_projects, gl_cookies, gl_user):
    project = data_gl_projects[0]
    pname: str = project['path_with_namespace']
    namespace = pname.replace('/', '%2F')
    responses.add(responses.GET, f"{gl_api_url}/user", json=gl_user, status=200)
    responses.add(responses.GET, f"{gl_api_url}/projects/{namespace}", json=project, status=200)
    responses.add(responses.POST, f"{gl_api_url}/projects/{project['id']}/members",
                  json=project, status=200)
    # 'https://gitlab.local/api/v4/user'
    response = rest_tools.make_request(client, f'/gitlab/project/members?project={pname}',
                                       method='post', cookies=gl_cookies)
    assert_response(response)


@pytest.mark.gitlab
@responses.activate
def test_set_gitlab_service_cookies(client, gl_api_url, gl_user, gl_cookies):
    responses.add(responses.GET, f"{gl_api_url}/user", json=gl_user, status=200)

    # 'https://gitlab.local/api/v4/user'
    response = rest_tools.make_request(client, f'/gitlab/service_token')
    assert_response(response)
    res_proj = rest_tools.extract_data(response)
    assert len(res_proj) == 6
    assert res_proj['username'] == gl_cookies['username']
    assert res_proj['gitlab_username'] == gl_cookies['gitlab_username']
    assert res_proj['gitlab_token'] == gl_cookies['gitlab_token']
    assert res_proj['gitlab_token_type'] == gl_cookies['gitlab_token_type']
    assert res_proj['gitlab_url']
    assert res_proj['gitlab_domain']

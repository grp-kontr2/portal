from portal.database import Group, Role, User


def add_user_to_course(user: User, role: Role, group: Group = None):
    user.roles.append(role)
    if group:
        user.groups.append(group)
    return user


def test_list_all_projects_that_user_belongs_to_should_all(session, db_student, db_c1_role1,
                                                           db_c1_project1, db_c1_project2,
                                                           db_c1_group1, db_c2_group1, db_c2_role2):
    add_user_to_course(db_student, db_c1_role1, db_c1_group1)
    add_user_to_course(db_student, db_c2_role2, db_c2_group1)
    session.add(db_student)
    session.add(db_c1_role1)
    session.flush()

    all_projects = db_student.get_all_projects()
    assert len(all_projects) == 4


def test_list_all_projects_that_user_belongs_to_should_course1(session, db_student, db_c1_role1,
                                                               db_c1_project1, db_c1_project2,
                                                               db_c1_group1, db_c2_group1,
                                                               db_c2_role2):
    add_user_to_course(db_student, db_c1_role1, db_c1_group1)
    session.add(db_student)
    session.add(db_c1_role1)
    session.flush()

    all_projects = db_student.get_all_projects()
    assert len(all_projects) == 2


def test_list_all_projects_user_one_group(session, db_student, db_c1_role1,
                                          db_c1_project1, db_c1_project2,
                                          db_c1_group1, db_c2_group1, db_c2_role2):
    add_user_to_course(db_student, db_c1_role1, db_c1_group1)
    add_user_to_course(db_student, db_c2_role2)
    session.add(db_student)
    session.add(db_c1_role1)
    session.flush()

    all_projects = db_student.get_all_projects_in_group()
    assert len(all_projects) == 2

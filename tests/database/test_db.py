from datetime import MAXYEAR, MINYEAR, datetime, timedelta
from time import sleep

import pytest
from sqlalchemy import exc

from portal.database.models import Client, Course, Group, Project, Review, \
    ReviewItem, Role, Secret, Submission, User
from portal.database import ProjectState
from portal.database.enums import ClientType
from portal.tools import time


@pytest.fixture(autouse=True)
def course1(session) -> Course:
    course = Course(name="Test course 1", codename="tc1")
    session.add(course)
    return course


@pytest.fixture(autouse=True)
def course2(session) -> Course:
    course = Course(name="Test course 2", codename="tc2")
    session.add(course)
    return course


@pytest.fixture(autouse=True)
def user1(session) -> User:
    user = User(username="tuser1", email="tuser1@example.com", uco=100)
    session.add(user)
    return user


@pytest.fixture(autouse=True)
def user2(session) -> User:
    user = User(username="tuser2", email="tuser2@example.com", uco=200)
    session.add(user)
    return user


def bind_user(session, user):
    session.add(user)
    session.flush()
    client = Client(client_type=ClientType.USER, owner_id=user.id)
    session.add(client)
    session.flush()
    return client


# Entity constraints & creation
def test_user_create_valid(session, user1):
    user1.name = "Test user"
    session.flush()
    users = User.query.all()
    assert len(users) == 2
    assert user1 in users
    clients = Client.query.all()
    assert len(clients) == 2


def test_user_create_duplicate_email(session, user1, user2):
    user1.email = user2.email

    with pytest.raises(exc.IntegrityError):
        session.flush()


def user_update_duplicate_username(session, user1, user2):
    session.flush()

    user2.username = user1.username
    with pytest.raises(exc.IntegrityError):
        session.flush()


def user_update_duplicate_uco(session, user1, user2):
    session.flush()

    user2.uco = user1.uco
    with pytest.raises(exc.IntegrityError):
        session.flush()


def user_update_duplicate_email(session, user1, user2):
    session.add_all([user1, user2])
    session.flush()

    user2.email = user1.email
    with pytest.raises(exc.IntegrityError):
        session.flush()


def test_course_create_valid(session, course1):
    session.flush()

    courses = Course.query.all()
    assert len(courses) == 2
    assert course1 in courses


def test_course_update_duplicate_name_will_not_raise(session, course1, course2):
    session.flush()

    course2.name = course1.name
    session.flush()


def test_course_update_duplicate_codename(session, course1, course2):
    session.flush()

    course2.codename = course1.codename
    with pytest.raises(exc.IntegrityError):
        session.flush()


def test_group_create_valid(session, course1):
    course = Course(name="C++", codename="testcourse1")
    group = Group(course=course, name="g1")
    # session.add(course)  # unnecessary, relationship takes care of
    # persisting the linked Course entity
    session.add(group)
    session.flush()

    groups = Group.query.all()
    assert len(groups) == 1
    assert group in groups
    assert groups[0].course == course


def test_group_update_duplicate_codename_1(session):
    course = Course(name="C++", codename="testcourse1")
    group1 = Group(course=course, codename="g1")
    group2 = Group(course=course, codename="g2")
    session.add_all([group1, group2])
    session.flush()

    group2.codename = group1.codename
    with pytest.raises(exc.IntegrityError):
        session.flush()


def test_group_update_duplicate_name_2(session, course1, course2):
    group1 = Group(course=course1, codename="g1")
    group2 = Group(course=course2, codename="g1")
    session.add_all([group1, group2])
    session.flush()

    group2.course = group1.course
    with pytest.raises(exc.IntegrityError):
        session.flush()


def test_project_create_valid(session, course1):
    project = Project(course=course1, codename="p1")
    session.add(project)
    session.flush()

    projects = Project.query.all()
    assert len(projects) == 1
    assert project in projects
    assert projects[0].course == course1


def test_project_update_duplicate_name_1(session, course1):
    project1 = Project(course=course1, codename="p1")
    project2 = Project(course=course1, codename="p2")
    session.add_all([project1, project2])
    session.flush()

    project2.codename = project1.codename
    with pytest.raises(exc.IntegrityError):
        session.flush()


def test_project_update_duplicate_name_2(session, course1, course2):
    project1 = Project(course=course1, codename="p1")
    project2 = Project(course=course2, codename="p1")
    session.add_all([project1, project2])
    session.flush()

    project2.course = project1.course
    with pytest.raises(exc.IntegrityError):
        session.flush()


def test_role_create_valid(session, course1):
    role = Role(course=course1, name='student')
    session.add(role)
    session.flush()

    roles = Role.query.all()
    assert len(roles) == 1
    assert role in roles
    assert roles[0].course == course1


def test_role_create_duplicate_name(session, course1):
    role1 = Role(course=course1, name='student')
    role2 = Role(course=course1, name='student')
    session.add_all([role1, role2])
    with pytest.raises(exc.IntegrityError):
        session.flush()


def test_role_update_duplicate_codename_1(session, course1):
    role1 = Role(course=course1, codename="p1")
    role2 = Role(course=course1, codename="p2")
    session.add_all([role1, role2])
    session.flush()

    role2.codename = role1.codename
    with pytest.raises(exc.IntegrityError):
        session.flush()


def test_role_update_duplicate_name_2(session, course1, course2):
    role1 = Role(course=course1, codename="p1")
    role2 = Role(course=course2, codename="p1")
    session.add_all([role1, role2])
    session.flush()

    role2.course = role1.course
    with pytest.raises(exc.IntegrityError):
        session.flush()


def test_submission_create_valid(session, user1, course1):
    project = Project(course=course1, name="p1")
    submission = Submission(user=user1, project=project, parameters={})
    submission.note['message'] = "This is a message!"
    session.add(submission)
    session.flush()

    submissions = Submission.query.all()
    assert len(submissions) == 1
    assert submission in submissions
    assert submissions[0].user == user1


def test_submission_update_user(session, user1, user2, course1):
    project = Project(course=course1, codename="p1")
    submission = Submission(user=user1, project=project, parameters={})

    session.add_all([submission])
    session.flush()

    submission.user = user2
    with pytest.raises(exc.SQLAlchemyError):
        session.flush()


# updated_at is changing; all entities share this behaviour
def test_user_updated_at(session, user1):
    import time
    session.flush()
    assert user1.created_at == user1.updated_at

    time.sleep(1)
    user1.name = "bar"
    session.flush()
    assert user1.created_at <= user1.updated_at


# Relationships testing
# Course
def test_relation_course_group_create_duplicate_group_assign(session):
    course = Course(name="C++", codename="testcourse1")
    group = Group(course=course, name="g1")
    course.groups.append(group)
    session.add(course)
    session.add(group)
    session.flush()

    assert group.id is not None
    course_from_db = Course.query.filter_by(name="C++").first()
    assert group in course_from_db.groups
    assert len(course_from_db.groups) == 1


def test_relation_course_group_update_add_group(session):
    course = Course(name="C++", codename="testcourse1")
    group1 = Group(course=course, codename="g1")
    session.add(course)
    session.add(group1)
    session.flush()

    group2 = Group(course=course, codename="g2")
    assert group2.id is None
    session.add(group2)
    session.flush()
    assert group2.id is not None
    assert group2.id != group1.id

    assert len(Group.query.all()) == 2
    assert group2 in Group.query.all()
    course_from_db = Course.query.filter_by(name="C++").first()
    assert len(course_from_db.groups) == 2
    assert group1 in course_from_db.groups
    assert group2 in course_from_db.groups


def test_relation_course_group_delete_course(session, course1, course2):
    # propagating delete from parent entity (course) to child entity (group)
    # by deleting the owning entity
    group1 = Group(course=course1, name="g1")
    group2 = Group(course=course1, name="g2")
    group3 = Group(course=course2, name="g3")

    session.add_all([group1, group2, group3])
    session.flush()
    assert len(Group.query.all()) == 3
    assert len(Course.query.all()) == 2

    session.delete(course1)
    session.flush()
    assert len(Group.query.all()) == 1
    assert len(Course.query.all()) == 1


def test_relation_course_group_delete_group_from_course(session, course1):
    # propagating delete from parent entity (course) to child entity (group)
    # by deleting an item in parent collection
    group1 = Group(course=course1, name="g1")
    group2 = Group(course=course1, name="g2")

    session.add_all([group1, group2])
    session.flush()
    assert len(Group.query.all()) == 2

    del course1.groups[0]
    session.flush()
    assert len(Group.query.all()) == 1
    assert len(course1.groups) == 1


def test_relation_course_group_delete_group_entity(session, course1):
    group1 = Group(course=course1, name="g1")
    group2 = Group(course=course1, name="g2")
    session.add(group1)
    session.add(group2)
    session.flush()

    session.delete(group1)
    # flush may not be necessary: ensures the appropriate change in FOREIGN_KEY on group,
    # but NOT the group collection present in course
    session.flush()
    # expire forces reloading of accessed attributes of 'course'.
    # See docs.sqlalchemy.org/en/latest/orm/session_basics.html
    # and
    # http://docs.sqlalchemy.org/en/latest/orm/session_state_management.html#session-expire
    session.expire(course1)
    assert group2 in course1.groups
    assert len(course1.groups) == 1


def test_relation_course_role_update_add_role(session, course1):
    role1 = Role(course=course1, name='student')
    session.add(role1)
    session.flush()

    role2 = Role(course=course1, name='teacher')
    # role2 is added to the session automatically because of
    # cascade=save-update on course
    session.flush()

    roles = Role.query.all()
    assert len(roles) == 2
    assert role1 in roles
    assert role2 in roles
    assert len(Course.query.all()[0].roles) == 2


def test_relation_course_role_update_swap_role(session, course1, course2):
    role = Role(course=course1, name='student')
    session.add(role)
    session.flush()
    assert role in course1.roles
    assert role not in course2.roles

    course2.roles.append(role)
    session.flush()

    assert len(Role.query.all()) == 1
    assert len(course1.roles) == 0
    assert len(course2.roles) == 1


def test_relation_course_role_update_null_course(session, course1):
    role1 = Role(course=course1, name='student')
    role2 = Role(course=course1, name='teacher')
    session.add(role1)
    session.flush()
    assert len(course1.roles) == 2

    role1.course = None
    session.flush()

    assert role2 in course1.roles
    assert len(course1.roles) == 1


def test_relation_course_role_delete_course(session, course1, course2):
    role1 = Role(course=course1, name='teacher')
    role2 = Role(course=course1, name='student')
    role3 = Role(course=course2, name='teacher')

    session.add_all([role1, role2, role3])
    session.flush()

    session.delete(course1)
    session.flush()
    assert len(Course.query.all()) == 1
    assert len(Role.query.all()) == 1
    assert role3 in course2.roles


def test_relation_course_role_delete_role_from_course(session, course1):
    role1 = Role(course=course1, name='student')
    role2 = Role(course=course1, name='teacher')
    session.add_all([role1, role2])
    session.flush()

    del course1.roles[1]
    session.flush()
    assert len(course1.roles) == 1
    assert len(Role.query.all()) == 1


def test_relation_course_role_delete_role_entity(session, course1):
    role1 = Role(course=course1, name='student')
    role2 = Role(course=course1, name='teacher')
    session.add_all([role1, role2])
    session.flush()

    session.delete(role2)
    session.flush()
    session.expire(course1)

    assert len(Role.query.all()) == 1
    assert len(course1.roles) == 1


def test_relation_course_project_update_add_project(session, course1):
    project1 = Project(course=course1, name="p1")
    session.add(project1)
    session.flush()
    assert len(course1.projects) == 1
    assert project1 in course1.projects

    project2 = Project(course=course1, name="p2")
    session.add(project2)
    session.flush()

    assert len(Project.query.all()) == 2
    assert len(course1.projects) == 2
    assert project2 in course1.projects


def test_relation_course_project_update_swap_project_from_project(session, course1, course2):
    project = Project(course=course1, name="p1")
    session.add_all([project, course2])
    session.flush()
    assert project in course1.projects
    assert len(course2.projects) == 0

    project.course = course2
    session.flush()

    assert project in course2.projects
    assert len(course1.projects) == 0


def test_relation_course_project_update_swap_project_from_course(session, course1, course2):
    project = Project(course=course1, name="p1")
    session.add_all([project])
    session.flush()
    assert project in course1.projects
    assert len(course2.projects) == 0

    course2.projects.append(project)
    session.flush()

    assert project in course2.projects
    assert len(course1.projects) == 0


def test_relation_course_project_update_null_course(session, course1):
    project1 = Project(course=course1, name="p1")
    project2 = Project(course=course1, name="p2")
    session.add_all([project1, project2])
    session.flush()

    assert len(course1.projects) == 2
    assert project1 in course1.projects

    project1.course = None
    session.flush()

    assert len(course1.projects) == 1
    assert project2 in course1.projects


def test_relation_course_project_delete_course(session, course1, course2):
    project1 = Project(course=course1, name="p1")
    project2 = Project(course=course1, name="p2")
    project3 = Project(course=course2, name="p3")

    session.add_all([project1, project2, project3])
    session.flush()

    session.delete(course1)
    session.flush()
    assert len(Course.query.all()) == 1
    assert len(Project.query.all()) == 1
    assert project3 in course2.projects


def test_relation_course_project_delete_project_from_course(session, course1, course2):
    project1 = Project(course=course1, name="p1")
    project2 = Project(course=course1, name="p2")
    project3 = Project(course=course2, name="p3")

    session.add_all([project1, project2, project3])
    session.flush()

    del course1.projects[1]
    session.flush()

    assert len(course1.projects) == 1
    assert project1 in course1.projects
    assert len(Project.query.all()) == 2


def test_relation_course_project_delete_project_entity(session, course1, course2):
    project1 = Project(course=course1, name="p1")
    project2 = Project(course=course1, name="p2")
    project3 = Project(course=course2, name="p3")

    session.add_all([project1, project2, project3])
    session.flush()

    session.delete(project2)
    session.flush()
    session.expire(course1)

    assert len(course1.projects) == 1
    assert project1 in course1.projects
    assert len(Project.query.all()) == 2


# client-role-course
def test_relation_client_role(session, user1, user2, course1, course2):
    role1 = Role(course=course1, name='teacher')
    role2 = Role(course=course2, name='teacher')

    role1.clients.append(user1)
    role1.clients.append(user2)
    role2.clients.append(user1)

    session.add_all([role1, role2])
    session.flush()

    assert len(Role.query.all()) == 2
    assert role1 in user1.roles
    assert role1 in user2.roles
    assert course1 in user1.courses
    assert course1 in user1.courses
    assert course1 in user2.courses
    assert course1 in user2.courses
    assert role2 in user1.roles
    assert role2 not in user2.roles
    assert course2 in user1.courses
    assert course2 not in user2.courses


def test_relation_user_roles_add_role_same_course(session, user1, course1):
    role1 = Role(course=course1, name='student')
    role2 = Role(course=course1, name='teacher')
    # a user may have several roles in a course
    session.add_all([role1, role2])
    session.flush()

    role1.clients.append(user1)
    session.flush()

    assert len(user1.roles) == 1
    assert len(role1.clients) == 1
    assert len(role2.clients) == 0
    assert role1 in user1.roles
    assert len(user1.courses) == 1
    assert len(user1.courses) == 1

    role2.clients.append(user1)
    session.flush()
    session.expire(user1)

    assert len(user1.roles) == 2
    assert len(user1.courses) == 1
    assert len(user1.courses) == 1
    assert user1 in role2.clients
    assert user1 in role1.clients
    assert role2 in user1.roles


# Project timing
def test_project_time_constraints_valid(session, course1):
    project = Project(course=course1, name="p1")
    project.config.submissions_allowed_from = time.current_time() - timedelta(hours=1)
    project.config.submissions_allowed_to = time.current_time()
    project.config.archive_from = time.current_time() + timedelta(days=1)

    session.add(project)
    session.flush()
    assert project.config.submissions_allowed_from < project.config.submissions_allowed_to
    assert project.config.submissions_allowed_to < project.config.archive_from


def test_project_time_constraints_valid_extreme(session, course1):
    project = Project(course=course1, name="p1")
    project.config.submissions_allowed_from = datetime(year=MINYEAR, month=1, day=1,
                                                       tzinfo=time.portal_timezone)
    project.config.submissions_allowed_to = time.current_time()
    project.config.archive_from = datetime(year=MAXYEAR, month=12, day=31,
                                           tzinfo=time.portal_timezone)

    session.add(project)
    session.flush()
    assert project.config.submissions_allowed_from < project.config.submissions_allowed_to
    assert project.config.submissions_allowed_to < project.config.archive_from


def test_project_time_constraints_valid_missing_from(session, course1):
    project = Project(course=course1, name="p1")
    project.config.submissions_allowed_to = time.current_time()
    project.config.archive_from = time.current_time() + timedelta(days=1)

    session.add(project)
    session.flush()
    assert project.config.submissions_allowed_to < project.config.archive_from


def test_project_time_constraints_valid_missing_to(session, course1):
    project = Project(course=course1, name="p1")
    project.config.submissions_allowed_from = time.current_time() - timedelta(hours=1)
    project.config.archive_from = time.current_time() + timedelta(days=1)

    session.add(project)
    session.flush()
    assert project.config.submissions_allowed_from < project.config.archive_from


def test_project_time_constraints_valid_missing_archive(session, course1):
    project = Project(course=course1, name="p1")
    project.config.submissions_allowed_from = time.current_time() - timedelta(hours=1)
    project.config.submissions_allowed_to = time.current_time()

    session.add(project)
    session.flush()
    assert project.config.submissions_allowed_from < project.config.submissions_allowed_to


def test_project_time_constraints_same_from_to(session, course1):
    project = Project(course=course1, name="p1")
    project.config.submissions_allowed_from = time.current_time()
    project.config.submissions_allowed_to = time.current_time()

    session.add(project)
    session.flush()


def test_project_time_constraints_same_to_archive(session, course1):
    project = Project(course=course1, name="p1")
    project.config.submissions_allowed_to = time.current_time()
    project.config.archive_from = time.current_time()

    session.add(project)
    session.flush()


def test_project_time_constraints_same_from_archive(session, course1):
    project = Project(course=course1, name="p1")
    project.config.submissions_allowed_from = time.current_time()
    project.config.archive_from = time.current_time()

    session.add(project)
    session.flush()


def _get_secret(expires=None):
    return Secret(name='test-secret', expires_at=expires, value='test-value')


def test_secret_expiration_is_empty(session, user1):
    secret = _get_secret()
    user1.secrets.append(secret)
    session.add(secret)
    session.flush()

    assert not secret.expired


def test_secret_expiration_now(session, user1):
    secret = _get_secret(time.current_time())
    user1.secrets.append(secret)
    session.add(secret)
    session.flush()

    assert secret.expired


def test_secret_expiration_yesterday(session, user1):
    secret = _get_secret(time.current_time() - timedelta(days=1))
    user1.secrets.append(secret)
    session.add(secret)
    session.flush()

    assert secret.expired


def test_secret_expiration_tomorrow(session, user1):
    secret = _get_secret(time.current_time() + timedelta(days=1))
    user1.secrets.append(secret)
    session.add(secret)
    session.flush()

    assert not secret.expired


def test_project_time_constraints_invalid_from_to(session, course1):
    project = Project(course=course1, name="p1")
    project.config.submissions_allowed_from = time.current_time()
    with pytest.raises(exc.SQLAlchemyError):
        project.config.submissions_allowed_to = time.current_time() - timedelta(minutes=1)


def test_project_time_constraints_invalid_from_archive(session, course1):
    project = Project(course=course1, name="p1")
    project.config.submissions_allowed_from = time.current_time()
    with pytest.raises(exc.SQLAlchemyError):
        project.config.archive_from = time.current_time() - timedelta(minutes=1)


def test_project_time_constraints_invalid_to_archive(session, course1):
    project = Project(course=course1, name="p1")
    project.config.submissions_allowed_to = time.current_time()
    with pytest.raises(exc.SQLAlchemyError):
        project.config.archive_from = time.current_time() - timedelta(minutes=1)


def test_project_state(session, course1):
    project = Project(course=course1, name="p1")

    days_delta = time.current_time() - timedelta(days=1)
    time_to_arch = time.current_time() + timedelta(days=1)
    project.config.submissions_allowed_from = days_delta
    project.config.submissions_allowed_to = time.current_time()
    project.config.archive_from = time_to_arch

    session.add(project)
    session.flush()

    assert project.get_state_by_timestamp(days_delta) == ProjectState.ACTIVE
    hour_less = time.current_time() - timedelta(hours=1)
    assert project.get_state_by_timestamp(hour_less) == ProjectState.ACTIVE
    min_less = time.current_time() - timedelta(minutes=1)
    assert project.get_state_by_timestamp(min_less) == ProjectState.ACTIVE
    sleep(3)
    assert project.get_state_by_timestamp() == ProjectState.INACTIVE
    day_min_less = time.current_time() - timedelta(days=1, minutes=1)
    assert project.get_state_by_timestamp(day_min_less) == ProjectState.INACTIVE

    assert project.get_state_by_timestamp(time_to_arch) == ProjectState.ARCHIVED


def test_project_config_update_dates(session, course1):
    project = Project(course=course1, name="p1")
    session.add(project)
    session.flush()

    project.config.submissions_allowed_to = time.current_time()
    session.flush()
    assert project.config.submissions_allowed_to == time.strip_seconds(
        time.current_time())


def test_role_permissions_create(session, course1):
    role = Role(course=course1, name='teacher')
    permissions = {
        "view_course_limited": True,
        "view_course_full": True,
        "read_roles": True,
        "read_groups": True,
        "read_submissions_all": True
    }
    role.set_permissions(**permissions)
    session.add(role)
    session.flush()

    assert role.view_course_limited is True
    assert role.view_course_full is True
    assert role.read_submissions_all is True
    assert role.evaluate_submissions is False


def test_client_role(session, course1, user1):
    role = Role(course=course1, name='teacher')
    user1.roles.append(role)
    session.add(user1)
    session.flush()

    assert role in user1.roles
    assert course1 in user1.courses


def test_user_submission_review(session, course1, user1):
    project = Project(course=course1, name="p1")

    submission = Submission(user=user1, project=project, parameters={})
    session.add(submission)
    session.flush()
    assert submission in user1.submissions
    review = Review(submission=submission)

    ri1 = ReviewItem(user=user1, review=review, file="foo", line=10, content="something")
    ri2 = ReviewItem(user=user1, review=review, file="bar", line=1, content="something else")
    session.flush()

    assert ri1 in user1.review_items
    assert ri2 in user1.review_items


def test_user_roles_for_course(session, user1, course1, course2):
    teacher_cpp = Role(course=course1, name='teacher')
    lect_cpp = Role(course=course1, name='lecturer')
    student_java = Role(course=course2, name='student')

    user1.roles.append(teacher_cpp)
    user1.roles.append(lect_cpp)
    user1.roles.append(student_java)

    session.add(user1)
    session.flush()

    assert len(user1.roles) == 3

    cpp_roles = user1.get_roles_in_course(course=course1)
    assert len(cpp_roles) == 2
    assert teacher_cpp in cpp_roles
    assert lect_cpp in cpp_roles
    assert len(course1.get_clients_filtered(client_type=ClientType.USER)) == 1

    java_roles = user1.get_roles_in_course(course=course2)
    assert len(java_roles) == 1
    assert student_java in java_roles


def test_user_groups_for_course(session):
    user = User(uco=123, email='foo', username='xfoo')
    course = Course(name="C++", codename="PB161")
    c_java = Course(name="Java", codename="PB162")

    g1_cpp = Group(course=course, name='group1')
    g2_cpp = Group(course=course, name='group2')
    g1_java = Group(course=c_java, name='g1')

    user.groups.append(g1_cpp)
    user.groups.append(g2_cpp)
    user.groups.append(g1_java)

    session.add(user)
    session.flush()

    assert len(user.groups) == 3

    cpp_groups = user.query_groups_in_course(course=course).all()
    assert len(cpp_groups) == 2
    assert g1_cpp in cpp_groups
    assert g2_cpp in cpp_groups

    java_groups = user.query_groups_in_course(course=c_java).all()
    assert len(java_groups) == 1
    assert g1_java in java_groups


def test_user_groups_for_course_and_project(session):
    user = User(uco=123, email='foo', username='xfoo')
    course = Course(name="C++", codename="PB161")
    c_java = Course(name="Java", codename="PB162")
    project = Project(name="hw01", course=course)

    g1_cpp = Group(course=course, name='group1')
    g2_cpp = Group(course=course, name='group2')
    g1_java = Group(course=c_java, name='group1')
    project.groups.append(g1_cpp)

    user.groups.append(g1_cpp)
    user.groups.append(g2_cpp)
    user.groups.append(g1_java)

    session.add(user)
    session.flush()

    assert len(user.groups) == 3

    cpp_groups = user.query_groups_in_course(course=course, project=project)
    assert cpp_groups.count() == 1
    assert g1_cpp in cpp_groups
    assert g2_cpp not in cpp_groups

    java_groups = user.query_groups_in_course(course=c_java)
    assert java_groups.count() == 1
    assert g1_java in java_groups


def test_get_users_in_group_based_on_role(session, course1, user1, user2):
    teacher2 = User(uco=125, email='bar2@bar.com', username='xbar2')
    course1 = Course(name="C++", codename="PB161")

    g1 = Group(course=course1, name='group1')
    students = Role(course=course1, name="students")
    teachers = Role(course=course1, name="teachers")

    students.clients.append(user1)
    teachers.clients.append(user2)
    teachers.clients.append(teacher2)

    user1.groups.append(g1)
    user2.groups.append(g1)
    teacher2.groups.append(g1)
    session.add(teacher2)
    session.flush()

    res = g1.get_users_based_on_role(role=teachers)
    assert len(res) == 2
    assert user2 in res
    assert teacher2 in res
    assert user1 not in res

    res = g1.get_users_based_on_role(role=students)
    assert len(res) == 1
    assert user1 in res
    assert user2 not in res
    assert teacher2 not in res


def test_review_item_version_create_valid(session, course1, user1):
    project = Project(course=course1, name="p1")

    submission = Submission(user=user1, project=project, parameters={})
    session.add(submission)
    session.flush()
    assert submission in user1.submissions
    review = Review(submission=submission)

    ri1 = ReviewItem(user=user1, review=review, file="foo", line=10, content="something")
    ri2 = ReviewItem(user=user1, review=review, file="bar", line=1, content="something else")
    session.flush()

    assert ri1 in user1.review_items
    assert ri2 in user1.review_items
    items1 = ReviewItem.query.filter_by(id=ri1.id).all()
    assert len(items1) == 1
    items2 = ReviewItem.query.filter_by(id=ri2.id).all()
    assert len(items2) == 1

    assert ri1.versions[0].file == "foo"
    assert ri2.versions[0].file == "bar"

    assert ri1.versions[0].index == 0
    assert ri2.versions[0].index == 0


@pytest.fixture()
def versioned_review_item(session, course1, user1, app) -> ReviewItem:
    project = Project(course=course1, name="p1")

    submission = Submission(user=user1, project=project, parameters={})
    session.add(submission)
    session.flush()
    assert submission in user1.submissions
    review = Review(submission=submission)

    ri1 = ReviewItem(user=user1, review=review, file="foo", line=10, content="something")
    session.commit()
    yield ri1
    from tests import init_db
    init_db(app)


def test_review_item_version_update_valid(session, course1, user1, versioned_review_item):
    ri1 = versioned_review_item
    assert ri1 in user1.review_items
    assert ri1.versions[0].index == 0

    ri1.content = "new content"
    session.commit()
    assert ri1 in user1.review_items
    assert ri1.versions[1].index == 1

    ri1.line_start = None
    session.commit()
    assert ri1 in user1.review_items
    assert ri1.versions[2].index == 2
    assert ri1.versions[2].content == "new content"

    assert len(versioned_review_item.versions.all()) == 3


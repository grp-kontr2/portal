import os

import pytest

from portal import create_app, db
from portal.database import Course, Group, Project, Role, User


@pytest.fixture(scope='session')
def app():
    os.environ["PORTAL_CONFIG_TYPE"] = 'test'
    _app = create_app(environment='test')
    from tests import init_db
    init_db(_app)
    with _app.app_context():
        yield _app
        db.session.remove()
        db.drop_all()


@pytest.fixture(scope='function')
def session(app):
    """
    Creates a new database session (with a working transaction)
    for the duration of a test.
    """
    # initialize a new app context
    with app.app_context():
        _session = db.create_scoped_session()
        yield _session
        # the code after yield serves as session teardown
        _session.rollback()
        _session.close()


''' SOURCES
http://piotr.banaszkiewicz.org/blog/2014/02/22/how-to-bite-flask-sqlalchemy-and-pytest-all-at-once/
http://alexmic.net/flask-sqlalchemy-pytest/
https://xvrdm.github.io/2017/07/03/testing-flask-sqlalchemy-database-with-pytest/
https://scotch.io/tutorials/build-a-crud-web-app-with-python-and-flask-part-one
'''


def _get_user(username):
    return User(username=username, email=f'username@example.com')


def _get_course(codename):
    return Course(codename)


def _get_role(course, codename):
    return Role(course=course, codename=codename)


def _get_group(course, codename):
    return Group(course=course, codename=codename)


@pytest.fixture(autouse=True)
def db_student():
    return _get_user(username='student1')


@pytest.fixture(autouse=True)
def db_student2():
    return _get_user(username='student1')


@pytest.fixture(autouse=True)
def db_teacher():
    return _get_user(username='teacher1')


@pytest.fixture(autouse=True)
def db_teacher():
    return _get_user(username='teacher2')


@pytest.fixture(autouse=True)
def db_course():
    return Course(codename='course1')


@pytest.fixture(autouse=True)
def db_course2():
    return Course(codename='course2')


@pytest.fixture(autouse=True)
def db_c1_project1(db_course):
    return Project(course=db_course, codename='project1')


@pytest.fixture(autouse=True)
def db_c1_project2(db_course):
    return Project(course=db_course, codename='project2')


@pytest.fixture(autouse=True)
def db_c2_project1(db_course2):
    return Project(course=db_course2, codename='project1')


@pytest.fixture(autouse=True)
def db_c2_project2(db_course2):
    return Project(course=db_course2, codename='project2')


@pytest.fixture(autouse=True)
def db_c1_role1(db_course):
    return Role(course=db_course, codename='role1')


@pytest.fixture(autouse=True)
def db_c1_role2(db_course):
    return Role(course=db_course, codename='role2')


@pytest.fixture(autouse=True)
def db_c2_role1(db_course2):
    return Role(course=db_course2, codename='role1')


@pytest.fixture(autouse=True)
def db_c2_role2(db_course2):
    return Role(course=db_course2, codename='role2')


@pytest.fixture(autouse=True)
def db_c1_group1(db_course):
    return Group(course=db_course, codename='group1')


@pytest.fixture(autouse=True)
def db_c1_group2(db_course):
    return Group(course=db_course, codename='group2')


@pytest.fixture(autouse=True)
def db_c2_group1(db_course2):
    return Group(course=db_course2, codename='group1')


@pytest.fixture(autouse=True)
def db_c2_group2(db_course2):
    return Group(course=db_course2, codename='group2')

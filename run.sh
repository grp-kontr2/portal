#! /bin/bash

ADDRESS="0.0.0.0"
PORT="8000"
BIND="${ADDRESS}:${PORT}"

WORKERS="1"

export FLASK_APP="app:app"

gunicorn --workers ${WORKERS}  --bind "${BIND}" "${FLASK_APP}"




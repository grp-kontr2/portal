"""
Main application module
- Creates instance of the flask app named app
- registers commands
"""
from pathlib import Path

from portal.logger import Logging

Logging().load_config()

import click
from flask import Flask
from flask.cli import AppGroup, run_command

import portal
from management.data import DataManagement
from portal import create_app, db, logger

log = logger.get_logger(__name__)

data_cli = AppGroup('data', help='Sample data initialization')
users_cli = AppGroup('users', help='Users management')
courses_cli = AppGroup('courses', help='Courses management')
devel_cli = AppGroup('devel', help='Development management')
submissions_cli = AppGroup('submissions', help='Submissions management')
projects_cli = AppGroup('projects', help='Projects management')
cfg_cli = AppGroup('cfg', help='Configuration management')
management_cli = AppGroup('mgmt', help='Portal management')

app: Flask = create_app()

app.cli.add_command(data_cli)
app.cli.add_command(users_cli)
app.cli.add_command(courses_cli)
app.cli.add_command(devel_cli)
app.cli.add_command(submissions_cli)
app.cli.add_command(projects_cli)
app.cli.add_command(management_cli)

manager = DataManagement(app, db)

celery = portal.get_celery(app)

"""
MANAGEMENT
"""


@management_cli.command('archive-submissions')
def cli_mgmt_arch_submissions():
    print("[MGMT] Archiving submissions - NOW")
    from portal.async_celery.periodic_tasks import archive_submissions_in_arch_project
    with app.app_context():
        archive_submissions_in_arch_project()


@management_cli.command('shell')
def cli_mgmg_shell():
    print("[MGMT] Starting interactive shell")
    with app.app_context():
        import IPython
        from portal.database import models
        from portal.service.services_collection import ServicesCollection
        services = ServicesCollection.get()
        IPython.embed()


@management_cli.command('load-script', help='DANGER ZONE - load script and '
                                            'execute it with app context')
@click.argument('file')
def cli_mgmt_load_script(file):
    print("[MGMT] loading script")
    with app.app_context():
        fp = Path(file).absolute()
        if fp.exists():
            content = fp.read_text('utf-8')
            exec(content)
        else:
            print(f"[ERR] File not exists: {fp}")


@management_cli.command('delete-cancelled')
def cli_mgmt_arch_submissions():
    print("[MGMT] Deleting cancelled submissions - NOW")
    from portal.async_celery.periodic_tasks import delete_cancelled_submissions
    with app.app_context():
        delete_cancelled_submissions()


@management_cli.command('env', help='Print flask configuration')
def cli_print_cfg():
    log.info("[CFG] Configuration:")
    for (key, val) in app.config.items():
        print(f"{key}: {val}")


"""
DATA
"""


@devel_cli.command('run', help='Runs the devel server with initializes db')
@click.option('-p', '--port', default=8000)
@click.pass_context
def cli_run_devel(ctx, port):
    manager.reset_db()
    manager.create_admin_user('admin', '789789')
    manager.init_data()
    ctx.forward(run_command, port)


#
# Data related commands
#

@data_cli.command('init', help='Initializes sample data')
@click.argument('env')
def cli_init_data(env):
    log.info("[CMD] Initializing data")
    manager.init_data(env)


"""
USERS
"""


@users_cli.command('create', help='Create admin user')
@click.argument('name')
@click.option('-p', '--password', help='Users password', prompt=True, hide_input=True,
              confirmation_prompt=True)
@click.option('-s', '--secret', help="User's secret")
@click.option('-G', '--gitlab', help="Gitlab username")
def cli_users_create(name, password, secret, gitlab):
    log.info(f"[CMD] Create User: {name}")
    manager.create_admin_user(name, password, secret, gitlab)


@users_cli.command('set-password', help='Sets password for the user')
@click.argument('name')
@click.option('-p', '--password', help='Users password', prompt=True, hide_input=True,
              confirmation_prompt=True)
def cli_users_set_password(name, password):
    log.info(f"[CMD] Update password for user: {name}")
    manager.update_users_password(name, password)


@users_cli.command('add-secret', help='Adds secret for the user')
@click.argument('name')
@click.option('-s', '--secret', help='Users password', prompt=True, hide_input=True)
def cli_users_add_secret(name, secret):
    log.info(f"[CMD] Add secret for the user: {name} ({secret})")
    manager.add_user_secret(name, secret)


@users_cli.command('delete', help="Deletes user")
@click.argument('name')
def cli_users_delete_user(name):
    log.info(f"[CMD] Delete user: {name}")
    ask: str = click.prompt('Type Y/y for confirmation', hide_input=True)
    if ask.lower()[0] != 'y':
        return
    manager.delete_user(name)
    log.info(f"[CMD] User deleted: {name}")


@users_cli.command('list', help='List all users')
def cli_users_list():
    log.info(f'[CMD] Listing users')
    manager.list_users()


@users_cli.command('import-csv', help='Import users from the csv')
@click.argument('course')
@click.argument('role')
@click.argument('group', required=False)
@click.option('-f', '--file', help="From which file they should be imported, default is stdin",
              required=False)
def cli_users_list(course, role, group, file):
    log.info(f'[CMD] Importing users from the csv users')
    manager.import_users(file, course=course, role=role, group=group)


"""
COURSES
"""


@courses_cli.command('create', help="Creates course with default roles")
@click.argument('name', )
def cli_course_creates(name):
    log.info(f"[CMD] Create Course: {name}")
    manager.create_course(name)


@courses_cli.command('definition', help="Shows course definition")
@click.argument('name', )
def cli_course_creates(name):
    log.info(f"[CMD] Definition for the Course: {name}")
    manager.dump_definition(name)


@courses_cli.command('list', help="List courses")
def cli_courses_list():
    log.info(f"[CMD] List Courses")
    manager.list_courses()


@courses_cli.command('create-role', help="Creates role by it's type in the course")
@click.argument('course')
@click.argument('type')
@click.argument('name', required=False)
def cli_course_role_creates(course, type, name=None):
    log.info(f"[CMD] Create Role: \"{type}\" in the \"{course}\"")
    manager.create_role(course, role_type=type, name=name)


@courses_cli.command('is-import-teachers', help="Import teachers for course")
@click.argument('course')
@click.argument('role_name')
def cli_course_role_creates(course, role_name):
    log.info(f"[CMD] Import teachers -> \"{role_name}\" in the \"{course}\"")
    manager.is_import_users(course, role_name=role_name, users_type='teachers')


@courses_cli.command('is-import-students', help="Import students for course")
@click.argument('course')
@click.argument('role_name')
def cli_course_role_creates(course, role_name):
    log.info(f"[CMD] Import students -> \"{role_name}\" in the \"{course}\"")
    manager.is_import_users(course, role_name=role_name, users_type='students')


"""
PROJECTS
"""


@projects_cli.command('activate', help="activates project")
@click.option('-p', '--project')
@click.option('-c', '--course')
def cli_projects_activate(project, course):
    log.info(f"[CMD] Activating project: {project} in course {course}")
    manager.activate_project(course, project)


@projects_cli.command('list', help="list projects")
@click.option('-c', '--course')
def cli_projects_list(course):
    log.info(f"[CMD] Listing projects in the course {course}")
    manager.list_projects(course)


"""
SUBMISSIONS
"""


@submissions_cli.command('cancel-all', help="Cancel all submissions")
def cli_submissions_cancel_all():
    log.info(f"[CMD] Cancelling all submissions")
    manager.cancel_all_submissions()


@submissions_cli.command('cancel', help="Cancel submission")
@click.argument('sid')
def cli_submissions_cancel(sid):
    log.info(f"[CMD] Cancelling all submissions")
    manager.cancel_submission(sid)


@submissions_cli.command('list', help="List all submissions")
def cli_submissions_list_all():
    log.info(f"[CMD] List all submissions")
    manager.list_all_submissions()


@submissions_cli.command('clean-all', help="Clean all submissions")
def cli_submissions_list_all():
    log.info(f"[CMD] Clean all submissions")
    manager.clean_all_submissions()


if __name__ == '__main__':
    cli_run_devel()

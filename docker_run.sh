#!/bin/bash


set -e

host="$1"

until PGPASSWORD=$DB_PASSWORD psql -h "$host" -U "postgres" -c '\q'; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 1
done

>&2 echo "Postgres is up - executing command"


ENVIRONMENT="${ENVIRONMENT:-dev}"
ADMIN_USER="${ADMIN_USER:-admin}"
ADMIN_PASSWORD="${ADMIN_PASSWORD:-789789}"
ADMIN_SECRET="${ADMIN_SECRET:-devel-admin-secret}"
ADMIN_GITLAB="${ADMIN_GITLAB}"

# INIT
echo "[INIT] Initializing the database"
flask db upgrade
echo "[INIT] Initializing data for env: $ENVIRONMENT"
flask data init ${ENVIRONMENT}
echo "[INIT] Initializing admin user: ${ADMIN_USER}"
flask users create ${ADMIN_USER} --password "${ADMIN_PASSWORD}" --secret "${ADMIN_SECRET}" --gitlab "${ADMIN_GITLAB}"

if [ "$DATASETS" != "" ]; then
    echo "[INIT] Additional datasets: $DATASETS"
    for dataset in $DATASETS; do
        echo "[INIT] Dataset: ${dataset}"
        flask data init "$dataset"
    done
else
    echo "[INIT] No additional datasets provided"
fi

# RUN THE SERVER
gunicorn -b 0.0.0.0:8000 --access-logfile - --reload "app:app"


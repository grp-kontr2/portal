import shutil
from pathlib import Path

from portal import storage_wrapper
from portal.database import models

UPDATE_NAME = "transform_storage_dirs"
DEPENDS_ON = []


def change_dirs(entity, base_path: Path):
    old_path = base_path / entity.id
    new_path = base_path / entity.storage_dirname
    if old_path.exists():
        print(f"[MOVE] FROM {old_path} to {new_path}")
        shutil.move(old_path, new_path)


def change_project_dirs():
    print("[UPDATE] Processing projects")
    tf_path = storage_wrapper.test_files.path
    for project in models.Project.query.all():
        change_dirs(project, tf_path)


def change_submission_dirs():
    print("[UPDATE] Processing submissions")
    source_path = storage_wrapper.submissions.path
    results_path = storage_wrapper.results.path
    for submission in models.Submission.query.all():
        change_dirs(submission, source_path)
        change_dirs(submission, results_path)


change_project_dirs()
change_submission_dirs()

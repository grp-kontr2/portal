= Backend configuration

To run the portal correctly you need to configure it.

== Configuration
Default environment configuration is stored in a `portal/config.py` file.

To override the defaults, you should edit the custom configuration file
``portal.local.cfg`` and place it in the project root directory.

== Variables
The configuration file is split into multiple sections.
Each variable has a short description and an example value.


== Features configuration
This section describes how to configure specific portal features.

=== Emails
- To test emails on the `localhost` without mail server you can use
link:https://github.com/mailhog/MailHog[MailHog]
- The easiest way to use it is to use the Docker:
[source,bash]
----
docker pull mailhog/mailhog

docker run -d \
           --restart unless-stopped \
           --name mailhog \
           -p 1025:1025 \
           -p 8025:8025 \
           mailhog/mailhog
----
- This will create a MailHog instance on your `localhost`
    - Web UI: `http://localhost:8025/`
    - SMTP port: `1025`
    - There is no additional security, no username and password












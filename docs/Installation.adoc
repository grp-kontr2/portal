= Kontr portal installation

There are several installation methods

== Global prerequisites

* Python 3.6 or higher
* https://github.com/pypa/pipenv[Pipenv]

== Development install
Checkout the repository and install all the dependencies

[source,bash]
----
git clone https://gitlab.fi.muni.cz/grp-kontr2/portal

pipenv install # installs dependencies
----


== Docker install
Pull the docker image

[source,bash]
----
docker pull kontr2/portal
----

== Docker Compose

Use the `docker-compose` to run the the portal backend.

=== Pull Images
Pull the images:
- Postgres
- Portal backend

[source,bash]
----
git clone https://gitlab.fi.muni.cz/grp-kontr2/docker

cd docker/portal

docker-compose -f docker-compose.backend.yml pull
----


=== Run Images

Run the images:
- Will create networks
- Init the database (if needed)
- Mount volumes and configuration

[source,bash]
----
git clone https://gitlab.fi.muni.cz/grp-kontr2/docker

cd docker/portal

docker-compose -f docker-compose.backend.yml up
----